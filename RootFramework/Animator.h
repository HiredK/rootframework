/**
* @file Animator.h
* @brief
*/

#pragma once

#include "Camera.h"
#include "FileHandle.h"
#include "Shader.h"

#include <ozz/base/containers/vector.h>
#include <ozz/base/maths/simd_math.h>
#include <ozz/base/maths/soa_transform.h>

#define GL_PTR_OFFSET(i) reinterpret_cast<void*>(static_cast<intptr_t>(i))

namespace ozz
{
	namespace animation
	{
		class Animation;
		class SamplingCache;
		class Skeleton;
	}
}

namespace root
{
	class Animator : public Transform
	{
		public:
			//! CTOR/DTOR:
			Animator();
			virtual ~Animator();

			//! SERVICES:
			bool LoadSkeleton(FileHandle* file);
			bool LoadMesh(FileHandle* file);
			bool AddAnimation(int index, FileHandle* file);
			void Animate(float dt);
			void Draw(Camera& camera);
			void DrawMesh();
			void Draw();
			void Clear();

			//! ACCESSORS:
			void SetCurrentAnimationIndex(int index);
			int GetCurrentAnimationIndex() const;

			// Defines a specialized mesh type with skinning information (joint indices and weights).
			struct SkinnedMesh
			{
				struct Part
				{
					int GetInfluencesCount() const;
					int GetVertexCount() const;

					ozz::Vector<float>::Std m_positions;
					ozz::Vector<float>::Std m_normals;
					ozz::Vector<float>::Std m_tangents;
					ozz::Vector<float>::Std m_texcoords;
					ozz::Vector<uint8_t>::Std m_colors;
					ozz::Vector<uint16_t>::Std m_joint_indices;
					ozz::Vector<float>::Std m_joint_weights;
				};

				int GetTriangleIndexCount() const;
				int GetVertexCount() const;
				int GetMaxInfluencesCount() const;
				bool IsSkinned() const;
				int GetNumJoints();

				ozz::Vector<Part>::Std m_parts;
				ozz::Vector<uint16_t>::Std m_indices;
				ozz::Vector<ozz::math::Float4x4>::Std m_inverse_bind_poses;
			};

			// Utility class that helps with controlling animation playback time.
			struct PlaybackController
			{
				PlaybackController();
				void Update(const ozz::animation::Animation* animation, float dt);
				void Reset();

				float m_time;
				float m_playback_speed;
				bool m_play;
			};

		protected:
			// Sampler structure contains all the data required to sample a single animation.
			struct Sampler
			{
				Sampler(const std::shared_ptr<ozz::animation::Animation>& animation);
				virtual ~Sampler();

				std::shared_ptr<ozz::animation::Animation> m_animation;
				ozz::animation::SamplingCache* m_cache;
				ozz::Range<ozz::math::SoaTransform> m_locals;
				PlaybackController m_controller;
				float m_weight_setting;
			};

			// Volatile memory buffer that can be used within function scope.
			struct ScratchBuffer
			{
				ScratchBuffer();
				virtual ~ScratchBuffer();

				void* Resize(size_t size);
				void* m_buffer;
				size_t m_size;
			};

			//! MEMBERS:
			std::shared_ptr<ozz::animation::Skeleton> m_skeleton;
			std::shared_ptr<SkinnedMesh> m_mesh;
			std::map<int, std::shared_ptr<Sampler>> m_samplers;
			ozz::Range<ozz::math::Float4x4> m_model_matrices;
			ozz::Range<ozz::math::Float4x4> m_skinning_matrices;
			ScratchBuffer m_scratch_buffer;
			GLuint m_dynamic_vbo;
			GLuint m_dynamic_ibo;
			int m_current_animation_index;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Animator::SkinnedMesh::Part inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline int Animator::SkinnedMesh::Part::GetInfluencesCount() const {
		const int vertex_count = GetVertexCount();
		if (vertex_count == 0) {
			return 0;
		}
		return static_cast<int>(m_joint_indices.size()) / vertex_count;
	}
	/*----------------------------------------------------------------------------*/
	inline int Animator::SkinnedMesh::Part::GetVertexCount() const {
		return static_cast<int>(m_positions.size()) / 3;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Animator::SkinnedMesh inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline int Animator::SkinnedMesh::GetTriangleIndexCount() const {
		return static_cast<int>(m_indices.size());
	}
	/*----------------------------------------------------------------------------*/
	inline int Animator::SkinnedMesh::GetVertexCount() const {
		int vertex_count = 0;
		for (size_t i = 0; i < m_parts.size(); ++i) {
			vertex_count += m_parts[i].GetVertexCount();
		}
		return vertex_count;
	}
	/*----------------------------------------------------------------------------*/
	inline int Animator::SkinnedMesh::GetMaxInfluencesCount() const {
		int max_influences_count = 0;
		for (size_t i = 0; i < m_parts.size(); ++i) {
			const int influences_count = m_parts[i].GetInfluencesCount();
			max_influences_count = influences_count > max_influences_count ? influences_count : max_influences_count;
		}
		return max_influences_count;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Animator::SkinnedMesh::IsSkinned() const {
		for (size_t i = 0; i < m_parts.size(); ++i) {
			if (m_parts[i].GetInfluencesCount() != 0) {
				return true;
			}
		}

		return false;
	}
	/*----------------------------------------------------------------------------*/
	inline int Animator::SkinnedMesh::GetNumJoints() {
		return static_cast<int>(m_inverse_bind_poses.size());
	}

	////////////////////////////////////////////////////////////////////////////////
	// Animator::PlaybackController inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Animator::PlaybackController::PlaybackController()
		: m_time(0.f), m_playback_speed(1.5f), m_play(true) {
	}
	/*----------------------------------------------------------------------------*/
	inline void Animator::PlaybackController::Reset() {
		m_time = 0.f;
		m_playback_speed = 1.5f;
		m_play = true;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Animator::Sampler inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Animator::Sampler::Sampler(const std::shared_ptr<ozz::animation::Animation>& animation)
		: m_animation(animation), m_cache(nullptr), m_weight_setting(1.0f) {
	}
	/*----------------------------------------------------------------------------*/
	inline Animator::Sampler::~Sampler() {
	}

	////////////////////////////////////////////////////////////////////////////////
	// Animator::ScratchBuffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Animator::ScratchBuffer::ScratchBuffer()
		: m_buffer(nullptr), m_size(0) {
	}
	/*----------------------------------------------------------------------------*/
	inline Animator::ScratchBuffer::~ScratchBuffer() {
		ozz::memory::default_allocator()->Deallocate(m_buffer);
	}
	/*----------------------------------------------------------------------------*/
	inline void* Animator::ScratchBuffer::Resize(size_t size) {
		if (size > m_size) {
			m_buffer = ozz::memory::default_allocator()->Reallocate(m_buffer, size, 16);
			m_size = size;
		}

		return m_buffer;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Animator inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Animator:: Animator()
		: m_dynamic_vbo(GL_ZERO)
		, m_dynamic_ibo(GL_ZERO)
		, m_current_animation_index(-1) {
	}
	/*----------------------------------------------------------------------------*/
	inline Animator::~Animator() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Animator::Draw(Camera& camera) {
		if (camera.GetFrustum().IsInside(GetBoundingBox().Transform(ComputeMatrix())) != Frustum::Invisible) {
			Shader::Get()->UniformMat4("u_ViewProjMatrix", camera.GetViewProjMatrix());
			Draw();
		}
	}

	/*----------------------------------------------------------------------------*/
	inline void Animator::SetCurrentAnimationIndex(int index) {
		m_current_animation_index = glm::clamp(-1, (int)m_samplers.size(), index);
	}
	/*----------------------------------------------------------------------------*/
	inline int Animator::GetCurrentAnimationIndex() const {
		return m_current_animation_index;
	}
}