#include "Material.h"

void root::Material::Bind()
{
	if (m_albedo_map) {
		glActiveTexture(GL_TEXTURE0);
		root::Shader::Get()->Sampler("s_Albedo", 0);
		m_albedo_map->Bind();
	}

	if (m_normal_map) {
		glActiveTexture(GL_TEXTURE1);
		root::Shader::Get()->Sampler("s_Normal", 1);
		m_normal_map->Bind();
	}
}