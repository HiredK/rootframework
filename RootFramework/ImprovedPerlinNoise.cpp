#include "ImprovedPerlinNoise.h"

void root::ImprovedPerlinNoise::GenerateData(int seed)
{
	const float GRADIENT3[] = {
		1, 1, 0,-1, 1, 0, 1,-1, 0,-1,-1, 0,
		1, 0, 1,-1, 0, 1, 1, 0,-1,-1, 0,-1,
		0, 1, 1, 0,-1, 1, 0, 1,-1, 0,-1,-1,
		1, 1, 0, 0,-1, 1,-1, 1, 0, 0,-1,-1,
	};

	int* perm = new int[m_size + m_size];
	std::srand(seed);

	int i, j, k;
	for (i = 0; i < m_size; i++) {
		perm[i] = i;
	}

	while (--i != 0) {
		k = perm[i];
		j = (int)glm::linearRand(0.0f, (float)m_size);
		perm[i] = perm[j];
		perm[j] = k;
	}

	for (i = 0; i < m_size; i++) {
		perm[m_size + i] = perm[i];
	}

	{
		std::vector<float> pixels;
		pixels.resize(m_size * m_size * 4);

		for (int i = 0; i < m_size; ++i)
		for (int j = 0; j < m_size; ++j)
		{
			int A = perm[i] + j;
			int AA = perm[A];
			int AB = perm[A + 1];

			int B = perm[i + 1] + j;
			int BA = perm[B];
			int BB = perm[B + 1];

			float* pixel = &pixels[(i + j * m_size) * 4];
			*pixel++ = AA / 255.0f;
			*pixel++ = AB / 255.0f;
			*pixel++ = BA / 255.0f;
			*pixel++ = BB / 255.0f;
		}

		Texture::Settings& settings = Texture::GetDefaultSettings();
		settings.iformat = GL_RGBA32F;
		settings.format = GL_RGBA;
		settings.type = GL_FLOAT;
		settings.wrap_mode = GL_REPEAT;

		m_textures[PERM_TABLE] = Texture::CreateTexture(m_size, m_size, 0, 0, &pixels[0], settings);
		pixels.clear();
	}

	{
		std::vector<float> pixels;
		pixels.resize(m_size * 3);

		for (int i = 0; i < m_size; ++i) {
			int idx = perm[i] % 16;

			float* pixel = &pixels[i * 3];
			*pixel++ = (GRADIENT3[idx * 3 + 0] + 1.0f) * 0.5f;
			*pixel++ = (GRADIENT3[idx * 3 + 1] + 1.0f) * 0.5f;
			*pixel++ = (GRADIENT3[idx * 3 + 2] + 1.0f) * 0.5f;
		}

		Texture::Settings& settings = Texture::GetDefaultSettings();
		settings.iformat = GL_RGB8;
		settings.format = GL_RGB;
		settings.type = GL_FLOAT;
		settings.wrap_mode = GL_REPEAT;

		m_textures[GRADIENT] = Texture::CreateTexture(m_size, 1, 0, 0, &pixels[0], settings);
		pixels.clear();
	}
}