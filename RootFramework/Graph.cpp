#include "Graph.h"
#include "Logger.h"

root::Graph::Curve* root::Graph::AddCurve(const dvec2& s, const dvec2& e)
{
	Node* n1 = NewNode(s);
	Node* n2 = NewNode(e);
	Curve* c = NewCurve(nullptr, n1, n2);
	return c;
}

root::Graph::Node* root::Graph::NewNode(const dvec2& coord)
{
	std::shared_ptr<Node> node = std::shared_ptr<Node>(new Node(*this, coord));
	m_nodes.insert(std::make_pair(node->GetID(), node));
	return node.get();
}

root::Graph::Curve* root::Graph::NewCurve(Curve* model, Node* s, Node* e)
{
	std::shared_ptr<Curve> curve = std::shared_ptr<Curve>(new Curve(*this, model, s, e));
	s->AddCurve(curve->GetID());
	e->AddCurve(curve->GetID());
	m_curves.insert(std::make_pair(curve->GetID(), curve));
	return curve.get();
}