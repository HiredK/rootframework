/**
* @file FrameBuffer.h
* @brief
*/

#pragma once

#include <GL/glew.h>
#include "Texture.h"

namespace root
{
	class FrameBuffer
	{
		public:
			//! CTOR/DTOR:
			FrameBuffer();
			virtual ~FrameBuffer();

			//! SERVICES:
			GLuint Bind();
			void Attach(Texture* texture, GLenum attachment, int level, int layer);
			void Attach(Texture* texture, GLenum attachment, int level);
			void Attach(Texture* texture, GLenum attachment);
			void* ReadPixels(Texture* texture, GLenum attachment, int x, int y, int w = 1, int h = 1);
			void BindOutput();
			void BindOutput(GLenum attachment);
			void Clear();

			static void Unbind();

			//! ACCESSORS:
			unsigned int GetW() const;
			unsigned int GetH() const;
			GLuint GetID() const;

		private:
			//! SERVICES:
			// Get the maximum number of color attachment
			// points supported in a framebuffer object.
			static unsigned int GetMaxColorAttachments();

			//! MEMBERS:
			unsigned int m_num_attachments;
			GLuint* m_attachments;
			GLuint m_id;
	};

	////////////////////////////////////////////////////////////////////////////////
	// FrameBuffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FrameBuffer::FrameBuffer()
		: m_num_attachments(0), m_id(GL_ZERO)
	{
		m_attachments = new GLuint[GetMaxColorAttachments()];
		glGenFramebuffers(1, &m_id);
	}
	/*----------------------------------------------------------------------------*/
	inline FrameBuffer::~FrameBuffer() {
		glDeleteFramebuffers(1, &m_id);
	}

	/*----------------------------------------------------------------------------*/
	inline GLuint FrameBuffer::Bind() {
		glBindFramebuffer(GL_FRAMEBUFFER, m_id);
		return m_id;
	}
	/*----------------------------------------------------------------------------*/
	inline void FrameBuffer::Unbind() {
		glBindFramebuffer(GL_FRAMEBUFFER, GL_ZERO);
		glDrawBuffer(GL_BACK);
	}

	/*----------------------------------------------------------------------------*/
	inline void FrameBuffer::Attach(Texture* texture, GLenum attachment, int level) {
		Attach(texture, attachment, level, -1);
	}
	/*----------------------------------------------------------------------------*/
	inline void FrameBuffer::Attach(Texture* texture, GLuint attachment) {
		Attach(texture, attachment, 0, -1);
	}

	/*----------------------------------------------------------------------------*/
	inline GLuint FrameBuffer::GetID() const {
		return m_id;
	}
}