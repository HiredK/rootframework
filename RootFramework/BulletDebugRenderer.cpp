#include "BulletDebugRenderer.h"
#include "Logger.h"
#include "Shader.h"

void root::BulletDebugRenderer::drawLine(const btVector3& from, const btVector3& to, const btVector3& from_color, const btVector3& to_color)
{
	glBegin(GL_LINES);
		glVertex3f((float)from.getX(), (float)from.getY(), (float)from.getZ());
		glVertex3f((float)to.getX(), (float)to.getY(), (float)to.getZ());
	glEnd();
}

void root::BulletDebugRenderer::drawLine(const btVector3& from, const btVector3& to, const btVector3& color)
{
	drawLine(from, to, color, color);
}

void root::BulletDebugRenderer::drawSphere(const btVector3& p, btScalar radius, const btVector3& color)
{
}

void root::BulletDebugRenderer::drawTriangle(const btVector3& a, const btVector3& b, const btVector3& c, const btVector3& color, btScalar alpha)
{
}

void root::BulletDebugRenderer::drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color)
{
}

void root::BulletDebugRenderer::reportErrorWarning(const char* str)
{
	LOG(Logger::Warning) << str;
}

void root::BulletDebugRenderer::draw3dText(const btVector3& location, const char* str)
{
}