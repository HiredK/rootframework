/**
* @file NetworkingBinding.h
* @brief
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "Network.h"
#include "TransformHistory.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// NetObjectBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct NetObject_wrapper : NetObject, ScriptObject_wrapper
	{
		NetObject_wrapper(Network* network)
			: ScriptObject_wrapper("net_object", ScriptObject::Dynamic)
			, NetObject(network) {
		}

		//! ALLOCATION ID:
		virtual void WriteAllocationID(RakNet::Connection_RM3* connection, RakNet::BitStream* bs) const {
			luabind::wrap_base::call<void>("__write_allocation_id", connection, bs);
		}
		static void WriteAllocationID_default(NetObject* ptr, RakNet::Connection_RM3* connection, RakNet::BitStream* bs) {
			ptr->NetObject::WriteAllocationID(connection, bs);
		}

		//! (DE)SERIALIZE CTOR:
		virtual void SerializeConstruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection) {
			luabind::wrap_base::call<void>("__serialize_construction", bs, connection);
		}
		static void SerializeConstruction_default(NetObject* ptr, RakNet::BitStream* bs, RakNet::Connection_RM3* connection) {
			ptr->NetObject::SerializeConstruction(bs, connection);
		}

		//! (DE)SERIALIZE DTOR:
		virtual bool DeserializeConstruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection) {
			return luabind::wrap_base::call<bool>("__deserialize_construction", bs, connection); \
		}
		static bool DeserializeConstruction_default(NetObject* ptr, RakNet::BitStream* bs, RakNet::Connection_RM3* connection) {
			return ptr->NetObject::DeserializeConstruction(bs, connection);
		}

		//! (DE)SERIALIZE:
		virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* parameters) {
			return luabind::wrap_base::call<RakNet::RM3SerializationResult>("__serialize", parameters); \
		}
		static RakNet::RM3SerializationResult Serialize_default(NetObject* ptr, RakNet::SerializeParameters* parameters) {
			return ptr->NetObject::Serialize(parameters);
		}
		virtual void Deserialize(RakNet::DeserializeParameters* parameters) {
			return luabind::wrap_base::call<void>("__deserialize", parameters);
		}
		static void Deserialize_default(NetObject* ptr, RakNet::DeserializeParameters* parameters) {
			return ptr->NetObject::Deserialize(parameters);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope NetObjectBinding()
	{
		return (
			luabind::class_<NetObject_wrapper, ScriptObject, std::shared_ptr<NetObject_wrapper>>("NetObject")
			.def(luabind::constructor<Network*>())

			.property("network", &NetObject::GetNetwork)
			.property("network_id", (RakNet::NetworkID(NetObject::*)() const) &NetObject::GetNetworkID)

			.def("__write_allocation_id", &NetObject::WriteAllocationID, &NetObject_wrapper::WriteAllocationID_default)
			.def("__serialize_construction", &NetObject::SerializeConstruction, &NetObject_wrapper::SerializeConstruction_default)
			.def("__deserialize_construction", &NetObject::DeserializeConstruction, &NetObject_wrapper::DeserializeConstruction_default)
			.def("__serialize", &NetObject::Serialize, &NetObject_wrapper::Serialize_default)
			.def("__deserialize", &NetObject::Deserialize, &NetObject_wrapper::Deserialize_default),

			luabind::class_<NetObject, NetObject_wrapper>("NetObject_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// NetworkBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Network_wrapper : Network, ScriptObject_wrapper
	{
		Network_wrapper(Network::Type type)
			: ScriptObject_wrapper("network", ScriptObject::Dynamic)
			, Network(type) {

			RegisterSignalHandler("on_connect", new root::SignalHandler<void>());
			RegisterSignalHandler("on_receive", new root::SignalHandler<void, const RakNet::Packet*>());
			RegisterSignalHandler("on_request", new root::SignalHandler<NetObject*, RakNet::BitStream*, RakNet::NetworkID>());
		}

		virtual void OnConnect() {
			ScriptObject::PushOwner(GetOwner());
			CallSignal<void>("on_connect");
			ScriptObject::PopStack();
		}

		virtual void OnReceive(const RakNet::Packet* packet) {
			CallSignal<void>("on_receive", packet);
		}

		virtual RakNet::NetworkID Request(RakNet::BitStream* bs)
		{
			RakNet::BitStream request_bs;
			request_bs.Write((unsigned char)ID_REQUEST_OBJECT);

			// Generate a new network id for the requested object, this is used
			// to find the replicated object when the server has responded.
			RakNet::NetworkID requested_id(m_network_id_manager.GenerateNetworkID());
			request_bs.Write(requested_id);

			// Write bitstream from the scripting side.
			request_bs.Write(bs);

			// Send object creation request to server.
			const RakNet::RakNetGUID& server_guid = m_peer->GetGUIDFromIndex(0);
			if (!m_peer->Send(&request_bs, IMMEDIATE_PRIORITY, RELIABLE_ORDERED, 0, server_guid, 0)) {
				LOG(Logger::Warning) << "Unable to request creation";
				return 0;
			}

			return requested_id;
		}

		virtual NetObject* OnRequest(RakNet::BitStream* bs)
		{
			RakNet::NetworkID requested_id;
			if (!bs->Read(requested_id)) {
				return nullptr;
			}

			ScriptObject::PushOwner(GetOwner());
			NetObject* requested_object = CallSignal<NetObject*>("on_request", bs, requested_id);
			ScriptObject::PopStack();

			if (requested_object) {
				requested_object->SetNetworkID(requested_id);
				Network::Reference(requested_object);
			}

			return requested_object;
		}

		virtual void Update(float dt) {
			Network::Receive();
		}
	};

	/*----------------------------------------------------------------------------*/
	luabind::object GetPacketData(RakNet::Packet const& packet, lua_State* L) {
		luabind::object result = luabind::newtable(L);
		for (unsigned int i = 0; i < packet.length; ++i) {
			result[i + 1] = packet.data[i];
		}

		return result;
	}
	/*----------------------------------------------------------------------------*/
	RakNet::BitStream* SerializeParameters_GetOutputBitstream(RakNet::SerializeParameters& self, int index) {
		return &self.outputBitstream[index];
	}
	/*----------------------------------------------------------------------------*/
	RakNet::BitStream* DeserializeParameters_GetSerializationBitstream(RakNet::DeserializeParameters& self, int index) {
		return &self.serializationBitstream[index];
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope NetworkBinding()
	{
		return (
			luabind::class_<Network_wrapper, ScriptObject, std::shared_ptr<Network_wrapper>>("Network")
			.def(luabind::constructor<Network::Type>())

			.scope
			[
				luabind::class_<RakNet::Packet>("Packet")
				.def_readonly("length", &RakNet::Packet::length)
				.property("data", &GetPacketData),

				luabind::class_<RakNet::SerializeParameters>("SerializeParameters")
				.def("get_bitstream", &SerializeParameters_GetOutputBitstream),

				luabind::class_<RakNet::DeserializeParameters>("DeserializeParameters")
				.def("get_bitstream", &DeserializeParameters_GetSerializationBitstream),

				luabind::class_<RakNet::Connection_RM3>("Connection_RM3")
			]
			
			.enum_("Type")
			[
				luabind::value("CLIENT_TO_SERVER", Network::CLIENT_TO_SERVER),
				luabind::value("SERVER_TO_CLIENT", Network::SERVER_TO_CLIENT),
				luabind::value("OFFLINE", Network::OFFLINE)
			]

			.enum_("MessageID")
			[
				luabind::value("ID_REQUEST_OBJECT", Network::ID_REQUEST_OBJECT),
				luabind::value("ID_USER_PACKET_ENUM", Network::ID_USER_PACKET_ENUM)
			]

			.enum_("SerializationResult")
			[
				luabind::value("BROADCAST_IDENTICALLY", RakNet::RM3SR_BROADCAST_IDENTICALLY),
				luabind::value("BROADCAST_IDENTICALLY_FORCE_SERIALIZATION", RakNet::RM3SR_BROADCAST_IDENTICALLY_FORCE_SERIALIZATION),
				luabind::value("SERIALIZED_UNIQUELY", RakNet::RM3SR_SERIALIZED_UNIQUELY),
				luabind::value("SERIALIZED_ALWAYS", RakNet::RM3SR_SERIALIZED_ALWAYS),
				luabind::value("SERIALIZED_ALWAYS_IDENTICALLY", RakNet::RM3SR_SERIALIZED_ALWAYS_IDENTICALLY),
				luabind::value("DO_NOT_SERIALIZE", RakNet::RM3SR_DO_NOT_SERIALIZE),
				luabind::value("NEVER_SERIALIZE_FOR_THIS_CONNECTION", RakNet::RM3SR_NEVER_SERIALIZE_FOR_THIS_CONNECTION)
			]
		
			.def("connect", &Network::Connect)
			.def("request", &Network::Request)
			.def("send", (void(Network::*)(RakNet::BitStream*)) &Network::Send)
			.def("reference", (void(Network::*)(NetObject*)) &Network::Reference)
			.def("get_object_from_id", &Network::GetObjectFromID)
			.def("shutdown", (void(Network::*)(unsigned int)) &Network::Shutdown)
			
			.property("auto_serialize_interval", &Network::GetAutoSerializeInterval, &Network::SetAutoSerializeInterval)
			.def("set_auto_serialize_interval", &Network::SetAutoSerializeInterval)
			.def("get_auto_serialize_interval", &Network::GetAutoSerializeInterval)

			.property("autoritative", &Network::IsAutoritative)
			.def("is_autoritative", &Network::IsAutoritative),

			luabind::class_<Network, Network_wrapper>("Network_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// BitStreamBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static std::shared_ptr<RakNet::BitStream> Bitstream_BuildFromPacket(const RakNet::Packet& packet, bool copy) {
		std::shared_ptr<RakNet::BitStream> message(new RakNet::BitStream(packet.data, packet.length, copy));
		message->IgnoreBytes(sizeof(RakNet::MessageID));
		return message;
	}
	/*----------------------------------------------------------------------------*/
	static std::shared_ptr<RakNet::BitStream> Bitstream_BuildFromPacket(const RakNet::Packet& packet) {
		return Bitstream_BuildFromPacket(packet, false);
	}
	/*----------------------------------------------------------------------------*/
	static std::shared_ptr<RakNet::BitStream> BitStream_Create(RakNet::MessageID message_id) {
		auto bs = std::shared_ptr<RakNet::BitStream>(new RakNet::BitStream());
		bs->Write(message_id);
		return bs;
	}
	/*----------------------------------------------------------------------------*/
	template<typename T> inline T Bitstream_Read(RakNet::BitStream& self) {
		T result;
		self.Read(result);
		return result;
	}
	/*----------------------------------------------------------------------------*/
	template<typename T> inline void Bitstream_Write(RakNet::BitStream& self, T v) {
		self.Write(v);
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope BitStreamBinding()
	{
		return (
			luabind::class_<RakNet::BitStream, luabind::no_bases, std::shared_ptr<RakNet::BitStream>>("BitStream")
			.def(luabind::constructor<>())

			.scope
			[
				luabind::def("build_from_packet", (std::shared_ptr<RakNet::BitStream>(*)(const RakNet::Packet&, bool)) &Bitstream_BuildFromPacket),
				luabind::def("build_from_packet", (std::shared_ptr<RakNet::BitStream>(*)(const RakNet::Packet&)) &Bitstream_BuildFromPacket)
			]

			.def("read_message_id", &Bitstream_Read<RakNet::MessageID>)
			.def("write_message_id", &Bitstream_Write<RakNet::MessageID>)
			.def("read_network_id", &Bitstream_Read<RakNet::NetworkID>)
			.def("write_network_id", &Bitstream_Write<RakNet::NetworkID>)
			.def("read_bool", &Bitstream_Read<bool>)
			.def("write_bool", &Bitstream_Write<bool>)
			.def("read_integer", &Bitstream_Read<int>)
			.def("write_integer", &Bitstream_Write<int>)
			.def("read_vec2", &Bitstream_Read<dvec2>)
			.def("write_vec2", &Bitstream_Write<dvec2>)
			.def("read_vec3", &Bitstream_Read<dvec3>)
			.def("write_vec3", &Bitstream_Write<dvec3>)
			.def("read_quat", &Bitstream_Read<dquat>)
			.def("write_quat", &Bitstream_Write<dquat>)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TransformHistoryBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TransformHistory_wrapper : TransformHistory, ScriptObject_wrapper
	{
		TransformHistory_wrapper(unsigned int max_write_interval, unsigned int max_history_time)
			: ScriptObject_wrapper("transform_history", ScriptObject::Static)
			, TransformHistory(max_write_interval, max_history_time) {
		}

		void Write(const Transform& transform) {
			TransformHistory::Write(transform, RakNet::GetTimeMS());
		}

		ReadResult Read(Transform& transform, unsigned int auto_serialize_interval) {
			return TransformHistory::Read(transform, RakNet::GetTimeMS() - auto_serialize_interval, RakNet::GetTimeMS());
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TransformHistoryBinding()
	{
		return (
			luabind::class_<TransformHistory_wrapper, ScriptObject, std::shared_ptr<TransformHistory_wrapper>>("TransformHistory")
			.def(luabind::constructor<unsigned int, unsigned int>())

			.enum_("ReadResult")
			[
				luabind::value("READ_OLDEST", TransformHistory::READ_OLDEST),
				luabind::value("VALUES_UNCHANGED", TransformHistory::VALUES_UNCHANGED),
				luabind::value("INTERPOLATED", TransformHistory::INTERPOLATED)
			]
			
			.def("write", &TransformHistory_wrapper::Write)
			.def("overwrite", &TransformHistory::Overwrite)
			.def("read", &TransformHistory_wrapper::Read)
			.def("clear", &TransformHistory::Clear),

			luabind::class_<TransformHistory, TransformHistory_wrapper>("TransformHistory_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// NetworkingBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope NetworkingBinding()
	{
		return (
			luabind::namespace_("root")
			[
				NetObjectBinding(),
				NetworkBinding(),
				BitStreamBinding(),
				TransformHistoryBinding()
			]
		);
	}
}

namespace luabind
{
	template<>
	struct default_converter<RakNet::NetworkID> : native_converter_base<RakNet::NetworkID>
	{
		static int compute_score(lua_State* L, int _index) {
			return lua_type(L, _index) == LUA_TSTRING ? 0 : -1;
		}

		RakNet::NetworkID to_cpp_deferred(lua_State* L, int index) {
			return std::stoull(lua_tostring(L, index));
		}

		void to_lua_deferred(lua_State* L, RakNet::NetworkID id) {
			lua_pushstring(L, std::to_string(id).c_str());
		}
	};

	template<>
	struct default_converter<const RakNet::NetworkID> :
	default_converter<RakNet::NetworkID>
	{};
	template<>
	struct default_converter<const RakNet::NetworkID&> :
	default_converter<RakNet::NetworkID>
	{};
}