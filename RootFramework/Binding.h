/**
* @file Binding.h
* @brief
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "LoggerBinding.h"
#include "MathBinding.h"
#include "FileSystemBinding.h"
#include "GLBinding.h"
#include "RenderingBinding.h"
#include "PhysicsBinding.h"
#include "NetworkingBinding.h"
#include "SFMLBinding.h"
#include "GwenBinding.h"
#include "JsonBinding.h"
#include "Engine.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// ColorBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static Color RandomColor()
	{
		Color result;
		result.r = glm::linearRand<int>(0, 255);
		result.g = glm::linearRand<int>(0, 255);
		result.b = glm::linearRand<int>(0, 255);
		result.a = 255;

		return result;
	}
	/*----------------------------------------------------------------------------*/
	std::ostream& operator<<(std::ostream& os, const Color& color) {
		return os << std::hex << color.ToInteger();
	}
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ColorBinding()
	{
		return (
			luabind::class_<Color>("Color")
			.scope
			[
				luabind::def("random", &RandomColor)
			]
			.def(luabind::constructor<unsigned char, unsigned char, unsigned char, unsigned char>())
			.def(luabind::constructor<unsigned char, unsigned char, unsigned char>())
			.def(luabind::tostring(luabind::const_self))
			.def(luabind::self == luabind::other<Color&>())
			.def("to_integer", &Color::ToInteger)
			.def_readwrite("r", &Color::r)
			.def_readwrite("g", &Color::g)
			.def_readwrite("b", &Color::b)
			.def_readwrite("a", &Color::a)
		);
	}
	/*----------------------------------------------------------------------------*/
	inline void ColorBinding_Globals(lua_State* L)
	{
		luabind::globals(L)["root"]["Color"]["Black"] = Color::Black;
		luabind::globals(L)["root"]["Color"]["White"] = Color::White;
		luabind::globals(L)["root"]["Color"]["Red"] = Color::Red;
		luabind::globals(L)["root"]["Color"]["Green"] = Color::Green;
		luabind::globals(L)["root"]["Color"]["Blue"] = Color::Blue;
		luabind::globals(L)["root"]["Color"]["Yellow"] = Color::Yellow;
		luabind::globals(L)["root"]["Color"]["Magenta"] = Color::Magenta;
		luabind::globals(L)["root"]["Color"]["Cyan"] = Color::Cyan;
		luabind::globals(L)["root"]["Color"]["Transparent"] = Color::Transparent;
	}

	////////////////////////////////////////////////////////////////////////////////
	// SystemBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	std::ostream& operator<<(std::ostream& os, const System::Version& version) {
		return os << version.major << "." << version.minor << "." << version.patch;
	}
	/*----------------------------------------------------------------------------*/
	std::ostream& operator<<(std::ostream& os, const System& system) {
		return os << system.GetVersion();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope SystemBinding()
	{
		return (
			luabind::class_<System>("System")
			.scope
			[
				luabind::class_<System::Version>("Version")
				.def(luabind::self == luabind::other<System::Version>())
				.def(luabind::tostring(luabind::const_self))
				.def_readwrite("major", &System::Version::major)
				.def_readwrite("minor", &System::Version::minor)
				.def_readwrite("patch", &System::Version::patch)
			]
			.def(luabind::tostring(luabind::const_self))
			.property("version", &System::GetVersion)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// EngineBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope EngineBinding()
	{
		return (
			luabind::class_<Engine, System>("Engine")
			.def("shutdown", &Engine::Shutdown)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// BuildLuaContext:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static lua_State* BuildLuaContext(Engine* engine_instance, lua_State* state)
	{
		luaL_openlibs(state);
		LoggerBinding_OverridePrintLib(state);
		luabind::open(state);
		luabind::module(state)
		[
			luabind::namespace_("root")
			[
				ColorBinding(),
				SystemBinding(),
				EngineBinding()
			],

			ScriptObjectBinding(),
			LoggerBinding(),
			MathBinding(),
			RenderingBinding(),
			PhysicsBinding(),
			FileSystemBinding(),
			NetworkingBinding(),
			GLBinding(),
			SFMLBinding(),
			GwenBinding(),
			JsonBinding()
		];

		ColorBinding_Globals(state);
		MathBinding_Globals(state);
		GLBinding_Globals(state);

		luabind::globals(state)["FileSystem"] = engine_instance->GetSystem<FileSystem>();
		luabind::globals(state)["Engine"] = engine_instance;
		luabind::bind_class_info(state);

		return state;
	};

} // root namespace