/**
* @file Texture3D.h
* @brief
*/

#pragma once

#include "Texture.h"

namespace root
{
	class Texture3D : public Texture
	{
		public:
			//! CTOR/DTOR:
			Texture3D(const Settings& settings, unsigned int w, unsigned int h, unsigned int depth);
			virtual ~Texture3D();

			//! SERVICES:
			static void Unbind();

		protected:
			//! VIRTUALS:
			virtual void Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level = 0);
			virtual GLenum GetTarget() const;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture2D inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture3D::Texture3D(const Settings& settings, unsigned int w, unsigned int h, unsigned int depth)
		: Texture(settings, w, h, depth) {
	}
	/*----------------------------------------------------------------------------*/
	inline Texture3D::~Texture3D() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture3D::Unbind() {
		glBindTexture(GL_TEXTURE_3D, GL_ZERO);
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture3D::Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level) {
		glTexImage3D(GetTarget(), 0, iformat, (GLsizei)w, (GLsizei)h, (GLsizei)depth, border, format, type, data);
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum Texture3D::GetTarget() const {
		return GL_TEXTURE_3D;
	}
}