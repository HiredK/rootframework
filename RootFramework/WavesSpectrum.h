/**
* @file WavesSpectrum.h
* @brief
*/

#pragma once

#include "Texture.h"
#include "Math.h"

#define OMEGA 0.84
#define cm 0.23
#define km 370.0

#define GRID1_SIZE 5488.0
#define GRID2_SIZE 392.0
#define GRID3_SIZE 28.0
#define GRID4_SIZE 2.0

namespace root
{
	class WavesSpectrum
	{
		public:
			//! TYPEDEF/ENUMS:
			enum { SPECTRUM_12, SPECTRUM_34, BUTTERFLY };

			//! CTOR/DTOR:
			WavesSpectrum(unsigned int passes = 8, float wind = 5.0f);
			virtual ~WavesSpectrum();

			//! SERVICES:
			void GenerateData(float wind, float amplitude = 1.0f);
			void Bind(int i);

			//! ACCESSORS:
			float GetSlopeVarianceDelta() const;
			unsigned int GetSize() const;

		private:
			//! MEMBERS:
			std::shared_ptr<Texture> m_textures[3];
			unsigned int m_passes;
			float m_slope_variance_delta;
	};

	////////////////////////////////////////////////////////////////////////////////
	// WavesSpectrum inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline WavesSpectrum::WavesSpectrum(unsigned int passes, float wind)
		: m_passes(passes)
		, m_slope_variance_delta(0.0f) {
		GenerateData(wind);
	}
	/*----------------------------------------------------------------------------*/
	inline WavesSpectrum::~WavesSpectrum() {
	}

	/*----------------------------------------------------------------------------*/
	inline void WavesSpectrum::Bind(int i) {
		m_textures[i]->Bind();
	}

	/*----------------------------------------------------------------------------*/
	inline float WavesSpectrum::GetSlopeVarianceDelta() const {
		return m_slope_variance_delta;
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int WavesSpectrum::GetSize() const {
		return 1 << m_passes;
	}
}