/**
* @file Transform.h
* @brief
*/

#pragma once

#include "Frustum.h"

namespace root
{
	class Transform
	{
		public:
			//! CTOR/DTOR:
			Transform(const dvec3& position = dvec3(), const dquat& rotation = dquat(), const dvec3& scale = dvec3(1));
			Transform(const Transform& other);
			virtual ~Transform();

			//! SERVICES:
			void Copy(const Transform& other);
			dmat4 ComputeMatrix();

			//! ACCESSORS:
			void SetPosition(const dvec3& position);
			const dvec3& GetPosition() const;
			dvec3& GetPosition();
			void SetRotation(const dquat& rotation);
			const dquat& GetRotation() const;
			dquat& GetRotation();
			void SetScale(const dvec3& scale);
			const dvec3& GetScale() const;
			dvec3& GetScale();

			Box3D GetTransformedBoundingBox();
			Box3D& GetBoundingBox();

		protected:
			//! MEMBERS:
			dvec3 m_position;
			dquat m_rotation;
			dvec3 m_scale;
			Box3D m_bounding_box;
	};

	////////////////////////////////////////////////////////////////////////////////
	// BoundingBox inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Transform::Transform(const dvec3& position, const dquat& rotation, const dvec3& scale)
		: m_position(position), m_rotation(rotation), m_scale(scale) {
	}
	/*----------------------------------------------------------------------------*/
	inline Transform::Transform(const Transform& other)
		: Transform(other.m_position, other.m_rotation, other.m_scale)  {
	}
	/*----------------------------------------------------------------------------*/
	inline Transform::~Transform() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Transform::Copy(const Transform& other)
	{
		m_position = other.m_position;
		m_rotation = other.m_rotation;
		m_scale = other.m_scale;
	}

	/*----------------------------------------------------------------------------*/
	inline void Transform::SetPosition(const dvec3& position) {
		m_position = position;
	}
	/*----------------------------------------------------------------------------*/
	inline const dvec3& Transform::GetPosition() const {
		return m_position;
	}
	/*----------------------------------------------------------------------------*/
	inline dvec3& Transform::GetPosition() {
		return m_position;
	}
	/*----------------------------------------------------------------------------*/
	inline void Transform::SetRotation(const dquat& rotation) {
		m_rotation = rotation;
	}
	/*----------------------------------------------------------------------------*/
	inline const dquat& Transform::GetRotation() const {
		return m_rotation;
	}
	/*----------------------------------------------------------------------------*/
	inline dquat& Transform::GetRotation() {
		return m_rotation;
	}
	/*----------------------------------------------------------------------------*/
	inline void Transform::SetScale(const dvec3& scale) {
		m_scale = scale;
	}
	/*----------------------------------------------------------------------------*/
	inline const dvec3& Transform::GetScale() const {
		return m_scale;
	}
	/*----------------------------------------------------------------------------*/
	inline dvec3& Transform::GetScale() {
		return m_scale;
	}

	/*----------------------------------------------------------------------------*/
	inline Box3D Transform::GetTransformedBoundingBox() {
		return m_bounding_box.Transform(ComputeMatrix());
	}
	/*----------------------------------------------------------------------------*/
	inline Box3D& Transform::GetBoundingBox() {
		return m_bounding_box;
	}
}