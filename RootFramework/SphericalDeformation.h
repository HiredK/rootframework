/**
* @file SphericalDeformation.h
*/

#pragma once

#include "Deformation.h"

namespace root
{
	class SphericalDeformation : public Deformation
	{
		public:
			//! CTOR/DTOR:
			SphericalDeformation(double radius);
			virtual ~SphericalDeformation();

			//! VIRTUALS:
			virtual dvec3 LocalToDeformed(const dvec3& local);
			virtual dmat4 LocalToDeformedDifferential(const dvec3& local, bool clamp = false);
			virtual dvec3 DeformedToLocal(const dvec3& deformed);
			virtual Box2D DeformedToLocalBounds(const dvec3& center, double radius);
			virtual dmat4 DeformedToTangentFrame(const dvec3& deformed);
			virtual Frustum::Visibility GetVisibility(const dvec3& position, const Frustum& frustum, const Box3D& box);
			virtual void SetUniforms(Camera& camera, const dmat3& matrix, int level, double ox, double oy, double l);
			virtual void SetUniforms(Camera& camera, const TerrainTile& tile);

		protected:
			//! SERVICES:
			Frustum::Visibility GetVisibility(const dvec4& clip, dvec3* b, double f);

			//! MEMBERS:
			double m_radius;
	};

	////////////////////////////////////////////////////////////////////////////////
	// SphericalDeformation inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline SphericalDeformation::SphericalDeformation(double radius)
		: m_radius(radius) {
	}
	/*----------------------------------------------------------------------------*/
	inline SphericalDeformation::~SphericalDeformation() {
	}

	/*----------------------------------------------------------------------------*/
	inline dvec3 SphericalDeformation::LocalToDeformed(const dvec3& local) {
		const dvec3 v = dvec3(local.x, local.y, m_radius);
		return v * (local.z + m_radius) / glm::length(v);
	}

	/*----------------------------------------------------------------------------*/
	inline dmat4 SphericalDeformation::LocalToDeformedDifferential(const dvec3& local, bool clamp)
	{
		dvec3 p = local;
		if (clamp) {
			p.x = p.x - glm::floor((p.x + m_radius) / (2.0 * m_radius)) * 2.0 * m_radius;
			p.y = p.y - glm::floor((p.y + m_radius) / (2.0 * m_radius)) * 2.0 * m_radius;
		}

		double l = p.x*p.x + p.y*p.y + m_radius*m_radius;
		double c0 = 1.0 / glm::sqrt(l);
		double c1 = c0 * m_radius / l;

		return dmat4(
			(p.y*p.y + m_radius*m_radius)*c1, -p.x*p.y*c1, p.x*c0, m_radius*p.x*c0,
			-p.x*p.y*c1, (p.x*p.x + m_radius*m_radius)*c1, p.y*c0, m_radius*p.y*c0,
			-p.x*m_radius*c1, -p.y*m_radius*c1, m_radius*c0, (m_radius*m_radius)*c0,
			0, 0, 0, 1
		);
	}

	/*----------------------------------------------------------------------------*/
	inline dvec3 SphericalDeformation::DeformedToLocal(const dvec3& deformed)
	{
		double l = glm::length(deformed);

		if (deformed.z >= glm::abs(deformed.x) && deformed.z >= glm::abs(deformed.y)) {
			return dvec3(deformed.x / deformed.z * m_radius, deformed.y / deformed.z * m_radius, l - m_radius);
		}
		if (deformed.z <= -glm::abs(deformed.x) && deformed.z <= -glm::abs(deformed.y)) {
			return dvec3(std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity(), std::numeric_limits<double>::infinity());
		}
		if (deformed.y >= glm::abs(deformed.x) && deformed.y >= glm::abs(deformed.z)) {
			return dvec3(deformed.x / deformed.y * m_radius, (2.0 - deformed.z / deformed.y) * m_radius, l - m_radius);
		}
		if (deformed.y <= -glm::abs(deformed.x) && deformed.y <= -glm::abs(deformed.z)) {
			return dvec3(-deformed.x / deformed.y * m_radius, (-2.0 - deformed.z / deformed.y) * m_radius, l - m_radius);
		}
		if (deformed.x >= glm::abs(deformed.y) && deformed.x >= glm::abs(deformed.z)) {
			return dvec3((2.0 - deformed.z / deformed.x) * m_radius, deformed.y / deformed.x * m_radius, l - m_radius);
		}
		if (deformed.x <= -glm::abs(deformed.y) && deformed.x <= -glm::abs(deformed.z)) {
			return dvec3((-2.0 - deformed.z / deformed.x) * m_radius, -deformed.y / deformed.x * m_radius, l - m_radius);
		}

		return dvec3(1);
	}

	/*----------------------------------------------------------------------------*/
	inline Box2D SphericalDeformation::DeformedToLocalBounds(const dvec3& center, double radius)
	{
		dvec3 p = DeformedToLocal(center);
		double r = radius;

		double k = (1.0 - r * r / (2.0 * m_radius * m_radius)) * glm::length(dvec3(p.x, p.y, m_radius));
		double A = k * k - p.x * p.x;
		double B = k * k - p.y * p.y;
		double C = -2.0 * p.x * p.y;
		double D = -2.0 * m_radius * m_radius * p.x;
		double E = -2.0 * m_radius * m_radius * p.y;
		double F = m_radius * m_radius * (k * k - m_radius * m_radius);

		double a = C * C - 4.0 * A * B;
		double b = 2.0 * C * E - 4.0 * B * D;
		double c = E * E - 4.0 * B * F;
		double d = glm::sqrt(b * b - 4.0 * a * c);
		double x1 = (-b - d) / (2.0 * a);
		double x2 = (-b + d) / (2.0 * a);

		b = 2.0 * C * D - 4.0 * A * E;
		c = D * D - 4.0 * A * F;
		d = glm::sqrt(b * b - 4.0 * a * c);
		double y1 = (-b - d) / (2.0 * a);
		double y2 = (-b + d) / (2.0 * a);

		return Box2D(dvec2(x1, y1), dvec2(x2, y2));
	}

	/*----------------------------------------------------------------------------*/
	inline dmat4 SphericalDeformation::DeformedToTangentFrame(const dvec3& deformed)
	{
		dvec3 Uz = glm::normalize(deformed);
		dvec3 Ux = glm::normalize(glm::cross(dvec3(0, 1, 0), Uz));
		dvec3 Uy = glm::cross(Uz, Ux);

		return dmat4(
			Ux.x, Ux.y, Ux.z, 0,
			Uy.x, Uy.y, Uy.z, 0,
			Uz.x, Uz.y, Uz.z, -m_radius,
			0, 0, 0, 1
		);
	}

	/*----------------------------------------------------------------------------*/
	inline Frustum::Visibility SphericalDeformation::GetVisibility(const dvec3& position, const Frustum& frustum, const Box3D& box)
	{
		dvec3 deformed_box[4];
		deformed_box[0] = LocalToDeformed(dvec3(box.GetMin().x, box.GetMin().y, box.GetMin().z));
		deformed_box[1] = LocalToDeformed(dvec3(box.GetMax().x, box.GetMin().y, box.GetMin().z));
		deformed_box[2] = LocalToDeformed(dvec3(box.GetMax().x, box.GetMax().y, box.GetMin().z));
		deformed_box[3] = LocalToDeformed(dvec3(box.GetMin().x, box.GetMax().y, box.GetMin().z));

		double a = (box.GetMax().z + m_radius) / (box.GetMin().z + m_radius);
		double dx = (box.GetMax().x - box.GetMin().x) / 2 * a;
		double dy = (box.GetMax().y - box.GetMin().y) / 2 * a;
		double dz = box.GetMax().z + m_radius;
		double f = glm::sqrt(dx * dx + dy * dy + dz * dz) / (box.GetMin().z + m_radius);

		Frustum::Visibility v0 = GetVisibility(frustum.GetPlane(Frustum::Right), deformed_box, f);
		if (v0 == Frustum::Invisible) {
			return Frustum::Invisible;
		}

		Frustum::Visibility v1 = GetVisibility(frustum.GetPlane(Frustum::Left), deformed_box, f);
		if (v1 == Frustum::Invisible) {
			return Frustum::Invisible;
		}

		Frustum::Visibility v2 = GetVisibility(frustum.GetPlane(Frustum::Bottom), deformed_box, f);
		if (v2 == Frustum::Invisible) {
			return Frustum::Invisible;
		}

		Frustum::Visibility v3 = GetVisibility(frustum.GetPlane(Frustum::Top), deformed_box, f);
		if (v3 == Frustum::Invisible) {
			return Frustum::Invisible;
		}

		Frustum::Visibility v4 = GetVisibility(frustum.GetPlane(Frustum::Front), deformed_box, f);
		if (v4 == Frustum::Invisible) {
			return Frustum::Invisible;
		}

		double lSq = glm::length2(position);
		double rm = m_radius + glm::min(0.0, box.GetMin().z);
		double rM = m_radius + box.GetMax().z;
		double rmSq = rm * rm;
		double rMSq = rM * rM;
		dvec4 far = dvec4(position, glm::sqrt((lSq - rmSq) * (rMSq - rmSq)) - rmSq);

		Frustum::Visibility v5 = GetVisibility(far, deformed_box, f);
		if (v5 == Frustum::Invisible) {
			return Frustum::Invisible;
		}

		if (v0 == Frustum::Completly && v1 == Frustum::Completly &&
			v2 == Frustum::Completly && v3 == Frustum::Completly &&
			v4 == Frustum::Completly && v5 == Frustum::Completly) {
			return Frustum::Completly;
		}

		return Frustum::Partially;
	}

	/*----------------------------------------------------------------------------*/
	inline Frustum::Visibility SphericalDeformation::GetVisibility(const dvec4& clip, dvec3* b, double f)
	{
		double o = b[0].x * clip.x + b[0].y * clip.y + b[0].z * clip.z;
		bool p = o + clip.w > 0.0;

		if ((o * f + clip.w > 0.0) == p) {
			o = b[1].x * clip.x + b[1].y * clip.y + b[1].z * clip.z;

			if ((o + clip.w > 0.0) == p && (o * f + clip.w > 0.0) == p) {
				o = b[2].x * clip.x + b[2].y * clip.y + b[2].z * clip.z;

				if ((o + clip.w > 0.0) == p && (o * f + clip.w > 0.0) == p) {
					o = b[3].x * clip.x + b[3].y * clip.y + b[3].z * clip.z;

					return 	(o + clip.w > 0.0) == p && (o * f + clip.w > 0.0) == p ?
						(p ? Frustum::Completly : Frustum::Invisible) :
						Frustum::Partially;
				}
			}
		}

		return Frustum::Partially;
	}
}