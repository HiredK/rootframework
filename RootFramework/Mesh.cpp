#include "Mesh.h"

root::Mesh::Mesh(const void* data, GLsizei vertex_count, GLsizei stride, GLenum mode, GLenum usage, const void* indices, GLsizei indice_count, GLenum type)
	: m_count((indice_count > 0) ? indice_count : vertex_count)
	, m_stride(stride)
	, m_mode(mode)
	, m_type(type)
	, m_color(Color::White)
{
	GLsizei type_size = 0;
	switch (type) {
		case GL_UNSIGNED_BYTE:
			type_size = sizeof(GLubyte);
			break;
		case GL_UNSIGNED_SHORT:
			type_size = sizeof(GLushort);
			break;
		case GL_UNSIGNED_INT:
			type_size = sizeof(GLuint);
			break;
	}

	m_buffers[VertexBuffer] = std::unique_ptr<GLBuffer>(
		new GLBuffer(stride * vertex_count, data, GL_ARRAY_BUFFER, usage)
	);
	m_buffers[IndiceBuffer] = std::unique_ptr<GLBuffer>(
		new GLBuffer(type_size * indice_count, indices, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW)
	);
}

void root::Mesh::UseDefaultVertex2D()
{
	AddVertexAttribute("vs_Position", 2, GL_FLOAT, false, offsetof(Vertex2D, x));
	AddVertexAttribute("vs_TexCoord", 2, GL_FLOAT, false, offsetof(Vertex2D, s));
	AddVertexAttribute("vs_Color", 4, GL_UNSIGNED_BYTE, true, offsetof(Vertex2D, r));
}

void root::Mesh::UseDefaultVertex3D()
{
	AddVertexAttribute("vs_Position", 4, GL_FLOAT, false, offsetof(Vertex3D, x));
	AddVertexAttribute("vs_TexCoord", 2, GL_FLOAT, false, offsetof(Vertex3D, s));
	AddVertexAttribute("vs_Color", 4, GL_UNSIGNED_BYTE, true, offsetof(Vertex3D, r));
	AddVertexAttribute("vs_Normal", 3, GL_FLOAT, false, offsetof(Vertex3D, nx));
	AddVertexAttribute("vs_Tangent", 3, GL_FLOAT, false, offsetof(Vertex3D, tx));
}

void root::Mesh::Draw()
{
	if (m_count > 0) {
		GLBuffer::ScopedBinder vbo_binder(*m_buffers[VertexBuffer].get());
		m_buffers[VertexBuffer]->Unmap();

		for (auto& attrib : m_attribs) {
			GLuint loc = Shader::Get()->GetAttribLocation(attrib.first);
			glEnableVertexAttribArray(loc);
			glVertexAttribPointer(loc, attrib.second.size, attrib.second.type, attrib.second.normalized, m_stride,
				m_buffers[VertexBuffer]->GetPointer(attrib.second.offset)
			);
		}

		Shader::Get()->UniformMat4("u_ModelMatrix", ComputeMatrix());
		Shader::Get()->UniformMat3("u_NormalMatrix", mat3(glm::mat4_cast(m_rotation)));

		if (m_mode == GL_PATCHES) {
			glPatchParameteri(GL_PATCH_VERTICES, 4);
		}

		if (m_buffers[IndiceBuffer]->GetSize() > 0) {
			GLBuffer::ScopedBinder ibo_binder(*m_buffers[IndiceBuffer].get());
			glDrawElements(m_mode, m_count, m_type, BUFFER_OFFSET(0));
		}
		else {
			glDrawArrays(m_mode, 0, m_count);
		}

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
		glDisableVertexAttribArray(4);
	}
}

std::shared_ptr<root::Mesh> root::Mesh::BuildQuad(float w, float h, const Color& color)
{
	Vertex2D data[] = {
		{ -w,-h, 0.0, 0.0, color.r, color.g, color.b, color.a },
		{  w,-h, 1.0, 0.0, color.r, color.g, color.b, color.a },
		{ -w, h, 0.0, 1.0, color.r, color.g, color.b, color.a },
		{  w, h, 1.0, 1.0, color.r, color.g, color.b, color.a }
	};

	std::shared_ptr<Mesh> quad_mesh = std::shared_ptr<Mesh>(new Mesh(data, 4, sizeof(Vertex2D), GL_TRIANGLE_STRIP, GL_STATIC_DRAW));
	quad_mesh->UseDefaultVertex2D();
	return quad_mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildBox(const vec3& min, const vec3& max, const root::Color& color)
{
	std::vector<Vertex3D> data = {

		{ min.x, min.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, min.y, max.z, 1.0, 1.0, 0.0, color.r, color.g, color.b, color.a, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, max.z, 1.0, 1.0, 1.0, color.r, color.g, color.b, color.a, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, min.z, 1.0, 0.0, 1.0, color.r, color.g, color.b, color.a, -1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, max.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, +1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, min.z, 1.0, 1.0, 0.0, color.r, color.g, color.b, color.a, +1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, min.z, 1.0, 1.0, 1.0, color.r, color.g, color.b, color.a, +1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, max.z, 1.0, 0.0, 1.0, color.r, color.g, color.b, color.a, +1.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, min.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, min.z, 1.0, 1.0, 0.0, color.r, color.g, color.b, color.a, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, max.z, 1.0, 1.0, 1.0, color.r, color.g, color.b, color.a, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, min.y, max.z, 1.0, 0.0, 1.0, color.r, color.g, color.b, color.a, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, +1.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, min.z, 1.0, 1.0, 0.0, color.r, color.g, color.b, color.a, 0.0, +1.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, max.z, 1.0, 1.0, 1.0, color.r, color.g, color.b, color.a, 0.0, +1.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, max.z, 1.0, 0.0, 1.0, color.r, color.g, color.b, color.a, 0.0, +1.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, min.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, min.z, 1.0, 1.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, min.z, 1.0, 1.0, 1.0, color.r, color.g, color.b, color.a, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, min.z, 1.0, 0.0, 1.0, color.r, color.g, color.b, color.a, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, max.y, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, +1.0, 0.0, 0.0, 0.0 },
		{ min.x, min.y, max.y, 1.0, 1.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, +1.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, max.y, 1.0, 1.0, 1.0, color.r, color.g, color.b, color.a, 0.0, 0.0, +1.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, max.y, 1.0, 0.0, 1.0, color.r, color.g, color.b, color.a, 0.0, 0.0, +1.0, 0.0, 0.0, 0.0 }
	};

	std::vector<ivec4> quads = { { 0, 1, 2, 3 }, { 4, 5, 6, 7 }, { 8, 9, 10, 11 }, { 12, 13, 14, 15 }, { 16, 17, 18, 19 }, { 20, 21, 22, 23 } };
	std::vector<unsigned short> indices;

	for (auto & q : quads)
	{
		indices.push_back(q.x);
		indices.push_back(q.y);
		indices.push_back(q.z);

		indices.push_back(q.x);
		indices.push_back(q.z);
		indices.push_back(q.w);
	}

	Mesh::ComputeNormals(data, indices, false);
	Mesh::ComputeTangents(data, indices);

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->GetBoundingBox().Enlarge(min);
	mesh->GetBoundingBox().Enlarge(max);
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildBoxWire(const vec3& min, const vec3& max, const Color& color)
{
	std::vector<Vertex3D> data = {
		{ min.x, min.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, min.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, min.y, max.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ min.x, max.y, max.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, min.y, max.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 },
		{ max.x, max.y, max.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 }
	};

	std::vector<unsigned short> indices{ 0, 1, 0, 2, 3, 1, 3, 2, 0, 4, 2, 5, 1, 6, 3, 7, 4, 6, 4, 5, 7, 6, 7, 5 };

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_LINES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->GetBoundingBox().Enlarge(min);
	mesh->GetBoundingBox().Enlarge(max);
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildSphere(float radius, int U, int V, const Color& color)
{
	std::vector<Vertex3D> data;
	std::vector<unsigned short> indices;

	for (int ui = 0; ui < U; ++ui)
	for (int vi = 0; vi < V; ++vi)
	{
		float u = float(ui) / (U - 1) * glm::pi<float>();
		float v = float(vi) / (V - 1) * 2 * glm::pi<float>();

		// spherical(theta, phi)
		vec3 n = glm::normalize(vec3(glm::cos(v) * glm::sin(u), glm::sin(v) * glm::sin(u), glm::cos(u)));

		data.push_back({ n.x * radius, n.y * radius, n.z * radius, 1.0, u, v, color.r, color.g, color.b, color.a, n.x, n.y, n.z, 0.0, 0.0, 0.0 });
	}

	for (int ui = 0; ui < U; ++ui)
	{
		int un = (ui + 1) % U;
		for (int vi = 0; vi < V; ++vi)
		{
			int vn = (vi + 1) % V;

			indices.push_back(ui * V + vi);
			indices.push_back(un * V + vi);
			indices.push_back(un * V + vn);

			indices.push_back(ui * V + vi);
			indices.push_back(un * V + vn);
			indices.push_back(ui * V + vn);
		}
	}

	Mesh::ComputeTangents(data, indices);

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildCylinder(float r1, float r2, float height, int R, int H, bool open, const Color& color)
{
	std::vector<Vertex3D> data;
	std::vector<unsigned short> indices;

	std::vector<std::vector<uint32_t>> vertexRowArray;
	float hh = height * 0.5f;

	// Body
	for (int y = 0; y <= H; y++)
	{
		std::vector<uint32_t> newRow;

		float v = (float)y / H;
		float r = v * (r2 - r1) + r1;

		for (int x = 0; x <= R; x++)
		{
			float u = (float)x / R;

			vec3 vertex;
			vertex.x = r * glm::sin(u * glm::two_pi<float>());
			vertex.y = -v * height + hh;
			vertex.z = r * glm::cos(u * glm::two_pi<float>());

			data.push_back({ vertex.x, vertex.y, vertex.z, 1.0, u, v, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 });
			newRow.push_back((int)data.size() - 1);
		}

		vertexRowArray.push_back(newRow);
	}

	for (int x = 0; x < R; x++)
	{
		for (int y = 0; y < H; y++)
		{
			uint32_t v1 = vertexRowArray[y][x];
			uint32_t v2 = vertexRowArray[y + 1][x];
			uint32_t v3 = vertexRowArray[y + 1][x + 1];
			uint32_t v4 = vertexRowArray[y][x + 1];

			indices.push_back(v1);
			indices.push_back(v2);
			indices.push_back(v4);

			indices.push_back(v2);
			indices.push_back(v3);
			indices.push_back(v4);
		}
	}

	// Top
	if (!open && r1 > 0)
	{
		data.push_back({ 0.0, hh, 0.0, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 });

		for (int x = 0; x < R; x++)
		{
			uint32_t v1 = vertexRowArray[0][x];
			uint32_t v2 = vertexRowArray[0][x + 1];
			uint32_t v3 = (int)data.size() - 1;

			indices.push_back(v1);
			indices.push_back(v2);
			indices.push_back(v3);
		}
	}

	// Bottom
	if (!open && r2 > 0)
	{
		data.push_back({ 0.0, -hh, 0.0, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 });

		for (int x = 0; x < R; x++)
		{
			uint32_t v1 = vertexRowArray[H][x + 1];
			uint32_t v2 = vertexRowArray[H][x];
			uint32_t v3 = (int)data.size() - 1;

			indices.push_back(v1);
			indices.push_back(v2);
			indices.push_back(v3);
		}
	}

	Mesh::ComputeNormals(data, indices, true);
	Mesh::ComputeTangents(data, indices);

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildCone(float r, float height, int R, const Color& color)
{
	std::vector<Vertex3D> data;
	std::vector<unsigned short> indices;
	data.resize(R * 2 + 1);

	const float dtheta = glm::two_pi<float>() / R;
	float theta = 0;

	for (int i = 0; i < data.size() - 1; i += 2)
	{
		vec3 vertex;
		vertex.x = r * glm::cos(theta);
		vertex.y = 0.5f - height;
		vertex.z = r * glm::sin(theta);

		// apex vertex
		data[i + 0] = { 0.0, 0.5, 0.0, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

		// rim vertex
		data[i + 1] = { vertex.x, vertex.y, vertex.z, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };
		theta += dtheta;
	}

	// disk center
	data[data.size() - 1] = { 0.0, 0.5f - height, 0.0, 1.0, 0.0, 0.0, color.r, color.g, color.b, color.a, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0 };

	// body indices
	for (int i = 0; i < R * 2; i += 2) {

		indices.push_back((i + 3) % (2 * R));
		indices.push_back((i + 1) % (2 * R));
		indices.push_back(i);
	}

	// disk indices (ccw)
	const int diskCenterIndex = (int)data.size() - 1;
	for (int i = 1; i < R * 2 + 1; i += 2) {

		indices.push_back(diskCenterIndex);
		indices.push_back(i);
		indices.push_back((i + 2) % (2 * R));
	}

	Mesh::ComputeNormals(data, indices, true);
	Mesh::ComputeTangents(data, indices);

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildCapsule(float radius, float length, int S, const Color& color)
{
	std::vector<Vertex3D> data;
	std::vector<unsigned short> indices;
	float hl = length * 0.5f;

	for (int j = 0; j < S * 2; ++j) {
		data.push_back({ 0.0, hl + radius, 0.0, 1.0, (j + 1) / float(S), 0.0, color.r, color.g, color.b, color.a, 0.0, +1.0, 0.0, 0.0, 0.0, 0.0 });
	}

	for (int i = 1; i < S; ++i)
	{
		float r = glm::sin(i * glm::pi<float>() / S) * radius;
		float y = glm::cos(i * glm::pi<float>() / S);
		float ty = (i < S / 2) ? (y * radius) + hl : (y * radius) - hl;
		vec3 n = glm::normalize(vec3(0.f, y, -1.f));

		data.push_back({ 0.0, ty, -r, 1.0, 0.0, i / float(S), color.r, color.g, color.b, color.a, n.x, n.y, n.z, 0.0, 0.0, 0.0 });

		for (int j = 1; j < S * 2; ++j)
		{
			float x = glm::sin(j * glm::two_pi<float>() / (S * 2));
			float z = -glm::cos(j * glm::two_pi<float>() / (S * 2));
			ty = (i < S / 2) ? (y * radius) + hl : (y * radius) - hl;
			n = glm::normalize(vec3(x, y, z));

			data.push_back({ x * r, ty, z * r, 1.0, j / float(S), i / float(S), color.r, color.g, color.b, color.a, n.x, n.y, n.z, 0.0, 0.0, 0.0 });
		}

		n = glm::normalize(vec3(0.f, y, -1.f));
		data.push_back({ 0.0, ty, -r, 1.0, 2, i / float(S), color.r, color.g, color.b, color.a, n.x, n.y, n.z, 0.0, 0.0, 0.0 });
	}

	for (int j = 0; j < S * 2; ++j)
	{
		float ty = -hl - radius;
		data.push_back({ 0.0, ty, 0.0, 1.0, (j + 1) / float(S), 1.0, color.r, color.g, color.b, color.a, 0.0, -1.0, 0.0, 0.0, 0.0, 0.0 });
	}

	int v = 0;
	for (int j = 0; j < S * 2; ++j)
	{
		indices.push_back(v);
		indices.push_back(v + (S * 2) + 1);
		indices.push_back(v + (S * 2));
		++v;
	}

	for (int i = 1; i < S - 1; ++i)
	{
		for (int j = 0; j < S * 2; ++j)
		{
			indices.push_back(v);
			indices.push_back(v + 1);
			indices.push_back(v + (S * 2) + 2);

			indices.push_back(v);
			indices.push_back(v + (S * 2) + 2);
			indices.push_back(v + (S * 2) + 1);
			++v;
		}

		++v;
	}

	for (int j = 0; j < S * 2; ++j)
	{
		indices.push_back(v);
		indices.push_back(v + 1);
		indices.push_back(v + (S * 2) + 1);
		++v;
	}

	Mesh::ComputeTangents(data, indices);

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildPlane(int w, int h, bool center, bool cw, const Color& color)
{
	std::vector<Vertex3D> data;
	data.resize(w * h);

	std::vector<unsigned short> indices;

	for (int i = 0; i < w; ++i)
	for (int j = 0; j < h; ++j)
	{
		vec2 offset((float)i / (float)(w - 1), (float)j / (float)(h - 1));

		if (center) {
			offset.x = (offset.x - 0.5f) * 2.0f;
			offset.y = (offset.y - 0.5f) * 2.0f;
		}

		data[i + j * w] = { offset.x, offset.y, 0.0, 1.0, offset.x, offset.y, color.r, color.g, color.b, color.a, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0 };

		if (i < w - 1 && j < h - 1)
		{
			if (cw) {
				indices.push_back(i + j * w);
				indices.push_back(i + (j + 1) * w);
				indices.push_back((i + 1) + j * w);

				indices.push_back(i + (j + 1) * w);
				indices.push_back((i + 1) + (j + 1) * w);
				indices.push_back((i + 1) + j * w);
			}
			else {
				indices.push_back(i + j * w);
				indices.push_back((i + 1) + j * w);
				indices.push_back(i + (j + 1) * w);

				indices.push_back(i + (j + 1) * w);
				indices.push_back((i + 1) + j * w);
				indices.push_back((i + 1) + (j + 1) * w);
			}
		}
	}

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->UseDefaultVertex3D();
	return mesh;
}

std::shared_ptr<root::Mesh> root::Mesh::BuildPatch(int w, int h, const Color& color)
{
	std::vector<Vertex3D> data;
	data.resize(w * h);

	std::vector<unsigned short> indices;

	for (int i = 0; i < w; ++i)
	for (int j = 0; j < h; ++j)
	{
		vec2 offset((float)i / (float)(w - 1), (float)j / (float)(h - 1));

		data[i + j * w] = { offset.x, offset.y, 0.0, 1.0, offset.x, offset.y, color.r, color.g, color.b, color.a, 0.0, 0.0, 1.0, 0.0, 1.0, 0.0 };

		if (i < w - 1 && j < h - 1)
		{
			unsigned short p1 = i + j * w;
			unsigned short p2 = p1 + w; // rect here w!=h ?
			unsigned short p3 = p2 + 1;
			unsigned short p4 = p1 + 1;

			indices.push_back(p1);
			indices.push_back(p2);
			indices.push_back(p3);
			indices.push_back(p4);
		}
	}

	std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&data[0], (int)data.size(), sizeof(Vertex3D), GL_PATCHES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
	mesh->UseDefaultVertex3D();
	return mesh;
}

void root::Mesh::ComputeNormals(std::vector<root::Mesh::Vertex3D>& data, const std::vector<unsigned short>& indices, bool smooth = true)
{
	static const double NORMAL_EPSILON = 0.0001;

	std::vector<unsigned int> uniqueVertIndices(data.size(), 0);
	if (smooth)
	{
		for (uint32_t i = 0; i < uniqueVertIndices.size(); ++i)
		{
			if (uniqueVertIndices[i] == 0)
			{
				uniqueVertIndices[i] = i + 1;

				vec3 v0;
				v0.x = data[i].x;
				v0.y = data[i].y;
				v0.z = data[i].z;

				for (auto j = i + 1; j < data.size(); ++j)
				{
					vec3 v1;
					v1.x = data[j].x;
					v1.y = data[j].y;
					v1.z = data[j].z;

					if (glm::length2(v1 - v0) < NORMAL_EPSILON) {
						uniqueVertIndices[j] = uniqueVertIndices[i];
					}
				}
			}
		}

		uint32_t idx0, idx1, idx2;
		for (size_t i = 0; i < indices.size(); i += 3)
		{
			idx0 = (smooth) ? uniqueVertIndices[indices[i + 0]] - 1 : indices[i + 0];
			idx1 = (smooth) ? uniqueVertIndices[indices[i + 1]] - 1 : indices[i + 1];
			idx2 = (smooth) ? uniqueVertIndices[indices[i + 2]] - 1 : indices[i + 2];

			const vec3 v0 = vec3(data[idx0].x, data[idx0].y, data[idx0].z);
			const vec3 v1 = vec3(data[idx1].x, data[idx1].y, data[idx1].z);
			const vec3 v2 = vec3(data[idx2].x, data[idx2].y, data[idx2].z);

			vec3 e0 = v1 - v0;
			vec3 e1 = v2 - v0;
			vec3 e2 = v2 - v1;

			if (glm::length2(e0) < NORMAL_EPSILON) continue;
			if (glm::length2(e1) < NORMAL_EPSILON) continue;
			if (glm::length2(e2) < NORMAL_EPSILON) continue;

			vec3 n = glm::normalize(glm::cross(e0, e1));

			data[indices[i + 0]].nx = n.x;
			data[indices[i + 0]].ny = n.y;
			data[indices[i + 0]].nz = n.z;

			data[indices[i + 1]].nx = n.x;
			data[indices[i + 1]].ny = n.y;
			data[indices[i + 1]].nz = n.z;

			data[indices[i + 2]].nx = n.x;
			data[indices[i + 2]].ny = n.y;
			data[indices[i + 2]].nz = n.z;

			if (smooth)
			{
				for (uint32_t i = 0; i < data.size(); ++i)
				{
					data[i].nx = data[uniqueVertIndices[i] - 1].nx;
					data[i].ny = data[uniqueVertIndices[i] - 1].ny;
					data[i].nz = data[uniqueVertIndices[i] - 1].nz;
				}
			}
		}
	}
}

void root::Mesh::ComputeTangents(std::vector<root::Mesh::Vertex3D>& data, const std::vector<unsigned short>& indices)
{
	for (size_t i = 0; i < indices.size(); i += 3)
	{
		const vec3& v0 = vec3(data[indices[i + 0]].x, data[indices[i + 0]].y, data[indices[i + 0]].z);
		const vec3& v1 = vec3(data[indices[i + 1]].x, data[indices[i + 1]].y, data[indices[i + 1]].z);
		const vec3& v2 = vec3(data[indices[i + 2]].x, data[indices[i + 2]].y, data[indices[i + 2]].z);

		const vec2& w0 = vec2(data[indices[i + 0]].s, data[indices[i + 0]].t);
		const vec2& w1 = vec2(data[indices[i + 1]].s, data[indices[i + 1]].t);
		const vec2& w2 = vec2(data[indices[i + 2]].s, data[indices[i + 2]].t);

		float x1 = v1.x - v0.x;
		float x2 = v2.x - v0.x;
		float y1 = v1.y - v0.y;
		float y2 = v2.y - v0.y;
		float z1 = v1.z - v0.z;
		float z2 = v2.z - v0.z;

		float s1 = w1.x - w0.x;
		float s2 = w2.x - w0.x;

		float t1 = w1.y - w0.y;
		float t2 = w2.y - w0.y;

		float r = (s1 * t2 - s2 * t1);

		if (r != 0.0f) {
			r = 1.0f / r;
		}

		// Tangent in the S direction
		vec3 tangent((t2 * x1 - t1 * x2) * r, (t2 * y1 - t1 * y2) * r, (t2 * z1 - t1 * z2) * r);

		data[indices[i + 0]].tx = tangent.x;
		data[indices[i + 0]].ty = tangent.y;
		data[indices[i + 0]].tz = tangent.z;

		data[indices[i + 1]].tx = tangent.x;
		data[indices[i + 1]].ty = tangent.y;
		data[indices[i + 1]].tz = tangent.z;

		data[indices[i + 2]].tx = tangent.x;
		data[indices[i + 2]].ty = tangent.y;
		data[indices[i + 2]].tz = tangent.z;
	}

	for (size_t i = 0; i < data.size(); ++i)
	{
		const vec3 normal = vec3(data[i].nx, data[i].ny, data[i].nz);
		const vec3 tangent = vec3(data[i].tx, data[i].ty, data[i].tz);

		// Gram-Schmidt orthogonalize
		vec3 ortho = (tangent - normal * dot(normal, tangent));
		data[i].tx = ortho.x;
		data[i].ty = ortho.y;
		data[i].tz = ortho.z;

		const float len = glm::length(vec3(data[i].tx, data[i].ty, data[i].tz));
		if (len > 0.0f) {
			data[i].tx /= glm::sqrt(len);
			data[i].ty /= glm::sqrt(len);
			data[i].tz /= glm::sqrt(len);
		}
	}
}