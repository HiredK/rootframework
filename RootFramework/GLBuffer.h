/**
* @file GLBuffer.h
* @brief Thin abstraction over OpenGL's Buffer Objects.
*/

#pragma once

#include <algorithm>
#include <memory>
#include <new>
#include <GL/glew.h>

#define BUFFER_OFFSET(i) ((char*) NULL + (i))

namespace root
{
	class GLBuffer
	{
		public:
			//! TYPEDEF/ENUMS:
			enum MapFlags { MAP_EXPLICIT_RANGE_MODIFY = 0x01 };

			//! CTOR/DTOR:
			GLBuffer(size_t size, const void* data, GLenum target, GLenum usage, unsigned int map_flags);
			GLBuffer(size_t size, const void* data, GLenum target, GLenum usage);
			virtual ~GLBuffer();

			//! SERVICES:
			GLuint Bind();
			void Unbind();
			void* Map();
			void Unmap();

			// Marks a range of mapped data as modified.
			// Fill() calls this internally for you.
			void SetMappedRangeModified(size_t offset, size_t size);

			// Fill a portion of the buffer with data and marks the range as modified.
			// Must be bound to use this function.
			void Fill(size_t offset, size_t size, const void *data);

			//! ACCESSORS:
			const void* GetPointer(size_t offset) const;
			bool GetMapExplicitRangeModifyActive() const;
			void SetMapExplicitRangeModifyActive();
			size_t GetSize() const;
			GLenum GetTarget() const;
			GLenum GetUsage() const;
			bool IsBound() const;
			bool IsMapped() const;

			class ScopedBinder
			{
				public:
					ScopedBinder(GLBuffer& handle);
					virtual ~ScopedBinder();

				private:
					GLBuffer& m_handle;
			};

			class ScopedMapper
			{
				public:
					ScopedMapper(GLBuffer& handle);
					virtual ~ScopedMapper();
					void* Get();

				private:
					GLBuffer& m_handle;
					void* m_memory_ptr;
			};

		private:
			//! MEMBERS:
			size_t m_size;
			GLuint m_vbo;
			GLenum m_target;
			GLenum m_usage;

			bool m_bound;
			bool m_mapped;

			unsigned int m_map_flags;
			size_t m_modified_offset;
			size_t m_modified_size;
			char* m_memory_map;
	};

	////////////////////////////////////////////////////////////////////////////////
	// GLBuffer::ScopedBinder inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline GLBuffer::ScopedBinder::ScopedBinder(GLBuffer& handle)
		: m_handle(handle) {
		m_handle.Bind();
	}
	/*----------------------------------------------------------------------------*/
	inline GLBuffer::ScopedBinder::~ScopedBinder() {
		m_handle.Unbind();
	}

	////////////////////////////////////////////////////////////////////////////////
	// GLBuffer::ScopedMapper inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline GLBuffer::ScopedMapper::ScopedMapper(GLBuffer& handle)
		: m_handle(handle), m_memory_ptr(handle.Map()) {
	}
	/*----------------------------------------------------------------------------*/
	inline GLBuffer::ScopedMapper::~ScopedMapper() {
		m_handle.Unmap();
	}
	/*----------------------------------------------------------------------------*/
	inline void* GLBuffer::ScopedMapper::Get() {
		return m_memory_ptr;
	}

	////////////////////////////////////////////////////////////////////////////////
	// GLBuffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline GLBuffer::GLBuffer(size_t size, const void* data, GLenum target, GLenum usage)
		: GLBuffer(size, data, target, usage, 0) {
	}
	/*----------------------------------------------------------------------------*/
	inline GLBuffer::~GLBuffer() {
		glDeleteBuffers(1, &m_vbo);
		delete[] m_memory_map;
	}

	/*----------------------------------------------------------------------------*/
	inline const void* GLBuffer::GetPointer(size_t offset) const {
		return BUFFER_OFFSET(offset);
	}
	/*----------------------------------------------------------------------------*/
	inline bool GLBuffer::GetMapExplicitRangeModifyActive() const {
		return (m_map_flags & MAP_EXPLICIT_RANGE_MODIFY) != 0;
	}
	/*----------------------------------------------------------------------------*/
	inline void GLBuffer::SetMapExplicitRangeModifyActive() {
		m_map_flags |= MAP_EXPLICIT_RANGE_MODIFY;
	}
	/*----------------------------------------------------------------------------*/
	inline size_t GLBuffer::GetSize() const {
		return m_size;
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum GLBuffer::GetTarget() const {
		return m_target;
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum GLBuffer::GetUsage() const {
		return m_usage;
	}
	/*----------------------------------------------------------------------------*/
	inline bool GLBuffer::IsBound() const {
		return m_bound;
	}
	/*----------------------------------------------------------------------------*/
	inline bool GLBuffer::IsMapped() const {
		return m_mapped;
	}

} // root namespace