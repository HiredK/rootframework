#include "Shader.h"
#include "Logger.h"

REGISTER_FILE_IMPL(root::Shader, 1, "glsl")

std::vector<root::Shader*> root::Shader::m_stack;

void root::Shader::LinkProgram(const std::string& data)
{
	std::string buffer = static_cast<const char*>(&data[0]);
	std::string separator = "-- ";

	size_t h_len = buffer.find(separator, 0);
	std::string header = buffer.substr(0, h_len - 1);
	size_t start = 0;

	while ((start = buffer.find(separator, start)) != std::string::npos)
	{
		size_t len = separator.length();
		size_t k_pos = start + separator.length();
		size_t k_len = buffer.find("\n", k_pos);
		std::string key = buffer.substr(k_pos, (k_len - k_pos) - 1);
		len += key.length();

		size_t s_pos = k_len;
		size_t s_len = buffer.find(separator, s_pos);
		std::string shd = buffer.substr(s_pos, (s_len - s_pos) - 1);
		len += shd.length();

		int type = 0;
		if (key == "vs")
			type = GL_VERTEX_SHADER;
		if (key == "tcs")
			type = GL_TESS_CONTROL_SHADER;
		if (key == "tes")
			type = GL_TESS_EVALUATION_SHADER;
		if (key == "gs")
			type = GL_GEOMETRY_SHADER;
		if (key == "fs")
			type = GL_FRAGMENT_SHADER;
		if (key == "cs")
			type = GL_COMPUTE_SHADER;

		if (!type) {
			LOG(Logger::Warning) << m_path << ": '" << key << "' not recognized";
			break;
		}

		m_stages[type] = glCreateShader(type);
		std::string processed = header + shd;

		const GLchar* source = (const GLchar*)processed.c_str();
		glShaderSource(m_stages[type], 1, &source, 0);
		glCompileShader(m_stages[type]);

		int status;
		glGetShaderiv(m_stages[type], GL_COMPILE_STATUS, &status);
		if (!status)
		{
			int lenght;
			glGetShaderiv(m_stages[type], GL_INFO_LOG_LENGTH, &lenght);

			char* log = new char[lenght + 1];
			glGetShaderInfoLog(m_stages[type], lenght, 0, log);
			LOG(Logger::Warning) << m_path << ": " << log;
			delete[] log;
			break;
		}

		if (m_id == GL_ZERO) {
			m_id = glCreateProgram();
		}

		glAttachShader(m_id, m_stages[type]);
		start += len;
	}

	if (!m_tf_varyings.empty()) {
		glTransformFeedbackVaryings(m_id, static_cast<GLsizei>(m_tf_varyings.size()), &m_tf_varyings[0], m_tf_mode);
	}

	glLinkProgram(m_id);

	// If the shader was on top of the stack before being linked
	// (usually overridden), bind it back.
	if (!m_stack.empty() && GetPath() == Shader::Get()->GetPath()) {
		glUseProgram(m_id);
	}
}

bool root::Shader::CreateData()
{
	if (m_id == GL_ZERO) {
		bool success = FileHandle::CreateData();
		FileHandle::ScanFileForInclude();

		LOG() << m_path << ": Linking";
		LinkProgram(m_data);
	}

	return true;
}

void root::Shader::OverrideDefine(const std::string& name, const std::string& value)
{
	if (m_id != GL_ZERO)
	{
		Shader::DeleteData();
		bool success = FileHandle::CreateData();
		FileHandle::ScanFileForInclude();

		std::string prefix = "#define " + name;
		size_t start = 0;

		while ((start = m_data.find(prefix, start)) != std::string::npos)
		{
			size_t k_pos = start + prefix.length() + 1;
			size_t k_new = m_data.find("\n", k_pos);

			m_data.replace(k_pos, (k_new - k_pos), value);
			start += (k_new - k_pos);
		}

		LOG() << m_path << ": Overriding";
		LinkProgram(m_data);
	}
}

void root::Shader::OverrideTransformFeedbackVaryings(const std::vector<const char*>& varyings, GLenum mode)
{
	if (m_id != GL_ZERO)
	{
		Shader::DeleteData();
		bool success = FileHandle::CreateData();
		FileHandle::ScanFileForInclude();

		m_tf_varyings = varyings;
		m_tf_mode = mode;

		LOG() << m_path << ": Overriding";
		LinkProgram(m_data);
	}
}

void root::Shader::DeleteData()
{
	if (m_id != GL_ZERO)
	{
		for (auto& stage : m_stages) {
			glDeleteShader(stage.second);
		}

		glDeleteProgram(m_id);
		m_id = GL_ZERO;
		m_stages.clear();

		FileHandle::DeleteData();
	}
}