/**
* @file TaskManager.h
* @brief
*/

#pragma once

#include <thread>
#include "Task.h"

namespace root
{
	class TaskManager
	{
		public:
			//! CTOR/DTOR:
			TaskManager(unsigned int num_threads = 0);
			virtual ~TaskManager();

			//! SERVICES:
			void Add(std::shared_ptr<Task> task);
			void Start();
			void Stop();

			//! ACCESSORS:
			unsigned int GetNumThreads() const;

		protected:
			//! SERVICES:
			void Worker();
			void Execute(std::shared_ptr<Task> task);
			void Synchronize();

		private:
			//! MEMBERS:
			std::vector<std::thread*> m_threads;
			std::condition_variable m_condition;
			mutable std::mutex m_mutex;

			Task::List m_task_list[2];
			Task::List m_background_tasks;
			Task::List m_sync_tasks;

			unsigned int m_num_threads;
			size_t m_num_tasks_to_wait;
			unsigned int m_W_list;
			unsigned int m_R_list;
			bool m_running;
	};

	////////////////////////////////////////////////////////////////////////////////
	// TaskManager inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TaskManager::TaskManager(unsigned int num_threads)
		: m_num_threads((num_threads == 0) ? std::thread::hardware_concurrency() + 1 : num_threads)
		, m_num_tasks_to_wait(0)
		, m_W_list(0)
		, m_R_list(1)
		, m_running(false) {
	}
	/*----------------------------------------------------------------------------*/
	inline TaskManager::~TaskManager()
	{
		Stop();
		for (auto it : m_threads) {
			it->join();
		}

		m_threads.clear();
	}

	/*----------------------------------------------------------------------------*/
	inline unsigned int TaskManager::GetNumThreads() const {
		return m_num_threads;
	}

} // root namespace