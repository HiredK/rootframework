/**
* @file Box2D.h
* @brief Todo
*/

#pragma once

#include "Math.h"

namespace root
{
	class Box2D
	{
		public:
			//! CTOR/DTOR:
			Box2D(dvec2 min = dvec2(-0.5), dvec2 max = dvec2(0.5));
			Box2D(const Box2D& other);
			virtual ~Box2D();

			//! SERVICES:
			Box2D Enlarge(double w) const;
			Box2D Enlarge(const dvec2& point) const;
			Box2D Enlarge(const Box2D& other) const;
			bool Contains(const dvec2& point) const;
			bool Contains(const Box2D& other) const;
			bool Intersects(const Box2D& other) const;
			dvec2 NearestInnerPoint(const dvec2& point) const;

			//! ACCESSORS:
			double GetWidth() const;
			double GetHeight() const;
			double GetArea() const;
			dvec2 GetCenter();

		private:
			//! MEMBERS:
			dvec2 m_min;
			dvec2 m_max;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Box2D inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Box2D::Box2D(dvec2 min, dvec2 max)
		: m_min(min), m_max(max) {
	}
	/*----------------------------------------------------------------------------*/
	inline Box2D::Box2D(const Box2D& other)
		: Box2D(other.m_min, other.m_max) {
	}
	/*----------------------------------------------------------------------------*/
	inline Box2D::~Box2D() {
	}

	/*----------------------------------------------------------------------------*/
	inline Box2D Box2D::Enlarge(double w) const {
		return Box2D(m_min - w, m_max + w);
	}
	/*----------------------------------------------------------------------------*/
	inline Box2D Box2D::Enlarge(const dvec2& point) const {
		return Box2D(glm::min(m_min, point), glm::max(m_max, point));
	}
	/*----------------------------------------------------------------------------*/
	inline Box2D Box2D::Enlarge(const Box2D& other) const {
		return Box2D(glm::min(m_min, other.m_min), glm::max(m_max, other.m_max));
	}
	/*----------------------------------------------------------------------------*/
	inline bool Box2D::Contains(const dvec2& point) const {
		return (
			point.x >= m_min.x &&
			point.x <= m_max.x &&
			point.y >= m_min.y &&
			point.y <= m_max.y
		);
	}
	/*----------------------------------------------------------------------------*/
	inline bool Box2D::Contains(const Box2D& other) const {
		return (
			other.m_min.x >= m_min.x &&
			other.m_max.x <= m_max.x &&
			other.m_min.y >= m_min.y &&
			other.m_max.y <= m_max.y
		);
	}
	/*----------------------------------------------------------------------------*/
	inline bool Box2D::Intersects(const Box2D& other) const {
		return (
			other.m_max.x >= m_min.x &&
			other.m_min.x <= m_max.x &&
			other.m_max.y >= m_min.y &&
			other.m_min.y <= m_max.y
		);
	}
	/*----------------------------------------------------------------------------*/
	inline dvec2 Box2D::NearestInnerPoint(const dvec2& point) const {
		dvec2 nearest = point;
		if (point.x < m_min.x) {
			nearest.x = m_min.x;
		}
		else if (point.x > m_max.x) {
			nearest.x = m_max.x;
		}
		if (point.y < m_min.y) {
			nearest.y = m_min.y;
		}
		else if (point.y > m_max.y) {
			nearest.y = m_max.y;
		}

		return nearest;
	}

	/*----------------------------------------------------------------------------*/
	inline double Box2D::GetWidth() const {
		return m_max.x - m_min.x;
	}
	/*----------------------------------------------------------------------------*/
	inline double Box2D::GetHeight() const {
		return m_max.y - m_min.y;
	}
	/*----------------------------------------------------------------------------*/
	inline double Box2D::GetArea() const {
		return GetWidth() * GetHeight();
	}
	/*----------------------------------------------------------------------------*/
	inline dvec2 Box2D::GetCenter() {
		return dvec2((m_min + m_max) * 0.5);
	}
}