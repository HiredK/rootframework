/**
* @file Engine.h
* @brief Performs operations on contained systems in a specific order.
*/

#pragma once

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include <GL/glew.h>

#include "TaskManager.h"
#include "System.h"

////////////////////////////////////////
// Current Version:

#define ENGINE_VERSION_MAJOR 1
#define ENGINE_VERSION_MINOR 0
#define ENGINE_VERSION_PATCH 0

namespace root
{
	class Engine : public System
	{
		public:
			//! CTOR/DTOR:
			Engine(unsigned int num_threads = 0);
			virtual ~Engine();

			//! SERVICES:
			int Execute(int argc, char** argv);
			void Shutdown();

			//! VIRTUALS:
			bool Init(int argc, char** argv);
			void Update(float dt);
			void Cleanup();

			//! ACCESSORS:
			template<typename T> T* GetSystem();
			TaskManager& GetTaskManager();
			sf::Context& GetContext();
			bool IsRunning() const;

		private:
			//! MEMBERS:
			TaskManager m_task_manager;
			System::List m_systems;
			sf::Context m_context;
			bool m_running;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Engine inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Engine::Engine(unsigned int num_threads)
		: System(this), m_task_manager(num_threads)
		, m_running(false) {

		m_version.major = ENGINE_VERSION_MAJOR;
		m_version.minor = ENGINE_VERSION_MINOR;
		m_version.patch = ENGINE_VERSION_PATCH;
	}
	/*----------------------------------------------------------------------------*/
	inline Engine::~Engine() {
		m_systems.clear();
	}

	/*----------------------------------------------------------------------------*/
	template <typename T> inline T* Engine::GetSystem() {
		for (auto system : m_systems) {
			T* instance = dynamic_cast<T*>(system.get());
			if (instance != nullptr) {
				return instance;
			}
		}

		return nullptr;
	}
	/*----------------------------------------------------------------------------*/
	inline TaskManager& Engine::GetTaskManager() {
		return m_task_manager;
	}
	/*----------------------------------------------------------------------------*/
	inline sf::Context& Engine::GetContext() {
		return m_context;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Engine::IsRunning() const {
		return m_running;
	}

}  // root namespace