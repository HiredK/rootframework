#include "Texture.h"
#include "Texture1D.h"
#include "Texture2D.h"
#include "Texture2DArray.h"
#include "Texture3D.h"
#include "TextureCube.h"
#include "Logger.h"

std::shared_ptr<root::Texture> root::Texture::CreateTexture(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, std::vector<const GLvoid*> data, const Settings& settings)
{
	const unsigned int maximum_size = GetMaxTextureSize();
	const unsigned int valid_size_w = GetValidSize(w);
	const unsigned int valid_size_h = GetValidSize(h);
	std::shared_ptr<root::Texture> texture;

	if (valid_size_w > 0 && valid_size_w <= maximum_size &&
		valid_size_h > 0 && valid_size_h <= maximum_size)
	{
		switch (settings.target)
		{
			case GL_TEXTURE_1D:
				texture = std::shared_ptr<Texture>(new Texture1D(settings, valid_size_w));
				break;
			case GL_TEXTURE_2D:
				texture = std::shared_ptr<Texture>(new Texture2D(settings, valid_size_w, valid_size_h));
				break;
			case GL_TEXTURE_2D_ARRAY:
				texture = std::shared_ptr<Texture>(new Texture2DArray(settings, valid_size_w, valid_size_h, depth));
				break;
			case GL_TEXTURE_3D:
				texture = std::shared_ptr<Texture>(new Texture3D(settings, valid_size_w, valid_size_h, depth));
				break;
			case GL_TEXTURE_CUBE_MAP:
				texture = std::shared_ptr<Texture>(new TextureCube(settings, valid_size_w, valid_size_h));
				break;

			default:
				LOG(Logger::Error) << "Invalid target!";
				return nullptr;
		}

		texture->Bind();
		if (settings.target == GL_TEXTURE_2D_ARRAY && data.size() > 1) {
			for (unsigned int i = 0; i < depth; ++i) {
				texture->Build(settings.iformat, valid_size_w, valid_size_h, depth, border, settings.format, settings.type, data[i], i);
			}
		}
		else {
			texture->Build(settings.iformat, valid_size_w, valid_size_h, depth, border, settings.format, settings.type, data.back());
		}


		glTexParameteri(texture->GetTarget(), GL_TEXTURE_MIN_FILTER, settings.filter_mode);
		glTexParameteri(texture->GetTarget(), GL_TEXTURE_MAG_FILTER, settings.filter_mode);
		glTexParameteri(texture->GetTarget(), GL_TEXTURE_WRAP_S, settings.wrap_mode);
		glTexParameteri(texture->GetTarget(), GL_TEXTURE_WRAP_T, settings.wrap_mode);
		glTexParameteri(texture->GetTarget(), GL_TEXTURE_WRAP_R, settings.wrap_mode);

		if (settings.format == GL_DEPTH_COMPONENT) {
			glTexParameteri(texture->GetTarget(), GL_TEXTURE_COMPARE_MODE, settings.compare_mode);
			glTexParameteri(texture->GetTarget(), GL_TEXTURE_COMPARE_FUNC, settings.compare_func);
		}

		if (settings.anisotropy > 1.0f) {
			glTexParameterf(texture->GetTarget(), GL_TEXTURE_MAX_ANISOTROPY_EXT, settings.anisotropy);
		}

		if (settings.mipmap) {
			glGenerateMipmap(texture->GetTarget());
		}

		Texture::Unbind();
	}
	else {
		LOG(Logger::Error) << "Invalid size!";
		return nullptr;
	}

	return texture;
}

void root::Texture::Unbind()
{
	Texture1D::Unbind();
	Texture2D::Unbind();
	Texture2DArray::Unbind();
	Texture3D::Unbind();
	TextureCube::Unbind();
}

unsigned int root::Texture::GetValidSize(unsigned int size)
{
	if (GLEW_ARB_texture_non_power_of_two) {
		return size;
	}

	unsigned int power_of_two = 1;
	while (power_of_two < size) {
		power_of_two *= 2;
	}

	return power_of_two;
}

unsigned int root::Texture::GetMaxTextureSize()
{
	GLint value;
	glGetIntegerv(GL_MAX_TEXTURE_SIZE, &value);
	return static_cast<unsigned int>(value);
}