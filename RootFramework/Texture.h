/**
* @file Texture.h
* @brief Base class for OpenGL's Texture Objects.
*/

#pragma once

#include "Image.h"
#include <GL/glew.h>

namespace root
{
	class Texture
	{
		public:
			struct Settings
			{
				GLuint target;
				GLuint iformat;
				GLuint format;
				GLuint type;

				GLuint filter_mode;
				GLuint wrap_mode;
				GLuint compare_mode;
				GLuint compare_func;

				float anisotropy;
				bool mipmap;
			};

			//! CTOR/DTOR:
			Texture(const Settings& settings, unsigned int w, unsigned int h, unsigned int depth = 0, unsigned int border = 0);
			virtual ~Texture();

			//! SERVICES:
			GLuint Bind();
			static void Unbind();

			void GenerateMipmap();

			//! Utility method to create default settings
			static Settings GetDefaultSettings();

			//! Factory function to create texture object
			static std::shared_ptr<Texture> CreateTexture(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, std::vector<const GLvoid*> data, const Settings& settings);
			static std::shared_ptr<Texture> CreateTexture(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, const GLvoid* data, const Settings& settings);

			//! Creates new texture object from settings
			static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, const Settings& settings);
			static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth, unsigned int border);
			static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth, const Settings& settings);
			static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth);
			static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, const Settings& settings);
			static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h);

			//! Creates new texture object from image file and settings
			static std::shared_ptr<Texture> FromImage(Image* image, const Settings& settings);
			static std::shared_ptr<Texture> FromImage(Image* image);

			//! ACCESSORS:
			const Settings& GetSettings() const;
			void SetBaseLevel(int level);
			void SetMaxLevel(int level);
			unsigned int GetW() const;
			unsigned int GetH() const;
			GLuint GetID() const;

		protected:
			//! VIRTUALS:
			virtual void Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level = 0) = 0;
			virtual GLenum GetTarget() const = 0;

			//! SERVICES:
			// This function checks whether the graphics
			// driver supports non power of two sizes or
			// not, and adjusts the size accordingly.
			static unsigned int GetValidSize(unsigned int size);

			// Get the maximum Texture size allowed.
			// (GL_MAX_TEXTURE_SIZE).
			static unsigned int GetMaxTextureSize();

			//! MEMBERS:
			const Settings m_settings;
			unsigned int m_w;
			unsigned int m_h;
			GLuint m_id;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture::Texture(const Settings& settings, unsigned int w, unsigned int h, unsigned int depth, unsigned int border)
		: m_settings(settings), m_w(w), m_h(h), m_id(GL_ZERO) {
		glGenTextures(1, &m_id);
	}
	/*----------------------------------------------------------------------------*/
	inline Texture::~Texture() {
		glDeleteTextures(1, &m_id);
	}

	/*----------------------------------------------------------------------------*/
	inline Texture::Settings Texture::GetDefaultSettings()
	{
		Settings settings;

		settings.target = GL_TEXTURE_2D;
		settings.iformat = GL_RGBA8;
		settings.format = GL_RGBA;
		settings.type = GL_UNSIGNED_BYTE;

		settings.filter_mode = GL_NEAREST;
		settings.wrap_mode = GL_CLAMP_TO_EDGE;
		settings.compare_mode = GL_NONE;
		settings.compare_func = GL_LESS;

		settings.anisotropy = 1.0f;
		settings.mipmap = false;

		return settings;
	}

	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::CreateTexture(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, const GLvoid* data, const Settings& settings) {
		return Texture::CreateTexture(w, h, depth, border, std::vector<const GLvoid*>{ data }, settings);
	}

	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromEmpty(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, const Settings& settings) {
		return CreateTexture(w, h, depth, border, nullptr, settings);
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromEmpty(unsigned int w, unsigned int h, unsigned int depth, unsigned int border) {
		return FromEmpty(w, h, depth, border, GetDefaultSettings());
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromEmpty(unsigned int w, unsigned int h, unsigned int depth, const Settings& settings) {
		return CreateTexture(w, h, depth, 0, nullptr, settings);
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromEmpty(unsigned int w, unsigned int h, unsigned int depth) {
		return FromEmpty(w, h, depth, GetDefaultSettings());
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromEmpty(unsigned int w, unsigned int h, const Settings& settings) {
		return FromEmpty(w, h, 0, settings);
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromEmpty(unsigned int w, unsigned int h) {
		return FromEmpty(w, h, GetDefaultSettings());
	}

	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromImage(Image* image, const Settings& settings) {
		return CreateTexture(image->GetW(), image->GetH(), 0, 0, image->GetPixels(), settings);
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Texture> Texture::FromImage(Image* image) {
		return FromImage(image, GetDefaultSettings());
	}

	/*----------------------------------------------------------------------------*/
	inline GLuint Texture::Bind() {
		glBindTexture(GetTarget(), m_id);
		return m_id;
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture::GenerateMipmap() {
		Texture::Bind();
		glGenerateMipmap(GetTarget());
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture::SetBaseLevel(int level) {
		Bind();
		glTexParameteri(GetTarget(), GL_TEXTURE_BASE_LEVEL, level);
	}
	/*----------------------------------------------------------------------------*/
	inline void Texture::SetMaxLevel(int level) {
		Bind();
		glTexParameteri(GetTarget(), GL_TEXTURE_MAX_LEVEL, level);
	}

	/*----------------------------------------------------------------------------*/
	inline const Texture::Settings& Texture::GetSettings() const {
		return m_settings;
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int Texture::GetW() const {
		return m_w;
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int Texture::GetH() const {
		return m_h;
	}
	/*----------------------------------------------------------------------------*/
	inline GLuint Texture::GetID() const {
		return m_id;
	}
}