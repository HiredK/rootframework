/**
* @file Network.h
* @brief Implements UDP network communication.
*/

#pragma once

#include "NetObject.h"
#include <string>

namespace root
{
	class Network : public RakNet::ReplicaManager3
	{
		public:
			friend class NetObject;

			//! TYPEDEF/ENUMS:
			enum Type { CLIENT_TO_SERVER, SERVER_TO_CLIENT, OFFLINE };
			enum {

				//! DEFINE NETWORK MESSAGE_ID:
				ID_REQUEST_OBJECT = ID_USER_PACKET_ENUM + 1,
				ID_USER_PACKET_ENUM
			};

			//! CTOR/DTOR:
			Network(Type type);
			virtual ~Network();

			//! SERVICES:
			bool Connect(const std::string& address, unsigned short port);
			void Send(RakNet::BitStream* bs, PacketPriority priority, PacketReliability reliability);
			void Send(RakNet::BitStream* bs);
			void Receive();
			void Reference(NetObject* object);
			void Shutdown(unsigned int block = 100);

			//! VIRTUALS:
			virtual void OnConnect() = 0;
			virtual void OnReceive(const RakNet::Packet* packet) = 0;
			virtual RakNet::NetworkID Request(RakNet::BitStream* bs) = 0;
			virtual NetObject* OnRequest(RakNet::BitStream* bs) = 0;

			//! RakNet::ReplicaManager3:
			virtual RakNet::Connection_RM3* AllocConnection(const RakNet::SystemAddress& address, RakNet::RakNetGUID guid) const;
			virtual void DeallocConnection(RakNet::Connection_RM3* connection) const;

			//! ACCESSORS:
			NetObject* GetObjectFromID(RakNet::NetworkID network_id);
			void SetAutoSerializeInterval(unsigned int t);
			unsigned int GetAutoSerializeInterval() const;
			bool IsAutoritative() const;

		protected:
			//! RakNet::Connection_RM3:
			struct Connection : public RakNet::Connection_RM3 {
				Connection(const RakNet::SystemAddress& address, RakNet::RakNetGUID guid);
				virtual RakNet::Replica3* AllocReplica(RakNet::BitStream* bs, ReplicaManager3* rm3);
				virtual bool QueryGroupDownloadMessages() const;
			};

			//! (Exposes GetNewNetworkID, not sure why it's protected)
			struct NetworkIDManager : public RakNet::NetworkIDManager {
				virtual RakNet::NetworkID GenerateNetworkID();
			};

			//! MEMBERS:
			RakNet::RakPeerInterface* m_peer;
			RakNet::SocketDescriptor m_sd;
			NetworkIDManager m_network_id_manager;
			Type m_type;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Network::Connection inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Network::Connection::Connection(const RakNet::SystemAddress& address, RakNet::RakNetGUID guid)
		: Connection_RM3(address, guid) {
	}
	/*----------------------------------------------------------------------------*/
	inline RakNet::Replica3* Network::Connection::AllocReplica(RakNet::BitStream* bs, ReplicaManager3* rm3) {
		return dynamic_cast<Network*>(rm3)->OnRequest(bs);
	}
	/*----------------------------------------------------------------------------*/
	inline bool Network::Connection::QueryGroupDownloadMessages() const {
		// Makes all messages between ID_REPLICA_MANAGER_DOWNLOAD_STARTED and
		// ID_REPLICA_MANAGER_DOWNLOAD_COMPLETE arrive in one tick
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Network::NetworkIDManager inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline RakNet::NetworkID Network::NetworkIDManager::GenerateNetworkID() {
		return NetworkIDManager::GetNewNetworkID();
	}

	////////////////////////////////////////////////////////////////////////////////
	// Network inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Network:: Network(Type type)
		: m_type(type) {
		m_peer = RakNet::RakPeerInterface::GetInstance();
		SetNetworkIDManager(&m_network_id_manager);
		m_peer->AttachPlugin(this);
	}
	/*----------------------------------------------------------------------------*/
	inline Network::~Network() {
		m_peer->DetachPlugin(this);
		RakNet::RakPeerInterface::DestroyInstance(m_peer);
	}

	/*----------------------------------------------------------------------------*/
	inline RakNet::Connection_RM3* Network::AllocConnection(const RakNet::SystemAddress& address, RakNet::RakNetGUID guid) const {
		return new Connection(address, guid);
	}
	/*----------------------------------------------------------------------------*/
	inline void Network::DeallocConnection(RakNet::Connection_RM3* connection) const {
		delete connection;
	}

	/*----------------------------------------------------------------------------*/
	inline void Network::Send(RakNet::BitStream* bs, PacketPriority priority, PacketReliability reliability) {
		RakNet::SystemAddress address = m_peer->GetSystemAddressFromIndex(0);
		m_peer->Send(bs, priority, reliability, 0, address, false);
	}
	/*----------------------------------------------------------------------------*/
	inline void Network::Send(RakNet::BitStream* bs) {
		Send(bs, IMMEDIATE_PRIORITY, RELIABLE_ORDERED);
	}

	/*----------------------------------------------------------------------------*/
	inline void Network::Shutdown(unsigned int block) {
		m_peer->Shutdown(block);
	}

	/*----------------------------------------------------------------------------*/
	inline NetObject* Network::GetObjectFromID(RakNet::NetworkID network_id) {
		return m_network_id_manager.GET_OBJECT_FROM_ID<NetObject*>(network_id);
	}
	/*----------------------------------------------------------------------------*/
	inline void Network::SetAutoSerializeInterval(unsigned int t) {
		RakNet::ReplicaManager3::SetAutoSerializeInterval((RakNet::Time)t);
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int Network::GetAutoSerializeInterval() const {
		return unsigned int(RakNet::ReplicaManager3::autoSerializeInterval);
	}
	/*----------------------------------------------------------------------------*/
	inline bool Network::IsAutoritative() const {
		return m_type == SERVER_TO_CLIENT || m_type == OFFLINE;
	}
}