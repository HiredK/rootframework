/**
* @file IOStream.h
* @brief
*/

#include <iostream>
#include <streambuf>
#include <physfs.h>

namespace root
{
	class file_buffer : public std::streambuf {
		public:
			file_buffer(PHYSFS_File * file, size_t size = 2048);
			virtual ~file_buffer();

		protected:
			PHYSFS_File * const file;

		private:
			file_buffer(const file_buffer& other);
			file_buffer& operator=(const file_buffer& other);

			int_type underflow();
			pos_type seekoff(off_type pos, std::ios_base::seekdir dir, std::ios_base::openmode mode);
			pos_type seekpos(pos_type pos, std::ios_base::openmode mode);
			int_type overflow(int_type c = traits_type::eof());
			int sync();

			const size_t size;
			char* buffer;
	};

	class base_fstream {
		public:
			enum Mode { READ, WRITE, APPEND };
			base_fstream(PHYSFS_File * file);
			virtual ~base_fstream();
			size_t length();

		protected:
			PHYSFS_File * const file;
	};

	class ifstream : public base_fstream, public std::istream {
		public:
			ifstream(const std::string& path);
			virtual ~ifstream();
	};

	class ofstream : public base_fstream, public std::ostream {
		public:
			ofstream(const std::string& path, Mode mode = WRITE);
			virtual ~ofstream();
	};

	class fstream : public base_fstream, public std::iostream {
		public:
			fstream(const std::string& path, Mode mode = READ);
			virtual ~fstream();
	};

	////////////////////////////////////////////////////////////////////////////////
	// file_buffer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline file_buffer::file_buffer(PHYSFS_File * file, size_t size)
		: file(file), size(size), buffer(new char[size]) {
		char* end = buffer + size;
		setg(end, end, end);
		setp(buffer, end);
	}

	/*----------------------------------------------------------------------------*/
	inline file_buffer::~file_buffer() {
		sync();
		delete[] buffer;
	}

	/*----------------------------------------------------------------------------*/
	inline std::streambuf::int_type file_buffer::underflow() {
		if (PHYSFS_eof(file)) {
			return traits_type::eof();
		}

		size_t bytes = static_cast<size_t>(PHYSFS_read(file, buffer, 1, PHYSFS_uint32(size)));
		if (bytes < 1) {
			return traits_type::eof();
		}

		setg(buffer, buffer, buffer + bytes);
		return (unsigned char)*gptr();
	}

	/*----------------------------------------------------------------------------*/
	inline std::streambuf::pos_type file_buffer::seekoff(off_type pos, std::ios_base::seekdir dir, std::ios_base::openmode mode) {
		switch (dir) {
			case std::ios_base::beg:
				PHYSFS_seek(file, pos);
				break;
			case std::ios_base::cur:
				PHYSFS_seek(file, (PHYSFS_tell(file) + pos) - (egptr() - gptr()));
				break;
			case std::ios_base::end:
				PHYSFS_seek(file, PHYSFS_fileLength(file) + pos);
				break;
		}

		if (mode & std::ios_base::in) {
			setg(egptr(), egptr(), egptr());
		}
		if (mode & std::ios_base::out) {
			setp(buffer, buffer);
		}

		return PHYSFS_tell(file);
	}

	/*----------------------------------------------------------------------------*/
	inline std::streambuf::pos_type file_buffer::seekpos(pos_type pos, std::ios_base::openmode mode) {
		PHYSFS_seek(file, pos);

		if (mode & std::ios_base::in) {
			setg(egptr(), egptr(), egptr());
		}
		if (mode & std::ios_base::out) {
			setp(buffer, buffer);
		}

		return PHYSFS_tell(file);
	}

	/*----------------------------------------------------------------------------*/
	inline std::streambuf::int_type file_buffer::overflow(int_type c) {
		if (pptr() == pbase() && c == traits_type::eof()) {
			return 0;
		}

		if (PHYSFS_write(file, pbase(), PHYSFS_uint32(pptr() - pbase()), 1) < 1) {
			return traits_type::eof();
		}

		if (c != traits_type::eof()) {
			if (PHYSFS_write(file, &c, 1, 1) < 1) {
				return traits_type::eof();
			}
		}

		return 0;
	}

	/*----------------------------------------------------------------------------*/
	inline int file_buffer::sync() {
		return overflow();
	}

	////////////////////////////////////////////////////////////////////////////////
	// base_fstream inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline base_fstream::base_fstream(PHYSFS_File* file)
		: file(file) {
	}

	/*----------------------------------------------------------------------------*/
	inline base_fstream::~base_fstream() {
		PHYSFS_close(file);
	}

	/*----------------------------------------------------------------------------*/
	inline size_t base_fstream::length() {
		return static_cast<size_t>(PHYSFS_fileLength(file));
	}

	////////////////////////////////////////////////////////////////////////////////
	// OpenWithMode inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline PHYSFS_File* OpenWithMode(char const* path, base_fstream::Mode mode) {
		PHYSFS_File* file = NULL;
		switch (mode) {
			case base_fstream::WRITE:
				file = PHYSFS_openWrite(path);
				break;
			case base_fstream::APPEND:
				file = PHYSFS_openAppend(path);
				break;
			case base_fstream::READ:
				file = PHYSFS_openRead(path);
				break;
		}

		return file;
	}

	////////////////////////////////////////////////////////////////////////////////
	// ifstream inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline ifstream::ifstream(const std::string& path)
		: base_fstream(OpenWithMode(path.c_str(), READ)), std::istream(new file_buffer(file)) {
	}

	/*----------------------------------------------------------------------------*/
	inline ifstream::~ifstream() {
		delete rdbuf();
	}

	////////////////////////////////////////////////////////////////////////////////
	// ofstream inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline ofstream::ofstream(const std::string& path, Mode mode)
		: base_fstream(OpenWithMode(path.c_str(), mode)), std::ostream(new file_buffer(file)) {
	}

	/*----------------------------------------------------------------------------*/
	inline ofstream::~ofstream() {
		delete rdbuf();
	}

	////////////////////////////////////////////////////////////////////////////////
	// fstream inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline fstream::fstream(const std::string& path, Mode mode)
		: base_fstream(OpenWithMode(path.c_str(), mode)), std::iostream(new file_buffer(file)) {
	}

	/*----------------------------------------------------------------------------*/
	inline fstream::~fstream() {
		delete rdbuf();
	}
}