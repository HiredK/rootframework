#include "TileProducer.h"
#include "Terrain.h"
#include "Shader.h"

std::string root::TileProducer::TileData::GetHash(int level, int tx, int ty)
{
	std::stringstream hash;
	size_t h0 = std::hash<int>()(level);
	size_t h1 = std::hash<int>()(tx);
	size_t h2 = std::hash<int>()(ty);
	hash << h0 << h1 << h2;
	return hash.str();
}

root::TileProducer::TileData* root::TileProducer::GetTile(int level, int tx, int ty)
{
	const std::string& hash = TileData::GetHash(level, tx, ty);
	auto tile = m_data.find(hash);
	if (tile == m_data.end())
	{
		glPushAttrib(GL_VIEWPORT_BIT);
		glViewport(0, 0, m_tile_size, m_tile_size);

		m_data[hash] = std::unique_ptr<TileData>(new TileData(level, tx, ty));
		Produce(m_data[hash].get());
		FrameBuffer::Unbind();
		glPopAttrib();
	}

	return m_data[hash].get();
}

void root::TileProducer::SetUniforms(int level, int tx, int ty)
{
	TileData* data = nullptr;
	QuadTree* tt = &m_root;
	QuadTree* tc = nullptr;
	int tl = 0;

	float dx = 0;
	float dy = 0;
	float dd = 1;
	float ds0 = (m_tile_size / 2) * 2.0f - 2.0f * GetBorder();
	float ds = ds0;

	while (tl != level && (tc = tt->children[((tx >> (level - tl - 1)) & 1) | ((ty >> (level - tl - 1)) & 1) << 1]) != nullptr) {
		tl += 1;
		tt = tc;
	}

	while (level > tl) {
		dx += (tx % 2) * dd;
		dy += (ty % 2) * dd;
		dd *= 2;
		ds /= 2;
		level -= 1;
		tx /= 2;
		ty /= 2;
	}

	data = tt->data;
	while (!data) {
		dx += (tx % 2) * dd;
		dy += (ty % 2) * dd;
		dd *= 2;
		ds /= 2;
		level -= 1;
		tx /= 2;
		ty /= 2;

		tt = tt->parent;
		if (tt) {
			data = tt->data;
		}
		else {
			break;
		}
	}

	if (data) {
		dx = dx * ((m_tile_size / 2) * 2 - 2 * GetBorder()) / dd;
		dy = dy * ((m_tile_size / 2) * 2 - 2 * GetBorder()) / dd;

		vec4 coords = vec4((dx + GetBorder()) / m_tile_size, (dy + GetBorder()) / m_tile_size, 0.0f, ds / m_tile_size);
		if (m_tile_size % 2 != 0) {
			coords.x = (dx + GetBorder() + 0.5f) / m_tile_size;
			coords.y = (dy + GetBorder() + 0.5f) / m_tile_size;
		}

		Shader::Get()->Uniform("u_HeightNormal_TileSize", coords.w, coords.w, (m_tile_size / 2) * 2.0f - 2.0f * GetBorder());
		Shader::Get()->Uniform("u_HeightNormal_TileCoords", coords.x, coords.y, coords.z);
		Shader::Get()->Uniform("u_Level", (float)level);

		for (unsigned int i = 0; i < data->textures.size(); ++i) {
			if (!data->textures[i]) {
				continue;
			}

			glActiveTexture(GL_TEXTURE0 + i);
			Shader::Get()->Sampler("s_HeightNormal_Slot" + std::to_string(i), i);
			data->textures[i]->Bind();
		}
	}
}

void root::TileProducer::Refresh()
{
	PutTiles(m_owner.GetRoot(), &m_root);
	GetTiles(m_owner.GetRoot(), &m_root);
}

void root::TileProducer::GetTiles(const TerrainTile* tile, QuadTree* tree, QuadTree* parent_tree)
{
	if (tree->need_tile) {
		tree->data = GetTile(tile->GetLevel(), tile->GetCoord().tx, tile->GetCoord().ty);
		tree->need_tile = false;
		return;
	}

	if (!tile->IsLeaf()) {
		for (unsigned int i = 0; i < 4; ++i) {
			if (!tree->children[i]) {
				tree->children[i] = new QuadTree(tree);
			}

			GetTiles(tile->GetChild(i), tree->children[i], tree);
		}
	}
}

void root::TileProducer::PutTiles(const TerrainTile* tile, QuadTree* tree)
{
}

dvec3 root::TileProducer::GetTileCoords(int level, int tx, int ty) const
{
	double ox = GetOwner().GetSize() * (double(tx) / (1 << level) - 0.5f);
	double oy = GetOwner().GetSize() * (double(ty) / (1 << level) - 0.5f);
	double l = GetOwner().GetSize() / (1 << level) * 2.0;
	return dvec3(ox, oy, l);
}

void root::TileProducer::GetDeformParameters(const dvec3& coords, dvec2& nx, dvec2& ny, dvec2& lx, dvec2& ly)
{
	double x = coords.x + coords.z / 2.0f;
	double y = coords.y + coords.z / 2.0f;
	double R = GetOwner().GetSize() / 2.0f;
	double yR = y*y + R*R;
	double xyR = x*x + yR;
	double d = R * glm::sqrt(xyR);
	double e = R / (glm::sqrt(yR) * xyR);

	nx = dvec2(x*y / d, yR / d);
	ny = dvec2(-((x*x + R*R) / d), -(x*y / d));
	lx = dvec2(e * yR, 0.0);
	ly = dvec2(-(e * x * y), e * d);
}