/**
* @file PhysicsBinding.h
* @brief
*/

#pragma once

#include <btBulletDynamicsCommon.h>
#include <BulletDynamics/Character/btKinematicCharacterController.h>
#include <BulletDynamics/Vehicle/btRaycastVehicle.h>
#include <BulletCollision/CollisionDispatch/btGhostObject.h>
#include "BulletDebugRenderer.h"
#include "ScriptObjectBinding.h"

namespace root
{
	/*----------------------------------------------------------------------------*/
	inline const btVector3 TobtVector3(const dvec3& p) {
		return btVector3(p.x, p.y, p.z);
	}

	////////////////////////////////////////////////////////////////////////////////
	// DynamicWorldBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct DynamicWorld : ScriptObject_wrapper
	{
		DynamicWorld()
			: ScriptObject_wrapper("dynamic_world", ScriptObject::Dynamic)
		{
			m_broadphase = std::unique_ptr<btBroadphaseInterface>(new bt32BitAxisSweep3(
				btVector3(-1000, -1000, -1000), btVector3(1000, 1000, 1000)));

			m_broadphase->getOverlappingPairCache()->setInternalGhostPairCallback(new btGhostPairCallback());
			btDefaultCollisionConfiguration* configuration = new btDefaultCollisionConfiguration();

			m_dispatcher = std::unique_ptr<btCollisionDispatcher>(new btCollisionDispatcher(configuration));
			m_solver = std::unique_ptr<btSequentialImpulseConstraintSolver>(new btSequentialImpulseConstraintSolver());
			m_instance = std::unique_ptr<btDiscreteDynamicsWorld>(new btDiscreteDynamicsWorld(
				m_dispatcher.get(), m_broadphase.get(), m_solver.get(), configuration));

			m_instance->getDispatchInfo().m_allowedCcdPenetration = 0.0001;
			m_instance->setGravity(btVector3(0, -10, 0));

			m_debug_renderer = std::unique_ptr<BulletDebugRenderer>(new BulletDebugRenderer());
			m_instance->setDebugDrawer(m_debug_renderer.get());
		}

		virtual ~DynamicWorld()
		{
			for (int i = m_instance->getNumConstraints()-1; i >= 0; i--) {
				m_instance->removeConstraint(m_instance->getConstraint(i));
			}

			for (auto& collider : m_terrain_colliders) {
				m_instance->removeRigidBody(collider.second.get());
			}

			for (int i = m_instance->getNumCollisionObjects() - 1; i >= 0; i--) {
				btCollisionObject* obj = m_instance->getCollisionObjectArray()[i];
				btRigidBody* body = btRigidBody::upcast(obj);
				if (body && body->getMotionState()) {
					delete body->getMotionState();
				}

				m_instance->removeCollisionObject(obj);
				delete obj;
			}
		}

		void Update(float dt)
		{
			m_instance->stepSimulation(dt);
		}

		void Draw(Camera* camera)
		{
			Shader::Get()->UniformMat4("u_ViewProjMatrix", camera->GetViewProjMatrix());
			Shader::Get()->Uniform("u_RenderingType", 4);
			Shader::Get()->Uniform("u_Color", 1.0, 0.0, 0.0, 1.0);
			m_instance->debugDrawWorld();
		}

		btCollisionWorld::ClosestRayResultCallback RayTest(const dvec3& origin, const dvec3& direction)
		{
			// Todo Win32 bs
			//auto cb = new(btAlignedAlloc(sizeof(btHashedOverlappingPairCache), 16)) btCollisionWorld::ClosestRayResultCallback(TobtVector3(origin), TobtVector3(direction));
			//btAlignedFree(cb);

			btCollisionWorld::ClosestRayResultCallback cb(TobtVector3(origin), TobtVector3(direction));
			m_instance->rayTest(TobtVector3(origin), TobtVector3(direction), cb);
			return cb;
		}

		void AddTerrainCollider(TileProducer* producer, const TerrainTile& tile)
		{
			const std::string& hash = TileProducer::TileData::GetHash(tile.GetLevel(), tile.GetCoord().tx, tile.GetCoord().ty);
			auto search = m_terrain_colliders.find(hash);
			if (search == m_terrain_colliders.end())
			{
				TileProducer::TileData* data = producer->GetTile(tile.GetLevel(), tile.GetCoord().tx, tile.GetCoord().ty);
				if (data == nullptr) {
					LOG(Logger::Warning) << "DynamicWorld::AddTerrainCollider() - Out of sync!";
					return;
				}

				const int TILE_SIZE = producer->GetTileSize();
				const int GRID_SIZE = TILE_SIZE - (producer->GetBorder() * 2);
				const int totalVerts = GRID_SIZE * GRID_SIZE;

				Texture* height_tex = data->textures[0].get();
				height_tex->Bind();

				float* height = new float[TILE_SIZE * TILE_SIZE * 2];
				glGetTexImage(GL_TEXTURE_2D, 0, GL_RG, GL_FLOAT, height);

				btVector3* gVertices = new btVector3[totalVerts];

				for (int i = 0; i < GRID_SIZE; ++i)
				for (int j = 0; j < GRID_SIZE; ++j)
				{
					dvec2 offset((double)i / (double)(GRID_SIZE - 1), (double)j / (double)(GRID_SIZE - 1));
					double h = (double)height[((i + producer->GetBorder()) + (j + producer->GetBorder()) * TILE_SIZE) * 2];

					dvec3 P = dvec3(offset * tile.GetCoord().length + dvec2(tile.GetCoord().ox, tile.GetCoord().oy), h);
					P = dmat3(tile.GetOwner().GetFaceMatrix()) * tile.GetOwner().GetDeformation()->LocalToDeformed(P);
					P.y = P.y - 6360000.0;

					gVertices[i + j * GRID_SIZE].setValue(P.x, P.y, P.z);
				}

				btTriangleMesh* trimesh = new btTriangleMesh();

				for (int i = 0; i < GRID_SIZE - 1; ++i)
				for (int j = 0; j < GRID_SIZE - 1; ++j)
				{
					trimesh->addTriangle(
						gVertices[j * GRID_SIZE + i],
						gVertices[j * GRID_SIZE + i + 1],
						gVertices[(j + 1)*GRID_SIZE + i + 1]
					);

					trimesh->addTriangle(
						gVertices[j * GRID_SIZE + i],
						gVertices[(j + 1)*GRID_SIZE + i + 1],
						gVertices[(j + 1)*GRID_SIZE + i]
					);
				}

				delete[] gVertices;
				delete[] height;

				btCollisionShape* trimeshShape = new btBvhTriangleMeshShape(trimesh, false);
				btDefaultMotionState* ms = new btDefaultMotionState(btTransform(btQuaternion(0, 0, 0, 1), btVector3(0, 0, 0)));
				btRigidBody::btRigidBodyConstructionInfo rbCI(0.0, ms, trimeshShape);

				m_terrain_colliders[hash] = std::unique_ptr<btRigidBody>(new btRigidBody(rbCI));
				m_terrain_colliders[hash]->setCollisionFlags(m_terrain_colliders[hash]->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
				m_instance->addRigidBody(m_terrain_colliders[hash].get());
			}
		}

		std::unique_ptr<btBroadphaseInterface> m_broadphase;
		std::unique_ptr<btCollisionDispatcher> m_dispatcher;
		std::unique_ptr<btSequentialImpulseConstraintSolver> m_solver;
		std::unique_ptr<btDiscreteDynamicsWorld> m_instance;
		std::unique_ptr<BulletDebugRenderer> m_debug_renderer;

		std::map<std::string, std::unique_ptr<btRigidBody>> m_terrain_colliders;
	};

	/*----------------------------------------------------------------------------*/
	inline btRigidBody* GetRayResultBody(const btCollisionWorld::RayResultCallback& self) {
		return (btRigidBody*)btRigidBody::upcast(self.m_collisionObject);
	}
	/*----------------------------------------------------------------------------*/
	inline dvec3 GetRayResultHitNormalWorld(const btCollisionWorld::ClosestRayResultCallback& self) {
		return dvec3(self.m_hitNormalWorld.x(), self.m_hitNormalWorld.y(), self.m_hitNormalWorld.z());
	}
	/*----------------------------------------------------------------------------*/
	inline dvec3 GetRayResultHitPointWorld(const btCollisionWorld::ClosestRayResultCallback& self) {
		return dvec3(self.m_hitPointWorld.x(), self.m_hitPointWorld.y(), self.m_hitPointWorld.z());
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope DynamicWorldBinding()
	{
		return (
			luabind::class_<DynamicWorld, ScriptObject, std::shared_ptr<DynamicWorld>>("DynamicWorld")
			.scope
			[
				luabind::class_<btCollisionWorld::RayResultCallback>("RayResultCallback")
				.property("body", &GetRayResultBody)
				.def("has_hit", &btCollisionWorld::RayResultCallback::hasHit),

				luabind::class_<btCollisionWorld::ClosestRayResultCallback, btCollisionWorld::RayResultCallback>("ClosestRayResultCallback")
				.property("hit_normal", &GetRayResultHitNormalWorld)
				.property("hit_point", &GetRayResultHitPointWorld)
			]
			.def(luabind::constructor<>())
			.def("add_terrain_collider", &DynamicWorld::AddTerrainCollider)
			.def("ray_test", &DynamicWorld::RayTest)
			.def("draw", &DynamicWorld::Draw)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CharacterControllerBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct CharacterController : Transform, ScriptObject_wrapper
	{
		CharacterController(const DynamicWorld& owner, float radius, float height)
			: ScriptObject_wrapper("character_controller", ScriptObject::Dynamic)
			, Transform(), m_owner(owner)
		{
			m_ghost = std::unique_ptr<btPairCachingGhostObject>(new btPairCachingGhostObject());
			btConvexShape* capsule_shape = new btCapsuleShapeZ(radius, height);
			m_ghost->setCollisionShape(capsule_shape);
			m_ghost->setCollisionFlags(btCollisionObject::CF_CHARACTER_OBJECT);

			m_controller = std::unique_ptr<btKinematicCharacterController>(
				new btKinematicCharacterController(m_ghost.get(), capsule_shape, 0.35, btVector3(0, 1, 0)));

			m_owner.m_instance->addCollisionObject(m_ghost.get(),
				btBroadphaseProxy::CharacterFilter,
				btBroadphaseProxy::StaticFilter | btBroadphaseProxy::DefaultFilter);

			m_owner.m_instance->addAction(m_controller.get());
			m_owner.m_instance->getBroadphase()->getOverlappingPairCache()->cleanProxyFromPairs(
				m_ghost->getBroadphaseHandle(), m_owner.m_dispatcher.get());

			m_ghost->setCollisionFlags(m_ghost->getCollisionFlags() | btCollisionObject::CF_DISABLE_VISUALIZE_OBJECT);
		}

		virtual ~CharacterController()
		{
			m_owner.m_instance->removeAction(m_controller.get());
			m_owner.m_instance->removeCollisionObject(m_ghost.get());
		}

		void Update(float dt)
		{
			m_position.x = m_ghost->getWorldTransform().getOrigin().x();
			m_position.y = m_ghost->getWorldTransform().getOrigin().y();
			m_position.z = m_ghost->getWorldTransform().getOrigin().z();
		}

		void SetWalkDirection(const dvec3& direction)
		{
			m_controller->setWalkDirection(btVector3(direction.x, 0, direction.z));
		}

		void Jump(double speed)
		{
			if (m_controller->canJump()) {
				m_controller->setJumpSpeed(speed);
				m_controller->jump();
			}
		}

		void Wrap(const dvec3& origin)
		{
			m_controller->warp(btVector3(origin.x, origin.y, origin.z));
		}

		std::unique_ptr<btKinematicCharacterController> m_controller;
		std::unique_ptr<btPairCachingGhostObject> m_ghost;
		const DynamicWorld& m_owner;
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope CharacterControllerBinding()
	{
		return (
			luabind::class_<CharacterController, Transform, std::shared_ptr<CharacterController>>("CharacterController")
			.def(luabind::constructor<const DynamicWorld&, float, float>())
			.def("set_walk_direction", &CharacterController::SetWalkDirection)
			.def("jump", &CharacterController::Jump)
			.def("wrap", &CharacterController::Wrap)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// VehiculeControllerBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct VehiculeController : ScriptObject_wrapper
	{
		struct Settings
		{
			float chassis_half_extent;
			float wheel_width;
			float wheel_radius;
			float wheel_connection_height;
			float suspension_rest_length;

			float suspension_stiffness;
			float suspension_damping;
			float suspension_compression;
			float wheel_friction;
			float roll_influence;

			float max_engine_force;
		};

		static Settings GetDefaultSettings()
		{
			Settings settings;

			settings.chassis_half_extent = 1.0f;
			settings.wheel_width = 0.4f;
			settings.wheel_radius = 0.5f;
			settings.wheel_connection_height = -0.4f;
			settings.suspension_rest_length = 0.6f;

			settings.suspension_stiffness = 14.0f;
			settings.suspension_damping = 2.0f;
			settings.suspension_compression = 4.0f;
			settings.wheel_friction = 1000.0f;
			settings.roll_influence = 0.01f;

			settings.max_engine_force = 4500.0f;

			return settings;
		}

		VehiculeController(const DynamicWorld& owner, btRigidBody& body, const Settings& settings)
			: ScriptObject_wrapper("vehicule_controller", ScriptObject::Dynamic)
			, m_owner(owner), m_settings(settings)
			, m_desired_steering(0.0f)
			, m_current_steering(0.0f)
			, m_accelerator(0.0f)
		{
			m_raycaster = std::unique_ptr<btVehicleRaycaster>(new btDefaultVehicleRaycaster(owner.m_instance.get()));
			m_controller = std::unique_ptr<btRaycastVehicle>(new btRaycastVehicle(m_tuning, &body, m_raycaster.get()));
			m_owner.m_instance->addVehicle(m_controller.get());

			btVector3 wcp0;
			wcp0.setX( settings.chassis_half_extent - (settings.wheel_width * 0.3f));
			wcp0.setY( settings.wheel_connection_height);
			wcp0.setZ( settings.chassis_half_extent *  2.0f - settings.wheel_radius);

			btVector3 wcp1;
			wcp1.setX(-settings.chassis_half_extent + (settings.wheel_width * 0.3f));
			wcp1.setY( settings.wheel_connection_height);
			wcp1.setZ( settings.chassis_half_extent *  2.0f - settings.wheel_radius);

			btVector3 wcp2;
			wcp2.setX(-settings.chassis_half_extent + (settings.wheel_width * 0.3f));
			wcp2.setY( settings.wheel_connection_height);
			wcp2.setZ( settings.chassis_half_extent * -2.0f + settings.wheel_radius);

			btVector3 wcp3;
			wcp3.setX( settings.chassis_half_extent - (settings.wheel_width * 0.3f));
			wcp3.setY( settings.wheel_connection_height);
			wcp3.setZ( settings.chassis_half_extent * -2.0f + settings.wheel_radius);

			m_controller->addWheel(wcp0, btVector3(0,-1, 0), btVector3(-1, 0, 0), settings.suspension_rest_length, settings.wheel_radius, m_tuning, true);
			m_controller->addWheel(wcp1, btVector3(0,-1, 0), btVector3(-1, 0, 0), settings.suspension_rest_length, settings.wheel_radius, m_tuning, true);
			m_controller->addWheel(wcp2, btVector3(0,-1, 0), btVector3(-1, 0, 0), settings.suspension_rest_length, settings.wheel_radius, m_tuning, false);
			m_controller->addWheel(wcp3, btVector3(0,-1, 0), btVector3(-1, 0, 0), settings.suspension_rest_length, settings.wheel_radius, m_tuning, false);

			for (int i = 0; i < m_controller->getNumWheels(); ++i)
			{
				btWheelInfo& wi = m_controller->getWheelInfo(i);

				wi.m_suspensionStiffness = settings.suspension_stiffness;
				wi.m_wheelsDampingRelaxation = settings.suspension_damping;
				wi.m_wheelsDampingCompression = settings.suspension_compression;
				wi.m_frictionSlip = settings.wheel_friction;
				wi.m_rollInfluence = settings.roll_influence;
			}

			m_controller->setCoordinateSystem(0, 1, 2);
			m_controller->resetSuspension();
			body.setDamping(0.2f, 0.5f);
		}

		VehiculeController(const DynamicWorld& owner, btRigidBody& body)
			: VehiculeController(owner, body, GetDefaultSettings()) {
		}

		virtual ~VehiculeController() {
			m_owner.m_instance->removeVehicle(m_controller.get());
		}

		void SetSteering(float value) {
			m_desired_steering = glm::clamp(value, -1.0f, 1.0f);
		}

		void SetAccelerator(float value) {
			m_accelerator = glm::clamp(value, -1.0f, 1.0f);
		}

		void Update(float dt)
		{
			if (m_desired_steering != 0.0f) {
				m_current_steering = glm::clamp(m_current_steering + (m_desired_steering * 0.012f), -1.0f, 1.0f);
			}
			else {
				m_current_steering = (glm::abs(m_current_steering) > 0.001f) ? m_current_steering * 0.95f : 0.0f;
			}

			float f = m_settings.max_engine_force * m_accelerator;
			if (m_desired_steering != 0.0f || f != 0.0f) {
				m_controller->getRigidBody()->activate(true);
			}

			m_controller->setSteeringValue(m_current_steering, 0);
			m_controller->setSteeringValue(m_current_steering, 1);
			for (int i = 0; i < 2; ++i) { // 2x wheel drive
				m_controller->applyEngineForce(f, i);
			}
		}

		std::unique_ptr<btVehicleRaycaster> m_raycaster;
		std::unique_ptr<btRaycastVehicle> m_controller;
		btRaycastVehicle::btVehicleTuning m_tuning;
		const DynamicWorld& m_owner;

		const Settings m_settings;
		float m_desired_steering;
		float m_current_steering;
		float m_accelerator;
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope VehiculeControllerBinding()
	{
		return (
			luabind::class_<VehiculeController, ScriptObject, std::shared_ptr<VehiculeController>>("VehiculeController")
			.def(luabind::constructor<const DynamicWorld&, btRigidBody&>())
			.def("set_steering", &VehiculeController::SetSteering)
			.def("set_accelerator", &VehiculeController::SetAccelerator)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// RigidBodyBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct RigidBody : btRigidBody, Transform
	{
		RigidBody(const DynamicWorld& owner, const Transform& trans, const btRigidBody::btRigidBodyConstructionInfo& info)
			: btRigidBody(info), Transform(trans), m_owner(owner)
		{
			m_owner.m_instance->addRigidBody(this);
			m_owner.m_instance->setInternalTickCallback(OnTickCallback, 0, true);
			GetReferences().push_back(this);
		}

		virtual ~RigidBody()
		{
			m_owner.m_instance->removeRigidBody(this);
			auto it = GetReferences().begin();
			while (it != GetReferences().end())
			{
				if ((*it) == this) {
					GetReferences().erase(it);
					break;
				}

				++it;
			}
		}

		static std::vector<RigidBody*>& GetReferences()
		{
			static std::vector<RigidBody*> references;
			return references;
		}

		static void OnTickCallback(btDynamicsWorld* world, btScalar timeStep)
		{
			for (auto& body : GetReferences())
			{
				body->m_position.x = body->getWorldTransform().getOrigin().x();
				body->m_position.y = body->getWorldTransform().getOrigin().y();
				body->m_position.z = body->getWorldTransform().getOrigin().z();

				body->m_rotation.x = body->getWorldTransform().getRotation().x();
				body->m_rotation.y = body->getWorldTransform().getRotation().y();
				body->m_rotation.z = body->getWorldTransform().getRotation().z();
				body->m_rotation.w = body->getWorldTransform().getRotation().w();
			}
		}

		void Activate() {
			btRigidBody::activate(false);
		}

		const DynamicWorld& m_owner;
	};

	/*----------------------------------------------------------------------------*/
	struct RigidBody_wrapper : RigidBody, ScriptObject_wrapper
	{
		static std::shared_ptr<RigidBody> CreateRigidBody(const DynamicWorld& owner, const Transform& trans, btCollisionShape* shape, double mass)
		{
			btTransform startTransform;
			startTransform.setIdentity();
			startTransform.setOrigin(TobtVector3(trans.GetPosition()));
			startTransform.setRotation(btQuaternion(trans.GetRotation().x, trans.GetRotation().y, trans.GetRotation().z, trans.GetRotation().w));
			shape->setLocalScaling(TobtVector3(trans.GetScale()));

			btVector3 localInertia(0, 0, 0);
			if (mass != 0.0) {
				shape->calculateLocalInertia(mass, localInertia);
			}

			btDefaultMotionState* ms = new btDefaultMotionState(startTransform);
			btRigidBody::btRigidBodyConstructionInfo cInfo(mass, ms, shape, localInertia);
			return std::shared_ptr<RigidBody>(new RigidBody(owner, trans, cInfo));
		}

		static std::shared_ptr<RigidBody> CreateBox(const DynamicWorld& owner, const Transform& trans, const dvec3& extents, double mass) {
			return CreateRigidBody(owner, trans, new btBoxShape(TobtVector3(extents)), mass);
		}
		static std::shared_ptr<RigidBody> CreateSphere(const DynamicWorld& owner, const Transform& trans, double radius, double mass) {
			return CreateRigidBody(owner, trans, new btSphereShape(radius), mass);
		}
		static std::shared_ptr<RigidBody> CreateCylinder(const DynamicWorld& owner, const Transform& trans, double radius, double height, double mass) {
			return CreateRigidBody(owner, trans, new btCylinderShape(btVector3(radius, height * 0.5, radius)), mass);
		}
		static std::shared_ptr<RigidBody> CreateCone(const DynamicWorld& owner, const Transform& trans, double radius, double height, double mass) {
			return CreateRigidBody(owner, trans, new btConeShape(radius, height), mass);
		}
		static std::shared_ptr<RigidBody> CreateCapsule(const DynamicWorld& owner, const Transform& trans, double radius, double length, double mass) {
			return CreateRigidBody(owner, trans, new btCapsuleShape(radius, length), mass);
		}
		static std::shared_ptr<RigidBody> CreateCollider(const DynamicWorld& owner, const Transform& trans, Model* source, double mass) {
			return CreateRigidBody(owner, trans, source->BuildCollider(), mass);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline dmat4 GetCenterOfMass(const RigidBody& self) {
		btScalar mat[16];
		self.getCenterOfMassTransform().getOpenGLMatrix(mat);
		return glm::make_mat4(mat);
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope RigidBodyBinding()
	{
		return (
			luabind::class_<RigidBody_wrapper, Transform, std::shared_ptr<RigidBody_wrapper>>("RigidBody")
			.scope
			[
				luabind::def("create_box", (std::shared_ptr<RigidBody>(*)(const DynamicWorld&, const Transform&, const dvec3&, double)) &RigidBody_wrapper::CreateBox),
				luabind::def("create_sphere", (std::shared_ptr<RigidBody>(*)(const DynamicWorld&, const Transform&, double, double)) &RigidBody_wrapper::CreateSphere),
				luabind::def("create_cylinder", (std::shared_ptr<RigidBody>(*)(const DynamicWorld&, const Transform&, double, double, double)) &RigidBody_wrapper::CreateCylinder),
				luabind::def("create_cone", (std::shared_ptr<RigidBody>(*)(const DynamicWorld&, const Transform&, double, double, double)) &RigidBody_wrapper::CreateCone),
				luabind::def("create_capsule", (std::shared_ptr<RigidBody>(*)(const DynamicWorld&, const Transform&, double, double, double)) &RigidBody_wrapper::CreateCapsule),
				luabind::def("create_collider", (std::shared_ptr<RigidBody>(*)(const DynamicWorld&, const Transform&, Model*, double)) &RigidBody_wrapper::CreateCollider)
			]

			.enum_("ActivationState")
			[
				luabind::value("ACTIVE_TAG", ACTIVE_TAG),
				luabind::value("ISLAND_SLEEPING", ISLAND_SLEEPING),
				luabind::value("WANTS_DEACTIVATION", WANTS_DEACTIVATION),
				luabind::value("DISABLE_DEACTIVATION", DISABLE_DEACTIVATION),
				luabind::value("DISABLE_SIMULATION", DISABLE_SIMULATION)
			]

			.property("center_of_mass", &GetCenterOfMass)
			.def("set_activation_state", &btRigidBody::setActivationState)
			.def("is_static_object", &btRigidBody::isStaticObject)
			.def("activate", &RigidBody::Activate),

			luabind::class_<RigidBody, RigidBody_wrapper>("RigidBody_impl__"),
			luabind::class_<btRigidBody, RigidBody>("btRigidBody_impl__") // hughh
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Point2PointConstraintBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Point2PointConstraint_wrapper : btPoint2PointConstraint, ScriptObject_wrapper
	{
		Point2PointConstraint_wrapper(const DynamicWorld& owner, btRigidBody& body, const dvec3& pivot)
			: ScriptObject_wrapper("point_2_point_constraint", ScriptObject::Static)
			, btPoint2PointConstraint(body, TobtVector3(pivot))
			, m_owner(owner)
			, m_removed(false)
		{
			m_owner.m_instance->addConstraint(this, true);
			m_setting.m_impulseClamp = 30.0;
			m_setting.m_tau = 0.001;
		}

		virtual ~Point2PointConstraint_wrapper() {
			if (!m_removed) {
				m_owner.m_instance->removeConstraint(this);
			}
		}

		void SetPivotA(const dvec3& pivot) {
			btPoint2PointConstraint::setPivotA(TobtVector3(pivot));
		}
		void SetPivotB(const dvec3& pivot) {
			btPoint2PointConstraint::setPivotB(TobtVector3(pivot));
		}

		void Remove() {
			m_owner.m_instance->removeConstraint(this);
			m_removed = true;
		}

		const DynamicWorld& m_owner;
		bool m_removed;
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope Point2PointConstraintBinding()
	{
		return (
			luabind::class_<Point2PointConstraint_wrapper, ScriptObject, std::shared_ptr<Point2PointConstraint_wrapper>>("Point2PointConstraint")
			.def(luabind::constructor<const DynamicWorld&, btRigidBody&, const dvec3&>())
			.def("set_pivot_a", &Point2PointConstraint_wrapper::SetPivotA)
			.def("set_pivot_b", &Point2PointConstraint_wrapper::SetPivotB)
			.def("remove", &Point2PointConstraint_wrapper::Remove),

			luabind::class_<btPoint2PointConstraint, Point2PointConstraint_wrapper>("Point2PointConstraint_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// PhysicsBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope PhysicsBinding()
	{
		return (
			luabind::namespace_("bt")
			[
				DynamicWorldBinding(),
				CharacterControllerBinding(),
				VehiculeControllerBinding(),
				RigidBodyBinding(),
				Point2PointConstraintBinding()
			]
		);
	}
}