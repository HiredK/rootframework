/**
* @file Graph.h
* @brief A Graph contains vectorial data representing areas, roads, rivers...
*/

#pragma once

#include "Math.h"
#include <memory>
#include <vector>
#include <set>
#include <map>

namespace root
{
	class Graph
	{
		public:
			struct Node;
			struct Curve;
			struct Area;

			//! TYPEDEF/ENUMS:
			typedef union NodeID {
				unsigned int id;
				Node* reference;
			};

			typedef union CurveID {
				unsigned int id;
				Curve* reference;
			};

			// A Node is described by it's XY coordinates.
			struct Node
			{
				//! TYPEDEF/ENUMS:
				// An abstract iterator to iterate over the elements of a Graph.
				struct Iterator {
					typedef std::map<NodeID, std::shared_ptr<Node>> T;
					T::const_iterator iterator;
					T::const_iterator end;

					//! CTOR/DTOR:
					Iterator(const T& nodes);

					//! SERVICES:
					bool HasNext();
					Node* Next();
				};

				//! CTOR/DTOR:
				Node(const Graph& owner, const dvec2& coord);
				virtual ~Node();

				//! SERVICES:
				void AddCurve(CurveID c);
				void RemoveCurve(CurveID c);

				//! ACCESSORS:
				int GetCurveCount() const;
				int GetCurveCount(const std::set<CurveID>& included) const;
				Curve* GetCurve(int i);
				NodeID GetID() const;

				//! MEMBERS:
				const Graph& owner;
				dvec2 coord;
				std::vector<CurveID> curves;
			};

			// A Curve is made of 2 nodes (start and end points) and a set of vertices.
			struct Curve
			{
				//! TYPEDEF/ENUMS:
				// Specialized iterator for curves, since a multimap is used here.
				struct Iterator {
					typedef std::multimap<CurveID, std::shared_ptr<Curve>> T;
					T::const_iterator iterator;
					T::const_iterator end;

					//! CTOR/DTOR:
					Iterator(const T& curves);

					//! SERVICES:
					bool HasNext();
					Curve* Next();
				};

				// Represents a vertex inside a curve.
				struct Vertex : dvec2
				{
					//! CTOR/DTOR:
					Vertex(double x, double y, float s, bool control);
					Vertex(const dvec2& point, float s, bool control);
					Vertex(const Vertex& other);

					//! MEMBERS:
					float s, l;
					bool control;
				};

				//! CTOR/DTOR:
				Curve(const Graph& owner, Curve* model, Node* s, Node* e);
				virtual ~Curve();

				//! SERVICES:
				dvec2 GetXY(int i) const;
				float GetS(int i) const;
				Box2D GetBounds() const;

				//! ACCESSORS:
				Curve* GetAncestor() const;
				CurveID GetID() const;
				int GetSize() const;

				//! MEMBERS:
				const Graph& owner;
				Curve* parent;
				float width;
				float s0;
				float s1;
				float length;
				Node* start;
				Node* end;
				std::vector<Vertex> vertices;
			};

			//! CTOR/DTOR:
			Graph();
			virtual ~Graph();

			//! SERVICES:
			Curve* AddCurve(const dvec2& s, const dvec2& e);
			Node* NewNode(const dvec2& coord);
			Curve* NewCurve(Curve* model, Node* s, Node* e);

			//! ACCESSORS:
			Node::Iterator GetNodes();
			Curve::Iterator GetCurves();

			//! MEMBERS:
			std::map<NodeID, std::shared_ptr<Node>> m_nodes;
			std::multimap<CurveID, std::shared_ptr<Curve>> m_curves;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Graph::Node::Iterator inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Graph::Node::Iterator::Iterator(const T& nodes)
		: iterator(nodes.begin()), end(nodes.end()) {
	}
	/*----------------------------------------------------------------------------*/
	inline bool Graph::Node::Iterator::HasNext() {
		return iterator != end;
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Node* Graph::Node::Iterator::Next() {
		return (iterator++)->second.get();
	}

	////////////////////////////////////////////////////////////////////////////////
	// Graph::Node inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Graph::Node:: Node(const Graph& owner, const dvec2& coord)
		: owner(owner), coord(coord) {
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Node::~Node() {
		curves.clear();
	}

	/*----------------------------------------------------------------------------*/
	inline void Graph::Node::AddCurve(CurveID c) {
		if (std::find(curves.begin(), curves.end(), c) == curves.end()) {
			curves.push_back(c);
		}
	}
	/*----------------------------------------------------------------------------*/
	inline void Graph::Node::RemoveCurve(CurveID c) {
		auto it = std::find(curves.begin(), curves.end(), c);
		if (it != curves.end()) {
			curves.erase(it);
		}
	}

	/*----------------------------------------------------------------------------*/
	inline int Graph::Node::GetCurveCount() const {
		return static_cast<int>(curves.size());
	}
	/*----------------------------------------------------------------------------*/
	inline int Graph::Node::GetCurveCount(const std::set<CurveID>& included) const {
		int total = 0;
		for (int i = 0; i < GetCurveCount(); ++i) {
			if (included.find(curves[i]) != included.end()) {
				++total;
			}
		}

		return total;
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve* Graph::Node::GetCurve(int i) {
		return curves[i].reference;
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::NodeID Graph::Node::GetID() const {
		NodeID id;
		id.reference = const_cast<Node*>(this);
		return id;
	}

	/*----------------------------------------------------------------------------*/
	inline bool operator <(const Graph::NodeID& u, const Graph::NodeID& v) {
		return u.id < v.id;
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator ==(const Graph::NodeID& u, const Graph::NodeID& v) {
		return u.id == v.id;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Graph::Curve::Iterator inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve::Iterator::Iterator(const T& curves)
		: iterator(curves.begin()), end(curves.end()) {
	}
	/*----------------------------------------------------------------------------*/
	inline bool Graph::Curve::Iterator::HasNext() {
		return iterator != end;
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve* Graph::Curve::Iterator::Next() {
		return (iterator++)->second.get();
	}

	////////////////////////////////////////////////////////////////////////////////
	// Graph::Node::Vertex inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve::Vertex::Vertex(double x, double y, float s, bool control)
		: dvec2(x, y), s(s), l(-1), control(control) {
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve::Vertex::Vertex(const dvec2& point, float s, bool control)
		: Vertex(point.x, point.y, s, control) {
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve::Vertex::Vertex(const Vertex& other)
		: Vertex(other.x, other.y, other.s, other.control) {
	}

	////////////////////////////////////////////////////////////////////////////////
	// Graph::Curve inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve:: Curve(const Graph& owner, Curve* model, Node* s, Node* e)
		: owner(owner), parent(nullptr), length(-1), start(s), end(e)
		, width(0.0f), s0(0.0f), s1(1.0f) {
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve::~Curve() {
	}

	/*----------------------------------------------------------------------------*/
	inline dvec2 Graph::Curve::GetXY(int i) const {
		if (i == 0) {
			return start->coord;
		}
		else if (i < GetSize() - 1) {
			return vertices[i - 1];
		}
		else {
			return end->coord;
		}
	}
	/*----------------------------------------------------------------------------*/
	inline float Graph::Curve::GetS(int i) const {
		if (i == 0) {
			return s0;
		}
		if (i < GetSize() - 1) {
			return vertices[i - 1].s;
		}

		return s1;
	}
	/*----------------------------------------------------------------------------*/
	inline Box2D Graph::Curve::GetBounds() const
	{
		double xmin =  std::numeric_limits<double>::infinity();
		double xmax = -std::numeric_limits<double>::infinity();
		double ymin =  std::numeric_limits<double>::infinity();
		double ymax = -std::numeric_limits<double>::infinity();

		for (int i = 0; i < GetSize(); ++i) {
			const dvec2& p = GetXY(i);
			xmin = glm::min(xmin, p.x * 2.0); // temp
			xmax = glm::max(xmax, p.x * 2.0); // temp
			ymin = glm::min(ymin, p.y * 2.0); // temp
			ymax = glm::max(ymax, p.y * 2.0); // temp
		}

		return Box2D(dvec2(xmin, ymin), dvec2(xmax, ymax));
	}

	/*----------------------------------------------------------------------------*/
	inline Graph::Curve* Graph::Curve::GetAncestor() const {
		if (parent == nullptr) {
			return (Curve*)this;
		}
		else {
			return parent->GetAncestor();
		}
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::CurveID Graph::Curve::GetID() const {
		CurveID id;
		id.reference = const_cast<Curve*>(this);
		return id;
	}
	/*----------------------------------------------------------------------------*/
	inline int Graph::Curve::GetSize() const {
		return static_cast<int>(vertices.size() + 2); // size + start + end
	}

	/*----------------------------------------------------------------------------*/
	inline bool operator <(const Graph::CurveID& u, const Graph::CurveID& v) {
		return u.id < v.id;
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator ==(const Graph::CurveID& u, const Graph::CurveID& v) {
		return u.id == v.id;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Graph inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Graph:: Graph() {
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::~Graph() {
	}

	/*----------------------------------------------------------------------------*/
	inline Graph::Node::Iterator Graph::GetNodes() {
		return Node::Iterator(m_nodes);
	}
	/*----------------------------------------------------------------------------*/
	inline Graph::Curve::Iterator Graph::GetCurves() {
		return Curve::Iterator(m_curves);
	}
}