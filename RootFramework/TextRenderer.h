/**
* @file TextRenderer.h
* @brief Implements an atlas based text renderer using fontstash.
* https://github.com/memononen/fontstash
*/

#include <memory>
#include <string>
#include <fontstash.h>
#include <GL/glew.h>
#include "Color.h"

namespace root
{
	class FileHandle;

	class TextRenderer
	{
		public:
			//! CTOR/DTOR:
			TextRenderer(int w, int h, unsigned char flags);
			TextRenderer(int w, int h);
			virtual ~TextRenderer();

			//! SERVICES:
			void RenderText(int font, float x, float y, float size, const std::string& text, const Color& color);
			float MeasureText(int font, float size, const std::string& text);
			int BuildFont(FileHandle* file);

		protected:
			//! INTERNALS:
			static int RenderCreate(void* user_data, int w, int h);
			static int RenderResize(void* user_data, int w, int h);
			static void RenderUpdate(void* user_data, int* rect, const unsigned char* data);
			static void RenderDraw(void* user_data, const float* verts, const float* tcoords, const unsigned int* colors, int nverts);
			static void RenderDelete(void* user_data);

		private:
			//! MEMBERS:
			FONScontext* m_stash;
	};

	////////////////////////////////////////////////////////////////////////////////
	// TextRenderer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TextRenderer::TextRenderer(int w, int h) 
		: TextRenderer(w, h, FONS_ZERO_TOPLEFT) {
	}
	/*----------------------------------------------------------------------------*/
	inline TextRenderer::~TextRenderer() {
		fonsDeleteInternal(m_stash);
	}
}