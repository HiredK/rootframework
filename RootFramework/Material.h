/**
* @file Material.h
* @brief
*/

#pragma once

#include "Texture.h"
#include "Object.h"
#include "Shader.h"

namespace root
{
	class Material : public Object
	{
		public:
			//! CTOR/DTOR:
			Material(const std::string& name);
			virtual ~Material();

			//! SERVICES:
			void Bind();

			//! ACCESSORS:
			void SetAlbedoMap(std::shared_ptr<Texture> texture);
			void SetNormalMap(std::shared_ptr<Texture> texture);

		private:
			//! MEMBERS:
			std::shared_ptr<Texture> m_albedo_map;
			std::shared_ptr<Texture> m_normal_map;
			float m_roughness;
			float m_metalness;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Material inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Material::Material(const std::string& name)
		: Object(name), m_roughness(0.0f), m_metalness(0.0f) {
	}
	/*----------------------------------------------------------------------------*/
	inline Material::~Material() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Material::SetAlbedoMap(std::shared_ptr<Texture> texture) {
		m_albedo_map = texture;
	}
	/*----------------------------------------------------------------------------*/
	inline void Material::SetNormalMap(std::shared_ptr<Texture> texture) {
		m_normal_map = texture;
	}
}