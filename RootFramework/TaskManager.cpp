#include "TaskManager.h"

void root::TaskManager::Add(std::shared_ptr<Task> task)
{
	if (task->GetTaskFlags() & Task::THREADSAFE) {
		if (task->GetTaskFlags() & Task::FRAME_SYNC) {
			m_sync_tasks.push(task);
		}
		else {
			m_background_tasks.push(task);
		}
	}
	else {
		m_task_list[m_W_list].push(task);
	}
}

void root::TaskManager::Start()
{
	m_running = true;
	for (unsigned int i = 0; i < m_num_threads; ++i) {
		m_threads.push_back(new std::thread(std::bind(&TaskManager::Worker, this)));
	}

	while (m_running)
	{
		if (!m_task_list[m_R_list].empty()) {
			std::shared_ptr<Task> task = m_task_list[m_R_list].wait_pop();
			Execute(task);
		}
		else {
			Synchronize();
			std::swap(m_R_list, m_W_list);
		}

		std::this_thread::yield();
	}
}

void root::TaskManager::Stop()
{
	m_running = false;
}

void root::TaskManager::Worker()
{
	std::shared_ptr<Task> task;
	while (m_running)
	{
		bool exec = m_background_tasks.try_pop(task);
		if (exec)
		{
			Execute(task);

			if (task->GetTaskFlags() & Task::FRAME_SYNC)
			{
				{
					std::lock_guard<std::mutex> lock(m_mutex);
					m_num_tasks_to_wait -= 1;
				}

				m_condition.notify_one();
			}

			std::this_thread::yield();
		}
		else {
			// nothing is ready to run, sleep for 1.667 milliseconds (1/10th of a frame @ 60 FPS)
			std::this_thread::sleep_for(std::chrono::microseconds(1667));
		}
	}
}

void root::TaskManager::Execute(std::shared_ptr<Task> task)
{
	task->Run();
	if (task->GetTaskFlags() & Task::REPEATING) {
		Add(task);
	}
}

void root::TaskManager::Synchronize()
{
	std::unique_lock<std::mutex> lock(m_mutex);
	while (m_num_tasks_to_wait > 0)
	{
		m_condition.wait(lock);
	}

	m_num_tasks_to_wait = m_sync_tasks.size();
	while (!m_sync_tasks.empty())
	{
		m_background_tasks.push(m_sync_tasks.wait_pop());
	}
}