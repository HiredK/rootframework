#include "WavesSpectrum.h"

#pragma warning(push)
#pragma warning(disable : 4244)
#pragma warning(disable : 4018)
#pragma warning(disable : 4305)

inline float sqr(float x)
{
	return x * x;
}

inline float omega(float k)
{
	return sqrt(9.81 * k * (1.0 + sqr(k / km)));
}

inline long lrandom(long *seed)
{
	*seed = (*seed * 1103515245 + 12345) & 0x7FFFFFFF;
	return *seed;
}

inline float frandom(long *seed)
{
	long r = lrandom(seed) >> (31 - 24);
	return r / (float)(1 << 24);
}

inline float grandom(float mean, float stdDeviation, long *seed)
{
	float x1, x2, w, y1;
	static float y2;
	static int use_last = 0;

	if (use_last) {
		y1 = y2;
		use_last = 0;
	}
	else {
		do {
			x1 = 2.0f * frandom(seed) - 1.0f;
			x2 = 2.0f * frandom(seed) - 1.0f;
			w = x1 * x1 + x2 * x2;
		} while (w >= 1.0f);
		w = sqrt((-2.0f * log(w)) / w);
		y1 = x1 * w;
		y2 = x2 * w;
		use_last = 1;
	}

	return mean + y1 * stdDeviation;
}

inline int bitReverse(int i, int N)
{
	int j = i;
	int M = N;
	int Sum = 0;
	int W = 1;
	M = M / 2;
	while (M != 0) {
		j = (i & M) > M - 1;
		Sum += j * W;
		W *= 2;
		M = M / 2;
	}
	return Sum;
}

inline void computeWeight(int N, int k, float &Wr, float &Wi)
{
	Wr = cosl(2.0 * M_PI * k / float(N));
	Wi = sinl(2.0 * M_PI * k / float(N));
}

void root::WavesSpectrum::GenerateData(float wind, float amplitude)
{
	auto GetSpectrum = [&wind, &amplitude](float kx, float ky, bool omnispectrum) -> float
	{
		// phase speed
		float k = sqrt(kx * kx + ky * ky);
		float c = omega(k) / k;

		// spectral peak
		float kp = 9.81 * sqr(OMEGA / wind);
		float cp = omega(kp) / kp;

		// friction velocity
		float z0 = 3.7e-5 * sqr(wind) / 9.81 * pow(wind / cp, 0.9f);
		float u_star = 0.41 * wind / log(10.0 / z0);

		float Lpm = exp(-5.0 / 4.0 * sqr(kp / k));
		float gamma = OMEGA < 1.0 ? 1.7 : 1.7 + 6.0 * log(OMEGA);
		float sigma = 0.08 * (1.0 + 4.0 / pow(OMEGA, 3.0f));
		float Gamma = exp(-1.0 / (2.0 * sqr(sigma)) * sqr(sqrt(k / kp) - 1.0));
		float Jp = pow(gamma, Gamma);
		float Fp = Lpm * Jp * exp(-OMEGA / sqrt(10.0) * (sqrt(k / kp) - 1.0));
		float alphap = 0.006 * sqrt(OMEGA);
		float Bl = 0.5 * alphap * cp / c * Fp;

		float alpham = 0.01 * (u_star < cm ? 1.0 + log(u_star / cm) : 1.0 + 3.0 * log(u_star / cm));
		float Fm = exp(-0.25 * sqr(k / km - 1.0));
		float Bh = 0.5 * alpham * cm / c * Fm * Lpm;
		Bh *= Lpm; // bug fix???

		if (omnispectrum) {
			return amplitude * (Bl + Bh) / (k * sqr(k));
		}

		float a0 = log(2.0) / 4.0; float ap = 4.0; float am = 0.13 * u_star / cm;
		float Delta = tanh(a0 + ap * pow(c / cp, 2.5f) + am * pow(cm / c, 2.5f));

		float phi = atan2(ky, kx);

		if (kx < 0.0) {
			return 0.0;
		}
		else {
			Bl *= 2.0;
			Bh *= 2.0;
		}

		float tweak = sqrt(glm::max(kx / sqrt(kx*kx + ky*ky), 0.0f));

		return amplitude * (Bl + Bh) * (1.0 + Delta * cos(2.0 * phi)) / (2.0 * M_PI * sqr(sqr(k))) * tweak;
	};

	auto GetSpectrumSample = [&GetSpectrum](int i, int j, float scale, float kmin, float *result)
	{
		static long seed = 1234;
		float dk = 2.0 * M_PI / scale;
		float kx = i * dk;
		float ky = j * dk;

		if (abs(kx) < kmin && abs(ky) < kmin) {
			result[0] = 0.0;
			result[1] = 0.0;
		}
		else {
			float S = GetSpectrum(kx, ky, false);
			float h = sqrt(S / 2.0) * dk;
			float phi = frandom(&seed) * 2.0 * M_PI;
			result[0] = h * cos(phi);
			result[1] = h * sin(phi);
		}
	};

	auto ComputeButterflyLookup = [this]() -> float*
	{
		float *data = new float[GetSize() * m_passes * 4];

		for (int i = 0; i < m_passes; i++) {
			int nBlocks = (int)powf(2.0, float(m_passes - 1 - i));
			int nHInputs = (int)powf(2.0, float(i));
			for (int j = 0; j < nBlocks; j++) {
				for (int k = 0; k < nHInputs; k++) {
					int i1, i2, j1, j2;
					if (i == 0) {
						i1 = j * nHInputs * 2 + k;
						i2 = j * nHInputs * 2 + nHInputs + k;
						j1 = bitReverse(i1, GetSize());
						j2 = bitReverse(i2, GetSize());
					}
					else {
						i1 = j * nHInputs * 2 + k;
						i2 = j * nHInputs * 2 + nHInputs + k;
						j1 = i1;
						j2 = i2;
					}

					float wr, wi;
					computeWeight(GetSize(), k * nBlocks, wr, wi);

					int offset1 = 4 * (i1 + i * GetSize());
					data[offset1 + 0] = (j1 + 0.5) / GetSize();
					data[offset1 + 1] = (j2 + 0.5) / GetSize();
					data[offset1 + 2] = wr;
					data[offset1 + 3] = wi;

					int offset2 = 4 * (i2 + i * GetSize());
					data[offset2 + 0] = (j1 + 0.5) / GetSize();
					data[offset2 + 1] = (j2 + 0.5) / GetSize();
					data[offset2 + 2] = -wr;
					data[offset2 + 3] = -wi;
				}
			}
		}

		return data;
	};

	auto GetSlopeVariance = [](float kx, float ky, float* spectrum) -> float
	{
		float kSquare = kx * kx + ky * ky;
		float real = spectrum[0];
		float img = spectrum[1];
		float hSquare = real * real + img * img;
		return kSquare * hSquare * 2.0;
	};

	float* spectrum12 = new float[GetSize() * GetSize() * 4];
	float* spectrum34 = new float[GetSize() * GetSize() * 4];

	for (int y = 0; y < GetSize(); ++y)
	for (int x = 0; x < GetSize(); ++x)
	{
		int offset = 4 * (x + y * GetSize());
		int i = x >= GetSize() / 2 ? x - GetSize() : x;
		int j = y >= GetSize() / 2 ? y - GetSize() : y;

		GetSpectrumSample(i, j, GRID1_SIZE, M_PI / GRID1_SIZE, spectrum12 + offset);
		GetSpectrumSample(i, j, GRID2_SIZE, M_PI * GetSize() / GRID1_SIZE, spectrum12 + offset + 2);
		GetSpectrumSample(i, j, GRID3_SIZE, M_PI * GetSize() / GRID2_SIZE, spectrum34 + offset);
		GetSpectrumSample(i, j, GRID4_SIZE, M_PI * GetSize() / GRID3_SIZE, spectrum34 + offset + 2);
	}

	float theoreticSlopeVariance = 0.0;
	float k = 5e-3;
	while (k < 1e3) {
		float nextK = k * 1.001;
		theoreticSlopeVariance += k * k * GetSpectrum(k, 0, true) * (nextK - k);
		k = nextK;
	}

	float totalSlopeVariance = 0.0;
	for (int y = 0; y < GetSize(); ++y)
	for (int x = 0; x < GetSize(); ++x)
	{
		int offset = 4 * (x + y * GetSize());
		float i = 2.0 * M_PI * (x >= GetSize() / 2 ? x - GetSize() : x);
		float j = 2.0 * M_PI * (y >= GetSize() / 2 ? y - GetSize() : y);

		totalSlopeVariance += GetSlopeVariance(i / GRID1_SIZE, j / GRID1_SIZE, spectrum12 + offset);
		totalSlopeVariance += GetSlopeVariance(i / GRID2_SIZE, j / GRID2_SIZE, spectrum12 + offset + 2);
		totalSlopeVariance += GetSlopeVariance(i / GRID3_SIZE, j / GRID3_SIZE, spectrum34 + offset);
		totalSlopeVariance += GetSlopeVariance(i / GRID4_SIZE, j / GRID4_SIZE, spectrum34 + offset + 2);
	}

	{
		Texture::Settings settings = Texture::GetDefaultSettings();
		settings.iformat = GL_RGBA16F;
		settings.format = GL_RGBA;
		settings.type = GL_FLOAT;
		settings.wrap_mode = GL_REPEAT;

		m_textures[SPECTRUM_12] = Texture::CreateTexture(GetSize(), GetSize(), 0, 0, spectrum12, settings);
		m_textures[SPECTRUM_34] = Texture::CreateTexture(GetSize(), GetSize(), 0, 0, spectrum34, settings);
	}
	{
		Texture::Settings settings = Texture::GetDefaultSettings();
		settings.iformat = GL_RGBA16F;
		settings.format = GL_RGBA;
		settings.type = GL_FLOAT;
		settings.wrap_mode = GL_CLAMP_TO_EDGE;

		m_textures[BUTTERFLY] = Texture::CreateTexture(GetSize(), m_passes, 0, 0, ComputeButterflyLookup(), settings);
	}

	m_slope_variance_delta = 0.5f * (theoreticSlopeVariance - totalSlopeVariance);
}

#pragma warning(pop)