/**
* @file Task.h
* @brief
*/

#pragma once

#include "ConcurrentQueue.h"

namespace root
{
	class TaskManager;

	class Task
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef ConcurrentQueue<std::shared_ptr<Task>> List;

			enum Flags
			{
				NONE = 0x0,

				REPEATING = 0x1 << 0,
				THREADSAFE = 0x1 << 1,
				FRAME_SYNC = 0x1 << 2,

				SINGLETHREADED = NONE,
				BACKGROUND = THREADSAFE,
				BACKGROUND_REPEATING = THREADSAFE | REPEATING,
				BACKGROUND_SYNC = THREADSAFE | FRAME_SYNC,
				BACKGROUND_SYNC_REPEATING = THREADSAFE | REPEATING | FRAME_SYNC,

				ALL = ~0x0
			};

			//! CTOR/DTOR:
			Task(unsigned int flags = SINGLETHREADED);
			virtual ~Task();

			//! VIRTUALS:
			virtual void Run() = 0;

			//! ACCESSORS:
			unsigned int GetTaskFlags() const;

		private:
			//! MEMBERS:
			unsigned int m_flags;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Task inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Task::Task(unsigned int flags) : m_flags(flags) {
	}
	/*----------------------------------------------------------------------------*/
	inline Task::~Task() {
	}

	/*----------------------------------------------------------------------------*/
	inline unsigned int Task::GetTaskFlags() const {
		return m_flags;
	}

} // root namespace