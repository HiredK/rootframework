/**
* @file ScriptObjectBinding.h
* @brief
*/

#pragma once

#include <luabind/luabind.hpp>
#include <luabind/operator.hpp>
#include <luabind/adopt_policy.hpp>
#include <luabind/iterator_policy.hpp>
#include <luabind/detail/crtp_iterator.hpp>
#include <luabind/class_info.hpp>
#include <lua.hpp>

#include "SignalHandler.h"
#include "ScriptObject.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// SignalHandlerBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static sigc::connection Connect(ISignalHandler* handler, const luabind::object& f) {
		return handler->Connect(f);
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope SignalHandlerBinding()
	{
		return (
			luabind::class_<sigc::connection>("Connection")
			.def("disconnect", &sigc::connection::disconnect),

			luabind::class_<ISignalHandler>("SignalHandler")
			.def("clear", &ISignalHandler::Clear),

			luabind::def("connect", &Connect)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ScriptObjectBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ScriptObject_wrapper : ScriptObject, luabind::wrap_base
	{
		ScriptObject_wrapper(const std::string& name, Type type)
			: ScriptObject(name, type) {
		}
		ScriptObject_wrapper(const std::string& name)
			: ScriptObject_wrapper(name, Static) {
		}
		ScriptObject_wrapper(Type type)
			: ScriptObject_wrapper("script_object", type) {
		}
		ScriptObject_wrapper()
			: ScriptObject_wrapper("script_object") {
		}

		virtual void Update(float dt)
		{
			ScriptObject::PushOwner(GetOwner());

			try {
				luabind::wrap_base::call<void>("__update", dt);
			}
			catch (std::exception const& e) {
				LOG(Logger::Error) << e.what();
			}
			catch (...) {
				LOG(Logger::Error) << "Error!";
			}

			ScriptObject::PopStack();
		}
		static void Update_default(ScriptObject* ptr, float dt) {
			ptr->ScriptObject::Update(dt);
		}

		void RegisterSignalHandler(const std::string& name, ISignalHandler* signal) {
			auto stack = std::unique_ptr<luabind::object>(new luabind::object(luabind::from_stack(GetLuaState(), 1)));
			m_signals[name] = std::unique_ptr<ISignalHandler>(signal);
			(*stack)[name] = signal;
		}

		template <typename Result, typename ...Args> Result CallSignal(const std::string& name, Args... args) {
			SignalHandler<Result, Args...>* signal = dynamic_cast<SignalHandler<Result, Args...>*>(m_signals[name].get());
			return (*signal)(args...);
		}

		template <typename Result> Result CallSignal(const std::string& name) {
			SignalHandler<Result>* signal = dynamic_cast<SignalHandler<Result>*>(m_signals[name].get());
			return (*signal)();
		}

		std::map<std::string, std::unique_ptr<ISignalHandler>> m_signals;
	};

	/*----------------------------------------------------------------------------*/
	std::ostream& operator<<(std::ostream& os, const Object& object) {
		return os << object.GetName();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope ScriptObjectBinding()
	{
		return (
			luabind::namespace_("root")
			[
				luabind::class_<Object>("Object")
				.property("name", &Object::GetName, &Object::SetName)
				.property("unique_color", &Object::GetUniqueColor)
				.def(luabind::self == luabind::other<Object&>())
				.def(luabind::tostring(luabind::const_self)),
				
				luabind::class_<ScriptObject, Object, std::shared_ptr<ScriptObject>, ScriptObject_wrapper>("ScriptObject")
				.enum_("Type")
				[
					luabind::value("Static", ScriptObject::Static),
					luabind::value("Dynamic", ScriptObject::Dynamic)
				]

				.def(luabind::constructor<const std::string&>())
				.def(luabind::constructor<const std::string&, ScriptObject::Type>())
				.def(luabind::constructor<ScriptObject::Type>())
				.def(luabind::constructor<>())

				.property("owner", &ScriptObject::GetOwner)
				.property("type", &ScriptObject::GetType, &ScriptObject::SetType)
				.def("__update", &ScriptObject::Update, &ScriptObject_wrapper::Update_default),

				SignalHandlerBinding()
			]
		);
	}

} // root namespace