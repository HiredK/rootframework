#include "Terrain.h"
#include "Logger.h"

void root::Terrain::Cull(Camera& camera)
{
	dmat4 local_to_camera = camera.GetViewMatrix() * dmat4(GetFaceMatrix(m_face));
	dmat4 local_to_screen = camera.GetProjMatrix() * local_to_camera;
	dmat4 local_to_camera_inv = glm::inverse(local_to_camera);

	m_deformed_camera_position = dvec3(local_to_camera_inv * dvec4(0, 0, 0, 1));
	m_local_camera_position = m_deformation->DeformedToLocal(m_deformed_camera_position);

	dmat4 M = m_deformation->LocalToDeformedDifferential(m_local_camera_position, true);
	double x = glm::length(dvec3(M[0].x, M[1].x, M[2].x));
	double y = glm::length(dvec3(M[0].y, M[1].y, M[2].y));
	m_dist_factor = (float)glm::max(x, y);

	m_local_frustum.Transform(camera.GetProjMatrix(), local_to_camera);
	dvec3 lp = dvec3(m_local_frustum.GetPlane(Frustum::Left));
	dvec3 rp = dvec3(m_local_frustum.GetPlane(Frustum::Right));
	float thfov = (float)glm::tan(glm::acos(glm::min(1.0, glm::max(-1.0, -glm::dot(lp, rp)))) * 0.5);
	float w = (float)1280.0;

	m_split_dist = m_split_factor * w / 1024.0f * glm::tan(glm::radians(40.0f)) / thfov;
	if (m_split_dist < 1.5f || !(std::isinf(m_split_dist) || std::isnan(m_split_dist))) {
		m_split_dist = 1.5f;
	}

	if (m_horizon_culling && m_local_camera_position.z <= m_root.GetCoord().zmax)
	{
		dvec3 deformed_direction = dvec3(local_to_camera_inv * dvec4(0, 0, 1, 1));
		dvec3 local_direction = dvec3(glm::normalize(dvec2(m_deformation->DeformedToLocal(deformed_direction) - m_local_camera_position)), 0);
		m_local_camera_direction = dmat2(local_direction.y, -local_direction.x, -local_direction.x, -local_direction.y);

		for (int i = 0; i < m_horizon_size; ++i) {
			m_horizon[i] = -std::numeric_limits<float>::infinity();
		}
	}

	m_root.RecursiveCulling();
}

void root::Terrain::Draw(Camera& camera)
{
	Cull(camera);
	m_root.RecursiveDraw(camera);
}

void root::Terrain::Process()
{
	for (auto& producer : m_producers) {
		producer->Refresh();
	}
}

bool root::Terrain::AddOccluder(const Box3D& occluder) const
{
	if (!m_horizon_culling || m_local_camera_position.z > m_root.GetCoord().zmax) {
		return false;
	}

	dvec2 corners[4];
	dvec2 o = dvec2(m_local_camera_position);
	corners[0] = m_local_camera_direction * (dvec2(occluder.GetMin().x, occluder.GetMin().y) - o);
	corners[1] = m_local_camera_direction * (dvec2(occluder.GetMin().x, occluder.GetMax().y) - o);
	corners[2] = m_local_camera_direction * (dvec2(occluder.GetMax().x, occluder.GetMin().y) - o);
	corners[3] = m_local_camera_direction * (dvec2(occluder.GetMax().x, occluder.GetMax().y) - o);
	if (corners[0].y <= 0.0 || corners[1].y <= 0.0 || corners[2].y <= 0.0 || corners[3].y <= 0.0) {
		return false;
	}

	double dzmin = occluder.GetMin().z - m_local_camera_position.z;
	double dzmax = occluder.GetMax().z - m_local_camera_position.z;

	dvec3 bounds[4];
	bounds[0] = dvec3(corners[0].x, dzmin, dzmax) / corners[0].y;
	bounds[1] = dvec3(corners[1].x, dzmin, dzmax) / corners[1].y;
	bounds[2] = dvec3(corners[2].x, dzmin, dzmax) / corners[2].y;
	bounds[3] = dvec3(corners[3].x, dzmin, dzmax) / corners[3].y;

	double xmin = glm::min(glm::min(bounds[0].x, bounds[1].x), glm::min(bounds[2].x, bounds[3].x)) * 0.33 + 0.5;
	double xmax = glm::max(glm::max(bounds[0].x, bounds[1].x), glm::max(bounds[2].x, bounds[3].x)) * 0.33 + 0.5;
	double zmin = glm::min(glm::min(bounds[0].y, bounds[1].y), glm::min(bounds[2].y, bounds[3].y));
	double zmax = glm::max(glm::max(bounds[0].z, bounds[1].z), glm::max(bounds[2].z, bounds[3].z));

	int imin = glm::max((int)glm::floor(xmin * m_horizon_size), 0);
	int imax = glm::min((int)glm::ceil(xmax * m_horizon_size), m_horizon_size - 1);

	bool occluded = (imax >= imin);
	for (int i = imin; i <= imax; ++i) {
		if (zmax > m_horizon[i]) {
			occluded = false;
			break;
		}
	}

	if (!occluded) {
		imin = glm::max((int)glm::ceil(xmin * m_horizon_size), 0);
		imax = glm::min((int)glm::floor(xmax * m_horizon_size), m_horizon_size - 1);
		for (int i = imin; i <= imax; ++i) {
			m_horizon[i] = (float)glm::max(m_horizon[i], (float)zmin);
		}
	}

	return occluded;
}

bool root::Terrain::IsOccluded(const Box3D& box) const
{
	if (!m_horizon_culling || m_local_camera_position.z > m_root.GetCoord().zmax) {
		return false;
	}

	dvec2 corners[4];
	dvec2 o = dvec2(m_local_camera_position);
	corners[0] = m_local_camera_direction * (dvec2(box.GetMin().x, box.GetMin().y) - o);
	corners[1] = m_local_camera_direction * (dvec2(box.GetMin().x, box.GetMax().y) - o);
	corners[2] = m_local_camera_direction * (dvec2(box.GetMax().x, box.GetMin().y) - o);
	corners[3] = m_local_camera_direction * (dvec2(box.GetMax().x, box.GetMax().y) - o);
	if (corners[0].y <= 0.0 || corners[1].y <= 0.0 || corners[2].y <= 0.0 || corners[3].y <= 0.0) {
		return false;
	}

	double dz = box.GetMax().z - m_local_camera_position.z;
	corners[0] = dvec2(corners[0].x, dz) / corners[0].y;
	corners[1] = dvec2(corners[1].x, dz) / corners[1].y;
	corners[2] = dvec2(corners[2].x, dz) / corners[2].y;
	corners[3] = dvec2(corners[3].x, dz) / corners[3].y;

	double xmin = glm::min(glm::min(corners[0].x, corners[1].x), glm::min(corners[2].x, corners[3].x)) * 0.33 + 0.5;
	double xmax = glm::max(glm::max(corners[0].x, corners[1].x), glm::max(corners[2].x, corners[3].x)) * 0.33 + 0.5;
	double zmax = glm::max(glm::max(corners[0].y, corners[1].y), glm::max(corners[2].y, corners[3].y));

	int imin = glm::max((int)glm::floor(xmin * m_horizon_size), 0);
	int imax = glm::min((int)glm::ceil(xmax * m_horizon_size), m_horizon_size - 1);

	for (int i = imin; i <= imax; ++i) {
		if (zmax > m_horizon[i]) {
			return false;
		}
	}

	return (imax >= imin);
}

dmat3 root::Terrain::GetFaceMatrix(Face face) const
{
	switch (face) {
		case PosX: return dmat3( 0, 0, 1, 0, 1, 0, 1, 0, 0 );
		case NegX: return dmat3( 0, 0,-1, 0, 1, 0,-1, 0, 0 );
		case PosY: return dmat3( 1, 0, 0, 0, 0, 1, 0, 1, 0 );
		case NegY: return dmat3( 1, 0, 0, 0, 0,-1, 0,-1, 0 );
		case PosZ: return dmat3(-1, 0, 0, 0, 1, 0, 0, 0, 1 );
		case NegZ: return dmat3( 1, 0, 0, 0, 1, 0, 0, 0,-1 );
		default: return dmat3(1);
	}
}

double root::Terrain::GetCameraDist(const Box3D& box, const dvec3& point) const
{
	return(
		glm::max(glm::abs(point.z - box.GetMax().z) / m_dist_factor,
		glm::max(glm::min(glm::abs(point.x - box.GetMin().x),
		glm::abs(point.x - box.GetMax().x)),
		glm::min(glm::abs(point.y - box.GetMin().y),
		glm::abs(point.y - box.GetMax().y))))
	);
}