#include "FileHandle.h"
#include "Logger.h"

bool root::FileHandle::Process()
{
	if (PHYSFS_exists(m_path.c_str()))
	{
		if (m_allocate)
		{
			if (m_state != Created)
			{
				LOG() << m_path << ": Loading file";
				m_state = (CreateData()) ? Created : m_state;
				m_mt = PHYSFS_getLastModTime(m_path.c_str());
				return true;
			}

			if (m_state == Created)
			{
				PHYSFS_sint64 mt = PHYSFS_getLastModTime(m_path.c_str());
				bool modified = ((mt - m_mt) > PHYSFS_sint64(0));
				m_mt = mt;

				if (modified || m_reload)
				{
					LOG() << m_path << ": Reloading file";
					DeleteData();
					m_state = (CreateData()) ? Created : Empty;
					m_reload = false;
					return true;
				}
			}
		}
		else if (m_state == Created)
		{
			LOG() << m_path << ": Unloading file";
			DeleteData();
			m_state = Empty;
			return true;
		}
	}
	else if (m_state != Missing)
	{
		LOG() << m_path << ": Removing file";
		DeleteData();
		m_state = Missing;
		return true;
	}

	return false;
}

void root::FileHandle::ScanFileForInclude(std::string& data)
{
	std::string prefix = "#include ";
	size_t start = 0;

	auto ExtractPath = [](const std::string& str) -> std::string {
		return str.substr(0, str.find_last_of("\\/"));
	};

	while ((start = data.find(prefix, start)) != std::string::npos) {
		size_t pos = start + prefix.length() + 1;
		size_t len = data.find("\"", pos);
		std::string content = "";

		std::string root = ExtractPath(GetPath());
		std::string path = root + "/" + data.substr(pos, len - pos);
		if (PHYSFS_exists(path.c_str()))
		{
			PHYSFS_file* handle = PHYSFS_openRead(path.c_str());
			if (handle) {
				PHYSFS_uint32 length = static_cast<PHYSFS_uint32>(PHYSFS_fileLength(handle));
				content.resize(length);

				PHYSFS_read(handle, &content[0], 1, length);
				ScanFileForInclude(content);
				PHYSFS_close(handle);
			}
		}
		else {
			LOG(Logger::Warning) << path << ": Missing file";
			break;
		}

		data.replace(start, (len + 1) - start, content);
		start += content.length();
	}
}

void root::FileHandle::ScanFileForDefine(std::string& data)
{
	std::string prefix = "#define ";
	size_t start = 0;

	std::map<std::string, std::string> defines;
	while ((start = data.find(prefix, start)) != std::string::npos)
	{
		size_t len = prefix.length();
		size_t k_pos = start + prefix.length();
		size_t k_new = data.find("\n", k_pos);
		size_t k_len = data.find(" ", k_pos);

		if (k_new > k_len) {
			std::string key = data.substr(k_pos, k_len - k_pos);
			len += key.length();

			size_t v_pos = k_len + 1;
			size_t v_len = data.find("\n", v_pos);
			std::string val = data.substr(v_pos, v_len - v_pos);
			defines[key] = val;
			len += val.length();
		}
		else {
			std::string key = data.substr(k_pos, (k_new - k_pos) - 1);
			defines[key] = "1";
			len += key.length();
		}

		data.erase(start, len);
	}

	for (auto& define : defines)
	{
		start = 0;
		while ((start = data.find(define.first, start)) != std::string::npos) {
			data.replace(start, define.first.length(), define.second);
			start += define.second.length();
		}
	}
}

bool root::FileHandle::CreateData()
{
	if (m_data.empty())
	{
		bool success = true;
		PHYSFS_file* handle = PHYSFS_openRead(m_path.c_str());
		if (handle)
		{
			PHYSFS_uint32 len = static_cast<PHYSFS_uint32>(PHYSFS_fileLength(handle));
			m_data.resize(len);
			
			if (PHYSFS_read(handle, &m_data[0], 1, len) == -1) {
				success = false;
			}

			PHYSFS_close(handle);
		}
		else {
			success = false;
		}

		if (!success) {
			LOG(Logger::Error) << m_path << ": " << PHYSFS_getLastError();
			return false;
		}
	}

	return true;
}

void root::FileHandle::DeleteData()
{
	if (!m_data.empty())
	{
		m_data.clear();
		m_data.shrink_to_fit();
	}
}