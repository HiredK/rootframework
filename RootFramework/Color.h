/**
* @file Color.h
* @brief 
*/

#pragma once

#include <algorithm>

namespace root
{
	class Color
	{
		public:
			//! CTOR/DTOR:
			Color(unsigned char r = 0, unsigned char g = 0, unsigned char b = 0, unsigned char a = 255);
			virtual ~Color();

			//! SERVICES:
			static float ToFloat(unsigned int v);
			unsigned int ToInteger() const;

			//! OPERATORS:
			// Colors can be added and modulated (multiplied)
			// using these overloaded operators.
			Color operator+(const Color& other);
			Color operator-(const Color& other);
			Color operator*(const Color& other);
			bool operator==(const Color& other);
			bool operator!=(const Color& other);

			//! PREDEFINED COLORS:
			// Common colors are defined as static variables.
			static const Color Black;
			static const Color White;
			static const Color Red;
			static const Color Green;
			static const Color Blue;
			static const Color Yellow;
			static const Color Magenta;
			static const Color Cyan;
			static const Color Transparent;

			//! MEMBERS:
			// Components are stored in the range [0->255].
			unsigned char r; // r component
			unsigned char g; // g component
			unsigned char b; // b component
			unsigned char a; // a (opacity) component
	};

	////////////////////////////////////////////////////////////////////////////////
	// farore::Color inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Color::Color(
		unsigned char r, unsigned char g, unsigned char b, unsigned char a)
		: r(r), g(g), b(b), a(a) {
	}
	/*----------------------------------------------------------------------------*/
	inline Color::~Color() {
	}

	/*----------------------------------------------------------------------------*/
	inline float Color::ToFloat(unsigned int v) {
		return v * (1.0f / 255.0f);
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int Color::ToInteger() const {
		return (r) | (g << 8) | (b << 16) | (a << 24);
	}
}