#include "ParticleEmitter.h"

void root::ParticleEmitter::Rebuild()
{
	Clear();

	Particle* data = new Particle[m_max_particles];

	// init first particle as the system emmiter
	data[0].type = 0.0f;
	data[0].position = vec3();
	data[0].velocity = vec3();
	data[0].age = 0.0f;

	glGenTransformFeedbacks(BUFFER_COUNT, m_tfo);
	glGenBuffers(BUFFER_COUNT, m_pbo);

	for (unsigned int i = 0; i < BUFFER_COUNT; ++i) {
		glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_tfo[i]);
		glBindBuffer(GL_ARRAY_BUFFER, m_pbo[i]);
		glBufferData(GL_ARRAY_BUFFER, sizeof(Particle) * m_max_particles, data, GL_DYNAMIC_DRAW);
		glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, m_pbo[i]);
	}

	m_first = true;
}

void root::ParticleEmitter::Simulate(float dt)
{
	if (m_first) {
		std::vector<const char*> varyings = { "gs_Type1", "gs_Position1", "gs_Velocity1", "gs_Age1" };
		Shader::Get()->OverrideTransformFeedbackVaryings(varyings, GL_INTERLEAVED_ATTRIBS);
	}

	Shader::Get()->Uniform("u_EmitterOrigin", Transform::GetPosition());
	glEnable(GL_RASTERIZER_DISCARD);

	glBindBuffer(GL_ARRAY_BUFFER, m_pbo[m_curr_pbo]);
	glBindTransformFeedback(GL_TRANSFORM_FEEDBACK, m_tfo[m_curr_tfo]);
	for (unsigned int i = 0; i < 4; ++i) {
		glEnableVertexAttribArray(i);
	}

	glVertexAttribPointer(0, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), 0);                  // Particle::type
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*)  4); // Particle::position
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*) 16); // Particle::velocity
	glVertexAttribPointer(3, 1, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*) 28); // Particle::age
	glBeginTransformFeedback(GL_POINTS);

	if (m_first) {
		glDrawArrays(GL_POINTS, 0, 1);
		m_first = false;
	}
	else {
		glDrawTransformFeedback(GL_POINTS, m_tfo[m_curr_pbo]);
	}

	glEndTransformFeedback();
	for (unsigned int i = 0; i < 4; ++i) {
		glDisableVertexAttribArray(i);
	}

	std::swap(m_curr_tfo, m_curr_pbo);
	glDisable(GL_RASTERIZER_DISCARD);
}

void root::ParticleEmitter::Draw()
{
	if (m_tfo[FRONT] == GL_ZERO || m_pbo[FRONT] == GL_ZERO) {
		return;
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_pbo[m_curr_tfo]);
	glEnableVertexAttribArray(0);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Particle), (const GLvoid*) 4);  // Particle::position
	glDrawTransformFeedback(GL_POINTS, m_tfo[m_curr_tfo]);
	glDisableVertexAttribArray(0);
}

void root::ParticleEmitter::Clear()
{
	if (m_tfo[FRONT] != GL_ZERO) {
		glDeleteTransformFeedbacks(BUFFER_COUNT, m_tfo);
		m_tfo[FRONT] = GL_ZERO;
		m_tfo[BACK ] = GL_ZERO;
	}

	if (m_pbo[FRONT] != GL_ZERO) {
		glDeleteBuffers(BUFFER_COUNT, m_pbo);
		m_pbo[FRONT] = GL_ZERO;
		m_pbo[BACK ] = GL_ZERO;
	}
}