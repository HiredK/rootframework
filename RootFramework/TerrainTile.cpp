#include "TerrainTile.h"
#include "Terrain.h"

void root::TerrainTile::RecursiveCulling()
{
	Frustum::Visibility v = (!m_parent) ? Frustum::Partially : m_parent->m_visibility;
	if (v == Frustum::Partially) {
		m_visibility = m_owner.GetVisibility(m_local_box);
	}
	else {
		m_visibility = v;
	}

	if (m_visibility != Frustum::Invisible) {
		m_occluded = m_owner.IsOccluded(m_local_box);
		if (m_occluded) {
			m_visibility = Frustum::Invisible;
		}
	}

	const Box3D base = Box3D(dvec3(m_coord.ox, m_coord.oy, 0), dvec3(m_coord.ox + m_coord.length, m_coord.oy + m_coord.length, 0));
	if (m_owner.GetCameraDist(base) < (m_coord.length * 0.5))
	{
		if (!m_paged_in) {
			m_owner.PageIn(*this);
			m_paged_in = true;
		}
	}

	if (m_owner.GetCameraDist(base) < (m_coord.length * m_owner.GetSplitDist()) &&
		m_visibility != Frustum::Invisible &&
		m_level < m_owner.GetMaxLevel())
	{
		if (IsLeaf()) {
			Subdivide();
		}

		int order[4];
		double ox = m_owner.GetLocalCameraPosition().x;
		double oy = m_owner.GetLocalCameraPosition().y;
		double cx = m_coord.ox + m_coord.length * 0.5;
		double cy = m_coord.oy + m_coord.length * 0.5;

		if (oy < cy) {
			if (ox < cx) {
				order[0] = 0;
				order[1] = 1;
				order[2] = 2;
				order[3] = 3;
			}
			else {
				order[0] = 1;
				order[1] = 0;
				order[2] = 3;
				order[3] = 2;
			}
		}
		else {
			if (ox < cx) {
				order[0] = 2;
				order[1] = 0;
				order[2] = 3;
				order[3] = 1;
			}
			else {
				order[0] = 3;
				order[1] = 1;
				order[2] = 2;
				order[3] = 0;
			}
		}

		m_children[order[0]]->RecursiveCulling();
		m_children[order[1]]->RecursiveCulling();
		m_children[order[2]]->RecursiveCulling();
		m_children[order[3]]->RecursiveCulling();

		m_occluded = (
			m_children[0]->m_occluded &&
			m_children[1]->m_occluded &&
			m_children[2]->m_occluded &&
			m_children[3]->m_occluded
		);
	}
	else {
		if (m_visibility != Frustum::Invisible) {
			m_occluded = m_owner.AddOccluder(m_local_box);
			if (m_occluded) {
				m_visibility = Frustum::Invisible;
			}
		}

		if (!IsLeaf()) {
			Release();
		}
	}
}

void root::TerrainTile::RecursiveDraw(Camera& camera)
{
	if (m_visibility == Frustum::Invisible) {
		return;
	}

	if (IsLeaf()) {
		for (unsigned int i = 0; i < m_owner.GetNumOfProducers(); ++i) {
			m_owner.GetProducer(i)->SetUniforms(m_level, m_coord.tx, m_coord.ty);
		}

		m_owner.GetDeformation()->SetUniforms(camera, *this);
		m_owner.GetTileMesh()->Draw();
	}
	else
	{
		int order[4];
		double ox = m_owner.GetLocalCameraPosition().x;
		double oy = m_owner.GetLocalCameraPosition().y;
		double cx = m_coord.ox + m_coord.length / 2.0;
		double cy = m_coord.oy + m_coord.length / 2.0;

		if (oy < cy) {
			if (ox < cx) {
				order[0] = 0;
				order[1] = 1;
				order[2] = 2;
				order[3] = 3;
			}
			else {
				order[0] = 1;
				order[1] = 0;
				order[2] = 3;
				order[3] = 2;
			}
		}
		else {
			if (ox < cx) {
				order[0] = 2;
				order[1] = 0;
				order[2] = 3;
				order[3] = 1;
			}
			else {
				order[0] = 3;
				order[1] = 1;
				order[2] = 2;
				order[3] = 0;
			}
		}

		int done = 0;
		for (int i = 0; i < 4; ++i)
		{
			if (GetChild(order[i])->m_visibility == Frustum::Invisible) {
				done |= (1 << order[i]);
			}
			else {
				GetChild(order[i])->RecursiveDraw(camera);
				done |= (1 << order[i]);
			}
		}
	}
}

void root::TerrainTile::Subdivide()
{
	float hl = (float)m_coord.length / 2.0f;

	// bottom left quad
	Coord coord0 = { 2 * m_coord.tx, 2 * m_coord.ty, m_coord.ox, m_coord.oy, hl, m_coord.zmin, m_coord.zmax };
	m_children[0] = std::unique_ptr<TerrainTile>(new TerrainTile(m_owner, coord0, this));

	// bottom right quad
	Coord coord1 = { 2 * m_coord.tx + 1, 2 * m_coord.ty, m_coord.ox + hl, m_coord.oy, hl, m_coord.zmin, m_coord.zmax };
	m_children[1] = std::unique_ptr<TerrainTile>(new TerrainTile(m_owner, coord1, this));

	// top left quad
	Coord coord2 = { 2 * m_coord.tx, 2 * m_coord.ty + 1, m_coord.ox, m_coord.oy + hl, hl, m_coord.zmin, m_coord.zmax };
	m_children[2] = std::unique_ptr<TerrainTile>(new TerrainTile(m_owner, coord2, this));

	// top right quad
	Coord coord3 = { 2 * m_coord.tx + 1, 2 * m_coord.ty + 1, m_coord.ox + hl, m_coord.oy + hl, hl, m_coord.zmin, m_coord.zmax };
	m_children[3] = std::unique_ptr<TerrainTile>(new TerrainTile(m_owner, coord3, this));
}

void root::TerrainTile::Release()
{
	for (unsigned int i = 0; i < 4; ++i) {
		m_children[i] = nullptr;
	}
}