/**
* @file FileHandle.h
* @brief
*/

#pragma once

#include <vector>
#include <memory>
#include "FileFactory.h"
#include "IOStream.h"

namespace root
{
	class Engine;

	class FileHandle
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef std::vector<std::shared_ptr<FileHandle>> List;
			enum State { Empty, Created, Missing };

			//! CTOR/DTOR:
			FileHandle(Engine* engine_instance, const std::string& path);
			virtual ~FileHandle();

			//! SERVICES:
			static bool Ready(FileHandle* file);
			bool Process();

			//! ACCESSORS:
			const std::string& GetPath() const;
			const void* GetData() const;
			std::size_t GetSize() const;
			void SetAllocate(bool allocate);
			bool GetAllocate() const;
			void Reload();

			//! VIRTUALS:
			virtual bool IsProcessBackground();

		protected:
			//! SERVICES:
			void ScanFileForInclude(std::string& data);
			void ScanFileForInclude();
			void ScanFileForDefine(std::string& data);
			void ScanFileForDefine();

			//! VIRTUALS:
			virtual bool CreateData();
			virtual void DeleteData();

			//! MEMBERS:
			Engine* m_engine_instance;
			std::string m_path;
			std::string m_data;
			State m_state;

			PHYSFS_sint64 m_mt;
			bool m_allocate;
			bool m_reload;
	};

	////////////////////////////////////////////////////////////////////////////////
	// FileHandle inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FileHandle::FileHandle(Engine* engine_instance, const std::string& path)
		: m_engine_instance(engine_instance), m_path(path), m_state(Empty)
		, m_mt(PHYSFS_getLastModTime(m_path.c_str()))
		, m_allocate(false)
		, m_reload(false) {
	}
	/*----------------------------------------------------------------------------*/
	inline FileHandle::~FileHandle() {
	}

	/*----------------------------------------------------------------------------*/
	inline bool FileHandle::Ready(FileHandle* file) {
		return (file != nullptr && file->m_state == Created);
	}

	/*----------------------------------------------------------------------------*/
	inline const std::string& FileHandle::GetPath() const {
		return m_path;
	}
	/*----------------------------------------------------------------------------*/
	inline const void* FileHandle::GetData() const {
		return &m_data[0];
	}
	/*----------------------------------------------------------------------------*/
	inline std::size_t FileHandle::GetSize() const {
		return m_data.size();
	}
	/*----------------------------------------------------------------------------*/
	inline void FileHandle::SetAllocate(bool allocate) {
		m_allocate = allocate;
	}
	/*----------------------------------------------------------------------------*/
	inline bool FileHandle::GetAllocate() const {
		return m_allocate;
	}
	/*----------------------------------------------------------------------------*/
	inline void FileHandle::Reload() {
		if (m_allocate) {
			m_reload = true;
		}
	}

	/*----------------------------------------------------------------------------*/
	inline bool FileHandle::IsProcessBackground() {
		return true;
	}

	/*----------------------------------------------------------------------------*/
	inline void FileHandle::ScanFileForInclude() {
		return ScanFileForInclude(m_data);
	}
	/*----------------------------------------------------------------------------*/
	inline void FileHandle::ScanFileForDefine() {
		return ScanFileForDefine(m_data);
	}

}  // root namespace