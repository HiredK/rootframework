/**
* @file Mesh.h
* @brief
*/

#pragma once

#include <map>
#include "Camera.h"
#include "GLBuffer.h"
#include "Color.h"
#include "Shader.h"

namespace root
{
	class Mesh : public Transform
	{
		public:
			//! TYPEDEF/ENUMS:
			enum { VertexBuffer, IndiceBuffer };

			//! CTOR/DTOR:
			Mesh(const void* data, GLsizei vertex_count, GLsizei stride, GLenum mode, GLenum usage, const void* indices, GLsizei indice_count, GLenum type = GL_UNSIGNED_SHORT);
			Mesh(const void* data, GLsizei vertex_count, GLsizei stride, GLenum mode, GLenum usage);
			virtual ~Mesh();

			struct VertexAttrib
			{
				GLsizei size;
				GLenum type;
				bool normalized;
				GLsizei offset;
			};

			struct Vertex2D
			{
				float x, y;
				float s, t;
				unsigned char r, g, b, a;
			};

			struct Vertex3D
			{
				float x, y, z, w;
				float s, t;
				unsigned char r, g, b, a;
				float nx, ny, nz;
				float tx, ty, tz;
			};

			//! SERVICES:
			void AddVertexAttribute(const std::string& name, GLint size, GLenum type, bool normalized, GLsizei offset);
			void AddVertexAttribute(const std::string& name, const VertexAttrib& attrib);
			void UseDefaultVertex2D();
			void UseDefaultVertex3D();

			void Draw(Camera& camera);
			void Draw();

			static std::shared_ptr<Mesh> BuildQuad(float w = 1.0f, float h = 1.0f, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildBox(const vec3& min, const vec3& max, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildBoxWire(const vec3& min, const vec3& max, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildCube(float extent, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildCubeWire(float extent, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildSphere(float radius, int U = 16, int V = 16, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildCylinder(float r1, float r2, float height, int R = 32, int H = 16, bool open = false, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildCone(float r, float height, int R = 16, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildCapsule(float radius, float length, int S = 16, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildPlane(int w = 10, int h = 10, bool center = false, bool cw = true, const Color& color = Color::White);
			static std::shared_ptr<Mesh> BuildPatch(int w = 10, int h = 10, const Color& color = Color::White);

			//! ACCESSORS:
			void SetVertexCount(GLsizei count);
			GLsizei GetVertexCount() const;
			void SetMode(GLenum mode);
			GLenum GetMode() const;
			GLenum GetType() const;
			void SetColor(const Color& color);
			const Color& GetColor() const;

		protected:
			//! SERVICES:
			static void ComputeNormals(std::vector<root::Mesh::Vertex3D>& data, const std::vector<unsigned short>& indices, bool smooth);
			static void ComputeTangents(std::vector<root::Mesh::Vertex3D>& data, const std::vector<unsigned short>& indices);

			//! MEMBERS:
			std::unique_ptr<GLBuffer> m_buffers[2];
			std::map<std::string, VertexAttrib> m_attribs;
			GLsizei m_count;
			GLsizei m_stride;
			GLenum m_mode;
			GLenum m_type;
			Color m_color;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Mesh inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Mesh::Mesh(const void* data, GLsizei vertex_count, GLsizei stride, GLenum mode, GLenum usage)
		: Mesh(data, vertex_count, stride, mode, usage, nullptr, 0) {
	}
	/*----------------------------------------------------------------------------*/
	inline Mesh::~Mesh() {
		m_buffers[VertexBuffer].release();
		m_buffers[IndiceBuffer].release();
		m_attribs.clear();
	}

	/*----------------------------------------------------------------------------*/
	inline void Mesh::AddVertexAttribute(const std::string& name, GLint size, GLenum type, bool normalized, GLsizei offset) {
		AddVertexAttribute(name, { size, type, normalized, offset });
	}
	/*----------------------------------------------------------------------------*/
	inline void Mesh::AddVertexAttribute(const std::string& name, const VertexAttrib& attrib) {
		m_attribs[name] = attrib;
	}

	/*----------------------------------------------------------------------------*/
	inline void Mesh::Draw(Camera& camera) {
		if (camera.GetFrustum().IsInside(GetBoundingBox().Transform(ComputeMatrix())) != Frustum::Invisible) {
			Shader::Get()->UniformMat4("u_ViewProjMatrix", camera.GetViewProjMatrix());
			Draw();
		}
	}

	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Mesh> Mesh::BuildCube(float extent, const Color& color) {
		return BuildBox(vec3(-extent), vec3(extent), color);
	}
	/*----------------------------------------------------------------------------*/
	inline std::shared_ptr<Mesh> Mesh::BuildCubeWire(float extent, const Color& color) {
		return BuildBoxWire(vec3(-extent), vec3(extent), color);
	}

	/*----------------------------------------------------------------------------*/
	inline void Mesh::SetVertexCount(GLsizei count) {
		m_count = count;
	}
	/*----------------------------------------------------------------------------*/
	inline GLsizei Mesh::GetVertexCount() const {
		return m_count;
	}
	/*----------------------------------------------------------------------------*/
	inline void Mesh::SetMode(GLenum mode) {
		m_mode = mode;
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum Mesh::GetMode() const {
		return m_mode;
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum Mesh::GetType() const {
		return m_type;
	}
	/*----------------------------------------------------------------------------*/
	inline void Mesh::SetColor(const Color& color) {
		m_color = color;
	}
	/*----------------------------------------------------------------------------*/
	inline const Color& Mesh::GetColor() const {
		return m_color;
	}

} // root namespace