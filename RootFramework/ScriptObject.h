/**
* @file ScriptObject.h
* @brief
*/

#pragma once

#include <mutex>
#include "Script.h"
#include "Object.h"

namespace root
{
	class Engine;

	class ScriptObject : public Object
	{
		friend class Script;

		public:
			//! TYPEDEF/ENUMS:
			enum Type { Static, Dynamic };

			//! CTORS/DTORS:
			ScriptObject(const std::string& name, Type type);
			virtual ~ScriptObject();

			//! VIRTUALS:
			virtual void Update(float dt) {}

			//! SERVICES:
			static void UpdateAll(float dt);
			static void PushOwner(Script* owner);
			static void PopStack();

			//! ACCESSORS:
			Engine* GetEngineInstance();
			lua_State* GetLuaState();
			Script* GetOwner() const;
			void SetType(Type type);
			Type GetType() const;

		protected:
			// Keeps track of the active script owner.
			static std::vector<Script*> ActiveOwnerStack;

			// Indicate if the reference list changed.
			static bool RefListChanged;

			//! MEMBERS:
			static std::vector<ScriptObject*> m_references;
			bool m_isUpdated;
			Script* m_owner;
			Type m_type;
	};

	////////////////////////////////////////////////////////////////////////////////
	// ScriptObject inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline void ScriptObject::PushOwner(Script* owner) {
		if (ActiveOwnerStack.empty() || ActiveOwnerStack.back()->GetPath() != owner->GetPath()) {
			ActiveOwnerStack.push_back(owner);
		}
	}
	/*----------------------------------------------------------------------------*/
	inline void ScriptObject::PopStack() {
		if (ActiveOwnerStack.size() > 1) {
			ActiveOwnerStack.pop_back();
		}
	}

	/*----------------------------------------------------------------------------*/
	inline Engine* ScriptObject::GetEngineInstance() {
		return m_owner->m_engine_instance;
	}
	/*----------------------------------------------------------------------------*/
	inline lua_State* ScriptObject::GetLuaState() {
		return m_owner->m_state;
	}
	/*----------------------------------------------------------------------------*/
	inline Script* ScriptObject::GetOwner() const {
		return m_owner;
	}
	/*----------------------------------------------------------------------------*/
	inline void ScriptObject::SetType(Type type) {
		m_type = type;
	}
	/*----------------------------------------------------------------------------*/
	inline ScriptObject::Type ScriptObject::GetType() const {
		return m_type;
	}

} // root namespace