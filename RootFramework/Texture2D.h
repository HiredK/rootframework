/**
* @file Texture2D.h
* @brief
*/

#pragma once

#include "Texture.h"

namespace root
{
	class Texture2D : public Texture
	{
		public:
			//! CTOR/DTOR:
			Texture2D(const Settings& settings, unsigned int w, unsigned int h);
			virtual ~Texture2D();

			//! SERVICES:
			static void Unbind();

		protected:
			//! VIRTUALS:
			virtual void Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level = 0);
			virtual GLenum GetTarget() const;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture2D inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture2D::Texture2D(const Settings& settings, unsigned int w, unsigned int h)
		: Texture(settings, w, h) {
	}
	/*----------------------------------------------------------------------------*/
	inline Texture2D::~Texture2D() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture2D::Unbind() {
		glBindTexture(GL_TEXTURE_2D, GL_ZERO);
	}

	/*----------------------------------------------------------------------------*/
	inline void  Texture2D::Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level) {
		glTexImage2D(GetTarget(), 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data);
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum Texture2D::GetTarget() const {
		return GL_TEXTURE_2D;
	}
}