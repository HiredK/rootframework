#include "Color.h"

const root::Color root::Color::Black(0, 0, 0);
const root::Color root::Color::White(255, 255, 255);
const root::Color root::Color::Red(255, 0, 0);
const root::Color root::Color::Green(0, 255, 0);
const root::Color root::Color::Blue(0, 0, 255);
const root::Color root::Color::Yellow(255, 255, 0);
const root::Color root::Color::Magenta(255, 0, 255);
const root::Color root::Color::Cyan(0, 255, 255);
const root::Color root::Color::Transparent(0, 0, 0, 0);

root::Color root::Color::operator+(const Color& other)
{
	return Color(
		unsigned char(std::min(int(r) + other.r, 255)),
		unsigned char(std::min(int(g) + other.g, 255)),
		unsigned char(std::min(int(b) + other.b, 255)),
		unsigned char(std::min(int(a) + other.a, 255))
	);
}

root::Color root::Color::operator-(const Color& other)
{
	return Color(
		unsigned char(std::max(int(r) - other.r, 0)),
		unsigned char(std::max(int(g) - other.g, 0)),
		unsigned char(std::max(int(b) - other.b, 0)),
		unsigned char(std::max(int(a) - other.a, 0))
	);
}

root::Color root::Color::operator*(const Color& other)
{
	return Color(
		unsigned char(int(r) * other.r / 255),
		unsigned char(int(g) * other.g / 255),
		unsigned char(int(b) * other.b / 255),
		unsigned char(int(a) * other.a / 255)
	);
}

bool root::Color::operator==(const Color& other)
{
	return (r == other.r && g == other.g && b == other.b && a == other.a);
}

bool root::Color::operator!=(const Color& other)
{
	return (r != other.r || g != other.g || b != other.b || a != other.a);
}