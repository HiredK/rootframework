/**
* @file System.h
* @brief
*/

#pragma once

#include <memory>
#include <vector>

namespace root
{
	class Engine;

	class System
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef std::vector<std::shared_ptr<System>> List;

			//! CTOR/DTOR:
			System(Engine* engine_instance);
			virtual ~System();

			//! VIRTUALS:
			virtual bool Init(int argc, char** argv) = 0;
			virtual void Update(float dt) = 0;
			virtual void Cleanup() = 0;

			struct Version
			{
				//! OPERATORS:
				friend bool operator==(Version& a, Version& b);
				friend bool operator!=(Version& a, Version& b);

				//! MEMBERS:
				unsigned int major;
				unsigned int minor;
				unsigned int patch;
			};

			//! ACCESSORS:
			const Version& GetVersion() const;

		protected:
			//! MEMBERS:
			Engine* m_engine_instance;
			Version m_version;
	};

	////////////////////////////////////////////////////////////////////////////////
	// System::Version inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline bool operator==(System::Version& a, System::Version& b) {
		return (
			a.major == b.major &&
			a.minor == b.minor &&
			a.patch == b.patch
		);
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator!=(System::Version& a, System::Version& b) {
		return (
			a.major != b.major ||
			a.minor != b.minor ||
			a.patch != b.patch
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// System inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline System::System(Engine* engine_instance)
		: m_engine_instance(engine_instance)
		, m_version({ 0, 0, 0 }) {
	}
	/*----------------------------------------------------------------------------*/
	inline System::~System() {
	}

	/*----------------------------------------------------------------------------*/
	inline const System::Version& System::GetVersion() const {
		return m_version;
	}

} // root namespace