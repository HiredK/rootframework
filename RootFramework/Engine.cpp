#include "Engine.h"
#include "Logger.h"
#include "ScriptObject.h"
#include "Shader.h"

#include "FileSystem.h"

int root::Engine::Execute(int argc, char** argv)
{
	LOG() << "Execute root::Engine version " <<
		m_version.major << "." <<
		m_version.minor << "." <<
		m_version.patch;

	m_systems.push_back(std::shared_ptr<FileSystem>(new FileSystem(this)));

	if (Init(argc, argv))
	{
		class MainLoopTask : public Task
		{
			public:
				MainLoopTask(Engine* engine_instance, const std::string& entry_script)
					: Task(SINGLETHREADED)
					, m_engine_instance(engine_instance)
				{
					m_script = (Script*)m_engine_instance->GetSystem<FileSystem>()->Search(entry_script, true);
					m_shader = (Shader*)m_engine_instance->GetSystem<FileSystem>()->Search("shaders/default.glsl", true);
				}

				void Run()
				{
					if (FileHandle::Ready(m_script) && FileHandle::Ready(m_shader))
					{
						sf::Clock clock;
						while (m_engine_instance->IsRunning())
						{
							if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape)) {
								m_engine_instance->Shutdown();
								break;
							}

							float dt = clock.restart().asSeconds();
							m_engine_instance->Update(dt);

							m_shader->Bind();
							ScriptObject::UpdateAll(dt);
							Shader::PopStack();
						}
					}
					else {
						LOG(Logger::Error) << "Missing entry files!";
						sf::sleep(sf::seconds(1));
					}

					Shader::Unbind();
					m_engine_instance->GetTaskManager().Stop();
					m_engine_instance->Cleanup();
				}

			private:
				Engine* m_engine_instance;
				Script* m_script;
				Shader* m_shader;
		};

		auto ExtractFilename = [](const std::string& str) -> std::string {
			return str.substr(str.find_last_of("\\/") + 1);
		};

		std::string entry_script = "client.lua";
		for (int i = 1; i < argc; ++i) {
			const std::string& filename = ExtractFilename(argv[i]);
			if (FileFactory::ExtractExt(filename) == "LUA" ||
				FileFactory::ExtractExt(filename) == "LUAC") {
				entry_script = filename;
				break;
			}
		}

		if (m_engine_instance->GetSystem<FileSystem>()->Mount("sandbox")) {
			m_task_manager.Add(std::shared_ptr<Task>(new MainLoopTask(this, entry_script)));
			m_task_manager.Start();

			LOG() << "EXIT_SUCCESS";
			return EXIT_SUCCESS;
		}
	}

	LOG() << "EXIT_FAILURE";
	return EXIT_FAILURE;
}

void root::Engine::Shutdown() {
	LOG() << "Shutdown root::Engine";
	m_running = false;
}

bool root::Engine::Init(int argc, char** argv)
{
	Logger::ScopedTimer timer(LOG, "root::Engine::Init()");
	m_running = true;

	GLenum error = glewInit();
	if (error == GLEW_OK) {
		LOG() << "Initialized Glew version " << glewGetString(GLEW_VERSION);
		LOG() << "Using " << glGetString(GL_VENDOR) << " " << glGetString(GL_RENDERER);
		LOG() << "Using OpenGL Version " << glGetString(GL_VERSION);
	}
	else {
		LOG(Logger::Error) << glewGetErrorString(error);
		return false;
	}

	for (auto& system : m_systems) {
		m_running &= system->Init(argc, argv);
	}

	return m_running;
}

void root::Engine::Update(float dt)
{
	for (auto& system : m_systems) {
		system->Update(dt);
	}
}

void root::Engine::Cleanup()
{
	Logger::ScopedTimer timer(LOG, "root::Engine::Cleanup()");
	for (auto& system : m_systems) {
		system->Cleanup();
	}
}