/**
* @file TextureCube.h
* @brief
*/

#pragma once

#include "Texture.h"

namespace root
{
	class TextureCube : public Texture
	{
		public:
			//! CTOR/DTOR:
			TextureCube(const Settings& settings, unsigned int w, unsigned int h);
			virtual ~TextureCube();

			//! SERVICES:
			static void Unbind();

		protected:
			//! VIRTUALS:
			virtual void Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level = 0);
			virtual GLenum GetTarget() const;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture2D inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TextureCube::TextureCube(const Settings& settings, unsigned int w, unsigned int h) :
		Texture(settings, w, h) {
	}
	/*----------------------------------------------------------------------------*/
	inline TextureCube::~TextureCube() {
	}

	/*----------------------------------------------------------------------------*/
	inline void TextureCube::Unbind() {
		glBindTexture(GL_TEXTURE_CUBE_MAP, GL_ZERO);
	}

	/*----------------------------------------------------------------------------*/
	inline void TextureCube::Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data); // todo offset mem
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data);
		glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, 0, iformat, (GLsizei)w, (GLsizei)h, border, format, type, data);
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum TextureCube::GetTarget() const {
		return GL_TEXTURE_CUBE_MAP;
	}
}