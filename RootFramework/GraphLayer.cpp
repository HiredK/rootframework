#include "GraphLayer.h"
#include "Terrain.h"

void root::GraphLayer::DrawCurve(const dvec3& coords, const Graph::Curve& p, float width, float cap, float scale,
	const dvec2& nx, const dvec2& ny, const dvec2& lx, const dvec2& ly)
{
	const int n = p.GetSize();
	if (width * scale > 2.0f)
	{
		std::vector<Mesh::Vertex2D> vertices;
		float w = width / 2;

		dvec2 prev = dvec2();
		dvec2 curr = p.GetXY(0);
		dvec2 next = p.GetXY(1);
		float prevl = 0.0f;
		float nextl = (float)glm::length(next - curr);
		float dx, dy;

		auto DrawCurveCap = [&coords, &width, &cap, &curr, &dx, &dy, &w](float dir)
		{
			std::vector<Mesh::Vertex2D> cap_vertices;
			float f = cap / width;

			dvec2 a = (dvec2(curr.x - dy, curr.y + dx) - dvec2(coords)) * coords.z;
			dvec2 b = (dvec2(curr.x - dy * f, curr.y + dx * f) - dvec2(coords)) * coords.z;
			dvec2 c = (dvec2(curr.x + dy * f, curr.y - dx * f) - dvec2(coords)) * coords.z;
			dvec2 d = (dvec2(curr.x + dy, curr.y - dx) - dvec2(coords)) * coords.z;

			cap_vertices.push_back({ (float)a.x, (float)a.y, 0.0f, w, 0, 0, 0, 0 });
			cap_vertices.push_back({ (float)b.x, (float)b.y, 0.0f, cap / 2, 0, 0, 0, 0 });
			cap_vertices.push_back({ (float)c.x, (float)c.y, 0.0f, cap / 2, 0, 0, 0, 0 });
			cap_vertices.push_back({ (float)d.x, (float)d.y, 0.0f, w, 0, 0, 0, 0 });

			float idx = (dx * dir) * (width - cap) / width;
			float idy = (dy * dir) * (width - cap) / width;

			a = (dvec2(curr.x - dy - idx, curr.y + dx - idy) - dvec2(coords)) * coords.z;
			b = (dvec2(curr.x - dy * f - idx, curr.y + dx * f - idy) - dvec2(coords)) * coords.z;
			c = (dvec2(curr.x + dy * f - idx, curr.y - dx * f - idy) - dvec2(coords)) * coords.z;
			d = (dvec2(curr.x + dy - idx, curr.y - dx - idy) - dvec2(coords)) * coords.z;

			cap_vertices.push_back({ (float)a.x, (float)a.y, 0.0f, w, 0, 0, 0, 0 });
			cap_vertices.push_back({ (float)b.x, (float)b.y, 0.0f, w, 0, 0, 0, 0 });
			cap_vertices.push_back({ (float)c.x, (float)c.y, 0.0f, w, 0, 0, 0, 0 });
			cap_vertices.push_back({ (float)d.x, (float)d.y, 0.0f, w, 0, 0, 0, 0 });

			std::vector<unsigned short> indices { 0, 1, 4, 4, 1, 5, 1, 2, 5, 5, 2, 6, 2, 3, 7, 7, 6, 2 };
			std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&cap_vertices[0], (int)cap_vertices.size(), sizeof(Mesh::Vertex2D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
			mesh->UseDefaultVertex2D();
			mesh->Draw();
		};

		for (int i = 0; i < n; ++i)
		{
			if (i == 0) {
				dx = (float)(next.x - curr.x) / nextl;
				dy = (float)(next.y - curr.y) / nextl;
			}
			else if (i == n - 1) {
				dx = (float)(curr.x - prev.x) / prevl;
				dy = (float)(curr.y - prev.y) / prevl;
			}
			else {
				dx = (float)(curr.x - prev.x) / prevl;
				dy = (float)(curr.y - prev.y) / prevl;
				dx += (float)(next.x - curr.x) / nextl;
				dy += (float)(next.y - curr.y) / nextl;
				float l = (dx*dx + dy*dy) * 0.5f;
				dx = dx / l;
				dy = dy / l;
			}

			dx = dx * w;
			dy = dy * w;
			float s = p.GetS(i);

			if (i == 0 && cap > 0) {
				Graph::Curve* parent = p.GetAncestor();
				if (s == parent->GetS(0) && p.start->GetCurveCount() == 1) {
					DrawCurveCap(1.0f);
				}
			}

			dvec2 a = (dvec2(curr.x - dy, curr.y + dx) - dvec2(coords)) * coords.z;
			dvec2 b = (dvec2(curr.x + dy, curr.y - dx) - dvec2(coords)) * coords.z;

			vertices.push_back({ (float)a.x, (float)a.y, 0.0f, -w, 0, 0, 0, 0 });
			vertices.push_back({ (float)b.x, (float)b.y, 0.0f,  w, 0, 0, 0, 0 });

			if (i == n - 1 && cap > 0) {
				Graph::Curve* parent = p.GetAncestor();
				int pn = parent->GetSize();
				if (s == parent->GetS(pn - 1) && p.end->GetCurveCount() == 1) {
					DrawCurveCap(-1.0f);
				}
			}

			prev = curr;
			prevl = nextl;
			curr = next;
			if (i < n - 2) {
				next = p.GetXY(i + 2);
				nextl = (float)glm::length(next - curr);
			}
		}

		std::unique_ptr<Mesh> mesh = std::unique_ptr<Mesh>(new Mesh(&vertices[0], (int)vertices.size(), sizeof(Mesh::Vertex2D), GL_TRIANGLE_STRIP, GL_STATIC_DRAW));
		mesh->UseDefaultVertex2D();
		mesh->Draw();
	}
}

void root::GraphLayer::DrawCurveAltitude(const dvec3& coords, const Graph::Curve& p, float width, float nwidth, float step, bool caps,
	const dvec2& nx, const dvec2& ny, const dvec2& lx, const dvec2& ly)
{
	std::vector<Mesh::Vertex2D> vertices;
	const int n = p.GetSize();

	dvec2 prev = dvec2();
	dvec2 curr = p.GetXY(0);
	dvec2 next = p.GetXY(1);
	double prevl = 0.0f;
	double nextl = glm::length(next - curr);
	double currs = p.GetS(0);
	double currz = 50.0; // temp
	double nexts = p.GetS(1);
	double nextz = 50.0; // temp
	double w = width / 2;
	double dx, dy;

	auto DrawCurveCap = [&coords, &width, &nwidth, &curr, &currz, &dx, &dy](float dir)
	{
		std::vector<Mesh::Vertex2D> cap_vertices;

		dvec2 a = (dvec2(curr.x - dy, curr.y + dx) - dvec2(coords)) * coords.z;
		dvec2 b = (dvec2(curr.x - dy / nwidth, curr.y + dx / nwidth) - dvec2(coords)) * coords.z;
		dvec2 c = (dvec2(curr.x + dy / nwidth, curr.y - dx / nwidth) - dvec2(coords)) * coords.z;
		dvec2 d = (dvec2(curr.x + dy, curr.y - dx) - dvec2(coords)) * coords.z;

		cap_vertices.push_back({ (float)a.x, (float)a.y, (float)currz, (float)nwidth, 0, 0, 0, 0 });
		cap_vertices.push_back({ (float)b.x, (float)b.y, (float)currz, 1.0f, 0, 0, 0, 0 });
		cap_vertices.push_back({ (float)c.x, (float)c.y, (float)currz, 1.0f, 0, 0, 0, 0 });
		cap_vertices.push_back({ (float)d.x, (float)d.y, (float)currz, (float)nwidth, 0, 0, 0, 0 });

		double idx = (dx * dir) * (nwidth - 1) / nwidth;
		double idy = (dy * dir) * (nwidth - 1) / nwidth;

		a = (dvec2(curr.x - dy - idx, curr.y + dx - idy) - dvec2(coords)) * coords.z;
		b = (dvec2(curr.x - dy / nwidth - idx, curr.y + dx / nwidth - idy) - dvec2(coords)) * coords.z;
		c = (dvec2(curr.x + dy / nwidth - idx, curr.y - dx / nwidth - idy) - dvec2(coords)) * coords.z;
		d = (dvec2(curr.x + dy - idx, curr.y - dx - idy) - dvec2(coords)) * coords.z;

		cap_vertices.push_back({ (float)a.x, (float)a.y, (float)currz, (float)nwidth, 0, 0, 0, 0 });
		cap_vertices.push_back({ (float)b.x, (float)b.y, (float)currz, (float)nwidth, 0, 0, 0, 0 });
		cap_vertices.push_back({ (float)c.x, (float)c.y, (float)currz, (float)nwidth, 0, 0, 0, 0 });
		cap_vertices.push_back({ (float)d.x, (float)d.y, (float)currz, (float)nwidth, 0, 0, 0, 0 });

		std::vector<unsigned short> indices{ 0, 1, 4, 4, 1, 5, 1, 2, 5, 5, 2, 6, 2, 3, 7, 7, 6, 2 };
		std::shared_ptr<Mesh> mesh = std::shared_ptr<Mesh>(new Mesh(&cap_vertices[0], (int)cap_vertices.size(), sizeof(Mesh::Vertex2D), GL_TRIANGLES, GL_STATIC_DRAW, &indices[0], (int)indices.size(), GL_UNSIGNED_SHORT));
		mesh->UseDefaultVertex2D();
		mesh->Draw();
	};

	for (int i = 0; i < n; ++i)
	{
		if (i == 0) {
			dx = (next.x - curr.x) / nextl;
			dy = (next.y - curr.y) / nextl;
		}
		else if (i == n - 1) {
			dx = (curr.x - prev.x) / prevl;
			dy = (curr.y - prev.y) / prevl;
		}
		else {
			dx = (curr.x - prev.x) / prevl;
			dy = (curr.y - prev.y) / prevl;
			dx += (next.x - curr.x) / nextl;
			dy += (next.y - curr.y) / nextl;
			double l = (dx*dx + dy*dy) * 0.5f;
			dx = dx / l;
			dy = dy / l;
		}

		dx = dx * w;
		dy = dy * w;

		if (i == 0) {
			Graph::Curve* parent = p.GetAncestor();
			if (caps && currs == parent->GetS(0) && Extremity(*parent, parent->start)) {
				DrawCurveCap(1.0f);
			}
		}

		dvec2 a = (dvec2(curr.x - dy, curr.y + dx) - dvec2(coords)) * coords.z;
		dvec2 b = (dvec2(curr.x + dy, curr.y - dx) - dvec2(coords)) * coords.z;

		vertices.push_back({ (float)a.x, (float)a.y, (float)currz, -nwidth, 0, 0, 0, 0 });
		vertices.push_back({ (float)b.x, (float)b.y, (float)currz,  nwidth, 0, 0, 0, 0 });

		if (i == n - 1) {
			Graph::Curve* parent = p.GetAncestor();
			int pn = parent->GetSize();
			if (caps && currs == parent->GetS(pn - 1) && Extremity(*parent, parent->end)) {
				DrawCurveCap(-1.0f);
			}
		}

		prev = curr;
		prevl = nextl;
		curr = next;
		currs = nexts;
		currz = nextz;
		if (i < n - 2) {
			next = p.GetXY(i + 2);
			nextl = glm::length(next - curr);
			nexts = p.GetS(i + 2);
			nextz = 50.0; // temp
		}
	}

	std::unique_ptr<Mesh> mesh = std::unique_ptr<Mesh>(new Mesh(&vertices[0], (int)vertices.size(), sizeof(Mesh::Vertex2D), GL_TRIANGLE_STRIP, GL_STATIC_DRAW));
	mesh->UseDefaultVertex2D();
	mesh->Draw();
}

bool root::GraphLayer::Extremity(const Graph::Curve& p, Graph::Node* n)
{
	if (n->GetCurveCount() == 1) {
		return true;
	}

	for (int i = 0; i < n->GetCurveCount(); ++i) {
		Graph::Curve* c = n->GetCurve(i);
		if (&p != c) {
			return false;
		}
	}

	return true;
}