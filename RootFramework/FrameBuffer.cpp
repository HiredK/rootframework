#include "FrameBuffer.h"
#include "Texture2D.h"
#include "Logger.h"

void root::FrameBuffer::Attach(Texture* texture, GLenum attachment, int level, int layer)
{
	FrameBuffer::Bind();

	if (layer >= 0) {
		glFramebufferTextureLayer(GL_FRAMEBUFFER, attachment, texture->GetID(), level, layer);
	}
	else {
		if (texture->GetSettings().target == GL_TEXTURE_CUBE_MAP) {
			glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, attachment, GL_TEXTURE_CUBE_MAP_POSITIVE_X + level, texture->GetID(), 0);
		}
		else {
			glFramebufferTexture(GL_FRAMEBUFFER, attachment, texture->GetID(), level);
		}
	}

	bool is_color_attachment = bool(attachment >= GL_COLOR_ATTACHMENT0 && attachment <= GL_COLOR_ATTACHMENT15);
	if (is_color_attachment) {
		m_attachments[m_num_attachments] = attachment;
		m_num_attachments++;
	}
}

void* root::FrameBuffer::ReadPixels(Texture* texture, GLenum attachment, int x, int y, int w, int h)
{
	FrameBuffer::Bind();
	size_t type_size = (texture->GetSettings().type == GL_FLOAT) ? sizeof(float) : sizeof(unsigned char);
	void* data = nullptr;

	switch (texture->GetSettings().format) {
		case GL_ALPHA:
		case GL_DEPTH_COMPONENT:
		case GL_R:
			data = malloc(type_size * w * h);
			break;
		case GL_RG:
			data = malloc(type_size * w * h * 2);
			break;
		case GL_RGB:
			data = malloc(type_size * w * h * 3);
			break;
		case GL_RGBA:
			data = malloc(type_size * w * h * 4);
			break;
	}

	glReadBuffer(attachment);
	texture->Bind();

	glReadPixels(x, y, w, h, texture->GetSettings().format, texture->GetSettings().type, data);
	return data;
}

void root::FrameBuffer::BindOutput()
{
	FrameBuffer::Bind();

	if (m_num_attachments > 1) {
		glDrawBuffers(m_num_attachments, m_attachments);
	}
	else {
		glDrawBuffer(GL_BACK);
	}
}

void root::FrameBuffer::BindOutput(GLenum attachment)
{
	FrameBuffer::Bind();

	glDrawBuffer(attachment);
}

void root::FrameBuffer::Clear()
{
	m_attachments = new GLuint[GetMaxColorAttachments()];
	m_num_attachments = 0;
}

unsigned int root::FrameBuffer::GetMaxColorAttachments()
{
	GLint value;
	glGetIntegerv(GL_MAX_COLOR_ATTACHMENTS, &value);
	return static_cast<unsigned int>(value);
}