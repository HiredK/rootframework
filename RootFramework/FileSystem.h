/**
* @file FileSystem.h
* @brief
*/

#pragma once

#include "System.h"
#include "Task.h"
#include "FileHandle.h"

////////////////////////////////////////
// Current Version:

#define FILESYSTEM_VERSION_MAJOR 1
#define FILESYSTEM_VERSION_MINOR 0
#define FILESYSTEM_VERSION_PATCH 0

namespace root
{
	class FileSystem : public System
	{
		public:
			//! TYPEDEF/ENUMS:
			enum { SingleThreaded, Background };

			//! CTOR/DTOR:
			FileSystem(Engine* engine_instance);
			virtual ~FileSystem();

			//! VIRTUALS:
			virtual bool Init(int argc, char** argv);
			virtual void Update(float dt);
			virtual void Cleanup();

			struct Group
			{
				//! TYPEDEF/ENUMS:
				typedef std::map<std::string, FileHandle::List> LookupTable;

				//! CTOR/DTOR:
				Group(const std::string& path);
				virtual ~Group();

				//! SERVICES:
				void SetAllocate(bool alloc);

				//! MEMBERS:
				LookupTable files[2];
				std::string path;
			};

			//! SERVICES:
			Group* Mount(const std::string& path);
			bool Unmount(const std::string& path);
			bool Unmount(Group* group);
			unsigned int GetNumberOfGroups() const;
			Group* GetGroup(unsigned int i);
			FileHandle* Search(const std::string& path, bool allocate);
			FileHandle* Search(const std::string& path);
			FileHandle* Write(const std::string& path, const void* data, size_t size, bool allocate);
			FileHandle* Write(const std::string& path, const void* data, size_t size);
			FileHandle* Write(const std::string& path, std::string data, bool allocate);
			FileHandle* Write(const std::string& path, std::string data);

		private:
			//! TYPEDEF/ENUMS:
			typedef std::vector<std::shared_ptr<Group>> GroupList;

			//! SERVICES:
			Group* Discover(Group* group, const std::string& path, bool recursive);
			Group* Discover(Group* group, const std::string& path);

			//! MEMBERS:
			GroupList m_groups;
			std::recursive_mutex m_mutex;
			float m_current_delay;
			bool m_interrupt;
	};

	////////////////////////////////////////////////////////////////////////////////
	// FileSystem::Group inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FileSystem::Group::Group(const std::string& path)
		: path(path) {
	}
	/*----------------------------------------------------------------------------*/
	inline FileSystem::Group::~Group() {
		for (unsigned int process_type = 0; process_type < 2; ++process_type) {
			files[process_type].clear();
		}
	}
	/*----------------------------------------------------------------------------*/
	inline void FileSystem::Group::SetAllocate(bool alloc) {
		for (unsigned int process_type = 0; process_type < 2; ++process_type) {
			for (auto& i : files[process_type])
			for (auto& j : i.second) {
				j->SetAllocate(alloc);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// FileSystem inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline FileSystem::FileSystem(Engine* engine_instance)
		: System(engine_instance)
		, m_current_delay(1.0f)
		, m_interrupt(false) {

		m_version.major = FILESYSTEM_VERSION_MAJOR;
		m_version.minor = FILESYSTEM_VERSION_MINOR;
		m_version.patch = FILESYSTEM_VERSION_PATCH;
	}
	/*----------------------------------------------------------------------------*/
	inline FileSystem::~FileSystem() {
	}

	/*----------------------------------------------------------------------------*/
	inline FileSystem::Group* FileSystem::Discover(Group* group, const std::string& path) {
		return Discover(group, path, true);
	}
	/*----------------------------------------------------------------------------*/
	inline bool FileSystem::Unmount(Group* group) {
		return Unmount(group->path);
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int FileSystem::GetNumberOfGroups() const {
		return static_cast<unsigned int>(m_groups.size());
	}
	/*----------------------------------------------------------------------------*/
	inline FileSystem::Group* FileSystem::GetGroup(unsigned int i) {
		return m_groups[i].get();
	}
	/*----------------------------------------------------------------------------*/
	inline FileHandle* FileSystem::Search(const std::string& path) {
		return Search(path, false);
	}

	/*----------------------------------------------------------------------------*/
	inline FileHandle* FileSystem::Write(const std::string& path, const void* data, size_t size) {
		return Write(path, data, size, false);
	}
	/*----------------------------------------------------------------------------*/
	inline FileHandle* FileSystem::Write(const std::string& path, std::string data, bool allocate) {
		return Write(path, &data[0], data.size(), allocate);
	}
	/*----------------------------------------------------------------------------*/
	inline FileHandle* FileSystem::Write(const std::string& path, std::string data) {
		return Write(path, data, false);
	}

} // root namespace