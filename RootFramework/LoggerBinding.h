/**
* @file LoggerBinding.h
* @brief
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "Logger.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// LoggerBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope LoggerBinding()
	{
		return (
			luabind::namespace_("root")
			[
				luabind::class_<Logger>("Logger")
				.enum_("Level")
				[
					luabind::value("Info", Logger::Info),
					luabind::value("Warning", Logger::Warning),
					luabind::value("Error", Logger::Error),
					luabind::value("Timer", Logger::Timer)
				]
			]
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// LoggerBinding_Print:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static int LoggerBinding_Print(lua_State* L)
	{
		int n = lua_gettop(L);
		int i;

		lua_getglobal(L, "tostring");
		std::stringstream ss;

		for (i = 1; i <= n; i++)
		{
			const char* s;
			lua_pushvalue(L, -1);
			lua_pushvalue(L, i);
			lua_call(L, 1, 1);

			s = lua_tostring(L, -1);
			if (s == NULL) {
				LOG(Logger::Error) << "lua_tostring failed!";
			}
			else {
				ss << ((i == 1) ? "" : " ") << s;
			}

			lua_pop(L, 1);
		}

		LOG() << ss.str();
		return 0;
	}
	/*----------------------------------------------------------------------------*/
	inline void LoggerBinding_OverridePrintLib(lua_State *L) {
		static const struct luaL_Reg printlib[] = {
			{ "print", LoggerBinding_Print },
			{ NULL, NULL }
		};

		lua_getglobal(L, "_G");
		luaL_register(L, NULL, printlib);
		lua_pop(L, 1);
	}
}