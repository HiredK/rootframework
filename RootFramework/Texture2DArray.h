/**
* @file Texture2DArray.h
* @brief
*/

#pragma once

#include "Texture.h"

namespace root
{
	class Texture2DArray : public Texture
	{
		public:
			//! CTOR/DTOR:
			Texture2DArray(const Settings& settings, unsigned int w, unsigned int h, unsigned int depth);
			virtual ~Texture2DArray();

			//! SERVICES:
			static void Unbind();

		protected:
			//! VIRTUALS:
			virtual void Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level = 0);
			virtual GLenum GetTarget() const;

			//! MEMBERS:
			unsigned int m_depth;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture2DArray inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture2DArray::Texture2DArray(const Settings& settings, unsigned int w, unsigned int h, unsigned int depth)
		: Texture(settings, w, h), m_depth(depth) {
	}
	/*----------------------------------------------------------------------------*/
	inline Texture2DArray::~Texture2DArray() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture2DArray::Unbind() {
		glBindTexture(GL_TEXTURE_2D_ARRAY, GL_ZERO);
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture2DArray::Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level) {
		if (level == 0) {
			glTexImage3D(GetTarget(), 0, iformat, (GLsizei)w, (GLsizei)h, depth, border, format, type, nullptr);
		}

		if (data != nullptr) {
			glTexSubImage3D(GetTarget(), 0, 0, 0, level, w, h, 1, format, type, data);
		}
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum Texture2DArray::GetTarget() const {
		return GL_TEXTURE_2D_ARRAY;
	}
}