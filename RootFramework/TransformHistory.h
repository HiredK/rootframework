/**
* @file TransformHistory.h
* @brief Todo
*/

#pragma once

#include <RakNet/RakMemoryOverride.h>
#include <RakNet/DS_Queue.h>
#include "Transform.h"

namespace root
{
	class TransformHistory
	{
		public:
			//! TYPEDEF/ENUMS:
			enum ReadResult { READ_OLDEST, VALUES_UNCHANGED, INTERPOLATED };

			//! CTOR/DTOR:
			TransformHistory(unsigned int max_write_interval = 10, unsigned int max_history_time = 1000);
			virtual ~TransformHistory();

			//! SERVICES:
			void Write(const Transform& transform, unsigned int t);
			void Overwrite(const Transform& transform, unsigned int t);
			ReadResult Read(Transform& transform, unsigned int when, unsigned int t);
			void Clear();

		private:
			struct Cell {
				dvec3 position; // lerp
				dquat rotation; // slerp
				unsigned int t; // alpha
			};

			//! MEMBERS:
			DataStructures::Queue<Cell> m_history;
			unsigned int m_max_history_length;
			unsigned int m_write_interval;
	};

	////////////////////////////////////////////////////////////////////////////////
	// TransformHistory inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TransformHistory::TransformHistory(unsigned int max_write_interval, unsigned int max_history_time)
		: m_max_history_length(max_history_time / max_write_interval + 1)
		, m_write_interval(max_write_interval) {
		m_history.ClearAndForceAllocation(m_max_history_length + 1, _FILE_AND_LINE_);
	}
	/*----------------------------------------------------------------------------*/
	inline TransformHistory::~TransformHistory() {
		Clear();
	}

	/*----------------------------------------------------------------------------*/
	inline void TransformHistory::Clear() {
		m_history.Clear(_FILE_AND_LINE_);
	}
}