#include "Image.h"
#include "Logger.h"

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

REGISTER_FILE_IMPL(root::Image, 8, "jpg", "png", "tga", "bmp", "psd", "gif", "hdr", "pic");

bool root::Image::CreateData()
{
	if (m_pixels.empty()) {
		if (FileHandle::CreateData()) {
			const unsigned char* buffer = static_cast<const unsigned char*>((const void*)&m_data[0]);
			void* ptr = stbi_load_from_memory(buffer, static_cast<int>(m_data.size()), &m_w, &m_h, &m_channels, STBI_rgb_alpha);
			m_pixels.resize(m_w * m_h * m_channels);

			FileHandle::DeleteData();

			if (ptr && m_w && m_h) {
				memcpy(&m_pixels[0], ptr, m_pixels.size());
				stbi_image_free(ptr);
			}
			else {
				LOG(Logger::Error) << stbi_failure_reason();
				DeleteData();
				return false;
			}
		}
	}

	return true;
}

bool root::Image::ReadImageInfo()
{
	if (FileHandle::CreateData()) {
		const unsigned char* buffer = static_cast<const unsigned char*>((const void*)&m_data[0]);
		stbi_info_from_memory(buffer, static_cast<int>(m_data.size()), &m_w, &m_h, &m_channels);
		FileHandle::DeleteData();
		return true;
	}

	return false;
}

void root::Image::DeleteData()
{
	m_pixels.clear();
	m_pixels.shrink_to_fit();
	m_channels = 0;
	m_w = 0;
	m_h = 0;
}