/**
* @file Shader.h
* @brief
*/


#pragma once

#include "FileHandle.h"
#include "Color.h"
#include "Math.h"
#include <GL/glew.h>

#include "Logger.h" // breaks in terrain if removed, to fix

namespace root
{
	class Shader : public FileHandle
	{
		REGISTER_FILE(Shader)

		public:
			//! CTOR/DTOR:
			Shader(Engine* engine_instance, const std::string& path);
			virtual ~Shader();

			//! SERVICES:
			void Bind();
			static void PopStack();
			static void Unbind();
			static Shader* Get();

			//! UNIFORMS:
			void UniformMat4Array(const std::string& name, unsigned int count, const float* data);
			void UniformMat4(const std::string& name, const float* data);
			void UniformMat4(const std::string& name, const dmat4& matrix);

			void UniformMat3Array(const std::string& name, unsigned int count, const float* data);
			void UniformMat3(const std::string& name, const float* data);
			void UniformMat3(const std::string& name, const dmat3& matrix);

			void Uniform(const std::string& name, float v1);
			void Uniform(const std::string& name, float v1, float v2);
			void Uniform(const std::string& name, float v1, float v2, float v3);
			void Uniform(const std::string& name, float v1, float v2, float v3, float v4);
			void Uniform(const std::string& name, const dvec2& v);
			void Uniform(const std::string& name, const dvec3& v);
			void Uniform(const std::string& name, const dvec4& v);
			void Uniform(const std::string& name, const Color& color);

			void Sampler(const std::string& name, int sampler); // uniformi
			GLuint GetAttribLocation(const std::string& name);

			//! SERVICES:
			void OverrideDefine(const std::string& name, const std::string& value);
			void OverrideTransformFeedbackVaryings(const std::vector<const char*>& varyings, GLenum mode = GL_INTERLEAVED_ATTRIBS);

			//! VIRTUALS:
			virtual bool IsProcessBackground();

		protected:
			//! VIRTUALS:
			virtual bool CreateData();
			virtual void DeleteData();

		private:
			//! SERVICES:
			void LinkProgram(const std::string& data);

			//! MEMBERS:
			static std::vector<Shader*> m_stack;
			std::map<int, GLuint> m_stages;
			GLuint m_id;

			// transform feedback varyings elements
			std::vector<const char*> m_tf_varyings;
			GLenum m_tf_mode;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Shader inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Shader::Shader(Engine* engine_instance, const std::string& path)
		: FileHandle(engine_instance, path)
		, m_id(GL_ZERO)
		, m_tf_mode(GL_INTERLEAVED_ATTRIBS) {
	}
	/*----------------------------------------------------------------------------*/
	inline Shader::~Shader() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Shader::Bind() {
		if (m_stack.empty() || m_path != m_stack.back()->GetPath()) {
			m_stack.push_back(this);
			glUseProgram(m_id);
		}
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::PopStack() {
		if (m_stack.size() > 1) {
			m_stack.pop_back();
			glUseProgram(m_stack.back()->m_id);
		}
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Unbind() {
		m_stack.clear();
		glUseProgram(GL_ZERO);
	}
	/*----------------------------------------------------------------------------*/
	inline Shader* Shader::Get() {
		return m_stack.back();
	}

	/*----------------------------------------------------------------------------*/
	inline void Shader::UniformMat4Array(const std::string& name, unsigned int count, const float* data) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniformMatrix4fv(loc, count, false, data);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::UniformMat4(const std::string& name, const float* data) {
		UniformMat4Array(name, 1, data);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::UniformMat4(const std::string& name, const dmat4& matrix) {
		UniformMat4(name, glm::value_ptr(mat4(matrix)));
	}

	/*----------------------------------------------------------------------------*/
	inline void Shader::UniformMat3Array(const std::string& name, unsigned int count, const float* data) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniformMatrix3fv(loc, count, false, data);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::UniformMat3(const std::string& name, const float* data) {
		UniformMat3Array(name, 1, data);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::UniformMat3(const std::string& name, const dmat3& matrix) {
		UniformMat3(name, glm::value_ptr(mat3(matrix)));
	}

	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, float v1) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniform1f(loc, v1);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, float v1, float v2) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniform2f(loc, v1, v2);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, float v1, float v2, float v3) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniform3f(loc, v1, v2, v3);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, float v1, float v2, float v3, float v4) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniform4f(loc, v1, v2, v3, v4);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, const dvec2& v) {
		Uniform(name, (float)v.x, (float)v.y);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, const dvec3& v) {
		Uniform(name, (float)v.x, (float)v.y, (float)v.z);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, const dvec4& v) {
		Uniform(name, (float)v.x, (float)v.y, (float)v.z, (float)v.w);
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Uniform(const std::string& name, const Color& color) {
		Uniform(name, dvec4(Color::ToFloat(color.r), Color::ToFloat(color.g), Color::ToFloat(color.b), Color::ToFloat(color.a)));
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader::Sampler(const std::string& name, int sampler) {
		GLuint loc = glGetUniformLocation(m_id, name.c_str());
		glUniform1i(loc, sampler);
	}
	/*----------------------------------------------------------------------------*/
	inline GLuint Shader::GetAttribLocation(const std::string& name) {
		GLuint loc = glGetAttribLocation(m_id, name.c_str());
		return loc;
	}

	/*----------------------------------------------------------------------------*/
	inline bool Shader::IsProcessBackground() {
		return false;
	}
}