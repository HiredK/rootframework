/**
* @file Script.h
* @brief Implements a LuaJIT/Luabind scripting interface.
*/

#pragma once

#include "FileHandle.h"
#include "Logger.h"

struct lua_State;

namespace root
{
	class Script : public FileHandle
	{
		friend class ScriptObject;
		REGISTER_FILE(Script)

		public:
			//! CTOR/DTOR:
			Script(Engine* engine_instance, const std::string& path);
			virtual ~Script();

			//! SERVICES:
			bool DoString(const std::string& str);
			void CollectGarbage();

			//! VIRTUALS:
			virtual bool IsProcessBackground();

		protected:
			//! SERVICES:
			void OnLogReceived(Logger::Level level, const std::string& message);

			//! VIRTUALS:
			virtual bool CreateData();
			virtual void DeleteData();

			//! MEMBERS:
			lua_State* m_state;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Script inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Script::Script(Engine* engine_instance, const std::string& path)
		: FileHandle(engine_instance, path), m_state(nullptr)
	{
		LOG.AddCallback(std::bind(&Script::OnLogReceived, this, std::placeholders::_1, std::placeholders::_2));
	}
	/*----------------------------------------------------------------------------*/
	inline Script::~Script() {
	}

	/*----------------------------------------------------------------------------*/
	inline bool Script::IsProcessBackground() {
		return false;
	}

} // root namespace