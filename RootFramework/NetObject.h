/**
* @file NetObject.h
* @brief Base class for all replicated objects shared across a network.
*/

#pragma once

#include <RakNet/RakPeerInterface.h>
#include <RakNet/MessageIdentifiers.h>
#include <RakNet/NetworkIDManager.h>
#include <RakNet/ReplicaManager3.h>
#include <RakNet/GetTime.h>

namespace root
{
	class NetObject : public RakNet::Replica3
	{
		friend class Network;

		public:
			//! CTORS/DTORS:
			NetObject(Network* network);
			virtual ~NetObject();

			//! ALLOCATION ID:
			virtual void WriteAllocationID(RakNet::Connection_RM3* connection, RakNet::BitStream* bs) const;

			//! (DE)SERIALIZE CTOR:
			virtual void SerializeConstruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection);
			virtual bool DeserializeConstruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection);

			//! (DE)SERIALIZE DTOR:
			virtual void SerializeDestruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection);
			virtual bool DeserializeDestruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection);

			//! (DE)SERIALIZE:
			virtual RakNet::RM3SerializationResult Serialize(RakNet::SerializeParameters* parameters);
			virtual void Deserialize(RakNet::DeserializeParameters* parameters);

			//! RM3 QUERIES:
			virtual RakNet::RM3ConstructionState QueryConstruction(RakNet::Connection_RM3* connection, RakNet::ReplicaManager3* rm3);
			virtual bool QueryRemoteConstruction(RakNet::Connection_RM3* connection);
			virtual RakNet::RM3QuerySerializationResult QuerySerialization(RakNet::Connection_RM3* connection);
			virtual RakNet::RM3ActionOnPopConnection QueryActionOnPopConnection(RakNet::Connection_RM3* connection) const;

			//! RM3 DEALLOCATION:
			virtual void DeallocReplica(RakNet::Connection_RM3* connection);

			//! ACCESSORS:
			Network* GetNetwork() const;

		protected:
			//! MEMBERS:
			Network* m_network;
	};

	////////////////////////////////////////////////////////////////////////////////
	// NetObject inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline NetObject:: NetObject(Network* network)
		: m_network(network) {
	}
	/*----------------------------------------------------------------------------*/
	inline NetObject::~NetObject() {
	}

	/*----------------------------------------------------------------------------*/
	inline Network* NetObject::GetNetwork() const {
		return m_network;
	}
}