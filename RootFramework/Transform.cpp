#include "Transform.h"

dmat4 root::Transform::ComputeMatrix()
{
	dmat4 matrix = glm::translate(dmat4(), m_position) * glm::mat4_cast(m_rotation) * glm::scale(dmat4(), m_scale);
	return matrix;
}