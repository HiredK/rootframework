#include "TextRenderer.h"
#include "Texture2D.h"
#include "Shader.h"

#define FONTSTASH_IMPLEMENTATION
#include <fontstash.h>

root::TextRenderer::TextRenderer(int w, int h, unsigned char flags)
{
	FONSparams params = { w, h, flags };

	// alloc texture shared ptr user data
	params.userPtr = std::malloc(sizeof(std::shared_ptr<Texture>));
	std::memset(params.userPtr, 0, sizeof(std::shared_ptr<Texture>));

	params.renderCreate = RenderCreate;
	params.renderResize = RenderResize;
	params.renderUpdate = RenderUpdate;
	params.renderDraw = RenderDraw;
	params.renderDelete = RenderDelete;

	m_stash = fonsCreateInternal(&params);
}

void root::TextRenderer::RenderText(int font, float x, float y, float size, const std::string& text, const Color& color)
{
	fonsPushState(m_stash);
	fonsSetAlign(m_stash, FONS_ALIGN_LEFT | FONS_ALIGN_TOP);
	fonsSetColor(m_stash, color.ToInteger());
	fonsSetFont(m_stash, font);
	fonsSetSize(m_stash, size);

	//fonsSetBlur(m_stash, 1.5f);

	fonsDrawText(m_stash, x, y, text.c_str(), 0);
	fonsPopState(m_stash);
}

float root::TextRenderer::MeasureText(int font, float size, const std::string& text)
{
	fonsPushState(m_stash);
	fonsSetAlign(m_stash, FONS_ALIGN_LEFT | FONS_ALIGN_TOP);
	fonsSetFont(m_stash, font);
	fonsSetSize(m_stash, size);

	float length = fonsTextBounds(m_stash, 0, 0, text.c_str(), 0, 0);
	fonsPopState(m_stash);
	return length;
}

int root::TextRenderer::BuildFont(FileHandle* file)
{
	int font = FONS_INVALID;
	if (FileHandle::Ready(file)) {
		font = fonsAddFontMem(m_stash, file->GetPath().c_str(), (unsigned char*)file->GetData(), (int)file->GetSize(), 0);
	}

	return font;
}

int root::TextRenderer::RenderCreate(void* user_data, int w, int h)
{
	// free already loaded texture if exists
	if ((*static_cast<std::shared_ptr<Texture>*>(user_data))) {
		RenderDelete(user_data);
	}

	// create a texture object from empty
	std::shared_ptr<Texture> ptr = Texture::FromEmpty(w, h);

	// assign created texture to user data
	(*static_cast<std::shared_ptr<Texture>*>(user_data)) = ptr;
	return 1;
}

int root::TextRenderer::RenderResize(void* user_data, int w, int h)
{
	return RenderCreate(user_data, w, h);
}

void root::TextRenderer::RenderUpdate(void* user_data, int* rect, const unsigned char* data)
{
	Texture* ptr = (*static_cast<std::shared_ptr<Texture>*>(user_data)).get();
	int w = rect[2] - rect[0];
	int h = rect[3] - rect[1];

	glPushClientAttrib(GL_CLIENT_PIXEL_STORE_BIT);
	ptr->Bind();

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	glPixelStorei(GL_UNPACK_ROW_LENGTH, ptr->GetW());
	glPixelStorei(GL_UNPACK_SKIP_PIXELS, rect[0]);
	glPixelStorei(GL_UNPACK_SKIP_ROWS, rect[1]);
	glTexSubImage2D(GL_TEXTURE_2D, 0, rect[0], rect[1], w, h, GL_ALPHA, GL_UNSIGNED_BYTE, data);
	glPopClientAttrib();
}

void root::TextRenderer::RenderDraw(void* user_data, const float* verts, const float* tcoords, const unsigned int* colors, int nverts)
{
	Texture* ptr = (*static_cast<std::shared_ptr<Texture>*>(user_data)).get();
	Shader::Get()->Uniform("s_Tex0", 0);
	ptr->Bind();

	{
		// v2f position:
		GLuint loc = Shader::Get()->GetAttribLocation("vs_Position");
		glEnableVertexAttribArray(loc);
		glVertexAttribPointer(loc, 2, GL_FLOAT, false, 0, verts);
	}
	{
		// v2f texcoord:
		GLuint loc = Shader::Get()->GetAttribLocation("vs_TexCoord");
		glEnableVertexAttribArray(loc);
		glVertexAttribPointer(loc, 2, GL_FLOAT, false, 0, tcoords);
	}
	{
		// v4ub color:
		GLuint loc = Shader::Get()->GetAttribLocation("vs_Color");
		glEnableVertexAttribArray(loc);
		glVertexAttribPointer(loc, 4, GL_UNSIGNED_BYTE, true, 0, colors);
	}

	glDrawArrays(GL_TRIANGLES, 0, nverts);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
}

void root::TextRenderer::RenderDelete(void* user_data)
{
	// assign null to trigger destruction
	(*static_cast<std::shared_ptr<Texture>*>(user_data)) = nullptr;
	std::free(user_data);
}