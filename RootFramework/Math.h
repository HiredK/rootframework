/**
* @file MathDef.h
* @brief Defines all mathematic elements, includes GLM.
* http://glm.g-truc.net/
*/

#pragma once

enum Axis { PosX, NegX, PosY, NegY, PosZ, NegZ };

#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/random.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/matrix_decompose.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

typedef glm::ivec2 ivec2;
typedef glm::dvec2 dvec2;
typedef glm::vec2 vec2;

typedef glm::ivec3 ivec3;
typedef glm::dvec3 dvec3;
typedef glm::vec3 vec3;

typedef glm::ivec4 ivec4;
typedef glm::dvec4 dvec4;
typedef glm::vec4 vec4;

typedef glm::dmat2 dmat2;
typedef glm::mat2 mat2;

typedef glm::dmat3 dmat3;
typedef glm::mat3 mat3;

typedef glm::dmat4 dmat4;
typedef glm::mat4 mat4;

typedef glm::dquat dquat;
typedef glm::quat quat;

#include "Box2D.h"
#include "Box3D.h"