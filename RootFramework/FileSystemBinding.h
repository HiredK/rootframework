/**
* @file FileSystemBinding.h
* @brief
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "FileSystem.h"
#include "Image.h"
#include "Model.h"
#include "Script.h"
#include "Shader.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// FileHandleBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	std::ostream& operator<<(std::ostream& os, const FileHandle& handle) {
		return os << handle.GetPath();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope FileHandleBinding()
	{
		return (
			luabind::class_<FileHandle>("FileHandle")
			.def(luabind::tostring(luabind::const_self))
			.property("path", &FileHandle::GetPath)
			.def("set_allocate", &FileHandle::SetAllocate)
			.def("reload", &FileHandle::Reload)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ImageBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ImageBinding()
	{
		return (
			luabind::class_<Image, FileHandle>("Image")
			.property("w", &Image::GetW)
			.property("h", &Image::GetH)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ModelBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ModelBinding()
	{
		return (
			luabind::class_<Model, luabind::bases<FileHandle, Transform>>("Model")
			.def("draw", (void(Model::*)(Camera&)) &Model::Draw)
			.def("draw", (void(Model::*)()) &Model::Draw)
			.def("get_node_transform", (std::shared_ptr<Transform>(Model::*)(const std::string&)) &Model::GetNodeTransform)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ScriptBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ScriptBinding()
	{
		return (
			luabind::class_<Script, FileHandle>("Script")
			.def("do_string", (bool(Script::*)(const std::string&)) &Script::DoString)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ShaderBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline void Shader_UniformMat4Array(Shader& self, const std::string& name, const luabind::object& data)
	{
		std::vector<mat4> raw_matrices;
		for (luabind::iterator it(data), end; it != end; ++it) {
			raw_matrices.push_back(mat4(luabind::object_cast<dmat4>(*it)));
		}

		self.UniformMat4Array(name, (unsigned int)raw_matrices.size(),
			reinterpret_cast<float*>(&raw_matrices[0]));
	}
	/*----------------------------------------------------------------------------*/
	inline void Shader_UniformMat3Array(Shader& self, const std::string& name, const luabind::object& data)
	{
		std::vector<mat3> raw_matrices;
		for (luabind::iterator it(data), end; it != end; ++it) {
			raw_matrices.push_back(mat3(luabind::object_cast<dmat3>(*it)));
		}

		self.UniformMat3Array(name, (unsigned int)raw_matrices.size(),
			reinterpret_cast<float*>(&raw_matrices[0]));
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope ShaderBinding()
	{
		return (
			luabind::class_<Shader, FileHandle>("Shader")
			.scope
			[
				luabind::def("get", &Shader::Get),
				luabind::def("pop_stack", &Shader::PopStack)
			]
			.def("uniform", (void(Shader::*)(const std::string&, const dmat3&)) &Shader::UniformMat3)
			.def("uniform", (void(Shader::*)(const std::string&, const dmat4&)) &Shader::UniformMat4)
			.def("uniform", (void(Shader::*)(const std::string&, float)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, float, float)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, float, float, float)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, float, float, float, float)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, const dvec2&)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, const dvec3&)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, const dvec4&)) &Shader::Uniform)
			.def("uniform", (void(Shader::*)(const std::string&, const Color&)) &Shader::Uniform)
			.def("uniform_mat4_array", &Shader_UniformMat4Array)
			.def("uniform_mat3_array", &Shader_UniformMat3Array)
			.def("sampler", (void(Shader::*)(const std::string&, bool)) &Shader::Sampler)
			.def("sampler", (void(Shader::*)(const std::string&, int)) &Shader::Sampler)

			.def("override_define", &Shader::OverrideDefine)
			.def("bind", &Shader::Bind)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// FileSystemBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static luabind::object EnumerateFiles(lua_State* L, const std::string& path)
	{
		luabind::object table = luabind::newtable(L);
		char** rc = PHYSFS_enumerateFiles(path.c_str());
		int index = 0;

		for (char **i = rc; *i != 0; ++i, ++index) {
			table[index] = std::string(*i);
		}

		PHYSFS_freeList(rc);
		return table;
	}
	/*----------------------------------------------------------------------------*/
	static bool IsDirectory(const std::string& path) {
		return (PHYSFS_isDirectory(path.c_str()) != 0) ? true : false;
	}
	/*----------------------------------------------------------------------------*/
	static bool Exists(const std::string& path) {
		return (PHYSFS_exists(path.c_str()) != 0) ? true : false;
	}
	/*----------------------------------------------------------------------------*/
	static void Delete(const std::string& path) {
		PHYSFS_delete(path.c_str());
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope FileSystemBinding()
	{
		return (
			luabind::namespace_("root")
			[
				FileHandleBinding(),
				ImageBinding(),
				ModelBinding(),
				ScriptBinding(),
				ShaderBinding(),

				luabind::class_<FileSystem, System>("FileSystem")
				.scope
				[
					luabind::def("extract_ext", &FileFactory::ExtractExt),
					luabind::def("remove_ext", &FileFactory::RemoveExt),
					luabind::def("enumerate_files", &EnumerateFiles),
					luabind::def("is_directory", &IsDirectory),
					luabind::def("exists", &Exists),
					luabind::def("delete", &Delete),

					luabind::class_<FileSystem::Group>("Group")
					.property("path", &FileSystem::Group::path)
					.def("set_allocate", &FileSystem::Group::SetAllocate)
				]

				.property("number_of_groups", &FileSystem::GetNumberOfGroups)
				.def("mount", &FileSystem::Mount)
				.def("unmount", (bool(FileSystem::*)(const std::string&)) &FileSystem::Unmount)
				.def("unmount", (bool(FileSystem::*)(FileSystem::Group*)) &FileSystem::Unmount)
				.def("get_group", &FileSystem::GetGroup)
				.def("search", (FileHandle*(FileSystem::*)(const std::string&, bool)) &FileSystem::Search)
				.def("search", (FileHandle*(FileSystem::*)(const std::string&)) &FileSystem::Search)
				.def("write", (FileHandle*(FileSystem::*)(const std::string&, std::string, bool)) &FileSystem::Write)
				.def("write", (FileHandle*(FileSystem::*)(const std::string&, std::string)) &FileSystem::Write)
			]
		);
	}
}