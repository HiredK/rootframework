/**
* @file ImprovedPerlinNoise.h
* @brief
*/

#pragma once

#include "Texture.h"
#include "Math.h"

namespace root
{
	class ImprovedPerlinNoise
	{
		public:
			//! TYPEDEF/ENUMS:
			enum { PERM_TABLE, GRADIENT };

			//! CTOR/DTOR:
			ImprovedPerlinNoise(int size);
			virtual ~ImprovedPerlinNoise();

			//! SERVICES:
			void GenerateData(int seed);
			void Bind(int i);

		private:
			//! MEMBERS:
			std::shared_ptr<Texture> m_textures[2];
			int m_size;
	};

	////////////////////////////////////////////////////////////////////////////////
	// ImprovedPerlinNoise inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline ImprovedPerlinNoise::ImprovedPerlinNoise(int size)
		: m_size(size) {
	}
	/*----------------------------------------------------------------------------*/
	inline ImprovedPerlinNoise::~ImprovedPerlinNoise() {
	}

	/*----------------------------------------------------------------------------*/
	inline void ImprovedPerlinNoise::Bind(int i) {
		m_textures[i]->Bind();
	}
}