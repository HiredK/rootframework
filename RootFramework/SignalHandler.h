/**
* @file SignalHandler.h
* @brief
*/

#pragma once

#include <luabind/luabind.hpp>
#include <sigc++/sigc++.h>
#include <sigc++/signal.h>
#include <sigc++/signal_base.h>

namespace root
{
	class ISignalHandler
	{
		public:
			virtual sigc::connection Connect(const luabind::object& f) = 0;
			virtual void Clear() = 0;
	};

	template <typename Result, typename ...Args>
	class SignalHandler : public ISignalHandler
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef sigc::signal<Result, Args...> Signal;

			//! CTOR/DTOR:
			SignalHandler(sigc::signal_base* signal = new Signal());
			virtual ~SignalHandler();

			//! VIRTUALS:
			virtual sigc::connection Connect(const luabind::object& f);
			virtual void Clear();

			//! SERVICES:
			Result operator()(Args... args) const;

		private:
			//! MEMBERS:
			std::unique_ptr<sigc::signal_base> m_signal;
	};

	////////////////////////////////////////////////////////////////////////////////
	// SignalHandler inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	template<typename Result, typename ...Args>
	inline SignalHandler<Result, Args...>::SignalHandler(sigc::signal_base* signal)
		: m_signal(std::unique_ptr<sigc::signal_base>(signal)) {
	}
	/*----------------------------------------------------------------------------*/
	template<typename Result, typename ...Args>
	inline SignalHandler<Result, Args...>::~SignalHandler() {
		m_signal->clear();
	}

	/*----------------------------------------------------------------------------*/
	template<typename Result, typename ...Args>
	inline sigc::connection SignalHandler<Result, Args...>::Connect(const luabind::object& f)
	{
		struct Functor : public sigc::functor_base
		{
			typedef Result result_type;
			Functor(const luabind::object& f)
				: m_function(f) {
			}

			Result operator()(Args... args) const {
				return luabind::call_function<Result>(m_function, args...);
			}

			const luabind::object m_function;
		};

		Signal* signal = static_cast<Signal*>(m_signal.get());
		return signal->connect(Functor(f));
	}

	/*----------------------------------------------------------------------------*/
	template<typename Result, typename ...Args>
	inline void SignalHandler<Result, Args...>::Clear() {
		m_signal->clear();
	}

	/*----------------------------------------------------------------------------*/
	template<typename Result, typename ...Args>
	inline Result SignalHandler<Result, Args...>::operator()(Args... args) const {
		Signal* signal = static_cast<Signal*>(m_signal.get());
		return (*signal)(args...);
	}
}