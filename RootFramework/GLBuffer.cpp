#include "GLBuffer.h"
#include "Logger.h"

root::GLBuffer::GLBuffer(size_t size, const void* data, GLenum target, GLenum usage, unsigned int map_flags)
	: m_size(size), m_vbo(GL_ZERO), m_target(target), m_usage(usage), m_map_flags(map_flags)
	, m_bound(false)
	, m_mapped(false)
	, m_modified_offset(0)
	, m_modified_size(0)
	, m_memory_map(nullptr)
{
	try {
		m_memory_map = new char[size];
	}
	catch (std::bad_alloc& e) {
		LOG(Logger::Error) << "std::bad_alloc caught : " << e.what();
		m_size = 0;
	}

	glGenBuffers(1, &m_vbo);
	GLBuffer::ScopedBinder binder(*this);

	if (data != nullptr) {
		memcpy(m_memory_map, data, size);
		glBufferData(m_target, (GLsizeiptr)m_size, m_memory_map, m_usage);
	}
	else {
		glBufferData(m_target, (GLsizeiptr)m_size, nullptr, m_usage);
	}
}

GLuint root::GLBuffer::Bind()
{
	if (!m_bound && !m_mapped) {
		glBindBuffer(m_target, m_vbo);
		m_bound = true;
		return m_vbo;
	}

	return GL_ZERO;
}

void root::GLBuffer::Unbind()
{
	if (m_bound) {
		glBindBuffer(m_target, 0);
		m_bound = false;
	}
}

void* root::GLBuffer::Map()
{
	if (!m_mapped) {
		m_modified_offset = 0;
		m_modified_size = 0;
		m_mapped = true;
	}

	return m_memory_map;
}

void root::GLBuffer::Unmap()
{
	auto UnmapStatic = [](GLBuffer& self, char* mem, size_t offset, size_t size) {
		glBufferSubData(self.GetTarget(), (GLintptr)offset, (GLsizeiptr)size, mem + offset);
	};

	auto UnmapStream = [](GLBuffer& self, char* mem) {
		glBufferData(self.GetTarget(), (GLsizeiptr)self.GetSize(), nullptr, self.GetUsage());
		glBufferData(self.GetTarget(), (GLsizeiptr)self.GetSize(), mem, self.GetUsage());
	};

	if (m_mapped) {
		if (GetMapExplicitRangeModifyActive()) {
			m_modified_offset = std::min(m_modified_offset, m_size - 1);
			m_modified_size = std::min(m_modified_size, m_size - m_modified_offset);
		}
		else {
			m_modified_offset = 0;
			m_modified_size = m_size;
		}

		if (!m_bound) {
			glBindBuffer(m_target, m_vbo);
			m_bound = true;
		}

		if (m_modified_size > 0)
		{
			switch (m_usage) {
				case GL_STATIC_DRAW:
					UnmapStatic(*this, m_memory_map, m_modified_offset, m_modified_size);
					break;

				case GL_STREAM_DRAW:
					UnmapStream(*this, m_memory_map);
					break;

				default:
					break;
			}
		}

		m_modified_offset = 0;
		m_modified_size = 0;
		m_mapped = false;
	}
}

void root::GLBuffer::SetMappedRangeModified(size_t offset, size_t size)
{
	if (m_mapped && GetMapExplicitRangeModifyActive()) {
		size_t old_range_end = m_modified_offset + m_modified_size;
		m_modified_offset = std::min(m_modified_offset, offset);

		size_t new_range_end = std::max(offset + m_modified_size, old_range_end);
		m_modified_size = new_range_end - m_modified_offset;
	}
}

void root::GLBuffer::Fill(size_t offset, size_t size, const void *data)
{
	memcpy(m_memory_map + offset, data, size);
	if (m_mapped) {
		SetMappedRangeModified(offset, size);
	}
	else {
		glBufferSubData(m_target, (GLintptr)offset, (GLsizeiptr)size, data);
	}
}