#include "ScriptObject.h"

std::vector<root::ScriptObject*> root::ScriptObject::m_references;
std::vector<root::Script*> root::ScriptObject::ActiveOwnerStack;
bool root::ScriptObject::RefListChanged = false;

root::ScriptObject::ScriptObject(const std::string& name, Type type)
	: Object(name), m_isUpdated(false), m_owner(ScriptObject::ActiveOwnerStack.back()), m_type(type)
{
	m_references.push_back(this);
	RefListChanged = true;
}

root::ScriptObject::~ScriptObject()
{
	auto it = m_references.begin();
	while (it != m_references.end())
	{
		if ((*it) == this) {
			//LOG() << "Deleting '" << GetName() << "' (" << m_references.size() << " object remaining)";
			m_references.erase(it);
			RefListChanged = true;
			break;
		}

		++it;
	}
}

void root::ScriptObject::UpdateAll(float dt)
{
	auto it = m_references.begin();
	while (it != m_references.end()) {
		ScriptObject* ref = (*it);

		if (ref->GetType() == Dynamic && !ref->m_isUpdated)
		{
			ref->Update(dt);
			ref->m_isUpdated = true;

			if (RefListChanged) {
				ref->GetOwner()->CollectGarbage();
				RefListChanged = false;
				it = m_references.begin();
				continue;
			}
		}

		++it;
	}

	it = m_references.begin();
	while (it != m_references.end()) {
		(*it)->m_isUpdated = false;
		++it;
	}
}