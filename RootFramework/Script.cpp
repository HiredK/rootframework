#include "Script.h"
#include "Logger.h"

// Only include in source, luabind is heavy on compile time.
#pragma message ("Building Lua Context...")
#include "Binding.h"

REGISTER_FILE_IMPL(root::Script, 2, "lua", "luac")

bool root::Script::CreateData()
{
	if (m_state == nullptr) {
		if (FileHandle::CreateData())
		{
			m_state = BuildLuaContext(m_engine_instance, luaL_newstate());
			if (!m_state) {
				LOG(Logger::Warning) << m_path << ": Unable to build lua context";
				return false;
			}

			//luabind::globals(m_state)["LOG"] = &LOG;
			//luabind::globals(m_state)["LOG"]["on_received"] = new LoggerSignalHandler();

			FileHandle::ScanFileForInclude();
			FileHandle::ScanFileForDefine();

			const char* buffer = static_cast<const char*>(&m_data[0]);
			bool success = (luaL_loadbuffer(m_state, buffer, m_data.size(), m_path.c_str()) == 0);
			FileHandle::DeleteData();

			if (!success) {
				LOG(Logger::Error) << lua_tostring(m_state, -1);
				lua_pop(m_state, 1);
				DeleteData();
				return false;
			}

			ScriptObject::PushOwner(this);
			if (lua_pcall(m_state, 0, 0, -2)) {
				LOG(Logger::Error) << lua_tostring(m_state, -1);
				lua_pop(m_state, 2);
				return false;
			}

			CollectGarbage();
			ScriptObject::PopStack();
		}
	}

	return true;
}

bool root::Script::DoString(const std::string& str)
{
	if (FileHandle::Ready(this)) {
		bool result = true;

		ScriptObject::PushOwner(this);
		if (luaL_dostring(m_state, str.c_str())) {
			LOG(Logger::Error) << lua_tostring(m_state, -1);
			result = false;
		}

		ScriptObject::PopStack();
		return result;
	}

	return false;
}

void root::Script::CollectGarbage()
{
	if (FileHandle::Ready(this)) {
		lua_gc(m_state, LUA_GCCOLLECT, 0);
	}
}

void root::Script::OnLogReceived(Logger::Level level, const std::string& message)
{
	if (FileHandle::Ready(this)) {
		//lua_State* thread = lua_newthread(m_state);
		//int ref = luaL_ref(m_state, LUA_REGISTRYINDEX);
		//(*luabind::object_cast<LoggerSignalHandler*>(luabind::globals(thread)["LOG"]["on_received"]))(level, message);
		//luaL_unref(m_state, LUA_REGISTRYINDEX, ref);
	}
}

void root::Script::DeleteData()
{
	if (m_state != nullptr) {
		lua_close(m_state);
		m_state = nullptr;
	}
}