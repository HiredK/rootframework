#version 400 core

uniform mat4 u_Deform_ScreenQuadCorners;
uniform mat4 u_Deform_ScreenQuadVerticals;
uniform vec4 u_Deform_ScreenQuadCornerNorms;
uniform vec4 u_Deform_Offset;
uniform float u_Deform_Radius;
uniform mat3 u_Deform_LocalToWorld;
uniform mat4 u_Deform_TangentFrameToWorld;

uniform vec3 u_HeightNormal_TileSize;
uniform vec3 u_HeightNormal_TileCoords;
uniform sampler2D s_HeightNormal_Slot0;
uniform sampler2D s_HeightNormal_Slot1;
uniform sampler2D s_HeightNormal_Slot2;

uniform sampler2D s_AlbedoTex;

uniform vec3 u_WorldCamPosition;
uniform float u_Timer;

-- vs

void main()
{
	float px = (gl_VertexID % 800) * (1.0 / 800.0);
	float py = (gl_VertexID / 800) * (1.0 / 800.0);	
    gl_Position = vec4(px, 0, py, 0);
}

-- gs
layout(points) in;
layout(triangle_strip, max_vertices = 12) out;

smooth out vec2 fs_TexCoord;
smooth out vec3 fs_Deformed;
smooth out vec3 fs_Normal;

vec3 vLocalSeed;

vec4 texTile(sampler2D tile, vec2 uv, vec3 tileCoords, vec3 tileSize)
{
	uv = tileCoords.xy + uv * tileSize.xy;
	return texture2D(tile, uv);
}

vec4 getDeformedPos(vec3 pos, out vec3 deformed)
{
	float R = u_Deform_Radius;
	mat4 C = u_Deform_ScreenQuadCorners;
	mat4 N = u_Deform_ScreenQuadVerticals;
	vec4 L = u_Deform_ScreenQuadCornerNorms;
	vec3 P = vec3(pos.xz * u_Deform_Offset.z + u_Deform_Offset.xy, R);
	
	vec4 uvUV = vec4(pos.xz, vec2(1.0, 1.0) - pos.xz);
	vec4 alpha = uvUV.zxzx * uvUV.wwyy;
	vec4 alphaPrime = alpha * L / dot(alpha, L);
	
	float k = min(length(P) / dot(alpha, L) * 1.0000003, 1.0);
	float hPrime = (pos.y + R * (1.0 - k)) / k;
	
	deformed = (u_Deform_Radius + pos.y) * normalize(u_Deform_LocalToWorld * P);
	return (C + hPrime * N) * alphaPrime;
}

mat4 rotationMatrix(vec3 axis, float angle)
{
    axis = normalize(axis);
    float s = sin(angle);
    float c = cos(angle);
    float oc = 1.0 - c;
    
    return mat4(oc * axis.x * axis.x + c,           oc * axis.x * axis.y - axis.z * s,  oc * axis.z * axis.x + axis.y * s,  0.0,
                oc * axis.x * axis.y + axis.z * s,  oc * axis.y * axis.y + c,           oc * axis.y * axis.z - axis.x * s,  0.0,
                oc * axis.z * axis.x - axis.y * s,  oc * axis.y * axis.z + axis.x * s,  oc * axis.z * axis.z + c,           0.0,
                0.0,                                0.0,                                0.0,                                1.0);
}

float randZeroOne()
{
    uint n = floatBitsToUint(vLocalSeed.y * 214013.0 + vLocalSeed.x * 2531011.0 + vLocalSeed.z * 141251.0);
    n = n * (n * n * 15731u + 789221u);
    n = (n >> 9u) | 0x3F800000u;
 
    float fRes =  2.0 - uintBitsToFloat(n);
    vLocalSeed = vec3(vLocalSeed.x + 147158.0 * fRes, vLocalSeed.y*fRes  + 415161.0 * fRes, vLocalSeed.z + 324154.0*fRes);
    return fRes;
}

int randomInt(int min, int max)
{
	float fRandomFloat = randZeroOne();
	return int(float(min) + fRandomFloat * float(max - min));
}

void main()
{
    vec3 vGrassFieldPos = gl_in[0].gl_Position.xyz;
	float PIover180 = 3.1415 / 180.0;
	
	float grass_factor = texTile(s_HeightNormal_Slot2, vec2(vGrassFieldPos.x, vGrassFieldPos.z), u_HeightNormal_TileCoords, u_HeightNormal_TileSize).y;
	if (grass_factor < 0.85f) {
		return;
	}
	
	float h = texTile(s_HeightNormal_Slot0, vec2(vGrassFieldPos.x, vGrassFieldPos.z), u_HeightNormal_TileCoords, u_HeightNormal_TileSize).x;
	vGrassFieldPos.y = h - 0.2f;
	
	vec3 deformed_center;
	getDeformedPos(vGrassFieldPos, deformed_center);	
	if (distance(u_WorldCamPosition, deformed_center) > 150.0f) {
		return;
	}
	
	vec3 fn = texTile(s_HeightNormal_Slot1, vec2(vGrassFieldPos.x, vGrassFieldPos.z), u_HeightNormal_TileCoords, u_HeightNormal_TileSize).xyz * 2.0 - 1.0;
	fn.z = sqrt(max(0.0, 1.0 - dot(fn.xy, fn.xy)));
	mat3 TTW = mat3(u_Deform_TangentFrameToWorld);
	
	fs_Normal = normalize(TTW * vec3(-fn.x, -fn.y, fn.z));
	
	vec3 vBaseDir[3] = vec3[]
	(
		vec3(1.0, 0.0, 0.0),
		vec3(float(cos( 45.0 * PIover180)), 0.0f, float(sin( 45.0 * PIover180))),
		vec3(float(cos(-45.0 * PIover180)), 0.0f, float(sin(-45.0 * PIover180)))
	);
	
	float fGrassPatchSize = 0.005;
	float fWindStrength = 0.0006;
	
	vLocalSeed = deformed_center;
	vGrassFieldPos.x += clamp(randZeroOne() * 2.0 - 1.0, -0.0002, 0.0002);
	vGrassFieldPos.z += clamp(randZeroOne() * 2.0 - 1.0, -0.0002, 0.0002);
	
	vec3 vWindDirection = vec3(1.0, 0.0, 1.0);
	vWindDirection = normalize(vWindDirection);
	
	for(int i = 0; i < 3; i++)
	{
		vec3 vBaseDirRotated = (rotationMatrix(vec3(0, 1, 0), sin(u_Timer * 0.7f) * 0.1) * vec4(vBaseDir[i], 1.0)).xyz;
		
		vLocalSeed = deformed_center * float(i);
		int iGrassPatch = randomInt(0, 3);
		
		float fGrassPatchHeight = 1.2 + randZeroOne() * 0.8;		
		float fTCStartX = float(iGrassPatch) * 0.25f;
		float fTCEndX = fTCStartX + 0.25f;
		
		float fWindPower = 0.5f + sin(deformed_center.x / 30 + deformed_center.z / 30 + u_Timer * (1.2f + fWindStrength / 20.0f));
		fWindPower *= ((fWindPower < 0.0f) ? 0.2f : 0.3f) * fWindStrength;
		
		// Grass patch top left vertex
		vec3 vTL = vGrassFieldPos - vBaseDirRotated * fGrassPatchSize * 0.5f + vWindDirection * fWindPower;
		vTL.y += fGrassPatchHeight;
		vec3 vTL_deformed;
		gl_Position = getDeformedPos(vTL, vTL_deformed);
		fs_TexCoord = vec2(fTCStartX, 1.0);
		fs_Deformed = vTL_deformed;
		EmitVertex();
		
		// Grass patch bottom left vertex
		vec3 vBL = vGrassFieldPos - vBaseDir[i] * fGrassPatchSize * 0.5f;
		vec3 vBL_deformed;
		gl_Position = getDeformedPos(vBL, vBL_deformed);
		fs_TexCoord = vec2(fTCStartX, 0.0);
		fs_Deformed = vBL_deformed;
		EmitVertex();
		
		// Grass patch top right vertex
		vec3 vTR = vGrassFieldPos + vBaseDirRotated * fGrassPatchSize * 0.5f + vWindDirection * fWindPower;
		vTR.y += fGrassPatchHeight;
		vec3 vTR_deformed;
		gl_Position = getDeformedPos(vTR, vTR_deformed);
		fs_TexCoord = vec2(fTCEndX, 1.0);
		fs_Deformed = vTR_deformed;
		EmitVertex();
		
		// Grass patch bottom right vertex
		vec3 vBR = vGrassFieldPos + vBaseDir[i] * fGrassPatchSize * 0.5f;
		vec3 vBR_deformed;
		gl_Position = getDeformedPos(vBR, vBR_deformed);
		fs_TexCoord = vec2(fTCEndX, 0.0);
		fs_Deformed = vBR_deformed;
		EmitVertex();
		
		EndPrimitive();
	}
}

-- fs
layout(location = 0) out vec4 albedo;
layout(location = 1) out vec4 normal;

smooth in vec2 fs_TexCoord;
smooth in vec3 fs_Deformed;
smooth in vec3 fs_Normal;

void main()
{
	vec4 color = texture(s_AlbedoTex, vec2(fs_TexCoord.x, 1.0 - fs_TexCoord.y));           
	if(color.a < 0.25) {
		discard;
	}

	albedo = vec4(color.rgb, 0.8);
	normal = vec4(fs_Normal * 0.5 + 0.5, 0);
}