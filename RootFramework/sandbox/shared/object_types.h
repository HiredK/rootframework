--[[
- @file object_types.h
- @brief
]]

ID_BASE_OBJECT       =  0
ID_PLAYER_OBJECT     =  1

-- shapes
ID_BOX_OBJECT        =  2
ID_SPHERE_OBJECT     =  3
ID_CYLINDER_OBJECT   =  4
ID_CONE_OBJECT       =  5
ID_CAPSULE_OBJECT    =  6

-- lights
ID_DIRECTIONAL_LIGHT =  7
ID_POINT_LIGHT       =  8
ID_ENVPROBE          =  9

-- vehicles
ID_CAR_OBJECT        = 10

-- misc
ID_MODEL_OBJECT      = 11
ID_TREE_OBJECT       = 12
ID_PARTICLE_EMITTER  = 13