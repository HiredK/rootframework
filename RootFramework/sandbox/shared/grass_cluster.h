--[[
- @file grass_cluster.h
- @brief
]]

class 'grass_cluster' (root.ScriptObject)

function grass_cluster:__init(face, level, tx, ty)
    root.ScriptObject.__init(self, "grass_cluster")
	self.type = root.ScriptObject.Dynamic
	self.face_ = face
	
	-- local coord
	self.level_ = level
	self.tx_ = tx
	self.ty_ = ty
	
	self.shader_ = FileSystem:search("shaders/gbuffer_grass.glsl", true)
	self.albedo_tex_ = root.Texture.from_image(FileSystem:search("images/grass_pack.png", true), { filter_mode = gl.LINEAR_MIPMAP_LINEAR, anisotropy = 16, mipmap = true })
	self.timer_ = 0.0
end

function grass_cluster:__update(dt)
    self.timer_ = self.timer_ + dt
end

function grass_cluster:draw(camera)

	gl.Disable(gl.CULL_FACE)
	self.shader_:bind()
	
	local q = self.face_.producer_:get_tile_coords(self.level_, self.tx_, self.ty_)
	self.face_:get_deformation():set_uniforms(camera, self.face_:get_face_matrix(), self.level_, q.x * 2, q.y * 2, q.z) -- temp
	self.face_.producer_:set_uniforms(self.level_, self.tx_, self.ty_)
	
	root.Shader.get():uniform("u_WorldCamPosition", camera.position)
	root.Shader.get():uniform("u_Timer", self.timer_)
	
	gl.ActiveTexture(gl.TEXTURE3)
	root.Shader.get():sampler("s_AlbedoTex", 3)
	self.albedo_tex_:bind()
	
	gl.Enable(gl.MULTISAMPLE)
	gl.Enable(gl.SAMPLE_ALPHA_TO_COVERAGE)

	root.Mesh.draw_virtual(gl.POINTS, 800 * 800)
	
	gl.Disable(gl.MULTISAMPLE)
	gl.Disable(gl.SAMPLE_ALPHA_TO_COVERAGE)
	
	root.Shader.pop_stack()
end