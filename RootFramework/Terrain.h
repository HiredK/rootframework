/**
* @file Terrain.h
* @brief
*/

#pragma once

#include "Deformation.h"
#include "SphericalDeformation.h"
#include "GraphLayer.h"
#include "TileProducer.h"
#include "Mesh.h"

namespace root
{
	class Terrain : public Transform
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Face { PosX, NegX, PosY, NegY, PosZ, NegZ };

			//! CTOR/DTOR:
			Terrain(double size, int max_level, Face face = NegY);
			virtual ~Terrain();

			//! SERVICES:
			void Cull(Camera& camera);
			void Draw(Camera& camera);
			void Process();

			//! PRODUCERS:
			void AddProducer(TileProducer* producer);
			TileProducer* GetProducer(unsigned int i) const;
			unsigned int GetNumOfProducers() const;

			//! CALLBACKS:
			typedef std::function<void(const TerrainTile&)> PageInCallbackFunc;
			void AddPageInCallback(PageInCallbackFunc cb);
			void PageIn(const TerrainTile& tile) const;

			//! OCCLUSION:
			bool AddOccluder(const Box3D& occluder) const;
			bool IsOccluded(const Box3D& box) const;

			//! ACCESSORS:
			const TerrainTile* GetRoot() const;
			dmat3 GetFaceMatrix(Face face) const;
			dmat3 GetFaceMatrix() const;
			void SetDeformation(Deformation* deform);
			Deformation* GetDeformation() const;
			void SetUseTessellatedPatch(bool use = true);
			bool GetUseTessellatedPatch() const;
			Mesh* GetTileMesh() const;

			double GetCameraDist(const Box3D& box, const dvec3& point) const;
			double GetCameraDist(const Box3D& box) const;
			Frustum::Visibility GetVisibility(const Box3D& box) const;
			const dvec3& GetLocalCameraPosition() const;
			float GetSplitDist() const;
			float GetDistFactor() const;
			int GetMaxLevel() const;
			double GetSize() const;
			Face GetFace() const;

		protected:
			//! MEMBERS:
			TerrainTile m_root;
			double m_size;
			int m_max_level;
			Face m_face;

		private:
			TileProducer::List m_producers;
			std::vector<PageInCallbackFunc> m_page_in_callbacks;
			Deformation* m_deformation;

			std::shared_ptr<Mesh> m_plane_mesh;
			std::shared_ptr<Mesh> m_patch_mesh;
			bool m_use_tessellated_patch;

			dvec3 m_deformed_camera_position;
			dvec3 m_local_camera_position;
			dmat2 m_local_camera_direction;
			Frustum m_local_frustum;

			float m_split_factor;
			float m_split_dist;
			float m_dist_factor;
			int m_horizon_size;
			float* m_horizon;
			bool m_split_invisible_tiles;
			bool m_horizon_culling;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Terrain inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Terrain::Terrain(double size, int max_level, Face face)
		: m_root(*this, { 0, 0, -size, -size, size * 2, -500000, 500000 })
		, m_size(size)
		, m_max_level(max_level)
		, m_face(face)
		, m_deformation(new Deformation())
		, m_plane_mesh(Mesh::BuildPlane(25, 25))
		, m_patch_mesh(Mesh::BuildPatch(25, 25))
		, m_use_tessellated_patch(false)
		, m_split_factor(1.25f)
		, m_split_dist(0.0f)
		, m_dist_factor(0.0f)
		, m_horizon_size(256)
		, m_horizon(new float[m_horizon_size])
		, m_split_invisible_tiles(false)
		, m_horizon_culling(true) {
	}
	/*----------------------------------------------------------------------------*/
	inline Terrain::~Terrain() {
		m_producers.clear();
		std::free(m_horizon);
	}

	/*----------------------------------------------------------------------------*/
	inline void Terrain::AddProducer(TileProducer* producer) {
		m_producers.push_back(producer);
	}
	/*----------------------------------------------------------------------------*/
	inline TileProducer* Terrain::GetProducer(unsigned int i) const {
		return m_producers[i];
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int Terrain::GetNumOfProducers() const {
		return static_cast<unsigned int>(m_producers.size());
	}

	/*----------------------------------------------------------------------------*/
	inline void Terrain::AddPageInCallback(PageInCallbackFunc cb) {
		m_page_in_callbacks.push_back(cb);
	}
	/*----------------------------------------------------------------------------*/
	inline void Terrain::PageIn(const TerrainTile& tile) const {
		for (auto& cb : m_page_in_callbacks) {
			cb(tile);
		}
	}

	/*----------------------------------------------------------------------------*/
	inline const TerrainTile* Terrain::GetRoot() const {
		return &m_root;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat3 Terrain::GetFaceMatrix() const {
		return GetFaceMatrix(m_face);
	}
	/*----------------------------------------------------------------------------*/
	inline void Terrain::SetDeformation(Deformation* deform) {
		m_deformation = deform;
	}
	/*----------------------------------------------------------------------------*/
	inline Deformation* Terrain::GetDeformation() const {
		return m_deformation;
	}
	/*----------------------------------------------------------------------------*/
	inline void Terrain::SetUseTessellatedPatch(bool use) {
		m_use_tessellated_patch = use;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Terrain::GetUseTessellatedPatch() const {
		return m_use_tessellated_patch;
	}
	/*----------------------------------------------------------------------------*/
	inline Mesh* Terrain::GetTileMesh() const {
		return (GetUseTessellatedPatch()) ? m_patch_mesh.get() : m_plane_mesh.get();
	}

	/*----------------------------------------------------------------------------*/
	inline double Terrain::GetCameraDist(const Box3D& box) const {
		return GetCameraDist(box, m_local_camera_position);
	}
	/*----------------------------------------------------------------------------*/
	inline Frustum::Visibility Terrain::GetVisibility(const Box3D& box) const {
		return GetDeformation()->GetVisibility(m_deformed_camera_position, m_local_frustum, box);
	}
	/*----------------------------------------------------------------------------*/
	inline const dvec3& Terrain::GetLocalCameraPosition() const {
		return m_local_camera_position;
	}
	/*----------------------------------------------------------------------------*/
	inline float Terrain::GetSplitDist() const {
		return m_split_dist;
	}
	/*----------------------------------------------------------------------------*/
	inline float Terrain::GetDistFactor() const {
		return m_dist_factor;
	}
	/*----------------------------------------------------------------------------*/
	inline int Terrain::GetMaxLevel() const {
		return m_max_level;
	}
	/*----------------------------------------------------------------------------*/
	inline double Terrain::GetSize() const {
		return m_size;
	}
	/*----------------------------------------------------------------------------*/
	inline Terrain::Face Terrain::GetFace() const {
		return m_face;
	}

} // root namespace