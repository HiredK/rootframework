/**
* @file GraphLayer.h
* @brief An abstract layer sub class for layers using graphs.
*/

#pragma once

#include "TileProducer.h"
#include "Graph.h"

namespace root
{
	class GraphLayer
	{
		public:
			//! CTOR/DTOR:
			GraphLayer(const TileProducer& producer);
			virtual ~GraphLayer();

			//! VIRTUALS:
			void Blit(int level, int tx, int ty, int size) {}

			//! SERVICES:
			// Draws a curve shape in the current FrameBuffer.
			void DrawCurve(const dvec3& coords, const Graph::Curve& p, float width, float cap, float scale,
				const dvec2& nx, const dvec2& ny, const dvec2& lx, const dvec2& ly);

			// Draws the altitude profile of a curve. The curve
			// is drawn with its total footprint width, which
			// includes not only the curve itself, but also the
			// area where it may modify the terrain elevation.
			void DrawCurveAltitude(const dvec3& coords, const Graph::Curve& p, float width, float nwidth, float step, bool caps,
				const dvec2& nx, const dvec2& ny, const dvec2& lx, const dvec2& ly);

			// Checks whether a node is at the extremity of a curve.
			bool Extremity(const Graph::Curve& p, Graph::Node* n);

			//! ACCESSORS:
			const TileProducer& GetProducer() const;

		protected:
			//! MEMBERS:
			const TileProducer& m_producer;
	};

	////////////////////////////////////////////////////////////////////////////////
	// GraphLayer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline GraphLayer:: GraphLayer(const TileProducer& producer)
		: m_producer(producer) {
	}
	/*----------------------------------------------------------------------------*/
	inline GraphLayer::~GraphLayer() {
	}

	/*----------------------------------------------------------------------------*/
	inline const TileProducer& GraphLayer::GetProducer() const {
		return m_producer;
	}
}