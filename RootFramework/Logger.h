/**
* @file Logger.h
* @brief Header only basic logger implementation.
*/

#pragma once

#include <string>
#include <sstream>
#include <mutex>
#include <thread>
#include <memory>
#include <fstream>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <vector>

namespace root
{
	class Logger
	{
		public:
			//! TYPEDEF/ENUMS:
			enum Level	{ Info, Warning, Error, Timer };
			typedef std::function<void(Level, const std::string&)> CallbackFunc;

			//! CTOR/DTOR:
			Logger(const std::string& output_path);
			virtual ~Logger();

			class Stream : public std::ostringstream
			{
				public:
					//! CTOR/DTOR:
					Stream(Logger& logger, Level level);
					virtual ~Stream();

				private:
					//! MEMBERS:
					Logger& m_logger;
					Level m_level;
			};

			class ScopedTimer
			{
				public:
					//! TYPEDEF/ENUMS:
					enum Type { Nano, Micro, Milli, Second, Minute };

					//! CTOR/DTOR:
					ScopedTimer(Logger& logger, const std::string& name, Type type = Milli);
					virtual ~ScopedTimer();

				private:
					//! MEMBERS:
					std::chrono::high_resolution_clock::time_point m_start;
					Logger& m_logger;
					const std::string m_name;
					Type m_type;
			};

			//! SERVICES:
			void Log(Level level, const std::string& message);
			void AddCallback(CallbackFunc cb);
			void Flush();

			//! OPERATORS:
			Stream operator()();
			Stream operator()(Level nLevel);

		private:
			//! SERVICES:
			const tm* GetLocalTime();

			//! MEMBERS:
			std::vector<CallbackFunc> m_callbacks;
			std::ofstream m_output_stream;
			std::mutex m_mutex;
			std::tm m_tm;
	};

	/*----------------------------------------------------------------------------*/
	// Convert date and time info from tm to a character string
	// in format "YYYY-mm-DD HH:MM:SS" and send it to a stream.
	inline std::ostream& operator<<(std::ostream& stream, const tm* tm)
	{
		return stream << 1900 + tm->tm_year << '-' <<
			std::setfill('0') << std::setw(2) << tm->tm_mon + 1 << '-'
			<< std::setfill('0') << std::setw(2) << tm->tm_mday << ' '
			<< std::setfill('0') << std::setw(2) << tm->tm_hour << ':'
			<< std::setfill('0') << std::setw(2) << tm->tm_min << ':'
			<< std::setfill('0') << std::setw(2) << tm->tm_sec;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Logger::Stream inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Logger::Stream::Stream(Logger& logger, Level level)
		: m_logger(logger), m_level(level) {
	}
	/*----------------------------------------------------------------------------*/
	inline Logger::Stream::~Stream() {
		m_logger.Log(m_level, std::ostringstream::str());
	}

	////////////////////////////////////////////////////////////////////////////////
	// Logger::ScopedNanoTimer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Logger::ScopedTimer::ScopedTimer(Logger& logger, const std::string& name, Type type)
		: m_start(std::chrono::high_resolution_clock::now())
		, m_logger(logger), m_name(name), m_type(type) {
	}
	/*----------------------------------------------------------------------------*/
	inline Logger::ScopedTimer::~ScopedTimer() {
		auto now = std::chrono::high_resolution_clock::now();
		switch (m_type) {
			case Nano:
				m_logger(Timer) << "Executed [" << m_name << "] in " <<
					std::chrono::duration_cast<std::chrono::nanoseconds>(now - m_start).count() << " ns";
				break;
			case Micro:
				m_logger(Timer) << "Executed [" << m_name << "] in " <<
					std::chrono::duration_cast<std::chrono::microseconds>(now - m_start).count() << " us";
				break;
			case Milli:
				m_logger(Timer) << "Executed [" << m_name << "] in " <<
					std::chrono::duration_cast<std::chrono::milliseconds>(now - m_start).count() << " ms";
				break;
			case Second:
				m_logger(Timer) << "Executed [" << m_name << "] in " <<
					std::chrono::duration_cast<std::chrono::seconds>(now - m_start).count() << " seconds";
				break;
			case Minute:
				m_logger(Timer) << "Executed [" << m_name << "] in " <<
					std::chrono::duration_cast<std::chrono::minutes>(now - m_start).count() << " minutes";
				break;
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	// Logger inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Logger::Logger(const std::string& output_path) {
		m_output_stream.open(output_path, std::fstream::out | std::fstream::app | std::fstream::ate);
	}
	/*----------------------------------------------------------------------------*/
	inline Logger::~Logger()
	{
		Flush();
		m_output_stream.close();
	}

	/*----------------------------------------------------------------------------*/
	inline void Logger::Log(Level level, const std::string& message)
	{
		std::lock_guard<std::mutex> lock(m_mutex);

		std::stringstream ss;
		ss << "[" << GetLocalTime() << "][thread=" << std::this_thread::get_id() << "] " << message;
		for (auto& cb : m_callbacks) { cb(level, ss.str()); }
		ss << std::endl;

		m_output_stream << ss.str();
		printf(ss.str().c_str());
	}
	/*----------------------------------------------------------------------------*/
	inline void Logger::AddCallback(CallbackFunc cb) {
		m_callbacks.push_back(cb);
	}
	/*----------------------------------------------------------------------------*/
	inline void Logger::Flush() {
		m_output_stream.flush();
	}

	/*----------------------------------------------------------------------------*/
	inline Logger::Stream Logger::operator()() {
		return Stream(*this, Info);
	}
	/*----------------------------------------------------------------------------*/
	inline Logger::Stream Logger::operator()(Level level) {
		return Stream(*this, level);
	}

	/*----------------------------------------------------------------------------*/
	inline const tm* Logger::GetLocalTime() {
		auto in_time_t = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
		localtime_s(&m_tm, &in_time_t);
		return &m_tm;
	}

	////////////////////////////////////////////////////////////////////////////////
	// Extern Logger instance:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	extern Logger LOG;

}  // root namespace