/**
* @file TerrainTile.h
* @brief
*/

#pragma once

#include <memory>
#include <vector>
#include "Camera.h"

namespace root
{
	class Terrain;

	class TerrainTile
	{
		public:
			//! TYPEDEF/ENUMS:
			struct Coord {
				// The logical x, y coordinate
				// (between 0 and 2^level).
				int tx, ty;

				// The physical x,y coordinate.
				double ox, oy;

				// The physical length.
				double length;

				// The minimum/maximum elevation.
				float zmin, zmax;
			};

			//! CTOR/DTOR:
			TerrainTile(const Terrain& owner, const Coord& coord, TerrainTile* parent = nullptr);
			virtual ~TerrainTile();

			//! SERVICES:
			void RecursiveCulling();
			void RecursiveDraw(Camera& camera);
			void Subdivide();
			void Release();

			//! ACCESSORS:
			TerrainTile* GetChild(int i) const;
			const Terrain& GetOwner() const;
			const Coord& GetCoord() const;
			int GetLevel() const;
			bool IsVisible() const;
			bool IsLeaf() const;

			dvec3 GetCenter() const;
			bool IsPagedIn() const;

		private:
			//! MEMBERS:
			std::unique_ptr<TerrainTile> m_children[4];
			const Terrain& m_owner;
			Coord m_coord;
			TerrainTile* m_parent;
			int m_level;

			Frustum::Visibility m_visibility;
			Box3D m_local_box;
			bool m_occluded;
			bool m_paged_in;
	};

	////////////////////////////////////////////////////////////////////////////////
	// TerrainTile inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TerrainTile::TerrainTile(const Terrain& owner, const Coord& coord, TerrainTile* parent)
		: m_owner(owner), m_coord(coord), m_parent(parent)
		, m_level((parent) ? m_parent->m_level + 1 : 0)
		, m_local_box(dvec3(coord.ox, coord.oy, coord.zmin), dvec3(coord.ox + coord.length, coord.oy + coord.length, coord.zmax))
		, m_visibility(Frustum::Partially)
		, m_occluded(false)
		, m_paged_in(false) {
	}
	/*----------------------------------------------------------------------------*/
	inline TerrainTile::~TerrainTile() {
	}

	/*----------------------------------------------------------------------------*/
	inline TerrainTile* TerrainTile::GetChild(int i) const {
		return m_children[i].get();
	}
	/*----------------------------------------------------------------------------*/
	inline const Terrain& TerrainTile::GetOwner() const {
		return m_owner;
	}
	/*----------------------------------------------------------------------------*/
	inline const TerrainTile::Coord& TerrainTile::GetCoord() const {
		return m_coord;
	}

	/*----------------------------------------------------------------------------*/
	inline int TerrainTile::GetLevel() const {
		return m_level;
	}
	/*----------------------------------------------------------------------------*/
	inline bool TerrainTile::IsVisible() const {
		return m_visibility != Frustum::Invisible;
	}
	/*----------------------------------------------------------------------------*/
	inline bool TerrainTile::IsLeaf() const {
		return m_children[0] == nullptr;
	}

	/*----------------------------------------------------------------------------*/
	inline dvec3 TerrainTile::GetCenter() const {
		return m_local_box.GetCenter();
	}
}