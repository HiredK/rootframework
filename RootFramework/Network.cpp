#include "Network.h"
#include "Logger.h"

bool root::Network::Connect(const std::string& address, unsigned short port)
{
	bool success = false;
	switch (m_type)
	{
		case CLIENT_TO_SERVER:

			m_sd.port = port + 1;
			while (RakNet::IRNS2_Berkley::IsPortInUse(m_sd.port, m_sd.hostAddress, m_sd.socketFamily, SOCK_DGRAM)) {
				LOG() << "Client: Port '" << port << "' is already in use, trying '" << port + 1 << "'...";
				m_sd.port++;
			}

			if (m_peer->Startup(1, &m_sd, 1) == RakNet::RAKNET_STARTED) {
				switch (m_peer->Connect(address.c_str(), port, 0, 0, 0))
				{
					case RakNet::CONNECTION_ATTEMPT_STARTED:
						LOG() << "Client: CONNECTION_ATTEMPT_STARTED";
						success = true;
						break;

					case RakNet::ALREADY_CONNECTED_TO_ENDPOINT:
						LOG() << "Client: ALREADY_CONNECTED_TO_ENDPOINT";
						success = true;
						break;

					default:
						break;
				}
			}

			break;

		case SERVER_TO_CLIENT:

			m_sd.port = port;
			switch (m_peer->Startup(8, &m_sd, 1))
			{
				case RakNet::RAKNET_STARTED:
					LOG() << "Server: RAKNET_STARTED";
					m_peer->SetMaximumIncomingConnections(8);
					OnConnect();
					success = true;
					break;

				case RakNet::RAKNET_ALREADY_STARTED:
					LOG() << "Server: RAKNET_ALREADY_STARTED";
					success = true;
					break;

				default:
					break;
			}

			break;

		default:
		case OFFLINE:
			success = true;
			break;
	}

	return success;
}

void root::Network::Receive()
{
	for (RakNet::Packet* packet = m_peer->Receive(); packet; m_peer->DeallocatePacket(packet), packet = m_peer->Receive())
	{
		switch (m_type)
		{
			case CLIENT_TO_SERVER:
				switch (packet->data[0])
				{
					case ID_CONNECTION_REQUEST_ACCEPTED:
						LOG() << "Client: ID_CONNECTION_REQUEST_ACCEPTED";
						OnConnect();
						break;

					case ID_CONNECTION_ATTEMPT_FAILED:
						LOG() << "Client: ID_CONNECTION_ATTEMPT_FAILED";
						break;

					case ID_DISCONNECTION_NOTIFICATION:
						LOG() << "Client: ID_DISCONNECTION_NOTIFICATION";
						break;

					case ID_CONNECTION_LOST:
						LOG() << "Client: ID_CONNECTION_LOST";
						break;
				}
				break;

			case SERVER_TO_CLIENT:
				switch (packet->data[0])
				{
					case ID_NEW_INCOMING_CONNECTION:
						LOG() << "Server: ID_NEW_INCOMING_CONNECTION" << " [" << m_peer->NumberOfConnections() << "/" << m_peer->GetMaximumIncomingConnections() << "]";
						break;

					case ID_DISCONNECTION_NOTIFICATION:
						LOG() << "Server: ID_DISCONNECTION_NOTIFICATION" << " [" << m_peer->NumberOfConnections() << "/" << m_peer->GetMaximumIncomingConnections() << "]";
						break;

					case ID_REQUEST_OBJECT:
						LOG() << "Server: ID_REQUEST_OBJECT";
						{
							auto bs = std::unique_ptr<RakNet::BitStream>(new RakNet::BitStream(packet->data, packet->length, false));
							bs->IgnoreBytes(sizeof(unsigned char));
							OnRequest(bs.get());
						}
						break;
				}
				break;

			default:
			case OFFLINE:
				break;
		}

		// Send packet to scripting side...
		OnReceive(packet);
	}
}

void root::Network::Reference(NetObject* object)
{
	object->SetNetworkIDManager(&m_network_id_manager);
	if (IsAutoritative()) {
		ReplicaManager3::Reference(object);
	}
}