/**
* @file JsonBinding.h
* @brief
*/

#pragma once

#include "ScriptObjectBinding.h"
#include <picojson.h>

namespace root
{
	/*----------------------------------------------------------------------------*/
	static picojson::value Parse(FileHandle* file)
	{
		picojson::value value;
		if (FileHandle::Ready(file)) {
			const char* data = static_cast<const char*>(file->GetData());
			picojson::parse(value, data, data + file->GetSize());
		}

		return value;
	}

	/*----------------------------------------------------------------------------*/
	inline picojson::array GetArray(const picojson::value& self, const std::string& key) {
		return self.get(key).get<picojson::array>();
	}
	/*----------------------------------------------------------------------------*/
	inline void SetArray(picojson::object& self, const std::string& key, const picojson::array& arr) {
		self[key] = picojson::value(arr);
	}

	/*----------------------------------------------------------------------------*/
	inline std::string GetString(const picojson::value& self, const std::string& key) {
		return self.get(key).get<std::string>();
	}
	/*----------------------------------------------------------------------------*/
	inline void SetString(picojson::object& self, const std::string& key, const std::string& value) {
		self[key] = picojson::value(value);
	}

	/*----------------------------------------------------------------------------*/
	inline bool GetBoolean(const picojson::value& self, const std::string& key) {
		return self.get(key).get<bool>();
	}
	/*----------------------------------------------------------------------------*/
	inline void SetBoolean(picojson::object& self, const std::string& key, bool value) {
		self[key] = picojson::value(value);
	}

	/*----------------------------------------------------------------------------*/
	inline double GetNumber(const picojson::value& self, const std::string& key) {
		return self.get(key).get<double>();
	}
	/*----------------------------------------------------------------------------*/
	inline void SetNumber(picojson::object& self, const std::string& key, double value) {
		self[key] = picojson::value(value);
	}

	/*----------------------------------------------------------------------------*/
	inline dvec3 GetVector3(const picojson::value& self, const std::string& key) {
		const picojson::array& vector_array = self.get(key).get<picojson::array>();
		dvec3 value;
		value.x = vector_array[0].get<double>();
		value.y = vector_array[1].get<double>();
		value.z = vector_array[2].get<double>();
		return value;
	}
	/*----------------------------------------------------------------------------*/
	inline void SetVector3(picojson::object& self, const std::string& key, const dvec3& value) {
		picojson::array vector_array;
		vector_array.push_back(picojson::value(value.x));
		vector_array.push_back(picojson::value(value.y));
		vector_array.push_back(picojson::value(value.z));
		self[key] = picojson::value(vector_array);
	}

	/*----------------------------------------------------------------------------*/
	inline dquat GetQuaternion(const picojson::value& self, const std::string& key) {
		const picojson::array& vector_array = self.get(key).get<picojson::array>();
		dquat value;
		value.x = vector_array[0].get<double>();
		value.y = vector_array[1].get<double>();
		value.z = vector_array[2].get<double>();
		value.w = vector_array[3].get<double>();
		return value;
	}
	/*----------------------------------------------------------------------------*/
	inline void SetQuaternion(picojson::object& self, const std::string& key, const dquat& value) {
		picojson::array vector_array;
		vector_array.push_back(picojson::value(value.x));
		vector_array.push_back(picojson::value(value.y));
		vector_array.push_back(picojson::value(value.z));
		vector_array.push_back(picojson::value(value.w));
		self[key] = picojson::value(vector_array);
	}

	/*----------------------------------------------------------------------------*/
	inline picojson::value GetObject(picojson::array& self, unsigned int index) {
		return self[index];
	}
	/*----------------------------------------------------------------------------*/
	inline void PushObject(picojson::array& self, const picojson::object& object) {
		self.push_back(picojson::value(object));
	}

	////////////////////////////////////////////////////////////////////////////////
	// JsonBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope JsonBinding()
	{
		return (
			luabind::namespace_("json")
			[
				luabind::def("parse", &Parse),

				luabind::class_<picojson::value>("Value")
				.def(luabind::constructor<const picojson::object&>())
				.def("get_array", &GetArray)
				.def("get_string", &GetString)
				.def("get_bool", &GetBoolean)
				.def("get_number", &GetNumber)
				.def("get_vec3", &GetVector3)
				.def("get_quat", &GetQuaternion)
				.def("serialize", (std::string(picojson::value::*)(bool) const) &picojson::value::serialize),

				luabind::class_<picojson::array>("Array")
				.def(luabind::constructor<>())
				.property("size", (int(picojson::array::*)()) &picojson::array::size)
				.def("get", &GetObject)
				.def("push", &PushObject),

				luabind::class_<picojson::object>("Object")
				.def(luabind::constructor<>())
				.def("set_array", &SetArray)
				.def("set_string", &SetString)
				.def("set_bool", &SetBoolean)
				.def("set_number", &SetNumber)
				.def("set_vec3", &SetVector3)
				.def("set_quat", &SetQuaternion)
			]
		);
	}
}