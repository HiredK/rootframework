/**
* @file PointCloud.h
* @brief
*/

#pragma once

#include <nanoflann.hpp>
#include <vector>
#include "Math.h"

namespace root
{
	template <typename T> class PointCloud
	{
		public:
			//! TYPEDEF/ENUMS:
			struct Point { T x, y, z; };

			//! CTOR/DTOR:
			PointCloud(int max_leaf_size = 10);
			virtual ~PointCloud();

			//! ADAPTORS:
			size_t kdtree_get_point_count() const;
			T kdtree_distance(const T *p1, const size_t idx_p2, size_t size) const;
			T kdtree_get_pt(const size_t idx, int dim) const;
			template <class BBOX> bool kdtree_get_bbox(BBOX& bb) const { return false; }

			//! SERVICES:
			int AddPoint(const dvec3& point);
			void Build();
			std::vector<std::pair<size_t, T>> RadiusSearch(const dvec3& query, double radius);
			void Clear();

		protected:
			//! TYPEDEF/ENUMS:
			typedef nanoflann::KDTreeSingleIndexAdaptor<
				nanoflann::L2_Simple_Adaptor<T, PointCloud<T>>, PointCloud<T>, 3> Adaptor;

			//! MEMBERS:
			std::vector<Point> m_points;
			Adaptor m_index_adaptor;
	};

	////////////////////////////////////////////////////////////////////////////////
	// PointCloud inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline PointCloud<T>::PointCloud(int max_leaf_size) :
		m_index_adaptor(3, *this, nanoflann::KDTreeSingleIndexAdaptorParams(max_leaf_size)) {
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline PointCloud<T>::~PointCloud() {
		Clear();
	}

	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline size_t PointCloud<T>::kdtree_get_point_count() const {
		return m_points.size();
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline T PointCloud<T>::kdtree_distance(const T *p1, const size_t idx_p2, size_t size) const {
		const T d0 = p1[0] - m_points[idx_p2].x;
		const T d1 = p1[1] - m_points[idx_p2].y;
		const T d2 = p1[2] - m_points[idx_p2].z;
		return d0*d0 + d1*d1 + d2*d2;
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline T PointCloud<T>::kdtree_get_pt(const size_t idx, int dim) const {
		if (dim == 0) return m_points[idx].x;
		else if (dim == 1) return m_points[idx].y;
		else return m_points[idx].z;
	}

	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline int PointCloud<T>::AddPoint(const dvec3& point) {
		m_points.push_back({ (T)point.x, (T)point.y, (T)point.z });
		return static_cast<int>(m_points.size()) - 1;
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline void PointCloud<T>::Build() {
		m_index_adaptor.buildIndex();
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline std::vector<std::pair<size_t, T>> PointCloud<T>::RadiusSearch(
		const dvec3& query, double radius)
	{
		std::vector<std::pair<size_t, T>> ret_matches;
		nanoflann::SearchParams params;
		params.sorted = true;

		m_index_adaptor.radiusSearch(&query[0], radius, ret_matches, params);
		return ret_matches;
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline void PointCloud<T>::Clear() {
		m_points.clear();
		m_points.shrink_to_fit();
	}
}