/**
* @file Image.h
* @brief Implements loading/decoding image from memory.
* JPG, PNG, TGA, BMP, PSD, GIF, HDR, PIC
*/

#pragma once

#include "FileHandle.h"
#include "Color.h"

namespace root
{
	class Image : public FileHandle
	{
		REGISTER_FILE(Image)

		public:
			//! CTOR/DTOR:
			Image(Engine* engine_instance, const std::string& path);
			virtual ~Image();

			//! SERVICES:
			bool ReadImageInfo();

			//! ACCESSORS:
			Color GetPixel(unsigned int x, unsigned int y) const;
			const void* GetPixels() const;
			int GetNumChannels() const;
			int GetW() const;
			int GetH() const;

			//! VIRTUALS:
			virtual bool IsProcessBackground();

		protected:
			//! VIRTUALS:
			virtual bool CreateData();
			virtual void DeleteData();

		private:
			//! MEMBERS:
			std::vector<unsigned char> m_pixels;
			int m_channels, m_w, m_h;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Image inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Image::Image(Engine* engine_instance, const std::string& path) :
		FileHandle(engine_instance, path), m_channels(0), m_w(0), m_h(0) {
	}
	/*----------------------------------------------------------------------------*/
	inline Image::~Image() {
	}

	/*----------------------------------------------------------------------------*/
	inline Color Image::GetPixel(unsigned int x, unsigned int y) const {
		const unsigned char* pixel = &m_pixels[(x + y * m_w) * m_channels];
		return Color(pixel[0], pixel[1], pixel[2], pixel[3]);
	}

	/*----------------------------------------------------------------------------*/
	inline const void* Image::GetPixels() const {
		return (!m_pixels.empty()) ? &m_pixels[0] : nullptr;
	}
	/*----------------------------------------------------------------------------*/
	inline int Image::GetNumChannels() const {
		return m_channels;
	}
	/*----------------------------------------------------------------------------*/
	inline int Image::GetW() const {
		return m_w;
	}
	/*----------------------------------------------------------------------------*/
	inline int Image::GetH() const {
		return m_h;
	}

	/*----------------------------------------------------------------------------*/
	inline bool Image::IsProcessBackground() {
		return false;
	}

} // root namespace