/**
* @file SFMLBinding.h
* @brief
*/

#pragma once

#include <SFML/System.hpp>
#include <SFML/Window.hpp>
#include "ScriptObjectBinding.h"
#include "Shader.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// EventBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope EventBinding()
	{
		return (
			luabind::class_<sf::Keyboard>("Keyboard")
			.enum_("Key")
			[
				luabind::value("A", sf::Keyboard::A),
				luabind::value("B", sf::Keyboard::B),
				luabind::value("C", sf::Keyboard::C),
				luabind::value("D", sf::Keyboard::D),
				luabind::value("E", sf::Keyboard::E),
				luabind::value("F", sf::Keyboard::F),
				luabind::value("G", sf::Keyboard::G),
				luabind::value("H", sf::Keyboard::H),
				luabind::value("I", sf::Keyboard::I),
				luabind::value("J", sf::Keyboard::J),
				luabind::value("K", sf::Keyboard::K),
				luabind::value("L", sf::Keyboard::L),
				luabind::value("M", sf::Keyboard::M),
				luabind::value("N", sf::Keyboard::N),
				luabind::value("O", sf::Keyboard::O),
				luabind::value("P", sf::Keyboard::P),
				luabind::value("Q", sf::Keyboard::Q),
				luabind::value("R", sf::Keyboard::R),
				luabind::value("S", sf::Keyboard::S),
				luabind::value("T", sf::Keyboard::T),
				luabind::value("U", sf::Keyboard::U),
				luabind::value("V", sf::Keyboard::V),
				luabind::value("W", sf::Keyboard::W),
				luabind::value("X", sf::Keyboard::X),
				luabind::value("Y", sf::Keyboard::Y),
				luabind::value("Z", sf::Keyboard::Z),
				luabind::value("Num0", sf::Keyboard::Num0),
				luabind::value("Num1", sf::Keyboard::Num1),
				luabind::value("Num2", sf::Keyboard::Num2),
				luabind::value("Num3", sf::Keyboard::Num3),
				luabind::value("Num4", sf::Keyboard::Num4),
				luabind::value("Num5", sf::Keyboard::Num5),
				luabind::value("Num6", sf::Keyboard::Num6),
				luabind::value("Num7", sf::Keyboard::Num7),
				luabind::value("Num8", sf::Keyboard::Num8),
				luabind::value("Num9", sf::Keyboard::Num9),
				luabind::value("Escape", sf::Keyboard::Escape),
				luabind::value("LControl", sf::Keyboard::LControl),
				luabind::value("LShift", sf::Keyboard::LShift),
				luabind::value("LAlt", sf::Keyboard::LAlt),
				luabind::value("LSystem", sf::Keyboard::LSystem),
				luabind::value("RControl", sf::Keyboard::RControl),
				luabind::value("RShift", sf::Keyboard::RShift),
				luabind::value("RAlt", sf::Keyboard::RAlt),
				luabind::value("RSystem", sf::Keyboard::RSystem),
				luabind::value("Menu", sf::Keyboard::Menu),
				luabind::value("LBracket", sf::Keyboard::LBracket),
				luabind::value("RBracket", sf::Keyboard::RBracket),
				luabind::value("SemiColon", sf::Keyboard::SemiColon),
				luabind::value("RBracket", sf::Keyboard::RBracket),
				luabind::value("Comma", sf::Keyboard::Comma),
				luabind::value("Period", sf::Keyboard::Period),
				luabind::value("Quote", sf::Keyboard::Quote),
				luabind::value("Slash", sf::Keyboard::Slash),
				luabind::value("BackSlash", sf::Keyboard::BackSlash),
				luabind::value("Tilde", sf::Keyboard::Tilde),
				luabind::value("Equal", sf::Keyboard::Equal),
				luabind::value("Dash", sf::Keyboard::Dash),
				luabind::value("Space", sf::Keyboard::Space),
				luabind::value("Return", sf::Keyboard::Return),
				luabind::value("BackSpace", sf::Keyboard::BackSpace),
				luabind::value("Tab", sf::Keyboard::Tab),
				luabind::value("PageUp", sf::Keyboard::PageUp),
				luabind::value("PageDown", sf::Keyboard::PageDown),
				luabind::value("End", sf::Keyboard::End),
				luabind::value("Home", sf::Keyboard::Home),
				luabind::value("Insert", sf::Keyboard::Insert),
				luabind::value("Delete", sf::Keyboard::Delete),
				luabind::value("Add", sf::Keyboard::Add),
				luabind::value("Subtract", sf::Keyboard::Subtract),
				luabind::value("Multiply", sf::Keyboard::Multiply),
				luabind::value("Divide", sf::Keyboard::Divide),
				luabind::value("Left", sf::Keyboard::Left),
				luabind::value("Right", sf::Keyboard::Right),
				luabind::value("Up", sf::Keyboard::Up),
				luabind::value("Down", sf::Keyboard::Down),
				luabind::value("Numpad0", sf::Keyboard::Numpad0),
				luabind::value("Numpad1", sf::Keyboard::Numpad1),
				luabind::value("Numpad2", sf::Keyboard::Numpad2),
				luabind::value("Numpad3", sf::Keyboard::Numpad3),
				luabind::value("Numpad4", sf::Keyboard::Numpad4),
				luabind::value("Numpad5", sf::Keyboard::Numpad5),
				luabind::value("Numpad6", sf::Keyboard::Numpad6),
				luabind::value("Numpad7", sf::Keyboard::Numpad7),
				luabind::value("Numpad8", sf::Keyboard::Numpad8),
				luabind::value("Numpad9", sf::Keyboard::Numpad9),
				luabind::value("F1", sf::Keyboard::F1),
				luabind::value("F2", sf::Keyboard::F2),
				luabind::value("F3", sf::Keyboard::F3),
				luabind::value("F4", sf::Keyboard::F4),
				luabind::value("F5", sf::Keyboard::F5),
				luabind::value("F6", sf::Keyboard::F6),
				luabind::value("F7", sf::Keyboard::F7),
				luabind::value("F8", sf::Keyboard::F8),
				luabind::value("F9", sf::Keyboard::F9),
				luabind::value("F10", sf::Keyboard::F10),
				luabind::value("F11", sf::Keyboard::F11),
				luabind::value("F12", sf::Keyboard::F12),
				luabind::value("F13", sf::Keyboard::F13),
				luabind::value("F14", sf::Keyboard::F14),
				luabind::value("F15", sf::Keyboard::F15)
			]
			.scope
			[
				luabind::def("is_key_pressed", &sf::Keyboard::isKeyPressed)
			],

			luabind::class_<sf::Mouse>("Mouse")
			.enum_("Button")
			[
				luabind::value("Left", sf::Mouse::Left),
				luabind::value("Right", sf::Mouse::Right),
				luabind::value("Middle", sf::Mouse::Middle),
				luabind::value("XButton1", sf::Mouse::XButton1),
				luabind::value("XButton2", sf::Mouse::XButton2),
				luabind::value("ButtonCount", sf::Mouse::ButtonCount)
			]
			.scope
			[
				luabind::def("is_button_pressed", &sf::Mouse::isButtonPressed),
				luabind::def("get_position", (sf::Vector2i(*)()) &sf::Mouse::getPosition),
				luabind::def("get_position", (sf::Vector2i(*)(const sf::Window&)) &sf::Mouse::getPosition)
			],
			
			luabind::class_<sf::Joystick>("Joystick")
			.enum_("Axis")
			[
				luabind::value("X", sf::Joystick::X),
				luabind::value("Y", sf::Joystick::Y),
				luabind::value("Z", sf::Joystick::Z),
				luabind::value("R", sf::Joystick::R),
				luabind::value("U", sf::Joystick::U),
				luabind::value("V", sf::Joystick::V),
				luabind::value("PovX", sf::Joystick::PovX),
				luabind::value("PovY", sf::Joystick::PovY)
			].scope
			[
				luabind::def("get_axis_position", &sf::Joystick::getAxisPosition),
				luabind::def("is_button_pressed", &sf::Joystick::isButtonPressed),
				luabind::def("get_button_count", &sf::Joystick::getButtonCount)
			],

			luabind::class_<sf::Event, luabind::no_bases, std::shared_ptr<sf::Event>>("Event")
			.enum_("EventType")
			[
				luabind::value("Closed", sf::Event::Closed),
				luabind::value("Resized", sf::Event::Resized),
				luabind::value("LostFocus", sf::Event::LostFocus),
				luabind::value("GainedFocus", sf::Event::GainedFocus),
				luabind::value("TextEntered", sf::Event::TextEntered),
				luabind::value("KeyPressed", sf::Event::KeyPressed),
				luabind::value("KeyReleased", sf::Event::KeyReleased),
				luabind::value("MouseWheelMoved", sf::Event::MouseWheelMoved),
				luabind::value("MouseButtonPressed", sf::Event::MouseButtonPressed),
				luabind::value("MouseButtonReleased", sf::Event::MouseButtonReleased),
				luabind::value("MouseMoved", sf::Event::MouseMoved),
				luabind::value("MouseEntered", sf::Event::MouseEntered),
				luabind::value("MouseLeft", sf::Event::MouseLeft),
				luabind::value("JoystickButtonPressed", sf::Event::JoystickButtonPressed),
				luabind::value("JoystickButtonReleased", sf::Event::JoystickButtonReleased),
				luabind::value("JoystickMoved", sf::Event::JoystickMoved),
				luabind::value("JoystickConnected", sf::Event::JoystickConnected),
				luabind::value("JoystickDisconnected", sf::Event::JoystickDisconnected),
				luabind::value("TouchBegan", sf::Event::TouchBegan),
				luabind::value("TouchMoved", sf::Event::TouchMoved),
				luabind::value("TouchEnded", sf::Event::TouchEnded),
				luabind::value("SensorChanged", sf::Event::SensorChanged),
				luabind::value("Count", sf::Event::Count)
			]
			.scope
			[
				luabind::class_<sf::Event::SizeEvent>("SizeEvent")
				.def_readonly("w", &sf::Event::SizeEvent::width)
				.def_readonly("h", &sf::Event::SizeEvent::height),

				luabind::class_<sf::Event::KeyEvent>("KeyEvent")
				.def_readonly("code", &sf::Event::KeyEvent::code)
				.def_readonly("alt", &sf::Event::KeyEvent::alt)
				.def_readonly("control", &sf::Event::KeyEvent::control)
				.def_readonly("shift", &sf::Event::KeyEvent::shift)
				.def_readonly("system", &sf::Event::KeyEvent::system),

				luabind::class_<sf::Event::TextEvent>("TextEvent")
				.def_readonly("unicode", &sf::Event::TextEvent::unicode),

				luabind::class_<sf::Event::MouseMoveEvent>("MouseMoveEvent")
				.def_readonly("x", &sf::Event::MouseMoveEvent::x)
				.def_readonly("y", &sf::Event::MouseMoveEvent::y),

				luabind::class_<sf::Event::MouseButtonEvent>("MouseButtonEvent")
				.def_readonly("button", &sf::Event::MouseButtonEvent::button)
				.def_readonly("x", &sf::Event::MouseButtonEvent::x)
				.def_readonly("y", &sf::Event::MouseButtonEvent::y),

				luabind::class_<sf::Event::MouseWheelEvent>("MouseWheelEvent")
				.def_readonly("delta", &sf::Event::MouseWheelEvent::delta)
				.def_readonly("x", &sf::Event::MouseWheelEvent::x)
				.def_readonly("y", &sf::Event::MouseWheelEvent::y),

				luabind::class_<sf::Event::JoystickConnectEvent>("JoystickConnectEvent")
				.def_readonly("joystickId", &sf::Event::JoystickConnectEvent::joystickId),

				luabind::class_<sf::Event::JoystickMoveEvent>("JoystickMoveEvent")
				.def_readonly("joystickId", &sf::Event::JoystickMoveEvent::joystickId)
				.def_readonly("axis", &sf::Event::JoystickMoveEvent::axis)
				.def_readonly("position", &sf::Event::JoystickMoveEvent::position),

				luabind::class_<sf::Event::JoystickButtonEvent>("JoystickButtonEvent")
				.def_readonly("joystickId", &sf::Event::JoystickButtonEvent::joystickId)
				.def_readonly("button", &sf::Event::JoystickButtonEvent::button),

				luabind::class_<sf::Event::TouchEvent>("TouchEvent")
				.def_readonly("finger", &sf::Event::TouchEvent::finger)
				.def_readonly("x", &sf::Event::TouchEvent::x)
				.def_readonly("y", &sf::Event::TouchEvent::y),


				luabind::class_<sf::Event::SensorEvent>("SensorEvent")
				.def_readonly("type", &sf::Event::SensorEvent::type)
				.def_readonly("x", &sf::Event::SensorEvent::x)
				.def_readonly("y", &sf::Event::SensorEvent::y)
				.def_readonly("z", &sf::Event::SensorEvent::z)
			]

			.def(luabind::constructor<>())
			.def_readonly("type", &sf::Event::type)
			.def_readonly("size", &sf::Event::size)
			.def_readonly("key", &sf::Event::key)
			.def_readonly("text", &sf::Event::text)
			.def_readonly("mouse_move", &sf::Event::mouseMove)
			.def_readonly("mouse_button", &sf::Event::mouseButton)
			.def_readonly("mouse_wheel", &sf::Event::mouseWheel)
			.def_readonly("joystick_move", &sf::Event::joystickMove)
			.def_readonly("joystick_button", &sf::Event::joystickButton)
			.def_readonly("joystick_connect", &sf::Event::joystickConnect)
			.def_readonly("touch", &sf::Event::touch)
			.def_readonly("sensor", &sf::Event::sensor)
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// WindowBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Window_wrapper : sf::Window, ScriptObject_wrapper
	{
		Window_wrapper(sf::VideoMode mode, const std::string& title, unsigned int style, const sf::ContextSettings& settings)
			: ScriptObject_wrapper("window", ScriptObject::Dynamic) {
			sf::Window::create(mode, title, style, settings);
		}
		Window_wrapper(sf::VideoMode mode, const std::string& title, unsigned int style)
			: Window_wrapper(mode, title, style, sf::ContextSettings()) {
		}
		virtual ~Window_wrapper() {
			Shader::Unbind();
		}

		void Create(sf::VideoMode mode, const std::string& title, unsigned int style, const sf::ContextSettings& settings) {
			sf::Window::create(mode, title, style, settings);
		}
		void Create(sf::VideoMode mode, const std::string& title, unsigned int style) {
			sf::Window::create(mode, title, style);
		}
		void SetTitle(const std::string& title) {
			sf::Window::setTitle(title);
		}

		int GetX() const { return sf::Window::getPosition().x; }
		void SetX(int x) { sf::Window::setPosition(sf::Vector2i(x, sf::Window::getPosition().y)); }
		int GetY() const { return sf::Window::getPosition().y; }
		void SetY(int y) { sf::Window::setPosition(sf::Vector2i(sf::Window::getPosition().x, y)); }
		void SetPosition(int x, int y) { sf::Window::setPosition(sf::Vector2i(x, y)); }

		unsigned int GetW() const { return sf::Window::getSize().x; }
		void SetW(unsigned int w) { sf::Window::setSize(sf::Vector2u(w, sf::Window::getSize().y)); }
		unsigned int GetH() const { return sf::Window::getSize().y; }
		void SetH(unsigned int h) { sf::Window::setSize(sf::Vector2u(sf::Window::getSize().x, h)); }
		void SetSize(unsigned int w, unsigned int h) { sf::Window::setSize(sf::Vector2u(w, h)); }
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope WindowBinding()
	{
		return (
			luabind::class_<sf::Vector2i>("Vector2i")
			.def_readwrite("x", &sf::Vector2i::x)
			.def_readwrite("y", &sf::Vector2i::y),

			luabind::class_<sf::ContextSettings>("ContextSettings")
			.enum_("Attribute")
			[
				luabind::value("Default", sf::ContextSettings::Default),
				luabind::value("Core", sf::ContextSettings::Core),
				luabind::value("Debug", sf::ContextSettings::Debug)
			]
			.def(luabind::constructor<unsigned int, unsigned int, unsigned int, unsigned int, unsigned int>())
			.def(luabind::constructor<unsigned int, unsigned int, unsigned int>())
			.def_readwrite("depth_bits", &sf::ContextSettings::depthBits)
			.def_readwrite("stencil_bits", &sf::ContextSettings::stencilBits)
			.def_readwrite("antialiasing_level", &sf::ContextSettings::antialiasingLevel)
			.def_readwrite("major_version", &sf::ContextSettings::majorVersion)
			.def_readwrite("minor_version", &sf::ContextSettings::minorVersion)
			.def_readwrite("attribute_flags", &sf::ContextSettings::attributeFlags),

			luabind::class_<sf::VideoMode>("VideoMode")
			.scope
			[
				luabind::def("get_desktop_mode", &sf::VideoMode::getDesktopMode)
			]
			.def(luabind::constructor<unsigned int, unsigned int, unsigned int>())
			.def(luabind::constructor<unsigned int, unsigned int>())
			.def(luabind::constructor<>())
			.def(luabind::self == luabind::other<sf::VideoMode>())
			.def(luabind::self < luabind::other<sf::VideoMode>())
			.def(luabind::self <= luabind::other<sf::VideoMode>())
			.def("is_valid", &sf::VideoMode::isValid)
			.def_readwrite("w", &sf::VideoMode::width)
			.def_readwrite("h", &sf::VideoMode::height)
			.def_readwrite("bpp", &sf::VideoMode::bitsPerPixel),

			luabind::class_<Window_wrapper, ScriptObject, std::shared_ptr<Window_wrapper>>("Window")
			.enum_("Style")
			[
				luabind::value("None", sf::Style::None),
				luabind::value("Titlebar", sf::Style::Titlebar),
				luabind::value("Resize", sf::Style::Resize),
				luabind::value("Close", sf::Style::Close),
				luabind::value("Fullscreen", sf::Style::Fullscreen),
				luabind::value("Default", sf::Style::Default)
			]
			.def(luabind::constructor<sf::VideoMode, const std::string&, unsigned int, const sf::ContextSettings&>())
			.def(luabind::constructor<sf::VideoMode, const std::string&, unsigned int>())
			.property("x", &Window_wrapper::GetX, &Window_wrapper::SetX)
			.property("y", &Window_wrapper::GetY, &Window_wrapper::SetY)
			.def("set_position", &Window_wrapper::SetPosition)
			.property("w", &Window_wrapper::GetW, &Window_wrapper::SetW)
			.property("h", &Window_wrapper::GetH, &Window_wrapper::SetH)
			.def("set_size", &Window_wrapper::SetSize)

			.def("create", (void(Window_wrapper::*)(sf::VideoMode, const std::string&, unsigned int, const sf::ContextSettings&)) &Window_wrapper::Create)
			.def("create", (void(Window_wrapper::*)(sf::VideoMode, const std::string&, unsigned int)) &Window_wrapper::Create)
			.def("close", &sf::Window::close)
			.def("is_open", &sf::Window::isOpen)
			.def("get_settings", &sf::Window::getSettings)
			.def("poll_event", &sf::Window::pollEvent)
			.def("wait_event", &sf::Window::waitEvent)
			.def("set_title", &Window_wrapper::SetTitle)
			//void setIcon(unsigned int width, unsigned int height, const Uint8* pixels); // todo
			.def("set_visible", &sf::Window::setVisible)
			.def("set_vertical_sync_enabled", &sf::Window::setVerticalSyncEnabled)
			.def("set_mouse_cursor_visible", &sf::Window::setMouseCursorVisible)
			.def("set_key_repeat_enabled", &sf::Window::setKeyRepeatEnabled)
			.def("set_framerate_limit", &sf::Window::setFramerateLimit)
			.def("set_joystick_threshold", &sf::Window::setJoystickThreshold)
			.def("set_active", &sf::Window::setActive)
			.def("request_focus", &sf::Window::requestFocus)
			.def("has_focus", &sf::Window::hasFocus)
			.def("display", &sf::Window::display),

			luabind::class_<sf::Window, Window_wrapper>("Window_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// SFMLBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope SFMLBinding()
	{
		return (
			luabind::namespace_("sf")
			[
				EventBinding(),
				WindowBinding()
			]
		);
	}

} // root namespace