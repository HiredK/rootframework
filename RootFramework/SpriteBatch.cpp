#include "SpriteBatch.h"

root::SpriteBatch::SpriteBatch(GLsizei size, GLenum usage)
	: Mesh(nullptr, size * 4, sizeof(Vertex2D), GL_TRIANGLES, usage, nullptr, size * 6)
	, m_size(size)
	, m_next(0)
{
	m_buffers[VertexBuffer]->SetMapExplicitRangeModifyActive();
	Mesh::UseDefaultVertex2D();

	GLBuffer::ScopedBinder ibo_binder(*m_buffers[IndiceBuffer].get());
	GLBuffer::ScopedMapper ibo_mapper(*m_buffers[IndiceBuffer].get());
	GLushort* indices = static_cast<GLushort*>(ibo_mapper.Get());

	// v0---v2
	// |  / |
	// | /  |
	// v1---v3

	for (GLsizei i = 0; i < m_size; ++i)
	{
		indices[i * 6 + 0] = i * 4 + 0;
		indices[i * 6 + 1] = i * 4 + 1;
		indices[i * 6 + 2] = i * 4 + 2;

		indices[i * 6 + 3] = i * 4 + 2;
		indices[i * 6 + 4] = i * 4 + 1;
		indices[i * 6 + 5] = i * 4 + 3;
	}
}

unsigned int root::SpriteBatch::Add(float x, float y, float w, float h, float u1, float v1, float u2, float v2, int index)
{
	GLBuffer::ScopedBinder vbo_binder(*m_buffers[VertexBuffer].get());
	if (m_next > 0 &&
		m_buffers[VertexBuffer]->GetMapExplicitRangeModifyActive()) {
		m_buffers[VertexBuffer]->Map();
	}

	Vertex2D data[] = {
		{ x + 0, y + 0, u1, v1, m_color.r, m_color.g, m_color.b, m_color.a },
		{ x + 0, y + h, u1, v2, m_color.r, m_color.g, m_color.b, m_color.a },
		{ x + w, y + 0, u2, v1, m_color.r, m_color.g, m_color.b, m_color.a },
		{ x + w, y + h, u2, v2, m_color.r, m_color.g, m_color.b, m_color.a }
	};

	if (index == -1) {
		m_buffers[VertexBuffer]->Fill(m_next * sizeof(data), sizeof(data), data);
		return m_next++;
	}
	else {
		m_buffers[VertexBuffer]->Fill(index * sizeof(data), sizeof(data), data);
	}

	return index;
}

void root::SpriteBatch::Flush()
{
	GLBuffer::ScopedBinder ibo_binder(*m_buffers[VertexBuffer].get());
	m_buffers[VertexBuffer]->Unmap();
	SetVertexCount(m_next * 6);
}

void root::SpriteBatch::Clear()
{
	m_next = 0;
}