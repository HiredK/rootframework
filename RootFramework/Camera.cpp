#include "Camera.h"

void root::Camera::Refresh()
{
	if (m_ortho) {
		m_proj_matrix = glm::ortho(m_bounds.x, m_bounds.y, m_bounds.z, m_bounds.w, m_clip.x, m_clip.y);
	}
	else {
		m_proj_matrix = glm::perspective(m_fov, m_aspect, m_clip.x, m_clip.y);
	}

	m_view_matrix = glm::affineInverse(glm::translate(dmat4(), m_position) * glm::mat4_cast(m_rotation));
	m_vp_matrix = m_proj_matrix * m_view_matrix;

	m_view_origin_matrix = glm::affineInverse(glm::mat4_cast(m_rotation));
	m_vp_origin_matrix = m_proj_matrix * m_view_origin_matrix;
	m_frustum.Transform(m_proj_matrix, m_view_matrix);
}