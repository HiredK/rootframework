/**
* @file GwenBinding.h
* @brief
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "GwenRenderer.h"

#pragma warning(push)
#pragma warning(disable : 4267)
#pragma warning(disable : 4244)
#include "Gwen/Controls/CrossSplitter.h"
#include "Gwen/Controls/WindowControl.h"
#include "Gwen/Controls/NumericUpDown.h"
#include "Gwen/Controls/CollapsibleList.h"
#include "Gwen/Controls.h"
#pragma warning(pop)

#define BIND_CONTROL_EVENT(Class, Name, Event, Func) \
{ \
	Class* control = dynamic_cast<Class*>(object); \
	object->RegisterSignalHandler(Name, new root::SignalHandler<void, Gwen::Controls::Base*>()); \
	control->Event.Add(control, &Func); \
};

#define WRAP_CONTROL_EVENT(Name, Func) \
void Func(Gwen::Controls::Base* control) { \
	root::ScriptObject_wrapper* wrapper = dynamic_cast<root::ScriptObject_wrapper*>(control); \
	wrapper->CallSignal<void, Gwen::Controls::Base*>(Name, control); \
};

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// BaseBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Base_wrapper : Gwen::Controls::Base, ScriptObject_wrapper
	{
		Base_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("base", ScriptObject::Static)
			, Gwen::Controls::Base(parent)
			, m_override_render_call(false) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::Base, "on_hover_enter", onHoverEnter, OnHoverEnter_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Base, "on_hover_leave", onHoverLeave, OnHoverLeave_wrap)
		}

		WRAP_CONTROL_EVENT("on_hover_enter", OnHoverEnter_wrap)
		WRAP_CONTROL_EVENT("on_hover_leave", OnHoverLeave_wrap)

		virtual void Render(Gwen::Skin::Base* skin)
		{
			if (m_override_render_call)
			{
				Shader::Get()->UniformMat4("u_ViewProjMatrix", mat4());
				glDisable(GL_SCISSOR_TEST);
				glDisable(GL_BLEND);

				try {
					luabind::wrap_base::call<void>("__render", skin);
				}
				catch (std::exception const& e) {
					LOG(Logger::Error) << e.what();
				}
				catch (...) {
					LOG(Logger::Error) << "Error!";
				}

				Shader::Get()->UniformMat4("u_ViewProjMatrix", glm::ortho(0.0f, float(GetCanvas()->Width()), float(GetCanvas()->Height()), 0.0f, -1.0f, 1.0f));
				glActiveTexture(GL_TEXTURE0);
				glEnable(GL_SCISSOR_TEST);

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
				glEnable(GL_BLEND);
			}
		}
		static void Render_default(Base_wrapper* ptr, Gwen::Skin::Base* skin) {
			ptr->Base_wrapper::Render(skin);
		}

		int GetX() const { return Gwen::Controls::Base::X(); }
		void SetX(int x) { Gwen::Controls::Base::SetPos(x, Gwen::Controls::Base::Y()); }
		int GetY() const { return Gwen::Controls::Base::Y(); }
		void SetY(int y) { Gwen::Controls::Base::SetPos(Gwen::Controls::Base::X(), y); }

		int GetW() const { return Gwen::Controls::Base::Width(); }
		void SetW(int w) { Gwen::Controls::Base::SetWidth(w); }
		int GetH() const { return Gwen::Controls::Base::Height(); }
		void SetH(int h) { Gwen::Controls::Base::SetHeight(h); }

		int GetMarginTop() const { return Gwen::Controls::Base::GetMargin().top; }
		void SetMarginTop(int top) { return Gwen::Controls::Base::SetMargin(Gwen::Margin(GetMarginLeft(), top, GetMarginRight(), GetMarginBottom())); }
		int GetMarginBottom() const { return Gwen::Controls::Base::GetMargin().bottom; }
		void SetMarginBottom(int bottom) { return Gwen::Controls::Base::SetMargin(Gwen::Margin(GetMarginLeft(), GetMarginTop(), GetMarginRight(), bottom)); }
		int GetMarginLeft() const { return Gwen::Controls::Base::GetMargin().left; }
		void SetMarginLeft(int left) { return Gwen::Controls::Base::SetMargin(Gwen::Margin(left, GetMarginTop(), GetMarginRight(), GetMarginBottom())); }
		int GetMarginRight() const { return Gwen::Controls::Base::GetMargin().right; }
		void SetMarginRight(int right) { return Gwen::Controls::Base::SetMargin(Gwen::Margin(GetMarginLeft(), GetMarginTop(), right, GetMarginBottom())); }

		Gwen::Point LocalPosToCanvas() { return Gwen::Controls::Base::LocalPosToCanvas(Gwen::Point(0, 0)); }
		bool IsHovered() const { return (Gwen::HoveredControl == this); }

		void ClearToolTip() { Gwen::Controls::Base::SetToolTip(nullptr); }
		void EnableOverrideRenderCall() { m_override_render_call = true; }
		bool m_override_render_call;
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope BaseBinding()
	{
		return (
			luabind::class_<Base_wrapper, ScriptObject, std::shared_ptr<Base_wrapper>>("Base")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.enum_("Pos")
			[
				luabind::value("None", Gwen::Pos::None),
				luabind::value("Left", Gwen::Pos::Left),
				luabind::value("Right", Gwen::Pos::Right),
				luabind::value("Top", Gwen::Pos::Top),
				luabind::value("Bottom", Gwen::Pos::Bottom),
				luabind::value("CenterV", Gwen::Pos::CenterV),
				luabind::value("CenterH", Gwen::Pos::CenterH),
				luabind::value("Fill", Gwen::Pos::Fill),
				luabind::value("Center", Gwen::Pos::Center)
			]

			.def_readonly("canvas", &Gwen::Controls::Base::GetCanvas)
			.def("get_canvas", &Gwen::Controls::Base::GetCanvas)

			.def("local_pos_to_canvas", (Gwen::Point(Gwen::Controls::Base::*)(const Gwen::Point&)) &Gwen::Controls::Base::LocalPosToCanvas)
			.def("local_pos_to_canvas", (Gwen::Point(Base_wrapper::*)()) &Base_wrapper::LocalPosToCanvas)
			.def("canvas_pos_to_local", &Gwen::Controls::Base::CanvasPosToLocal)

			.def("bring_to_front", &Gwen::Controls::Base::BringToFront)
			.def("send_to_back", &Gwen::Controls::Base::SendToBack)

			.property("x", &Base_wrapper::GetX, &Base_wrapper::SetX)
			.property("y", &Base_wrapper::GetY, &Base_wrapper::SetY)
			.def("set_position", (void(Gwen::Controls::Base::*)(int, int)) &Gwen::Controls::Base::SetPos)
			.property("w", &Base_wrapper::GetW, &Base_wrapper::SetW)
			.property("h", &Base_wrapper::GetH, &Base_wrapper::SetH)
			.def("set_size", (bool(Gwen::Controls::Base::*)(int, int)) &Gwen::Controls::Base::SetSize)

			.property("margin_top", &Base_wrapper::GetMarginTop, &Base_wrapper::SetMarginTop)
			.property("margin_bottom", &Base_wrapper::GetMarginBottom, &Base_wrapper::SetMarginBottom)
			.property("margin_left", &Base_wrapper::GetMarginLeft, &Base_wrapper::SetMarginLeft)
			.property("margin_right", &Base_wrapper::GetMarginRight, &Base_wrapper::SetMarginRight)

			.property("num_children", &Gwen::Controls::Base::NumChildren)
			.def("set_parent", &Gwen::Controls::Base::SetParent)

			.def("set_bounds", (bool(Gwen::Controls::Base::*)(int, int, int, int)) &Gwen::Controls::Base::SetBounds)
			.def("set_bounds", (bool(Gwen::Controls::Base::*)(const Gwen::Rect&)) &Gwen::Controls::Base::SetBounds)

			.def("position", (void(Gwen::Controls::Base::*)(int, int, int)) &Gwen::Controls::Base::Position)
			.def("position", (void(Gwen::Controls::Base::*)(int)) &Gwen::Controls::Base::Position)
			.def("dock", &Gwen::Controls::Base::Dock)

			.property("disabled", &Gwen::Controls::Base::IsDisabled, &Gwen::Controls::Base::SetDisabled)
			.def("is_disabled", &Gwen::Controls::Base::IsDisabled)
			.def("set_disabled", &Gwen::Controls::Base::SetDisabled)
			
			.property("hidden", &Gwen::Controls::Base::Hidden, &Base_wrapper::SetHidden)
			.property("visible", &Gwen::Controls::Base::Visible)
			.def("hide", &Gwen::Controls::Base::Hide)
			.def("show", &Gwen::Controls::Base::Show)

			.def("is_hovered", &Base_wrapper::IsHovered)

			.def("has_focus", &Gwen::Controls::Base::HasFocus)
			.def("focus", &Gwen::Controls::Base::Focus)
			.def("blur", &Gwen::Controls::Base::Blur)

			.def("set_tooltip", (void(Gwen::Controls::Base::*)(const Gwen::TextObject&)) &Gwen::Controls::Base::SetToolTip)
			.def("set_tooltip", (void(Gwen::Controls::Base::*)(Gwen::Controls::Base*)) &Gwen::Controls::Base::SetToolTip)
			.def("get_tooltip", &Gwen::Controls::Base::GetToolTip)
			.def("clear_tooltip", &Base_wrapper::ClearToolTip)
			
			.def("redraw", &Gwen::Controls::Base::Redraw)
			.def("__render", &Gwen::Controls::Base::DoRender, &Base_wrapper::Render_default)
			.def("enable_override_render_call", &Base_wrapper::EnableOverrideRenderCall),

			luabind::class_<Gwen::Controls::Base, Base_wrapper>("Base_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CanvasBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Canvas_wrapper : Gwen::Controls::Canvas, ScriptObject_wrapper
	{
		Canvas_wrapper(unsigned int w, unsigned int h, const std::string& skin_path)
			: ScriptObject_wrapper("canvas", ScriptObject::Static)
			, Gwen::Controls::Canvas(nullptr)
			, renderer(new GwenRenderer(GetEngineInstance(), w, h))
			, mouse_x(0)
			, mouse_y(0)
		{
			Gwen::Skin::TexturedBase* textured_skin = new Gwen::Skin::TexturedBase(renderer.get());
			textured_skin->Init(skin_path);

			skin = std::unique_ptr<Gwen::Skin::Base>(textured_skin);
			skin->SetDefaultFont(L"arial.ttf", 12.0f);
			Canvas::SetSkin(skin.get());
			Canvas::SetSize(w, h);

			BindSignalHandlers(this);
		}
		Canvas_wrapper(unsigned int w, unsigned int h)
			: Canvas_wrapper(w, h, "") {
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		unsigned char TranslateKeyCode(int key)
		{
			switch (key) {
				case sf::Keyboard::BackSpace: return Gwen::Key::Backspace;
				case sf::Keyboard::Return:	  return Gwen::Key::Return;
				case sf::Keyboard::Escape:    return Gwen::Key::Escape;
				case sf::Keyboard::Tab:       return Gwen::Key::Tab;
				case sf::Keyboard::Space:	  return Gwen::Key::Space;
				case sf::Keyboard::Up:        return Gwen::Key::Up;
				case sf::Keyboard::Down:      return Gwen::Key::Down;
				case sf::Keyboard::Left:      return Gwen::Key::Left;
				case sf::Keyboard::Right:     return Gwen::Key::Right;
				case sf::Keyboard::Home:      return Gwen::Key::Home;
				case sf::Keyboard::End:	      return Gwen::Key::End;
				case sf::Keyboard::Delete:    return Gwen::Key::Delete;
				case sf::Keyboard::LControl:  return Gwen::Key::Control;
				case sf::Keyboard::LAlt:      return Gwen::Key::Alt;
				case sf::Keyboard::LShift:    return Gwen::Key::Shift;
				case sf::Keyboard::RControl:  return Gwen::Key::Control;
				case sf::Keyboard::RAlt:	  return Gwen::Key::Alt;
				case sf::Keyboard::RShift:	  return Gwen::Key::Shift;
				default:
					break;
			}
				
			return Gwen::Key::Invalid;
		}

		bool ProcessEvent(const sf::Event& e)
		{
			switch (e.type) {
				case sf::Event::MouseMoved: {
					int dx = e.mouseMove.x - mouse_x;
					int dy = e.mouseMove.y - mouse_y;
					mouse_x = e.mouseMove.x;
					mouse_y = e.mouseMove.y;

					return InputMouseMoved(mouse_x, mouse_y, dx, dy);
				}

				case sf::Event::MouseButtonPressed:
				case sf::Event::MouseButtonReleased:
					return InputMouseButton(e.mouseButton.button,
						(e.type == sf::Event::MouseButtonPressed));

				case sf::Event::MouseWheelMoved:
					return InputMouseWheel(e.mouseWheel.delta * 60);

				case sf::Event::TextEntered:
					if (e.text.unicode == 96) break; // ignore tilde '~'
					return InputCharacter(e.text.unicode);

				case sf::Event::KeyPressed:
				case sf::Event::KeyReleased: {
					bool pressed = (e.type == sf::Event::KeyPressed);
					if (e.key.control && pressed && e.key.code >= 'a' && e.key.code <= 'z') {
						return InputCharacter(e.key.code);
					}

					unsigned char key = TranslateKeyCode(e.key.code);
					return InputKey(key, pressed);
				}

				default:
					break;
			}

			return false;
		}

		void Draw()
		{
			glActiveTexture(GL_TEXTURE0);

			if (!renderer->GetRenderToFBO() || Canvas::NeedsRedraw()) {
				renderer->SetSize(Canvas::Width(), Canvas::Height());
				Canvas::RenderCanvas();
			}

			if (renderer->GetRenderToFBO()) {
				FrameBuffer::Unbind();
				renderer->BlitFBOTextureToScreenQuad();
			}
		}

		GwenRenderer* GetRenderer() {
			return renderer.get();
		}

		std::unique_ptr<GwenRenderer> renderer;
		std::unique_ptr<Gwen::Skin::Base> skin;
		int mouse_x;
		int mouse_y;
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope CanvasBinding()
	{
		return (
			luabind::class_<Canvas_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<Canvas_wrapper>>("Canvas")
			.def(luabind::constructor<unsigned int, unsigned int, const std::string&>())
			.def(luabind::constructor<unsigned int, unsigned int>())

			.property("renderer", &Canvas_wrapper::GetRenderer)
			.def("process_event", &Canvas_wrapper::ProcessEvent)
			.def("draw", &Canvas_wrapper::Draw),

			luabind::class_<Gwen::Controls::Canvas, Canvas_wrapper>("Canvas_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CrossSplitterBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct CrossSplitter_wrapper : Gwen::Controls::CrossSplitter, ScriptObject_wrapper
	{
		CrossSplitter_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("cross_splitter", ScriptObject::Static)
			, Gwen::Controls::CrossSplitter(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope CrossSplitterBinding()
	{
		return (
			luabind::class_<CrossSplitter_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<CrossSplitter_wrapper>>("CrossSplitter")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.def("set_panel", &Gwen::Controls::CrossSplitter::SetPanel),

			luabind::class_<Gwen::Controls::CrossSplitter, CrossSplitter_wrapper>("CrossSplitter_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// DockBaseBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct DockBase_wrapper : Gwen::Controls::DockBase, ScriptObject_wrapper
	{
		DockBase_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("dock_base", ScriptObject::Static)
			, Gwen::Controls::DockBase(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		void SetLeftSize(int w) { GetLeft()->SetWidth(w); }
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope DockBaseBinding()
	{
		return (
			luabind::class_<DockBase_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<DockBase_wrapper>>("DockBase")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.def("get_tab_control", &Gwen::Controls::DockBase::GetTabControl)
			.def("get_right", &Gwen::Controls::DockBase::GetRight)
			.def("set_left_size", &DockBase_wrapper::SetLeftSize)
			.def("get_left", &Gwen::Controls::DockBase::GetLeft)
			.def("get_top", &Gwen::Controls::DockBase::GetTop)
			.def("get_bottom", &Gwen::Controls::DockBase::GetBottom),

			luabind::class_<Gwen::Controls::DockBase, DockBase_wrapper>("DockBase_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ImagePanelBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ImagePanel_wrapper : Gwen::Controls::ImagePanel, ScriptObject_wrapper
	{
		ImagePanel_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("image_panel", ScriptObject::Static)
			, Gwen::Controls::ImagePanel(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		void SetDrawColor(const Color& col) {
			ImagePanel::SetDrawColor(Gwen::Color(col.r, col.g, col.a, col.a));
		}
		Gwen::Color& DrawColor() {
			return m_DrawColor;
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ImagePanelBinding()
	{
		return (
			luabind::class_<ImagePanel_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<ImagePanel_wrapper>>("ImagePanel")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.property("color", &ImagePanel_wrapper::DrawColor, (void(ImagePanel_wrapper::*)(const Color&)) &ImagePanel_wrapper::SetDrawColor)		
			.def("size_to_contents", &Gwen::Controls::ImagePanel::SizeToContents)
			.def("set_image", &Gwen::Controls::ImagePanel::SetImage)
			.def("set_uv", &Gwen::Controls::ImagePanel::SetUV),

			luabind::class_<Gwen::Controls::ImagePanel, ImagePanel_wrapper>("ImagePanel_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// LabelBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Label_wrapper : Gwen::Controls::Label, ScriptObject_wrapper
	{
		Label_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("label", ScriptObject::Static)
			, Gwen::Controls::Label(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		void SetTextColor(const Color& col) {
			m_Text->SetTextColor(Gwen::Color(col.r, col.g, col.a, col.a));
		}
		Gwen::Color& TextColor() {
			return const_cast<Gwen::Color&>(m_Text->TextColor());
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope LabelBinding()
	{
		return (
			luabind::class_<Label_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<Label_wrapper>>("Label")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.property("color", &Label_wrapper::TextColor, (void(Label_wrapper::*)(const Color&)) &Label_wrapper::SetTextColor)

			.property("text", &Gwen::Controls::Label::GetText, (void(Gwen::Controls::Label::*)(const Gwen::TextObject&)) &Gwen::Controls::Label::SetText)
			.def("set_text", (void(Gwen::Controls::Label::*)(const Gwen::TextObject&)) &Gwen::Controls::Label::SetText)
			.def("set_text", (void(Gwen::Controls::Label::*)(const Gwen::TextObject&, bool)) &Gwen::Controls::Label::SetText)
			.def("get_text", &Gwen::Controls::Label::GetText)
			
			.def("set_alignment", &Gwen::Controls::Label::SetAlignment)
			.def("set_wrap", &Gwen::Controls::Label::SetWrap)
			.def("set_font", (void(Gwen::Controls::Label::*)(Gwen::Font*)) &Gwen::Controls::Label::SetFont),

			luabind::class_<Gwen::Controls::Label, Label_wrapper>("Label_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ButtonBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Button_wrapper : Gwen::Controls::Button, ScriptObject_wrapper
	{
		Button_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("button", ScriptObject::Static)
			, Gwen::Controls::Button(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_press", onPress, OnPress_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_right_press", onRightPress, OnRightPress_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_down", onDown, OnDown_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_up", onUp, OnUp_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_double_click", onDoubleClick, OnDoubleClick_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_toggle", onToggle, OnToggle_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_toggle_on", onToggleOn, OnToggleOn_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::Button, "on_toggle_off", onToggleOff, OnToggleOff_wrap)
			Base_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_press", OnPress_wrap)
		WRAP_CONTROL_EVENT("on_right_press", OnRightPress_wrap)
		WRAP_CONTROL_EVENT("on_down", OnDown_wrap)
		WRAP_CONTROL_EVENT("on_up", OnUp_wrap)
		WRAP_CONTROL_EVENT("on_double_click", OnDoubleClick_wrap)
		WRAP_CONTROL_EVENT("on_toggle", OnToggle_wrap)
		WRAP_CONTROL_EVENT("on_toggle_on", OnToggleOn_wrap)
		WRAP_CONTROL_EVENT("on_toggle_off", OnToggleOff_wrap)

		// Special case where we want to define uvs for the icon to use a texture atlas
		// instead of loading each icons individually.
		void SetImage(const Gwen::TextObject& strName, bool bCenter,
			float u1, float v1, float u2, float v2, float w, float h)
		{
			Button_wrapper::SetImage(strName, bCenter);
			GetImage()->SetUV(u1, v1, u2, v2);
			GetImage()->SetSize((int)w, (int)h);

			Gwen::Padding padding = GetTextPadding();
			padding.left = (int)w + 2;
			SetTextPadding(padding);
		}

		void SetImage(const Gwen::TextObject& strName, bool bCenter) {
			Gwen::Controls::Button::SetImage(strName, bCenter);
		}

		// Special case where we want to keep a toggled button active when pressed.
		void SetToggleState(bool b, bool bFireEvent) {
			if (bFireEvent) {
				Gwen::Controls::Button::SetToggleState(b);
			}
			else {
				m_bToggleStatus = b;
				Redraw();
			}
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ButtonBinding()
	{
		return (
			luabind::class_<Button_wrapper, luabind::bases<Gwen::Controls::Label, ScriptObject>, std::shared_ptr<Button_wrapper>>("Button")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.property("toggle_state", &Gwen::Controls::Button::GetToggleState, (void(Gwen::Controls::Button::*)(bool)) &Gwen::Controls::Button::SetToggleState)
			.def("set_image", (void(Button_wrapper::*)(const Gwen::TextObject&, bool, float, float, float, float, float, float)) &Button_wrapper::SetImage)
			.def("set_image", (void(Button_wrapper::*)(const Gwen::TextObject&, bool)) &Button_wrapper::SetImage)
			.def("set_is_toggle", &Gwen::Controls::Button::SetIsToggle)
			.def("set_toggle_state", (void(Button_wrapper::*)(bool, bool)) &Button_wrapper::SetToggleState)
			.def("set_toggle_state", (void(Gwen::Controls::Button::*)(bool)) &Gwen::Controls::Button::SetToggleState)
			.def("get_toggle_state", &Gwen::Controls::Button::GetToggleState)
			.def("press", &Gwen::Controls::Button::OnPress)
			.def("size_to_contents", &Gwen::Controls::Button::SizeToContents),

			luabind::class_<Gwen::Controls::Button, Button_wrapper>("Button_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// GroupBoxBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct GroupBox_wrapper : Gwen::Controls::GroupBox, ScriptObject_wrapper
	{
		GroupBox_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("group_box", ScriptObject::Static)
			, Gwen::Controls::GroupBox(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope GroupBoxBinding()
	{
		return (
			luabind::class_<GroupBox_wrapper, luabind::bases<Gwen::Controls::Label, ScriptObject>, std::shared_ptr<GroupBox_wrapper>>("GroupBox")
			.def(luabind::constructor<Gwen::Controls::Base*>()),

			luabind::class_<Gwen::Controls::GroupBox, GroupBox_wrapper>("GroupBox_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TabButtonBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TabButton_wrapper : Gwen::Controls::TabButton, ScriptObject_wrapper
	{
		TabButton_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("tab_button", ScriptObject::Static)
			, Gwen::Controls::TabButton(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Button_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TabButtonBinding()
	{
		return (
			luabind::class_<TabButton_wrapper, luabind::bases<Gwen::Controls::Button, ScriptObject>, std::shared_ptr<TabButton_wrapper>>("TabButton")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			.property("page", &Gwen::Controls::TabButton::GetPage)
			.def("is_active", &Gwen::Controls::TabButton::IsActive),

			luabind::class_<Gwen::Controls::TabButton, TabButton_wrapper>("TabButton_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TabControlBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TabControl_wrapper : Gwen::Controls::TabControl, ScriptObject_wrapper
	{
		TabControl_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("tab_control", ScriptObject::Static)
			, Gwen::Controls::TabControl(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		TabButton_wrapper* AddPage(TabButton_wrapper* button, const Gwen::TextObject strText, Gwen::Controls::Base* pPage)
		{
			pPage->SetParent(this);
			button->SetParent(TabControl::GetTabStrip());
			button->SetText(strText);
			button->SetPage(pPage);
			button->SetTabable(false);

			TabControl::AddPage(button);
			return button;
		}

		TabButton_wrapper* AddPage(TabButton_wrapper* button, const Gwen::TextObject strText)
		{
			Gwen::Controls::Base* pPage = new Base(this);
			return AddPage(button, strText, pPage);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TabControlBinding()
	{
		return (
			luabind::class_<TabControl_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<TabControl_wrapper>>("TabControl")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.property("tab_count", &Gwen::Controls::TabControl::TabCount)
			.def("add_page", (TabButton_wrapper*(TabControl_wrapper::*)(TabButton_wrapper*, const Gwen::TextObject, Gwen::Controls::Base*)) &TabControl_wrapper::AddPage)
			.def("add_page", (TabButton_wrapper*(TabControl_wrapper::*)(TabButton_wrapper*, const Gwen::TextObject)) &TabControl_wrapper::AddPage)
			.def("add_page", (Gwen::Controls::TabButton*(Gwen::Controls::TabControl::*)(const Gwen::TextObject, Gwen::Controls::Base*)) &Gwen::Controls::TabControl::AddPage)
			.def("remove_page", &Gwen::Controls::TabControl::RemovePage),

			luabind::class_<Gwen::Controls::TabControl, TabControl_wrapper>("TabControl_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TreeNodeBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TreeNode_wrapper : Gwen::Controls::TreeNode, ScriptObject_wrapper
	{
		TreeNode_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("tree_node", ScriptObject::Static)
			, Gwen::Controls::TreeNode(parent) {
			BindSignalHandlers(this);

			// BugFix: Add onDoubleClick event from the title button
			RegisterSignalHandler("on_double_click", new root::SignalHandler<void, Gwen::Controls::Base*>());
			m_Title->onDoubleClick.Add(this, &TreeNode_wrapper::OnDoubleClick);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::TreeNode, "on_name_press", onNamePress, OnNamePress_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::TreeNode, "on_right_press", onRightPress, OnRightPress_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::TreeNode, "on_select_change", onSelectChange, OnSelectChange_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::TreeNode, "on_select", onSelect, OnSelect_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::TreeNode, "on_unselect", onUnselect, OnUnselect_wrap)
			Base_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_name_press", OnNamePress_wrap)
		WRAP_CONTROL_EVENT("on_right_press", OnRightPress_wrap)
		WRAP_CONTROL_EVENT("on_select_change", OnSelectChange_wrap)
		WRAP_CONTROL_EVENT("on_select", OnSelect_wrap)
		WRAP_CONTROL_EVENT("on_unselect", OnUnselect_wrap)

		void OnDoubleClick(Gwen::Controls::Base* control) {
			CallSignal<void, Gwen::Controls::Base*>("on_double_click", control);
		};

		TreeNode_wrapper* AddNode(std::shared_ptr<TreeNode_wrapper> node, const Gwen::TextObject& strLabel, const Gwen::TextObject& strIconName)
		{
			node->SetParent(this);
			node->SetText(strLabel);
			node->GetButton()->SetImage(strIconName);
			node->Dock(Gwen::Pos::Top);
			node->SetRoot(gwen_cast<Gwen::Controls::TreeControl>(this) != nullptr);
			node->SetTreeControl(m_TreeControl);

			if (m_TreeControl) {
				m_TreeControl->OnNodeAdded(node.get());
			}

			return node.get();
		}

		// Special case where we want to define uvs for the icon to use a texture atlas
		// instead of loading each icons individually.
		TreeNode_wrapper* AddNode(std::shared_ptr<TreeNode_wrapper> node, const Gwen::TextObject& strLabel, const Gwen::TextObject& strIconName,
			float u1, float v1, float u2, float v2, float w, float h)
		{
			TreeNode_wrapper* result = TreeNode_wrapper::AddNode(node, strLabel, strIconName);
			result->GetButton()->GetImage()->SetUV(u1, v1, u2, v2);
			result->GetButton()->GetImage()->SetSize((int)w, (int)h);

			Gwen::Padding padding = result->GetButton()->GetTextPadding();
			padding.left = (int)w + 2;
			result->GetButton()->SetTextPadding(padding);
			return result;
		}

		TreeNode_wrapper* AddNode(std::shared_ptr<TreeNode_wrapper> node, const Gwen::TextObject& strLabel) {
			return AddNode(node, strLabel, L"");
		}

		// Bugfix: When right-clicking multiple nodes, only the clicked on is selected.
		void SetSelected(bool b, bool FireEvents) {
			if (sf::Mouse::isButtonPressed(sf::Mouse::Button::Right) &&
				Gwen::Input::IsKeyDown(Gwen::Key::Control)) {
				return;
			}

			Gwen::Controls::TreeNode::SetSelected(b, FireEvents);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope TreeNodeBinding()
	{
		return (
			luabind::class_<TreeNode_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<TreeNode_wrapper>>("TreeNode")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.property("text", &Gwen::Controls::TreeNode::GetText, (void(Gwen::Controls::TreeNode::*)(const Gwen::TextObject&)) &Gwen::Controls::TreeNode::SetText)
			.def("set_text", (void(Gwen::Controls::TreeNode::*)(const Gwen::TextObject&)) &Gwen::Controls::TreeNode::SetText)
			.def("set_text", (void(Gwen::Controls::TreeNode::*)(const Gwen::TextObject&)) &Gwen::Controls::TreeNode::SetText)
			.def("get_text", &Gwen::Controls::TreeNode::GetText)
			
			.def("add_node", (TreeNode_wrapper*(TreeNode_wrapper::*)(std::shared_ptr<TreeNode_wrapper>, const Gwen::TextObject&, const Gwen::TextObject&, float, float, float, float, float, float)) &TreeNode_wrapper::AddNode)
			.def("add_node", (TreeNode_wrapper*(TreeNode_wrapper::*)(std::shared_ptr<TreeNode_wrapper>, const Gwen::TextObject&, const Gwen::TextObject&)) &TreeNode_wrapper::AddNode)
			.def("add_node", (TreeNode_wrapper*(TreeNode_wrapper::*)(std::shared_ptr<TreeNode_wrapper>, const Gwen::TextObject&)) &TreeNode_wrapper::AddNode)

			.def("set_selected", (void(Gwen::Controls::TreeNode::*)(bool, bool)) &Gwen::Controls::TreeNode::SetSelected)
			.def("is_selected", &Gwen::Controls::TreeNode::IsSelected)

			.def("open", &Gwen::Controls::TreeNode::Open)
			.def("expand_all", &Gwen::Controls::TreeNode::ExpandAll),

			luabind::class_<Gwen::Controls::TreeNode, TreeNode_wrapper>("TreeNode_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TreeControlBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TreeControl_wrapper : Gwen::Controls::TreeControl, ScriptObject_wrapper
	{
		TreeControl_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("tree_control", ScriptObject::Static)
			, Gwen::Controls::TreeControl(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			TreeNode_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TreeControlBinding()
	{
		return (
			luabind::class_<TreeControl_wrapper, luabind::bases<Gwen::Controls::TreeNode, ScriptObject>, std::shared_ptr<TreeControl_wrapper>>("TreeControl")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.def("allow_multiselect", &Gwen::Controls::TreeControl::AllowMultiSelect)
			.def("clear", &Gwen::Controls::TreeControl::Clear),

			luabind::class_<Gwen::Controls::TreeControl, TreeControl_wrapper>("TreeControl_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// SliderBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct HorizontalSlider_wrapper : Gwen::Controls::HorizontalSlider, ScriptObject_wrapper
	{
		HorizontalSlider_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("horizontal_slider_control", ScriptObject::Static)
			, Gwen::Controls::HorizontalSlider(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::Slider, "on_value_changed", onValueChanged, OnValueChanged_wrap)
			Base_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_value_changed", OnValueChanged_wrap)
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope SliderBinding()
	{
		return (
			luabind::class_<Gwen::Controls::Slider, Gwen::Controls::Base>("Slider")
			.property("value", &Gwen::Controls::Slider::GetFloatValue, (void(Gwen::Controls::Slider::*)(float)) &Gwen::Controls::Slider::SetFloatValue)
			.def("set_value", (void(Gwen::Controls::Slider::*)(float, bool)) &Gwen::Controls::Slider::SetFloatValue)
			.def("set_value", (void(Gwen::Controls::Slider::*)(float)) &Gwen::Controls::Slider::SetFloatValue)
			.def("get_value", &Gwen::Controls::Slider::GetFloatValue)
			.def("set_clamp_to_notches", &Gwen::Controls::Slider::SetClampToNotches)
			.def("set_range", &Gwen::Controls::Slider::SetRange)
			.def("set_notch_count", &Gwen::Controls::Slider::SetNotchCount),
			
			luabind::class_<HorizontalSlider_wrapper, luabind::bases<Gwen::Controls::Slider, ScriptObject>, std::shared_ptr<HorizontalSlider_wrapper>>("HorizontalSlider")
			.def(luabind::constructor<Gwen::Controls::Base*>()),

			luabind::class_<Gwen::Controls::HorizontalSlider, HorizontalSlider_wrapper>("HorizontalSlider_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// StatusBarBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct StatusBar_wrapper : Gwen::Controls::StatusBar, ScriptObject_wrapper
	{
		StatusBar_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("status_bar", ScriptObject::Static)
			, Gwen::Controls::StatusBar(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Label_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope StatusBarBinding()
	{
		return (
			luabind::class_<StatusBar_wrapper, luabind::bases<Gwen::Controls::Label, ScriptObject>, std::shared_ptr<StatusBar_wrapper>>("StatusBar")
			.def(luabind::constructor<Gwen::Controls::Base*>()),

			luabind::class_<Gwen::Controls::StatusBar, StatusBar_wrapper>("StatusBar_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TextBoxBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TextBox_wrapper : Gwen::Controls::TextBox, ScriptObject_wrapper
	{
		TextBox_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("textbox", ScriptObject::Static)
			, Gwen::Controls::TextBox(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::TextBox, "on_text_changed", onTextChanged, OnTextChanged_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::TextBox, "on_return_press", onReturnPressed, OnReturnPressed_wrap)
			Label_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_text_changed", OnTextChanged_wrap)
		WRAP_CONTROL_EVENT("on_return_press", OnReturnPressed_wrap)

		void SelectAll() {
			Gwen::Controls::TextBox::OnSelectAll(this);
			Gwen::Controls::TextBox::Focus();
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TextBoxBinding()
	{
		return (
			luabind::class_<TextBox_wrapper, luabind::bases<Gwen::Controls::Label, ScriptObject>, std::shared_ptr<TextBox_wrapper>>("TextBox")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.property("text_length", &Gwen::Controls::TextBox::TextLength)
			.def("move_caret_to_start", &Gwen::Controls::TextBox::MoveCaretToStart)
			.def("move_caret_to_end", &Gwen::Controls::TextBox::MoveCaretToEnd)
			.def("on_enter", &Gwen::Controls::TextBox::OnEnter)
			.def("select_all", &TextBox_wrapper::SelectAll),

			luabind::class_<Gwen::Controls::TextBox, TextBox_wrapper>("TextBox_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TextBoxNumericBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TextBoxNumeric_wrapper : Gwen::Controls::TextBoxNumeric, ScriptObject_wrapper
	{
		TextBoxNumeric_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("textbox_numeric", ScriptObject::Static)
			, Gwen::Controls::TextBoxNumeric(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			TextBox_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TextBoxNumericBinding()
	{
		return (
			luabind::class_<TextBoxNumeric_wrapper, luabind::bases<Gwen::Controls::TextBox, ScriptObject>, std::shared_ptr<TextBoxNumeric_wrapper>>("TextBoxNumeric")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.def("get_float_from_text", &Gwen::Controls::TextBoxNumeric::GetFloatFromText),

			luabind::class_<Gwen::Controls::TextBoxNumeric, TextBoxNumeric_wrapper>("TextBoxNumeric_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ResizableControlBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ResizableControl_wrapper : Gwen::Controls::ResizableControl, ScriptObject_wrapper
	{
		ResizableControl_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("resizable_control", ScriptObject::Static)
			, Gwen::Controls::ResizableControl(parent)
		{
			DisableResizing();
			GetResizer(8)->SetMouseInputEnabled(true);
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::ResizableControl, "on_resize", onResize, OnResize_wrap)
			Base_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_resize", OnResize_wrap)
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ResizableControlBinding()
	{
		return (
			luabind::class_<ResizableControl_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<ResizableControl_wrapper>>("ResizableControl")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			.def("disable_resizing", &Gwen::Controls::ResizableControl::DisableResizing),

			luabind::class_<Gwen::Controls::ResizableControl, ResizableControl_wrapper>("ResizableControl_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ScrollControlBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ScrollControl_wrapper : Gwen::Controls::ScrollControl, ScriptObject_wrapper
	{
		ScrollControl_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("scroll_control", ScriptObject::Static)
			, Gwen::Controls::ScrollControl(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		void SetVerticalScrolledAmount(float amount, bool force_update) {
			m_VerticalScrollBar->SetScrolledAmount(amount, force_update);
		}
		void SetVerticalScrolledAmount(float amount) {
			SetVerticalScrolledAmount(amount, true);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ScrollControlBinding()
	{
		return (
			luabind::class_<ScrollControl_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<ScrollControl_wrapper>>("ScrollControl")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.def("set_vertical_scrolled_amount", (void(ScrollControl_wrapper::*)(float, bool)) &ScrollControl_wrapper::SetVerticalScrolledAmount)
			.def("set_vertical_scrolled_amount", (void(ScrollControl_wrapper::*)(float)) &ScrollControl_wrapper::SetVerticalScrolledAmount)

			.def("scroll_to_left", &Gwen::Controls::ScrollControl::ScrollToLeft)
			.def("scroll_to_right", &Gwen::Controls::ScrollControl::ScrollToRight)
			.def("scroll_to_top", &Gwen::Controls::ScrollControl::ScrollToTop)
			.def("scroll_to_bottom", &Gwen::Controls::ScrollControl::ScrollToBottom),

			luabind::class_<Gwen::Controls::ScrollControl, ScrollControl_wrapper>("ScrollControl_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CollapsibleCategoryBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct CollapsibleCategory_wrapper : Gwen::Controls::CollapsibleCategory, ScriptObject_wrapper
	{
		CollapsibleCategory_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("collapsible_category", ScriptObject::Static)
			, Gwen::Controls::CollapsibleCategory(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope CollapsibleCategoryBinding()
	{
		return (
			luabind::class_<CollapsibleCategory_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<CollapsibleCategory_wrapper>>("CollapsibleCategory")
			.def(luabind::constructor<Gwen::Controls::Base*>()),

			luabind::class_<Gwen::Controls::CollapsibleCategory, CollapsibleCategory_wrapper>("CollapsibleCategory_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CollapsibleListBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct CollapsibleList_wrapper : Gwen::Controls::CollapsibleList, ScriptObject_wrapper
	{
		CollapsibleList_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("collapsible_list", ScriptObject::Static)
			, Gwen::Controls::CollapsibleList(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			ScrollControl_wrapper::BindSignalHandlers(object);
		}

		CollapsibleCategory_wrapper* Add(CollapsibleCategory_wrapper* category, const Gwen::TextObject& strLabel)
		{
			category->SetText(strLabel);
			Gwen::Controls::CollapsibleList::Add(category);
			return category;
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope CollapsibleListBinding()
	{
		return (
			luabind::class_<CollapsibleList_wrapper, luabind::bases<Gwen::Controls::ScrollControl, ScriptObject>, std::shared_ptr<CollapsibleList_wrapper>>("CollapsibleList")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			.def("add", &CollapsibleList_wrapper::Add),

			luabind::class_<Gwen::Controls::CollapsibleList, CollapsibleList_wrapper>("CollapsibleList_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// MenuItemBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct MenuItem_wrapper : Gwen::Controls::MenuItem, ScriptObject_wrapper
	{
		MenuItem_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("menu_item", ScriptObject::Static)
			, Gwen::Controls::MenuItem(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::MenuItem, "on_menu_item_selected", onMenuItemSelected, OnMenuItemSelected_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::MenuItem, "on_checked", onChecked, OnChecked_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::MenuItem, "on_unchecked", onUnChecked, OnUnChecked_wrap)
			BIND_CONTROL_EVENT(Gwen::Controls::MenuItem, "on_check_change", onCheckChange, OnCheckChange_wrap)
			Button_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_menu_item_selected", OnMenuItemSelected_wrap)
		WRAP_CONTROL_EVENT("on_checked", OnChecked_wrap)
		WRAP_CONTROL_EVENT("on_unchecked", OnUnChecked_wrap)
		WRAP_CONTROL_EVENT("on_check_change", OnCheckChange_wrap)
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope MenuItemBinding()
	{
		return (
			luabind::class_<MenuItem_wrapper, luabind::bases<Gwen::Controls::Button, ScriptObject>, std::shared_ptr<MenuItem_wrapper>>("MenuItem")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			.property("menu", &Gwen::Controls::MenuItem::GetMenu),

			luabind::class_<Gwen::Controls::MenuItem, MenuItem_wrapper>("MenuItem_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// MenuBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Menu_wrapper : Gwen::Controls::Menu, ScriptObject_wrapper
	{
		Menu_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("menu", ScriptObject::Static)
			, Gwen::Controls::Menu(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			ScrollControl_wrapper::BindSignalHandlers(object);
		}

		// Special case where we want to define uvs for the icon to use a texture atlas
		// instead of loading each icons individually.
		MenuItem_wrapper* AddItem(MenuItem_wrapper* item, const Gwen::TextObject& strName, const Gwen::TextObject& strIconName, 
			const Gwen::TextObject& strAccelerator, float u1, float v1, float u2, float v2, float w, float h)
		{
			MenuItem_wrapper* result = Menu_wrapper::AddItem(item, strName, strIconName, strAccelerator);
			result->GetImage()->SetUV(u1, v1, u2, v2);
			result->GetImage()->SetSize((int)w, (int)h);
			result->SizeToContents();
			return result;
		}

		MenuItem_wrapper* AddItem(MenuItem_wrapper* item, const Gwen::TextObject& strName, const Gwen::TextObject& strIconName, 
			const Gwen::TextObject& strAccelerator)
		{
			item->SetParent(this);
			item->SetPadding(Gwen::Padding(2, 4, 4, 4));
			item->SetText(strName);
			item->SetImage(strIconName);
			item->SetAccelerator(strAccelerator);
			OnAddItem(item);
			return item;
		}

		MenuItem_wrapper* AddItem(MenuItem_wrapper* item, const Gwen::TextObject& strName, const Gwen::TextObject& strIconName) {
			return AddItem(item, strName, strIconName, L"");
		}

		MenuItem_wrapper* AddItem(MenuItem_wrapper* item, const Gwen::TextObject& strName) {
			return AddItem(item, strName, L"", L"");
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope MenuBinding()
	{
		return (
			luabind::class_<Menu_wrapper, luabind::bases<Gwen::Controls::ScrollControl, ScriptObject>, std::shared_ptr<Menu_wrapper>>("Menu")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.def("add_item", (MenuItem_wrapper*(Menu_wrapper::*)(MenuItem_wrapper*, const Gwen::TextObject&, const Gwen::TextObject&, const Gwen::TextObject&, float, float, float, float, float, float)) &Menu_wrapper::AddItem)
			.def("add_item", (MenuItem_wrapper*(Menu_wrapper::*)(MenuItem_wrapper*, const Gwen::TextObject&, const Gwen::TextObject&, const Gwen::TextObject&)) &Menu_wrapper::AddItem)
			.def("add_item", (MenuItem_wrapper*(Menu_wrapper::*)(MenuItem_wrapper*, const Gwen::TextObject&, const Gwen::TextObject&)) &Menu_wrapper::AddItem)
			.def("add_item", (MenuItem_wrapper*(Menu_wrapper::*)(MenuItem_wrapper*, const Gwen::TextObject&)) &Menu_wrapper::AddItem)
			.def("add_divider", &Gwen::Controls::Menu::AddDivider),

			luabind::class_<Gwen::Controls::Menu, Menu_wrapper>("Menu_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// MenuStripBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct MenuStrip_wrapper : Gwen::Controls::MenuStrip, ScriptObject_wrapper
	{
		MenuStrip_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("menu_strip", ScriptObject::Static)
			, Gwen::Controls::MenuStrip(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Menu_wrapper::BindSignalHandlers(object);
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope MenuStripBinding()
	{
		return (
			luabind::class_<MenuStrip_wrapper, luabind::bases<Gwen::Controls::Menu, ScriptObject>, std::shared_ptr<MenuStrip_wrapper>>("MenuStrip")
			.def(luabind::constructor<Gwen::Controls::Base*>()),

			luabind::class_<Gwen::Controls::MenuStrip, MenuStrip_wrapper>("MenuStrip_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ComboBoxBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ComboBox_wrapper : Gwen::Controls::ComboBox, ScriptObject_wrapper
	{
		ComboBox_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("combo_box", ScriptObject::Static)
			, Gwen::Controls::ComboBox(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::ComboBox, "on_selection", onSelection, OnSelection_wrap)
			Button_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_selection", OnSelection_wrap)

		MenuItem_wrapper* AddItem(MenuItem_wrapper* item, const Gwen::TextObject& strLabel)
		{
			item->SetParent(m_Menu);

			// Menu::AddItem
			item->SetPadding(Gwen::Padding(2, 4, 4, 4));
			item->SetText(strLabel);

			// Menu::OnAddItem
			item->SetTextPadding(Gwen::Padding(m_Menu->IconMarginDisabled() ? 0 : 24, 0, 16, 0));
			item->Dock(Gwen::Pos::Top);
			item->SizeToContents();
			item->SetAlignment(Gwen::Pos::CenterV | Gwen::Pos::Left);
			item->onHoverEnter.Add(m_Menu, &Gwen::Controls::Menu::OnHoverItem);
			int w = item->Width() + 10 + 32;
			if (w < Width()) { w = Width(); }
			m_Menu->SetSize(w, Height());

			// ComboBox::AddItem
			item->onMenuItemSelected.Add((Gwen::Controls::ComboBox*)this, &ComboBox::OnItemSelected);
			if (m_SelectedItem == NULL) {
				OnItemSelected(item);
			}

			return item;
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ComboBoxBinding()
	{
		return (
			luabind::class_<ComboBox_wrapper, luabind::bases<Gwen::Controls::Button, ScriptObject>, std::shared_ptr<ComboBox_wrapper>>("ComboBox")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.property("page", &Gwen::Controls::TabButton::GetPage)
			.def("add_item", (MenuItem_wrapper*(ComboBox_wrapper::*)(MenuItem_wrapper*, const Gwen::TextObject&)) &ComboBox_wrapper::AddItem)
			.def("is_active", &Gwen::Controls::TabButton::IsActive),

			luabind::class_<Gwen::Controls::ComboBox, ComboBox_wrapper>("ComboBox_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ListBoxBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ListBox_wrapper : Gwen::Controls::ListBox, ScriptObject_wrapper
	{
		class ListBoxRow : public Gwen::Controls::Layout::TableRow
		{
			public:
				ListBoxRow(Gwen::Controls::Base* pParent, const Gwen::String& pName = "")
					: Gwen::Controls::Layout::TableRow(pParent, pName) {
					SetMouseInputEnabled(true);
					SetSelected(false);
				}

				void Render(Gwen::Skin::Base* skin) {
					skin->DrawListBoxLine(this, IsSelected(), GetEven());
				}

				bool IsSelected() const {
					return m_bSelected;
				}

				void DoSelect() {
					SetSelected(true);
					onRowSelected.Call(this);
					Redraw();
				}

				void OnMouseClickLeft(int x, int y, bool bDown) {
					if (bDown) {
						DoSelect();
					}
				}

				void SetSelected(bool b) {
					m_bSelected = b;
				}

			private:
				bool m_bSelected;
		};

		ListBox_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("list_box", ScriptObject::Static)
			, Gwen::Controls::ListBox(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::ListBox, "on_row_selected", onRowSelected, OnRowSelected_wrap)
			ScrollControl_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_row_selected", OnRowSelected_wrap)

		Gwen::Controls::Layout::TableRow* AddItem(const Gwen::String& text)
		{
			Gwen::Controls::Layout::TableRow* pRow = new ListBoxRow(this);
			m_Table->AddRow(pRow);
			pRow->SetCellText(0, text);
			pRow->onRowSelected.Add(this, &ListBox_wrapper::OnRowSelected);
			return pRow;
		}

		void SetSelectedRow(Gwen::Controls::Base* pControl, bool bClearOthers)
		{
			if (bClearOthers) {
				UnselectAll();
			}

			ListBoxRow* pRow = gwen_cast<ListBoxRow> (pControl);
			if (!pRow) {
				return;
			}

			pRow->SetSelected(true);
			m_SelectedRows.push_back(pRow);
			onRowSelected.Call(this);
			Redraw();
		}

		void UnselectAll() {
			ListBox::UnselectAll();
			Redraw();
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ListBoxBinding()
	{
		return (
			luabind::class_<ListBox_wrapper, luabind::bases<Gwen::Controls::ScrollControl, ScriptObject>, std::shared_ptr<ListBox_wrapper>>("ListBox")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.scope
			[
				luabind::class_<Gwen::Controls::Layout::TableRow, Gwen::Controls::Base>("TableRow")
				.def("get_cell_contents", &ListBox_wrapper::ListBoxRow::GetCellContents),

				luabind::class_<Gwen::Controls::Layout::Table, Gwen::Controls::Base>("Table")
				.def("get_row", &Gwen::Controls::Layout::Table::GetRow)
			]
			
			.property("table", &Gwen::Controls::ListBox::GetTable)

			.def("add_item", &ListBox_wrapper::AddItem)
			.def("set_selected_row", &ListBox_wrapper::SetSelectedRow)
			.def("unselect_all", &ListBox_wrapper::UnselectAll)

			.def("get_selected_row", &Gwen::Controls::ListBox::GetSelectedRow)
			.def("clear", &Gwen::Controls::ListBox::Clear),

			luabind::class_<Gwen::Controls::ListBox, ListBox_wrapper>("ListBox_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// WindowControlBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct WindowControl_wrapper : Gwen::Controls::WindowControl, ScriptObject_wrapper
	{
		WindowControl_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("window_control", ScriptObject::Static)
			, Gwen::Controls::WindowControl(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::WindowControl, "on_window_closed", onWindowClosed, OnWindowClosed_wrap)
			ResizableControl_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_window_closed", OnWindowClosed_wrap)

		void MakeModal() { Gwen::Controls::WindowControl::MakeModal(true); }
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope WindowControlBinding()
	{
		return (
			luabind::class_<WindowControl_wrapper, luabind::bases<Gwen::Controls::ResizableControl, ScriptObject>, std::shared_ptr<WindowControl_wrapper>>("WindowControl")
			.def(luabind::constructor<Gwen::Controls::Base*>())

			.def("set_title", (void(Gwen::Controls::WindowControl::*)(std::string)) &Gwen::Controls::WindowControl::SetTitle)
			.def("set_closable", &Gwen::Controls::WindowControl::SetClosable)
			.def("close_button_pressed", &Gwen::Controls::WindowControl::CloseButtonPressed)
			//.def("set_delete_on_close", &Gwen::Controls::WindowControl::SetDeleteOnClose) not supported
			.def("make_modal", (void(Gwen::Controls::WindowControl::*)(bool)) &Gwen::Controls::WindowControl::MakeModal)
			.def("make_modal", (void(WindowControl_wrapper::*)()) &WindowControl_wrapper::MakeModal)
			.def("destroy_modal", &Gwen::Controls::WindowControl::DestroyModal),

			luabind::class_<Gwen::Controls::WindowControl, WindowControl_wrapper>("WindowControl_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CheckboxProperty:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline bool CheckboxProperty_GetValue(Gwen::Controls::Property::Checkbox& self) {
		return (std::atoi(self.GetPropertyValue().c_str()) != 0) ? true : false;
	}
	/*----------------------------------------------------------------------------*/
	inline void CheckboxProperty_SetValue(Gwen::Controls::Property::Checkbox& self, bool value) {
		self.SetPropertyValue(std::to_string(value), true);
	}

	////////////////////////////////////////////////////////////////////////////////
	// NumericProperty:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	class NumericProperty : public Gwen::Controls::Property::Base
	{
		public:
			GWEN_CONTROL_INLINE(NumericProperty, Gwen::Controls::Property::Base) {
				m_TextBoxNumeric = new Gwen::Controls::TextBoxNumeric(this);
				m_TextBoxNumeric->Dock(Gwen::Pos::Fill);
				m_TextBoxNumeric->SetShouldDrawBackground(false);
				m_TextBoxNumeric->onTextChanged.Add(this, &BaseClass::OnPropertyValueChanged);
				m_TextBoxNumeric->onHoverEnter.Add(this, &NumericProperty::OnHoverEnter);
			}

			void OnHoverEnter(Gwen::Controls::Base* control) {
				m_TextBoxNumeric->SetSelectAllOnFocus(true);
			}

			virtual Gwen::TextObject GetPropertyValue() { return m_TextBoxNumeric->GetText(); }
			virtual void SetPropertyValue(const Gwen::TextObject & v, bool bFireChangeEvents) { m_TextBoxNumeric->SetText(v, bFireChangeEvents); }
			virtual bool IsEditing() { return m_TextBoxNumeric->HasFocus(); }
			virtual bool IsHovered() { return BaseClass::IsHovered() || m_TextBoxNumeric->IsHovered(); }
			Gwen::Controls::TextBoxNumeric* m_TextBoxNumeric;
	};
	/*----------------------------------------------------------------------------*/
	inline double NumericProperty_GetValue(NumericProperty& self) {
		return std::atof(self.GetPropertyValue().c_str());
	}
	/*----------------------------------------------------------------------------*/
	inline void NumericProperty_SetValue(NumericProperty& self, double value) {
		self.SetPropertyValue(std::to_string(value), true);
	}

	////////////////////////////////////////////////////////////////////////////////
	// NumericUpDownProperty:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	class NumericUpDownProperty : public Gwen::Controls::Property::Base
	{
		public:
			GWEN_CONTROL_INLINE(NumericUpDownProperty, Gwen::Controls::Property::Base) {
				m_NumericUpDown = new Gwen::Controls::NumericUpDown(this);
				m_NumericUpDown->Dock(Gwen::Pos::Fill);
				m_NumericUpDown->SetShouldDrawBackground(false);
				m_NumericUpDown->onTextChanged.Add(this, &BaseClass::OnPropertyValueChanged);
				m_NumericUpDown->onHoverEnter.Add(this, &NumericUpDownProperty::OnHoverEnter);
			}

			void OnHoverEnter(Gwen::Controls::Base* control) {
				m_NumericUpDown->SetSelectAllOnFocus(true);
			}

			virtual Gwen::TextObject GetPropertyValue() { return m_NumericUpDown->GetText(); }
			virtual void SetPropertyValue(const Gwen::TextObject & v, bool bFireChangeEvents) { m_NumericUpDown->SetText(v, bFireChangeEvents); }
			virtual bool IsEditing() { return m_NumericUpDown->HasFocus(); }
			virtual bool IsHovered() { return BaseClass::IsHovered() || m_NumericUpDown->IsHovered(); }
			Gwen::Controls::NumericUpDown* m_NumericUpDown;
	};
	/*----------------------------------------------------------------------------*/
	inline int NumericUpDownProperty_GetValue(NumericUpDownProperty& self) {
		return std::atoi(self.GetPropertyValue().c_str());
	}
	/*----------------------------------------------------------------------------*/
	inline void NumericUpDownProperty_SetValue(NumericUpDownProperty& self, int value) {
		self.SetPropertyValue(std::to_string(value), true);
	}

	////////////////////////////////////////////////////////////////////////////////
	// Vector3Property:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	class Vector3Property : public Gwen::Controls::Property::Base
	{
		public:
			enum Elements { X, Y, Z, Count };

			GWEN_CONTROL_INLINE(Vector3Property, Gwen::Controls::Property::Base)
			{
				for (unsigned int i = 0; i < Count; ++i)
				{
					m_ElementsLabel[i] = new Gwen::Controls::Label(this);
					switch (i) {
						case X: m_ElementsLabel[i]->SetText("X:"); break;
						case Y: m_ElementsLabel[i]->SetText("Y:"); break;
						case Z: m_ElementsLabel[i]->SetText("Z:"); break;
						default: break;
					}

					m_ElementsLabel[i]->SetMargin(Gwen::Margin(6, 2, 0, 0));
					m_ElementsLabel[i]->SetWidth(10);
					m_ElementsLabel[i]->Dock(Gwen::Pos::Left);

					m_ElementsTextBoxList[i] = new Gwen::Controls::TextBoxNumeric(this);
					m_ElementsTextBoxList[i]->SetMargin(Gwen::Margin(2, -1, 0, 0));
					m_ElementsTextBoxList[i]->SetWidth(60);
					m_ElementsTextBoxList[i]->Dock(Gwen::Pos::Left);
					m_ElementsTextBoxList[i]->onTextChanged.Add(this, &BaseClass::OnPropertyValueChanged);
					m_ElementsTextBoxList[i]->onHoverEnter.Add(this, &Vector3Property::OnHoverEnter);
					m_ElementsTextBoxList[i]->SetShouldDrawBackground(false);
				}
			}

			void OnHoverEnter(Gwen::Controls::Base* control) {
				for (unsigned int i = 0; i < Count; ++i) {
					if (!m_ElementsTextBoxList[i]->HasFocus()) {
						m_ElementsTextBoxList[i]->SetSelectAllOnFocus(true);
					}
				}
			}

			virtual Gwen::TextObject GetPropertyValue()
			{
				const std::string x = std::to_string(m_ElementsTextBoxList[X]->GetFloatFromText());
				const std::string y = std::to_string(m_ElementsTextBoxList[Y]->GetFloatFromText());
				const std::string z = std::to_string(m_ElementsTextBoxList[Z]->GetFloatFromText());
				return Gwen::TextObject(x + " " + y + " " + z);
			}

			virtual void SetPropertyValue(const Gwen::TextObject & v, bool bFireChangeEvents)
			{
				std::string input = std::string(v.c_str());
				std::istringstream iss(input);
				std::istream_iterator<std::string> beg(iss), end;
				std::vector<std::string> tokens(beg, end);
				unsigned int element_index = 0;

				for (auto& s : tokens) {
					switch (element_index) {
						case X: m_ElementsTextBoxList[X]->SetText(s, bFireChangeEvents); break;
						case Y: m_ElementsTextBoxList[Y]->SetText(s, bFireChangeEvents); break;
						case Z: m_ElementsTextBoxList[Z]->SetText(s, bFireChangeEvents); break;
						default: break;
					};

					element_index++;
				}
			}

			virtual bool IsEditing()
			{
				bool is_editing = false;
				for (unsigned int i = 0; i < Count; ++i) {
					if (m_ElementsTextBoxList[i]->HasFocus()) {
						is_editing = true;
						break;
					}
				}

				return is_editing;
			}

			virtual bool IsHovered()
			{
				bool is_hovered = BaseClass::IsHovered();
				for (unsigned int i = 0; i < Count; ++i) {
					if (m_ElementsTextBoxList[i]->IsHovered()) {
						is_hovered = true;
						break;
					}
				}

				return is_hovered;
			}

			Gwen::Controls::TextBoxNumeric* m_ElementsTextBoxList[Count];
			Gwen::Controls::Label* m_ElementsLabel[Count];
	};
	/*----------------------------------------------------------------------------*/
	inline dvec3 Vector3Property_GetValue(Vector3Property& self) {
		return dvec3(
			self.m_ElementsTextBoxList[Vector3Property::X]->GetFloatFromText(),
			self.m_ElementsTextBoxList[Vector3Property::Y]->GetFloatFromText(),
			self.m_ElementsTextBoxList[Vector3Property::Z]->GetFloatFromText()
		);
	}
	/*----------------------------------------------------------------------------*/
	inline void Vector3Property_SetValue(Vector3Property& self, const dvec3& value) {
		std::string input = std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z);
		self.SetPropertyValue(input, true);
	}

	////////////////////////////////////////////////////////////////////////////////
	// PropertyRowBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct PropertyRow_wrapper : Gwen::Controls::PropertyRow, ScriptObject_wrapper
	{
		PropertyRow_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("property_row", ScriptObject::Static)
			, Gwen::Controls::PropertyRow(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			BIND_CONTROL_EVENT(Gwen::Controls::PropertyRow, "on_change", onChange, OnChange_wrap)
			Base_wrapper::BindSignalHandlers(object);
		}

		WRAP_CONTROL_EVENT("on_change", OnChange_wrap)
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope PropertyRowBinding()
	{
		return (
			luabind::class_<PropertyRow_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<PropertyRow_wrapper>>("PropertyRow")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.scope
			[
				luabind::class_<Gwen::Controls::Property::Base, Gwen::Controls::Base>("PropertyBase")
				.property("value", &Gwen::Controls::Property::Base::GetPropertyValue, (void(Gwen::Controls::Property::Base::*)(const Gwen::TextObject&)) &Gwen::Controls::Property::Base::SetPropertyValue),

				luabind::class_<Gwen::Controls::Property::Checkbox, Gwen::Controls::Property::Base>("CheckboxProperty")
				.property("value", &CheckboxProperty_GetValue, &CheckboxProperty_SetValue),

				luabind::class_<NumericProperty, Gwen::Controls::Property::Base>("NumericProperty")
				.property("value", &NumericProperty_GetValue, &NumericProperty_SetValue),

				luabind::class_<NumericUpDownProperty, Gwen::Controls::Property::Base>("NumericUpDownProperty")
				.property("value", &NumericUpDownProperty_GetValue, &NumericUpDownProperty_SetValue),

				luabind::class_<Vector3Property, Gwen::Controls::Property::Base>("Vector3Property")
				.property("value", &Vector3Property_GetValue, &Vector3Property_SetValue)
			]
		
			.property("property", &Gwen::Controls::PropertyRow::GetProperty),

			luabind::class_<Gwen::Controls::PropertyRow, PropertyRow_wrapper>("PropertyRow_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// PropertiesBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Properties_wrapper : Gwen::Controls::Properties, ScriptObject_wrapper
	{
		Properties_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("properties", ScriptObject::Static)
			, Gwen::Controls::Properties(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			Base_wrapper::BindSignalHandlers(object);
		}

		PropertyRow_wrapper* AddProperty(PropertyRow_wrapper* row, const Gwen::TextObject& text, Gwen::Controls::Property::Base* prop, const Gwen::TextObject& value)
		{
			row->SetParent(this);
			row->Dock(Gwen::Pos::Top);
			row->GetLabel()->SetText(text);
			row->SetProperty(prop);
			prop->SetPropertyValue(value, true);
			m_SplitterBar->BringToFront();
			return row;
		}

		PropertyRow_wrapper* AddCheckbox(PropertyRow_wrapper* row, const Gwen::TextObject& text, bool enabled) {
			return AddProperty(row, text, new Gwen::Controls::Property::Checkbox(this), (enabled) ? "1" : "0");
		}

		PropertyRow_wrapper* AddComboBox(PropertyRow_wrapper* row, const Gwen::TextObject& text, const luabind::object& choices, const Gwen::TextObject& value) {
			Gwen::Controls::Property::ComboBox* pCombo = new Gwen::Controls::Property::ComboBox(this);
			for (luabind::iterator it(choices), end; it != end; ++it) {
				const std::string choice = luabind::object_cast<std::string>(*it);
				pCombo->GetComboBox()->AddItem(Gwen::TextObject(choice), choice);
			}

			return AddProperty(row, text, pCombo, value);
		}

		PropertyRow_wrapper* AddText(PropertyRow_wrapper* row, const Gwen::TextObject& text, const Gwen::TextObject& value) {
			return AddProperty(row, text, new Gwen::Controls::Property::Text(this), value);
		}
		PropertyRow_wrapper* AddNumeric(PropertyRow_wrapper* row, const Gwen::TextObject& text, double value) {
			return AddProperty(row, text, new NumericProperty(this), std::to_string(value));
		}
		PropertyRow_wrapper* AddNumericUpDown(PropertyRow_wrapper* row, const Gwen::TextObject& text, int value, int min, int max) {
			NumericUpDownProperty* prop = new NumericUpDownProperty(this);
			prop->m_NumericUpDown->SetMin(min);
			prop->m_NumericUpDown->SetMax(max);
			return AddProperty(row, text, prop, std::to_string(value));
		}
		PropertyRow_wrapper* AddNumericUpDown(PropertyRow_wrapper* row, const Gwen::TextObject& text, int value) {
			return AddProperty(row, text, new NumericUpDownProperty(this), std::to_string(value));
		}
		PropertyRow_wrapper* AddVector3(PropertyRow_wrapper* row, const Gwen::TextObject& text, const dvec3& value) {
			std::string input = std::to_string(value.x) + " " + std::to_string(value.y) + " " + std::to_string(value.z);
			return AddProperty(row, text, new Vector3Property(this), input);
		}

		// BugFix: Hide the parent (TreeNode) when hidding a properties, this should really be default.
		virtual void SetHidden(bool hidden) { GetParent()->SetHidden(hidden); }
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope PropertiesBinding()
	{
		return (
			luabind::class_<Properties_wrapper, luabind::bases<Gwen::Controls::Base, ScriptObject>, std::shared_ptr<Properties_wrapper>>("Properties")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.def("add_checkbox", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, bool)) &Properties_wrapper::AddCheckbox)
			.def("add_combobox", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, const luabind::object& choices, const Gwen::TextObject&)) &Properties_wrapper::AddComboBox)
			.def("add_text", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, const Gwen::TextObject&)) &Properties_wrapper::AddText)
			.def("add_numeric", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, double)) &Properties_wrapper::AddNumeric)
			.def("add_numeric_updown", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, int, int, int)) &Properties_wrapper::AddNumericUpDown)
			.def("add_numeric_updown", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, int)) &Properties_wrapper::AddNumericUpDown)
			.def("add_vec3", (PropertyRow_wrapper*(Properties_wrapper::*)(PropertyRow_wrapper*, const Gwen::TextObject&, const dvec3&)) &Properties_wrapper::AddVector3),

			luabind::class_<Gwen::Controls::Properties, Properties_wrapper>("Properties_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// PropertyTreeBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct PropertyTree_wrapper : Gwen::Controls::PropertyTree, ScriptObject_wrapper
	{
		PropertyTree_wrapper(Gwen::Controls::Base* parent)
			: ScriptObject_wrapper("property_tree", ScriptObject::Static)
			, Gwen::Controls::PropertyTree(parent) {
			BindSignalHandlers(this);
		}

		static void BindSignalHandlers(ScriptObject_wrapper* object) {
			TreeControl_wrapper::BindSignalHandlers(object);
		}

		Properties_wrapper* Add(Properties_wrapper* props, const Gwen::TextObject& text)
		{
			TreeNode* node = new Gwen::Controls::PropertyTreeNode(this);
			node->SetText(text);
			node->Dock(Gwen::Pos::Top);
			props->SetParent(node);
			props->Dock(Gwen::Pos::Top);
			return props;
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope PropertyTreeBinding()
	{
		return (
			luabind::class_<PropertyTree_wrapper, luabind::bases<Gwen::Controls::TreeControl, ScriptObject>, std::shared_ptr<PropertyTree_wrapper>>("PropertyTree")
			.def(luabind::constructor<Gwen::Controls::Base*>())
			
			.def("add", (Properties_wrapper*(PropertyTree_wrapper::*)(Properties_wrapper*, const Gwen::TextObject&)) &PropertyTree_wrapper::Add),

			luabind::class_<Gwen::Controls::PropertyTree, PropertyTree_wrapper>("PropertyTree_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// GwenBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	std::ostream& operator<<(std::ostream& os, const Gwen::Color& color) {
		return os << std::hex << Color(color.r, color.g, color.b, color.a).ToInteger();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope GwenBinding()
	{
		return (
			luabind::namespace_("gwen")
			[
				luabind::class_<Gwen::Color>("Color")
				.def_readwrite("r", &Gwen::Color::r)
				.def_readwrite("g", &Gwen::Color::g)
				.def_readwrite("b", &Gwen::Color::b)
				.def_readwrite("a", &Gwen::Color::a),

				luabind::class_<Gwen::Point>("Point")
				.def(luabind::constructor<int, int>())
				.def(luabind::constructor<>())
				.def_readwrite("x", &Gwen::Point::x)
				.def_readwrite("y", &Gwen::Point::y),

				luabind::class_<Gwen::Rect>("Rect")
				.def(luabind::constructor<int, int, int, int>())
				.def_readwrite("x", &Gwen::Rect::x)
				.def_readwrite("y", &Gwen::Rect::y)
				.def_readwrite("w", &Gwen::Rect::w)
				.def_readwrite("h", &Gwen::Rect::h),
				
				luabind::class_<Gwen::Font>("Font")
				.def(luabind::constructor<>())
				.def_readwrite("facename", &Gwen::Font::facename)
				.def_readwrite("size", &Gwen::Font::size),
				
				luabind::class_<Gwen::TextObject>("TextObject")
				.def(luabind::constructor<const std::string&>()),

				luabind::class_<Gwen::Renderer::Base>("Renderer")
				.def("translate", (void(Gwen::Renderer::Base::*)(Gwen::Rect&)) &Gwen::Renderer::Base::Translate),

				luabind::class_<GwenRenderer, Gwen::Renderer::Base>("GwenRenderer")
				.property("render_to_fbo", &GwenRenderer::GetRenderToFBO, &GwenRenderer::SetRenderToFBO)
				.def("bind_fbo", &GwenRenderer::BindFBO),

				luabind::class_<Gwen::Skin::Base>("Skin")
				.def("get_render", &Gwen::Skin::Base::GetRender),

				BaseBinding(),
				CanvasBinding(),
				CrossSplitterBinding(),
				DockBaseBinding(),
				ImagePanelBinding(),
				LabelBinding(),
				ButtonBinding(),
				GroupBoxBinding(),
				TabButtonBinding(),
				TabControlBinding(),
				TreeNodeBinding(),
				TreeControlBinding(),
				SliderBinding(),
				StatusBarBinding(),
				TextBoxBinding(),
				TextBoxNumericBinding(),
				ResizableControlBinding(),
				ScrollControlBinding(),
				CollapsibleCategoryBinding(),
				CollapsibleListBinding(),
				MenuItemBinding(),
				MenuBinding(),
				MenuStripBinding(),
				ComboBoxBinding(),
				ListBoxBinding(),
				WindowControlBinding(),

				PropertyRowBinding(),
				PropertiesBinding(),
				PropertyTreeBinding()
			]
		);
	}

} // root namespace

namespace luabind
{
	template<>
	struct default_converter<std::wstring> : native_converter_base<std::wstring>
	{
		static int compute_score(lua_State* L, int _index) {
			return lua_type(L, _index) == LUA_TSTRING ? 0 : -1;
		}

		Gwen::TextObject to_cpp_deferred(lua_State* L, int index) {
			return Gwen::Utility::StringToUnicode(lua_tostring(L, index));
		}

		void to_lua_deferred(lua_State* L, const std::wstring & str) {
			lua_pushstring(L, Gwen::Utility::UnicodeToString(str).c_str());
		}
	};

	template<>
	struct default_converter<const std::wstring> :
	default_converter<std::wstring>
	{};
	template<>
	struct default_converter<const std::wstring&> :
	default_converter<std::wstring>
	{};

	template<>
	struct default_converter<Gwen::TextObject> : native_converter_base<Gwen::TextObject>
	{
		static int compute_score(lua_State* L, int index) {
			return lua_type(L, index) == LUA_TSTRING ? 0 : -1;
		}

		Gwen::TextObject to_cpp_deferred(lua_State* L, int index) {
			return Gwen::TextObject(lua_tostring(L, index));
		}

		void to_lua_deferred(lua_State* L, const Gwen::TextObject & str) {
			lua_pushstring(L, str.c_str());
		}
	};

	template<>
	struct default_converter<const Gwen::TextObject> :
	default_converter<Gwen::TextObject>
	{};
	template<>
	struct default_converter<const Gwen::TextObject&> :
	default_converter<Gwen::TextObject>
	{};
}