/**
* @file ConcurrentQueue.h
* @brief A lock-free thread-safe implementation of std::queue.
*/

#pragma once

#include <condition_variable>
#include <memory>
#include <queue>

namespace root
{
	template <typename T>
	class ConcurrentQueue
	{
		public:
			//! SERVICES:
			bool empty() const;
			void push(const T& value);
			bool try_pop(T& result);
			T wait_pop();

			//! ACCESSORS:
			std::size_t size() const;

		private:
			//! MEMBERS:
			std::condition_variable m_condition;
			mutable std::mutex m_mutex;
			std::queue<T> m_queue;
	};

	////////////////////////////////////////////////////////////////////////////////
	// root::ConcurrentQueue inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline bool ConcurrentQueue<T>::empty() const {
		std::unique_lock<std::mutex> lock(m_mutex);
		return m_queue.empty();
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline void ConcurrentQueue<T>::push(const T& value) {
		{
			std::unique_lock<std::mutex> lock(m_mutex);
			m_queue.push(value);
		}

		m_condition.notify_one();
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline bool ConcurrentQueue<T>::try_pop(T& result) {
		std::unique_lock<std::mutex> lock(m_mutex);
		if (m_queue.empty()) {
			return false;
		}

		result = m_queue.front();
		m_queue.pop();
		return true;
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline T ConcurrentQueue<T>::wait_pop() {
		std::unique_lock<std::mutex> lock(m_mutex);
		while (m_queue.empty()) {
			m_condition.wait(lock);
		}

		T result(m_queue.front());
		m_queue.pop();
		return result;
	}
	/*----------------------------------------------------------------------------*/
	template <typename T>
	inline std::size_t ConcurrentQueue<T>::size() const {
		std::unique_lock<std::mutex> lock(m_mutex);
		return m_queue.size();
	}

} // root namespace