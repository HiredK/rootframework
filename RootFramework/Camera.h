/**
* @file Camera.h
* @brief
*/

#pragma once

#include "Transform.h"
#include "Frustum.h"

namespace root
{
	class Camera : public Transform
	{
		public:
			//! CTOR/DTOR:
			Camera(double fov, double aspect, const dvec2& clip);
			Camera(const dvec4& bounds, const dvec2& clip);
			virtual ~Camera();

			//! SERVICES:
			void Copy(const Camera& other);
			void Refresh();

			//! ACCESSORS:
			void SetProjMatrix(const dmat4& matrix);
			dmat4& GetProjMatrix();
			void SetViewMatrix(const dmat4& matrix);
			dmat4& GetViewMatrix();
			dmat4& GetViewOriginMatrix();
			dmat4& GetViewProjMatrix();
			dmat4& GetViewProjOriginMatrix();
			Frustum& GetFrustum();

			void SetFov(double fov);
			double GetFov() const;
			void SetAspect(double aspect);
			double GetAspect() const;
			void SetClip(const dvec2& clip);
			dvec2& GetClip();

			void SetBounds(const dvec4& bounds);
			dvec4& GetBounds();
			bool IsOrtho() const;

		protected:
			//! MEMBERS:
			dmat4 m_proj_matrix;
			dmat4 m_view_matrix;
			dmat4 m_vp_matrix;
			dmat4 m_view_origin_matrix;
			dmat4 m_vp_origin_matrix;
			Frustum m_frustum;

			double m_fov;
			double m_aspect;
			dvec2 m_clip;

			dvec4 m_bounds;
			bool m_ortho;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Camera inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Camera::Camera(double fov, double aspect, const dvec2& clip)
		: m_fov(fov), m_aspect(aspect), m_clip(clip)
		, m_ortho(false) {
	}
	/*----------------------------------------------------------------------------*/
	inline Camera::Camera(const dvec4& bounds, const dvec2& clip)
		: m_fov(0.1), m_aspect(1.0), m_clip(clip), m_bounds(bounds)
		, m_ortho(true) {
	}
	/*----------------------------------------------------------------------------*/
	inline Camera::~Camera() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Camera::Copy(const Camera& other)
	{
		m_fov = other.m_fov;
		m_aspect = other.m_aspect;
		m_clip = other.m_clip;
		m_bounds = other.m_bounds;
		m_ortho = other.m_ortho;

		Transform::Copy(other);
	}

	/*----------------------------------------------------------------------------*/
	inline void Camera::SetProjMatrix(const dmat4& matrix) {
		m_proj_matrix = matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4& Camera::GetProjMatrix() {
		return m_proj_matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetViewMatrix(const dmat4& matrix) {
		m_view_matrix = matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4& Camera::GetViewMatrix() {
		return m_view_matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4& Camera::GetViewOriginMatrix() {
		return m_view_origin_matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4& Camera::GetViewProjMatrix() {
		return m_vp_matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4& Camera::GetViewProjOriginMatrix() {
		return m_vp_origin_matrix;
	}
	/*----------------------------------------------------------------------------*/
	inline Frustum& Camera::GetFrustum() {
		return m_frustum;
	}

	/*----------------------------------------------------------------------------*/
	inline void Camera::SetFov(double fov) {
		m_fov = fov;
	}
	/*----------------------------------------------------------------------------*/
	inline double Camera::GetFov() const {
		return m_fov;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetAspect(double aspect) {
		m_aspect = aspect;
	}
	/*----------------------------------------------------------------------------*/
	inline double Camera::GetAspect() const {
		return m_aspect;
	}
	/*----------------------------------------------------------------------------*/
	inline void Camera::SetClip(const dvec2& clip) {
		m_clip = clip;
	}
	/*----------------------------------------------------------------------------*/
	inline dvec2& Camera::GetClip() {
		return m_clip;
	}

	/*----------------------------------------------------------------------------*/
	inline void Camera::SetBounds(const dvec4& bounds) {
		m_bounds = bounds;
	}
	/*----------------------------------------------------------------------------*/
	inline dvec4& Camera::GetBounds() {
		return m_bounds;
	}
	/*----------------------------------------------------------------------------*/
	inline bool Camera::IsOrtho() const {
		return m_ortho;
	}
}