/**
* @file ParticleEmitter.h
* @brief
*/

#pragma once

#include "Transform.h"
#include "Camera.h"
#include "Shader.h"

namespace root
{
	class ParticleEmitter : public Transform
	{
		public:
			//! TYPEDEF/ENUMS:
			enum { FRONT, BACK, BUFFER_COUNT };

			struct Particle
			{
				float type;
				vec3 position;
				vec3 velocity;
				float age;
			};

			//! CTOR/DTOR:
			ParticleEmitter(unsigned int max_particles = 1000);
			virtual ~ParticleEmitter();

			//! SERVICES:
			void Rebuild();
			void Simulate(float dt);
			void Draw(Camera& camera);
			void Draw();
			void Clear();

		private:
			//! MEMBERS:
			const unsigned int m_max_particles;
			GLuint m_tfo[BUFFER_COUNT];
			unsigned int m_curr_tfo;
			GLuint m_pbo[BUFFER_COUNT];
			unsigned int m_curr_pbo;
			bool m_first;
	};

	////////////////////////////////////////////////////////////////////////////////
	// ParticleEmitter inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline ParticleEmitter:: ParticleEmitter(unsigned int max_particles)
		: m_max_particles(max_particles), m_first(true)
	{
		m_tfo[FRONT] = GL_ZERO;
		m_tfo[BACK ] = GL_ZERO;
		m_curr_tfo = BACK;

		m_pbo[FRONT] = GL_ZERO;
		m_pbo[BACK ] = GL_ZERO;
		m_curr_pbo = FRONT;

		Rebuild();
	}
	/*----------------------------------------------------------------------------*/
	inline ParticleEmitter::~ParticleEmitter() {
		Clear();
	}

	/*----------------------------------------------------------------------------*/
	inline void ParticleEmitter::Draw(Camera& camera) {
		Shader::Get()->UniformMat4("u_ViewProjMatrix", camera.GetViewProjMatrix());
		Draw();
	}
}