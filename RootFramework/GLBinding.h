/**
* @file GLBinding.h
* @brief Todo
*/

#include <GL/glew.h>
#include "ScriptObjectBinding.h"
#include "Color.h"

// BugFix: http://stackoverflow.com/questions/13288547/luabind-calling-convention-issues
template<typename Signature>
struct wrap_known;

template<typename Ret, typename... Args>
struct wrap_known<Ret GLAPIENTRY(Args...)> {
	template <Ret GLAPIENTRY functor(Args...)>
	static Ret invoke(Args... arguments) {
		return functor(arguments...);
	}
};

#define wrap_gl(f) wrap_known<decltype(f)>::invoke<f>

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// GLBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	static void glClearColor_wrap(const Color& color) {
		glClearColor(Color::ToFloat(color.r), Color::ToFloat(color.g), Color::ToFloat(color.b), Color::ToFloat(color.a));
	}
	/*----------------------------------------------------------------------------*/
	static void glActiveTexture_wrap(GLenum texture) {
		glActiveTexture(texture);
	}
	/*----------------------------------------------------------------------------*/
	static void glBlendEquationSeparate_wrap(GLenum modeRGB, GLenum modeAlpha) {
		glBlendEquationSeparate(modeRGB, modeAlpha);
	}
	/*----------------------------------------------------------------------------*/
	static void glBlendFuncSeparate_wrap(GLenum sfactorRGB, GLenum dfactorRGB, GLenum sfactorAlpha, GLenum dfactorAlpha) {
		glBlendFuncSeparate(sfactorRGB, dfactorRGB, sfactorAlpha, dfactorAlpha);
	}

	static void glViewport_wrap(int x, int y, unsigned int w, unsigned int h)
	{
		glViewport(x, y, w, h);
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope GLBinding()
	{
		return (
			luabind::namespace_("gl")
			[
				luabind::def("Clear", (void(*)(GLbitfield)) wrap_gl(glClear)),
				luabind::def("ClearColor", (void(*)(GLclampf, GLclampf, GLclampf, GLclampf)) wrap_gl(glClearColor)),
				luabind::def("ClearColor", (void(*)(const Color&)) glClearColor_wrap),
				luabind::def("PolygonMode", (void(*)(GLenum, GLenum)) wrap_gl(glPolygonMode)),
				luabind::def("ActiveTexture", &glActiveTexture_wrap),
				luabind::def("Enable", (void(*)(GLenum)) wrap_gl(glEnable)),
				luabind::def("Disable", (void(*)(GLenum)) wrap_gl(glDisable)),
				luabind::def("Viewport", (void(*)(int, int, unsigned int, unsigned int)) wrap_gl(glViewport)),
				luabind::def("PolygonOffset", (void(*)(GLfloat, GLfloat)) wrap_gl(glPolygonOffset)),
				luabind::def("DepthFunc", (void(*)(GLenum)) wrap_gl(glDepthFunc)),
				luabind::def("CullFace", (void(*)(GLenum)) wrap_gl(glCullFace)),
				luabind::def("ColorMask", (void(*)(int, int, int, int)) wrap_gl(glColorMask)),
				luabind::def("DepthMask", (void(*)(bool)) wrap_gl(glDepthMask)),
				luabind::def("BlendFunc", (void(*)(GLenum, GLenum)) wrap_gl(glBlendFunc)),
				luabind::def("PushAttrib", (void(*)(GLbitfield)) wrap_gl(glPushAttrib)),
				luabind::def("PopAttrib", (void(*)()) wrap_gl(glPopAttrib)),
				luabind::def("LineWidth", (void(*)(GLfloat)) wrap_gl(glLineWidth)),

				luabind::def("StencilFunc", (void(*)(GLenum, GLint, GLuint)) wrap_gl(glStencilFunc)),
				luabind::def("StencilOp", (void(*)(GLenum, GLenum, GLenum)) wrap_gl(glStencilOp)),
				luabind::def("StencilMask", (void(*)(GLuint)) wrap_gl(glStencilMask)),

				luabind::def("Scissor", (void(*)(int, int, unsigned int, unsigned int)) wrap_gl(glScissor)),

				luabind::def("BlendEquationSeparate", (void(*)(GLenum, GLenum)) &glBlendEquationSeparate_wrap),
				luabind::def("BlendFuncSeparate", (void(*)(GLenum, GLenum, GLenum, GLenum)) &glBlendFuncSeparate_wrap)
			]
		);
	}

	/*----------------------------------------------------------------------------*/
	inline void GLBinding_Globals(lua_State* L)
	{
		luabind::globals(L)["gl"]["NONE"] = GL_NONE;

		luabind::globals(L)["gl"]["FUNC_ADD"] = GL_FUNC_ADD;

		luabind::globals(L)["gl"]["INCR"] = GL_INCR;

		luabind::globals(L)["gl"]["ONE"] = GL_ONE;
		luabind::globals(L)["gl"]["ZERO"] = GL_ZERO;
		luabind::globals(L)["gl"]["SRC_ALPHA"] = GL_SRC_ALPHA;
		luabind::globals(L)["gl"]["DST_COLOR"] = GL_DST_COLOR;
		luabind::globals(L)["gl"]["ONE_MINUS_DST_COLOR"] = GL_ONE_MINUS_DST_COLOR;
		luabind::globals(L)["gl"]["ONE_MINUS_SRC_ALPHA"] = GL_ONE_MINUS_SRC_ALPHA;

		luabind::globals(L)["gl"]["TEXTURE_1D"] = GL_TEXTURE_1D;
		luabind::globals(L)["gl"]["TEXTURE_2D"] = GL_TEXTURE_2D;
		luabind::globals(L)["gl"]["TEXTURE_2D_ARRAY"] = GL_TEXTURE_2D_ARRAY;
		luabind::globals(L)["gl"]["TEXTURE_3D"] = GL_TEXTURE_3D;
		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP"] = GL_TEXTURE_CUBE_MAP;

		// BufferBit
		luabind::globals(L)["gl"]["COLOR_BUFFER_BIT"] = GL_COLOR_BUFFER_BIT;
		luabind::globals(L)["gl"]["DEPTH_BUFFER_BIT"] = GL_DEPTH_BUFFER_BIT;
		luabind::globals(L)["gl"]["STENCIL_BUFFER_BIT"] = GL_STENCIL_BUFFER_BIT;
		luabind::globals(L)["gl"]["VIEWPORT_BIT"] = GL_VIEWPORT_BIT;

		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_POSITIVE_X"] = GL_TEXTURE_CUBE_MAP_POSITIVE_X;
		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_NEGATIVE_X"] = GL_TEXTURE_CUBE_MAP_NEGATIVE_X;
		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_POSITIVE_Y"] = GL_TEXTURE_CUBE_MAP_POSITIVE_Y;
		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_NEGATIVE_Y"] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Y;
		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_POSITIVE_Z"] = GL_TEXTURE_CUBE_MAP_POSITIVE_Z;
		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_NEGATIVE_Z"] = GL_TEXTURE_CUBE_MAP_NEGATIVE_Z;

		// Attachment
		luabind::globals(L)["gl"]["COLOR_ATTACHMENT0"] = GL_COLOR_ATTACHMENT0;
		luabind::globals(L)["gl"]["COLOR_ATTACHMENT1"] = GL_COLOR_ATTACHMENT1;
		luabind::globals(L)["gl"]["COLOR_ATTACHMENT2"] = GL_COLOR_ATTACHMENT2;
		luabind::globals(L)["gl"]["COLOR_ATTACHMENT3"] = GL_COLOR_ATTACHMENT3;
		luabind::globals(L)["gl"]["COLOR_ATTACHMENT4"] = GL_COLOR_ATTACHMENT4;
		luabind::globals(L)["gl"]["DEPTH_ATTACHMENT"] = GL_DEPTH_ATTACHMENT;
		luabind::globals(L)["gl"]["DEPTH_STENCIL_ATTACHMENT"] = GL_DEPTH_STENCIL_ATTACHMENT;

		luabind::globals(L)["gl"]["DEPTH_COMPONENT32F"] = GL_DEPTH_COMPONENT32F;
		luabind::globals(L)["gl"]["DEPTH_COMPONENT32"] = GL_DEPTH_COMPONENT32;
		luabind::globals(L)["gl"]["DEPTH_COMPONENT24"] = GL_DEPTH_COMPONENT24;
		luabind::globals(L)["gl"]["DEPTH_COMPONENT"] = GL_DEPTH_COMPONENT;

		luabind::globals(L)["gl"]["DEPTH32F_STENCIL8"] = GL_DEPTH32F_STENCIL8;
		luabind::globals(L)["gl"]["DEPTH24_STENCIL8"] = GL_DEPTH24_STENCIL8;

		// Format
		luabind::globals(L)["gl"]["RED"] = GL_RED;
		luabind::globals(L)["gl"]["R8"] = GL_R8;
		luabind::globals(L)["gl"]["R16F"] = GL_R16F;
		luabind::globals(L)["gl"]["R32F"] = GL_R32F;
		luabind::globals(L)["gl"]["RG"] = GL_RG;
		luabind::globals(L)["gl"]["RG8"] = GL_RG8;
		luabind::globals(L)["gl"]["RG16F"] = GL_RG16F;
		luabind::globals(L)["gl"]["RG32F"] = GL_RG32F;
		luabind::globals(L)["gl"]["RGB16F"] = GL_RGB16F;
		luabind::globals(L)["gl"]["RGB"] = GL_RGB;
		luabind::globals(L)["gl"]["RGB8"] = GL_RGB8;
		luabind::globals(L)["gl"]["RGBA"] = GL_RGBA;
		luabind::globals(L)["gl"]["RGBA16F"] = GL_RGBA16F;
		luabind::globals(L)["gl"]["RGBA32F"] = GL_RGBA32F;

		// Wrap
		luabind::globals(L)["gl"]["CLAMP_TO_EDGE"] = GL_CLAMP_TO_EDGE;
		luabind::globals(L)["gl"]["CLAMP"] = GL_CLAMP;
		luabind::globals(L)["gl"]["REPEAT"] = GL_REPEAT;

		// Compare Mode
		luabind::globals(L)["gl"]["COMPARE_R_TO_TEXTURE"] = GL_COMPARE_R_TO_TEXTURE;

		// Compare Func
		luabind::globals(L)["gl"]["LEQUAL"] = GL_LEQUAL;
		luabind::globals(L)["gl"]["GEQUAL"] = GL_GEQUAL;
		luabind::globals(L)["gl"]["LESS"] = GL_LESS;
		luabind::globals(L)["gl"]["GREATER"] = GL_GREATER;
		luabind::globals(L)["gl"]["EQUAL"] = GL_EQUAL;
		luabind::globals(L)["gl"]["NOTEQUAL"] = GL_NOTEQUAL;
		luabind::globals(L)["gl"]["ALWAYS"] = GL_ALWAYS;
		luabind::globals(L)["gl"]["NEVER"] = GL_NEVER;

		luabind::globals(L)["gl"]["KEEP"] = GL_KEEP;
		luabind::globals(L)["gl"]["REPLACE"] = GL_REPLACE;

		// Texture
		luabind::globals(L)["gl"]["TEXTURE0"] = GL_TEXTURE0;
		luabind::globals(L)["gl"]["TEXTURE1"] = GL_TEXTURE1;
		luabind::globals(L)["gl"]["TEXTURE2"] = GL_TEXTURE2;
		luabind::globals(L)["gl"]["TEXTURE3"] = GL_TEXTURE3;
		luabind::globals(L)["gl"]["TEXTURE4"] = GL_TEXTURE4;
		luabind::globals(L)["gl"]["TEXTURE5"] = GL_TEXTURE5;
		luabind::globals(L)["gl"]["TEXTURE6"] = GL_TEXTURE6;
		luabind::globals(L)["gl"]["TEXTURE7"] = GL_TEXTURE7;
		luabind::globals(L)["gl"]["TEXTURE8"] = GL_TEXTURE8;
		luabind::globals(L)["gl"]["TEXTURE9"] = GL_TEXTURE9;
		luabind::globals(L)["gl"]["TEXTURE10"] = GL_TEXTURE10;

		// UsageHint
		luabind::globals(L)["gl"]["STATIC_DRAW"] = GL_STATIC_DRAW;
		luabind::globals(L)["gl"]["STREAM_DRAW"] = GL_STREAM_DRAW;

		// Filter
		luabind::globals(L)["gl"]["NEAREST"] = GL_NEAREST;
		luabind::globals(L)["gl"]["LINEAR"] = GL_LINEAR;
		luabind::globals(L)["gl"]["LINEAR_MIPMAP_LINEAR"] = GL_LINEAR_MIPMAP_LINEAR;
		luabind::globals(L)["gl"]["LINEAR_MIPMAP_NEAREST"] = GL_LINEAR_MIPMAP_NEAREST;

		// Mode
		luabind::globals(L)["gl"]["POINTS"] = GL_POINTS;
		luabind::globals(L)["gl"]["LINES"] = GL_LINES;
		luabind::globals(L)["gl"]["LINE_STRIP"] = GL_LINE_STRIP;
		luabind::globals(L)["gl"]["LINE_LOOP"] = GL_LINE_LOOP;
		luabind::globals(L)["gl"]["TRIANGLES"] = GL_TRIANGLES;
		luabind::globals(L)["gl"]["TRIANGLE_STRIP"] = GL_TRIANGLE_STRIP;
		luabind::globals(L)["gl"]["TRIANGLE_FAN"] = GL_TRIANGLE_FAN;
		luabind::globals(L)["gl"]["QUADS"] = GL_QUADS;
		luabind::globals(L)["gl"]["QUAD_STRIP"] = GL_QUAD_STRIP;
		luabind::globals(L)["gl"]["POLYGON"] = GL_POLYGON;

		// Type
		luabind::globals(L)["gl"]["BYTE"] = GL_BYTE;
		luabind::globals(L)["gl"]["UNSIGNED_BYTE"] = GL_UNSIGNED_BYTE;
		luabind::globals(L)["gl"]["SHORT"] = GL_SHORT;
		luabind::globals(L)["gl"]["UNSIGNED_SHORT"] = GL_UNSIGNED_SHORT;
		luabind::globals(L)["gl"]["INT"] = GL_INT;
		luabind::globals(L)["gl"]["UNSIGNED_INT"] = GL_UNSIGNED_INT;
		luabind::globals(L)["gl"]["HALF_FLOAT"] = GL_HALF_FLOAT;
		luabind::globals(L)["gl"]["FLOAT"] = GL_FLOAT;
		luabind::globals(L)["gl"]["2_BYTES"] = GL_2_BYTES;
		luabind::globals(L)["gl"]["3_BYTES"] = GL_3_BYTES;
		luabind::globals(L)["gl"]["4_BYTES"] = GL_4_BYTES;
		luabind::globals(L)["gl"]["DOUBLE"] = GL_DOUBLE;

		// Face
		luabind::globals(L)["gl"]["FRONT_LEFT"] = GL_FRONT_LEFT;
		luabind::globals(L)["gl"]["FRONT_RIGHT"] = GL_FRONT_RIGHT;
		luabind::globals(L)["gl"]["BACK_LEFT"] = GL_BACK_LEFT;
		luabind::globals(L)["gl"]["BACK_RIGHT"] = GL_BACK_RIGHT;
		luabind::globals(L)["gl"]["FRONT"] = GL_FRONT;
		luabind::globals(L)["gl"]["BACK"] = GL_BACK;
		luabind::globals(L)["gl"]["LEFT"] = GL_LEFT;
		luabind::globals(L)["gl"]["RIGHT"] = GL_RIGHT;
		luabind::globals(L)["gl"]["FRONT_AND_BACK"] = GL_FRONT_AND_BACK;

		// RasterizeMode
		luabind::globals(L)["gl"]["POINT"] = GL_POINT;
		luabind::globals(L)["gl"]["LINE"] = GL_LINE;
		luabind::globals(L)["gl"]["FILL"] = GL_FILL;

		// Capability
		luabind::globals(L)["gl"]["CULL_FACE"] = GL_CULL_FACE;
		luabind::globals(L)["gl"]["DEPTH_TEST"] = GL_DEPTH_TEST;
		luabind::globals(L)["gl"]["BLEND"] = GL_BLEND;
		luabind::globals(L)["gl"]["CLIP_PLANE0"] = GL_CLIP_PLANE0;
		luabind::globals(L)["gl"]["SCISSOR_TEST"] = GL_SCISSOR_TEST;
		luabind::globals(L)["gl"]["STENCIL_TEST"] = GL_STENCIL_TEST;

		luabind::globals(L)["gl"]["TEXTURE_CUBE_MAP_SEAMLESS"] = GL_TEXTURE_CUBE_MAP_SEAMLESS;
		luabind::globals(L)["gl"]["FRAMEBUFFER_SRGB"] = GL_FRAMEBUFFER_SRGB;
		luabind::globals(L)["gl"]["POLYGON_OFFSET_FILL"] = GL_POLYGON_OFFSET_FILL;
		luabind::globals(L)["gl"]["MULTISAMPLE"] = GL_MULTISAMPLE;
		luabind::globals(L)["gl"]["SAMPLE_ALPHA_TO_COVERAGE"] = GL_SAMPLE_ALPHA_TO_COVERAGE;
	}
}