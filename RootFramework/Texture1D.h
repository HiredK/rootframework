/**
* @file Texture1D.h
* @brief
*/

#pragma once

#include "Texture.h"

namespace root
{
	class Texture1D : public Texture
	{
		public:
			//! CTOR/DTOR:
			Texture1D(const Settings& settings, unsigned int w);
			virtual ~Texture1D();

			//! SERVICES:
			static void Unbind();

		protected:
			//! VIRTUALS:
			virtual void Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level = 0);
			virtual GLenum GetTarget() const;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Texture2D inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Texture1D::Texture1D(const Settings& settings, unsigned int w) :
		Texture(settings, w, w) {
	}
	/*----------------------------------------------------------------------------*/
	inline Texture1D::~Texture1D() {
	}

	/*----------------------------------------------------------------------------*/
	inline void Texture1D::Unbind() {
		glBindTexture(GL_TEXTURE_1D, GL_ZERO);
	}

	/*----------------------------------------------------------------------------*/
	inline void  Texture1D::Build(GLint iformat, unsigned int w, unsigned int h, unsigned int depth, unsigned int border, GLenum format, GLenum type, const GLvoid* data, unsigned int level) {
		glTexImage1D(GetTarget(), 0, iformat, (GLsizei)w, border, format, type, data);
	}
	/*----------------------------------------------------------------------------*/
	inline GLenum Texture1D::GetTarget() const {
		return GL_TEXTURE_1D;
	}
}