/**
* @file RenderingBinding.h
* @brief Todo
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "Camera.h"
#include "Texture2D.h"
#include "FrameBuffer.h"
#include "Mesh.h"
#include "Animator.h"
#include "ParticleEmitter.h"
#include "PointCloud.h"
#include "ImprovedPerlinNoise.h"
#include "WavesSpectrum.h"
#include "Terrain.h"

#include "Engine.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// TransformBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Transform_wrapper : Transform, ScriptObject_wrapper
	{
		Transform_wrapper()
			: ScriptObject_wrapper("transform")
			, Transform() {
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TransformBinding()
	{
		return (
			luabind::class_<Transform_wrapper, ScriptObject, std::shared_ptr<Transform_wrapper>>("Transform")
			.def(luabind::constructor<>())
			.property("position", (dvec3&(Transform::*)()) &Transform::GetPosition, &Transform::SetPosition)
			.property("rotation", (dquat&(Transform::*)()) &Transform::GetRotation, &Transform::SetRotation)
			.property("scale", (dvec3&(Transform::*)()) &Transform::GetScale, &Transform::SetScale)
			.property("transfomed_box", &Transform::GetTransformedBoundingBox)
			.property("box", &Transform::GetBoundingBox)
			.def("copy", &Transform::Copy),

			luabind::class_<Transform, Transform_wrapper>("Transform_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// CameraBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Camera_wrapper : Camera, ScriptObject_wrapper
	{
		Camera_wrapper(double fov, double aspect, const dvec2& clip)
			: ScriptObject_wrapper("camera_persp", ScriptObject::Dynamic)
			, Camera(fov, aspect, clip) {
		}
		Camera_wrapper(const dvec4& bounds, const dvec2& clip)
			: ScriptObject_wrapper("camera_ortho", ScriptObject::Dynamic)
			, Camera(bounds, clip) {
		}
		Camera_wrapper()
			: Camera_wrapper(glm::radians(90.0), 1.0, dvec2(1, 1500)) {
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope CameraBinding()
	{
		return (
			luabind::class_<Camera_wrapper, Transform, std::shared_ptr<Camera_wrapper>>("Camera")

			.def(luabind::constructor<double, double, const dvec2&>())
			.def(luabind::constructor<const dvec4&, const dvec2&>())
			.def(luabind::constructor<>())

			.property("proj_matrix", &Camera::GetProjMatrix, &Camera::SetProjMatrix)
			.property("view_matrix", &Camera::GetViewMatrix, &Camera::SetViewMatrix)
			.property("view_origin_matrix", &Camera::GetViewOriginMatrix)
			.property("view_proj_matrix", &Camera::GetViewProjMatrix)
			.property("view_proj_origin_matrix", &Camera::GetViewProjOriginMatrix)

			.property("fov", &Camera::GetFov)
			.property("aspect", &Camera::GetAspect, &Camera::SetAspect)
			.property("clip", &Camera::GetClip)
			.property("bounds", &Camera::GetBounds, &Camera::SetBounds)
			.property("ortho", &Camera::IsOrtho)

			.def("copy", &Camera::Copy)
			.def("refresh", &Camera::Refresh),

			luabind::class_<Camera, Camera_wrapper>("Camera_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TextureBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Texture_wrapper : Texture, ScriptObject_wrapper
	{
		static Texture::Settings SettingsFromTable(const luabind::object& table)
		{
			Texture::Settings settings = Texture::GetDefaultSettings();
			for (luabind::iterator it(table), end; it != end; ++it) {
				std::string key = luabind::object_cast<std::string>(it.key());
				if (key == "target") {
					settings.target = luabind::object_cast<GLuint>(*it);
				}
				if (key == "iformat") {
					settings.iformat = luabind::object_cast<GLuint>(*it);
				}
				if (key == "format") {
					settings.format = luabind::object_cast<GLuint>(*it);
				}
				if (key == "type") {
					settings.type = luabind::object_cast<GLuint>(*it);
				}
				if (key == "filter_mode") {
					settings.filter_mode = luabind::object_cast<GLuint>(*it);
				}
				if (key == "wrap_mode") {
					settings.wrap_mode = luabind::object_cast<GLuint>(*it);
				}
				if (key == "compare_mode") {
					settings.compare_mode = luabind::object_cast<GLuint>(*it);
				}
				if (key == "compare_func") {
					settings.compare_func = luabind::object_cast<GLuint>(*it);
				}
				if (key == "anisotropy") {
					settings.anisotropy = luabind::object_cast<float>(*it);
				}
				if (key == "mipmap") {
					settings.mipmap = luabind::object_cast<bool>(*it);
				}
			}

			return settings;
		}

		static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth, unsigned int border, const luabind::object& settings) {
			return std::shared_ptr<Texture>(Texture::FromEmpty(w, h, depth, border, SettingsFromTable(settings)));
		}
		static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth, unsigned int border) {
			return std::shared_ptr<Texture>(Texture::FromEmpty(w, h, depth, border));
		}
		static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth, const luabind::object& settings) {
			return std::shared_ptr<Texture>(Texture::FromEmpty(w, h, depth, SettingsFromTable(settings)));
		}
		static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, unsigned int depth) {
			return std::shared_ptr<Texture>(Texture::FromEmpty(w, h, depth));
		}
		static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h, const luabind::object& settings) {
			return std::shared_ptr<Texture>(Texture::FromEmpty(w, h, SettingsFromTable(settings)));
		}
		static std::shared_ptr<Texture> FromEmpty(unsigned int w, unsigned int h) {
			return std::shared_ptr<Texture>(Texture::FromEmpty(w, h));
		}

		static std::shared_ptr<Texture> FromImage(Image* image, const luabind::object& settings) {
			return std::shared_ptr<Texture>(Texture::FromImage(image, SettingsFromTable(settings)));
		}
		static std::shared_ptr<Texture> FromImage(Image* image) {
			return std::shared_ptr<Texture>(Texture::FromImage(image));
		}

		static std::shared_ptr<Texture> FromImages(const luabind::object& images, const luabind::object& table)
		{
			Texture::Settings settings = SettingsFromTable(table);
			settings.target = GL_TEXTURE_2D_ARRAY;
			unsigned int w = 0, h = 0;

			std::vector<Image> sources;
			for (luabind::iterator it(images), end; it != end; ++it) {
				sources.push_back(luabind::object_cast<Image>(*it));
				w = sources.back().GetW();
				h = sources.back().GetH();
			}

			std::vector<const GLvoid*> data;
			for (auto& image : sources) {
				data.push_back(image.GetPixels());
			}

			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, (int)sources.size(), 0, data, settings));
		}

		static std::shared_ptr<Texture> FromRaw(unsigned int w, unsigned int h, unsigned int depth, FileHandle* file, const luabind::object& table) {
			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, depth, 0, file->GetData(), SettingsFromTable(table)));
		}
		static std::shared_ptr<Texture> FromRaw(unsigned int w, unsigned int h, unsigned int depth, FileHandle* file) {
			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, depth, 0, file->GetData(), Texture::GetDefaultSettings()));
		}
		static std::shared_ptr<Texture> FromRaw(unsigned int w, unsigned int h, FileHandle* file, const luabind::object& table) {
			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, 0, 0, file->GetData(), SettingsFromTable(table)));
		}
		static std::shared_ptr<Texture> FromRaw(unsigned int w, unsigned int h, FileHandle* file) {
			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, 0, 0, file->GetData(), Texture::GetDefaultSettings()));
		}

		static std::shared_ptr<Texture> FromColorTable(unsigned int w, unsigned int h, const luabind::object& data, const luabind::object& settings)
		{
			std::vector<unsigned char> pixels;
			for (luabind::iterator it(data), end; it != end; ++it) {
				Color pixel = luabind::object_cast<Color>(*it);
				pixels.push_back(pixel.r);
				pixels.push_back(pixel.g);
				pixels.push_back(pixel.b);
				pixels.push_back(pixel.a);
			}

			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, 0, 0, &pixels[0], SettingsFromTable(settings)));
		}

		static std::shared_ptr<Texture> FromFloatArray(unsigned int w, unsigned int h, const luabind::object& data)
		{
			std::vector<float> pixels;
			pixels.resize(w * h * 4);

			Texture::Settings settings = Texture::GetDefaultSettings();
			settings.iformat = GL_RGBA16F;
			settings.format = GL_RGBA;
			settings.type = GL_FLOAT;
			settings.wrap_mode = GL_REPEAT;

			for (unsigned int i = 0; i < pixels.size(); ++i) {
				pixels[i] = luabind::object_cast<float>(data[i]);
			}

			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, 0, 0, &pixels[0], settings));
		}

		static std::shared_ptr<Texture> FromRandom(unsigned int w, unsigned int h)
		{
			float* noise = new float[w*h*4];
			for (unsigned int y = 0; y < h; ++y)
			for (unsigned int x = 0; x < w; ++x)
			{
				int offset = 4 * (y*w + x);

				vec2 xy = glm::circularRand(1.0f);
				noise[offset + 0] = xy[0];
				noise[offset + 1] = xy[1];
				noise[offset + 2] = glm::linearRand(0.0f, 1.0f);
				noise[offset + 3] = glm::linearRand(0.0f, 1.0f);
			}

			Texture::Settings settings = Texture::GetDefaultSettings();
			settings.iformat = GL_RGBA16F;
			settings.format = GL_RGBA;
			settings.type = GL_FLOAT;
			settings.wrap_mode = GL_REPEAT;

			return std::shared_ptr<Texture>(Texture::CreateTexture(w, h, 0, 0, noise, settings));
		}

		static std::shared_ptr<Texture> FromRandom1D(unsigned int w)
		{
			float* noise = new float[w*3];
			for (unsigned int x = 0; x < w*3; x += 3)
			{
				noise[x + 0] = glm::linearRand(0.0f, 1.0f);
				noise[x + 1] = glm::linearRand(0.0f, 1.0f);
				noise[x + 2] = glm::linearRand(0.0f, 1.0f);
			}

			Texture::Settings settings = Texture::GetDefaultSettings();
			settings.target = GL_TEXTURE_1D;
			settings.iformat = GL_RGB16F;
			settings.format = GL_RGB;
			settings.type = GL_FLOAT;
			settings.filter_mode = GL_LINEAR;
			settings.wrap_mode = GL_REPEAT;

			return std::shared_ptr<Texture>(Texture::CreateTexture(w, w, 0, 0, noise, settings));
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope TextureBinding()
	{
		return (
			luabind::class_<Texture_wrapper, ScriptObject, std::shared_ptr<Texture_wrapper>>("Texture")
			.scope
			[
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, unsigned int, unsigned int, const luabind::object&)) &Texture_wrapper::FromEmpty),
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, unsigned int, unsigned int)) &Texture_wrapper::FromEmpty),
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, unsigned int, const luabind::object&)) &Texture_wrapper::FromEmpty),
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, unsigned int)) &Texture_wrapper::FromEmpty),
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, const luabind::object&)) &Texture_wrapper::FromEmpty),
				luabind::def("from_empty", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int)) &Texture_wrapper::FromEmpty),
				luabind::def("from_image", (std::shared_ptr<Texture>(*)(Image*, const luabind::object&)) &Texture_wrapper::FromImage),
				luabind::def("from_image", (std::shared_ptr<Texture>(*)(Image*)) &Texture_wrapper::FromImage),
				luabind::def("from_images", (std::shared_ptr<Texture>(*)(const luabind::object&, const luabind::object&)) &Texture_wrapper::FromImages),
				luabind::def("from_raw", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, unsigned int, FileHandle*, const luabind::object&)) &Texture_wrapper::FromRaw),
				luabind::def("from_raw", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, unsigned int, FileHandle*)) &Texture_wrapper::FromRaw),
				luabind::def("from_raw", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, FileHandle*, const luabind::object&)) &Texture_wrapper::FromRaw),
				luabind::def("from_raw", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, FileHandle*)) &Texture_wrapper::FromRaw),
				luabind::def("from_color_table", (std::shared_ptr<Texture>(*)(unsigned int, unsigned int, const luabind::object&, const luabind::object&)) &Texture_wrapper::FromColorTable),
				luabind::def("from_float_array", &Texture_wrapper::FromFloatArray),
				luabind::def("from_random", &Texture_wrapper::FromRandom),
				luabind::def("from_random_1D", &Texture_wrapper::FromRandom1D),
				luabind::def("unbind", &Texture::Unbind)
			]
			.property("w", &Texture::GetW)
			.property("h", &Texture::GetH)
			.def("bind", &Texture::Bind)
			.def("generate_mipmap", &Texture::GenerateMipmap)
			.def("set_base_level", &Texture::SetBaseLevel)
			.def("set_max_level", &Texture::SetMaxLevel),

			luabind::class_<Texture, Texture_wrapper>("Texture_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// FrameBufferBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct FrameBuffer_wrapper : FrameBuffer, ScriptObject_wrapper
	{
		FrameBuffer_wrapper()
			: ScriptObject_wrapper("frame_buffer")
			, FrameBuffer() {
		}

		dvec4 ReadPixel(Texture* texture, GLenum attachment, int x, int y)
		{
			void* data = FrameBuffer::ReadPixels(texture, attachment, x, y, 1, 1);
			dvec4 result = dvec4(0, 0, 0, 0);

			auto ConvertToFloat = [&texture](void* data, int i) {
				if (texture->GetSettings().type != GL_FLOAT) {
					return ceilf(float((1.0f / 255.0f) * static_cast<unsigned char*>(data)[i]) * 100) / 100;
				}
				else {
					return static_cast<float*>(data)[i];
				}
			};

			switch (texture->GetSettings().format) {
				case GL_ALPHA:
				case GL_DEPTH_COMPONENT:
				case GL_R:
					result.x = ConvertToFloat(data, 0);
					break;
				case GL_RG:
					result.x = ConvertToFloat(data, 0);
					result.y = ConvertToFloat(data, 1);
					break;
				case GL_RGB:
					result.x = ConvertToFloat(data, 0);
					result.y = ConvertToFloat(data, 1);
					result.z = ConvertToFloat(data, 2);
					break;
				case GL_RGBA:
					result.x = ConvertToFloat(data, 0);
					result.y = ConvertToFloat(data, 1);
					result.z = ConvertToFloat(data, 2);
					result.w = ConvertToFloat(data, 3);
					break;
			}

			return result;
		}

		Color ReadColor(Texture* texture, GLenum attachment, int x, int y)
		{
			void* data = FrameBuffer::ReadPixels(texture, attachment, x, y);
			Color result;

			switch (texture->GetSettings().format) {
				case GL_ALPHA:
				case GL_DEPTH_COMPONENT:
				case GL_R:
					result.r = static_cast<unsigned char*>(data)[0];
					break;
				case GL_RG:
					result.r = static_cast<unsigned char*>(data)[0];
					result.g = static_cast<unsigned char*>(data)[1];
					break;
				case GL_RGB:
					result.r = static_cast<unsigned char*>(data)[0];
					result.g = static_cast<unsigned char*>(data)[1];
					result.b = static_cast<unsigned char*>(data)[2];
					break;
				case GL_RGBA:
					result.r = static_cast<unsigned char*>(data)[0];
					result.g = static_cast<unsigned char*>(data)[1];
					result.b = static_cast<unsigned char*>(data)[2];
					result.a = static_cast<unsigned char*>(data)[3];
					break;
			}

			return result;
		}

		luabind::object ReadColors(Texture* texture, GLenum attachment, int x, int y, int w, int h)
		{
			void* data = FrameBuffer::ReadPixels(texture, attachment, x, y, w, h);
			std::map<unsigned int, Color> color_map;

			for (int i = 0; i < w; ++i)
			for (int j = 0; j < h; ++j)
			{
				int offset = 4 * ((i * h) + j);

				Color pixel;
				pixel.r = static_cast<unsigned char*>(data)[offset + 0];
				pixel.g = static_cast<unsigned char*>(data)[offset + 1];
				pixel.b = static_cast<unsigned char*>(data)[offset + 2];
				pixel.a = static_cast<unsigned char*>(data)[offset + 3];
				unsigned int hash = pixel.ToInteger();

				auto it = color_map.find(hash);
				if (it == color_map.end()) {
					color_map[hash] = pixel;
				}
			}

			luabind::object table = luabind::newtable(GetLuaState());
			int index = 1;

			for (auto& it : color_map) {
				table[index] = it.second;
				index++;
			}

			return table;
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope FrameBufferBinding()
	{
		return (
			luabind::class_<FrameBuffer_wrapper, ScriptObject, std::shared_ptr<FrameBuffer_wrapper>>("FrameBuffer")
			.def(luabind::constructor<>())
			.scope
			[
				luabind::def("unbind", &FrameBuffer::Unbind)
			]
			.def("attach", (void(FrameBuffer::*)(Texture*, GLenum, int, int)) &FrameBuffer::Attach)
			.def("attach", (void(FrameBuffer::*)(Texture*, GLenum, int)) &FrameBuffer::Attach)
			.def("attach", (void(FrameBuffer::*)(Texture*, GLenum)) &FrameBuffer::Attach)
			.def("read_pixel", &FrameBuffer_wrapper::ReadPixel)
			.def("read_color", &FrameBuffer_wrapper::ReadColor)
			.def("read_colors", &FrameBuffer_wrapper::ReadColors)
			.def("bind_output", (void(FrameBuffer::*)()) &FrameBuffer::BindOutput)
			.def("bind_output", (void(FrameBuffer::*)(GLenum)) &FrameBuffer::BindOutput)
			.def("bind", &FrameBuffer::Bind)
			.def("clear", &FrameBuffer::Clear),

			luabind::class_<FrameBuffer, FrameBuffer_wrapper>("FrameBuffer_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// MeshBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Mesh_wrapper : Mesh, ScriptObject_wrapper
	{
		static std::shared_ptr<Mesh> Build(const luabind::object& data, GLsizei vertex_count, 
			GLsizei stride, GLenum mode, GLenum usage, const luabind::object& indices)
		{
			std::vector<char> raw_vertex(stride * vertex_count);
			for (unsigned int i = 0; i < raw_vertex.size(); ++i) {
				raw_vertex[i] = luabind::object_cast<char>(data[i]);
			}

			std::vector<unsigned short> raw_indice;
			for (luabind::iterator it(indices), end; it != end; ++it) {
				raw_indice.push_back(luabind::object_cast<unsigned short>(*it));
			}

			return std::shared_ptr<Mesh>(new Mesh(
				reinterpret_cast<const void*>(&raw_vertex[0]),
				vertex_count, stride, mode, usage,
				reinterpret_cast<const void*>(&raw_indice[0]),
				static_cast<GLsizei>(raw_indice.size())
			));
		}

		static std::shared_ptr<Mesh> Build(const luabind::object& data, GLsizei vertex_count, 
			GLsizei stride, GLenum mode, GLenum usage)
		{
			std::vector<char> raw_vertex(stride * vertex_count);
			for (unsigned int i = 0; i < raw_vertex.size(); ++i) {
				raw_vertex[i] = luabind::object_cast<char>(data[i]);
			}

			return std::shared_ptr<Mesh>(new Mesh(
				reinterpret_cast<const void*>(&raw_vertex[0]),
				vertex_count, stride, mode, usage
			));
		}

		static std::shared_ptr<Mesh> BuildQuad(float w, float h) {
			return Mesh::BuildQuad(w, h);
		}
		static std::shared_ptr<Mesh> BuildQuad() {
			return Mesh::BuildQuad(1.0f, 1.0f);
		}
		static std::shared_ptr<Mesh> BuildBox(const dvec3& min, const dvec3& max) {
			return Mesh::BuildBox(min, max, Color::White);
		}
		static std::shared_ptr<Mesh> BuildBox(const dvec3& extent) {
			return Mesh::BuildBox(-extent, extent, Color::White);
		}
		static std::shared_ptr<Mesh> BuildCube(float extent) {
			return Mesh::BuildCube(extent, Color::White);
		}
		static std::shared_ptr<Mesh> BuildCube() {
			return Mesh::BuildCube(0.5f);
		}
		static std::shared_ptr<Mesh> BuildBoxWire(const dvec3& min, const dvec3& max) {
			return Mesh::BuildBoxWire(min, max, Color::White);
		}
		static std::shared_ptr<Mesh> BuildCubeWire(float extent) {
			return Mesh::BuildCubeWire(extent, Color::White);
		}
		static std::shared_ptr<Mesh> BuildCubeWire() {
			return Mesh::BuildCubeWire(0.5f);
		}
		static std::shared_ptr<Mesh> BuildSphere(float radius) {
			return Mesh::BuildSphere(radius);
		}
		static std::shared_ptr<Mesh> BuildSphere(float radius, int U, int V) {
			return Mesh::BuildSphere(radius, U, V);
		}
		static std::shared_ptr<Mesh> BuildCylinder(float radius, float height) {
			return Mesh::BuildCylinder(radius, radius, height);
		}
		static std::shared_ptr<Mesh> BuildCylinder(float radius, float height, int R, int H) {
			return Mesh::BuildCylinder(radius, radius, height, R, H);
		}
		static std::shared_ptr<Mesh> BuildCone(float radius, float height) {
			return Mesh::BuildCone(radius, height);
		}
		static std::shared_ptr<Mesh> BuildCone(float radius, float height, int R) {
			return Mesh::BuildCone(radius, height, R);
		}
		static std::shared_ptr<Mesh> BuildCapsule(float radius, float length) {
			return Mesh::BuildCapsule(radius, length);
		}
		static std::shared_ptr<Mesh> BuildCapsule(float radius, float length, int S) {
			return Mesh::BuildCapsule(radius, length, S);
		}
		static std::shared_ptr<Mesh> BuildPlane(int w, int h, bool center, bool cw) {
			return Mesh::BuildPlane(w, h, center, cw);
		}
		static std::shared_ptr<Mesh> BuildPlane(int w, int h) {
			return Mesh::BuildPlane(w, h);
		}
		static std::shared_ptr<Mesh> BuildPlane() {
			return Mesh::BuildPlane(1, 1);
		}
	};

	/*----------------------------------------------------------------------------*/
	static void Mesh_DrawVirtual(GLenum mode, GLsizei count)
	{
		static std::unique_ptr<GLBuffer> virtual_ibo = std::unique_ptr<GLBuffer>(
			new GLBuffer(0, nullptr, GL_ELEMENT_ARRAY_BUFFER, GL_STATIC_DRAW)
			);

		GLBuffer::ScopedBinder ibo_binder(*virtual_ibo.get());
		glDrawArrays(mode, 0, count);
	}

	/*----------------------------------------------------------------------------*/
	static void Mesh_DrawLine(Camera* camera, const dvec3& p1, const dvec3& p2)
	{
		Shader::Get()->UniformMat4("u_ViewProjMatrix", camera->GetViewProjMatrix());
		Shader::Get()->UniformMat4("u_ModelMatrix", dmat4());

		glBegin(GL_LINES);
			glVertex3f((float)p1.x, (float)p1.y, (float)p1.z);
			glVertex3f((float)p2.x, (float)p2.y, (float)p2.z);
		glEnd();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope MeshBinding()
	{
		return (
			luabind::class_<Mesh_wrapper, Transform, std::shared_ptr<Mesh_wrapper>>("Mesh")
			.scope
			[
				luabind::def("build", (std::shared_ptr<Mesh>(*)(const luabind::object&, GLsizei, GLsizei, GLenum, GLenum, const luabind::object&)) &Mesh_wrapper::Build),
				luabind::def("build", (std::shared_ptr<Mesh>(*)(const luabind::object&, GLsizei, GLsizei, GLenum, GLenum)) &Mesh_wrapper::Build),
				luabind::def("build_quad", (std::shared_ptr<Mesh>(*)(float, float, const Color&)) &Mesh::BuildQuad),
				luabind::def("build_quad", (std::shared_ptr<Mesh>(*)(float, float)) &Mesh_wrapper::BuildQuad),
				luabind::def("build_quad", (std::shared_ptr<Mesh>(*)()) &Mesh_wrapper::BuildQuad),
				luabind::def("build_box", (std::shared_ptr<Mesh>(*)(const dvec3&, const dvec3&, const Color&)) &Mesh::BuildBox),
				luabind::def("build_box", (std::shared_ptr<Mesh>(*)(const dvec3&, const dvec3&)) &Mesh_wrapper::BuildBox),
				luabind::def("build_box", (std::shared_ptr<Mesh>(*)(const dvec3&)) &Mesh_wrapper::BuildBox),
				luabind::def("build_cube", (std::shared_ptr<Mesh>(*)(float, const Color&)) &Mesh::BuildCube),
				luabind::def("build_cube", (std::shared_ptr<Mesh>(*)(float)) &Mesh_wrapper::BuildCube),
				luabind::def("build_cube", (std::shared_ptr<Mesh>(*)()) &Mesh_wrapper::BuildCube),
				luabind::def("build_box_wire", (std::shared_ptr<Mesh>(*)(const dvec3&, const dvec3&, const Color&)) &Mesh::BuildBoxWire),
				luabind::def("build_box_wire", (std::shared_ptr<Mesh>(*)(const dvec3&, const dvec3&)) &Mesh_wrapper::BuildBoxWire),
				luabind::def("build_cube_wire", (std::shared_ptr<Mesh>(*)(float, const Color&)) &Mesh::BuildCubeWire),
				luabind::def("build_cube_wire", (std::shared_ptr<Mesh>(*)(float)) &Mesh_wrapper::BuildCubeWire),
				luabind::def("build_cube_wire", (std::shared_ptr<Mesh>(*)()) &Mesh_wrapper::BuildCubeWire),
				luabind::def("build_sphere", (std::shared_ptr<Mesh>(*)(float, int, int)) &Mesh_wrapper::BuildSphere),
				luabind::def("build_sphere", (std::shared_ptr<Mesh>(*)(float)) &Mesh_wrapper::BuildSphere),
				luabind::def("build_cylinder", (std::shared_ptr<Mesh>(*)(float, float)) &Mesh_wrapper::BuildCylinder),
				luabind::def("build_cylinder", (std::shared_ptr<Mesh>(*)(float, float, int, int)) &Mesh_wrapper::BuildCylinder),
				luabind::def("build_cone", (std::shared_ptr<Mesh>(*)(float, float)) &Mesh_wrapper::BuildCone),
				luabind::def("build_cone", (std::shared_ptr<Mesh>(*)(float, float, int)) &Mesh_wrapper::BuildCone),
				luabind::def("build_capsule", (std::shared_ptr<Mesh>(*)(float, float)) &Mesh_wrapper::BuildCapsule),
				luabind::def("build_capsule", (std::shared_ptr<Mesh>(*)(float, float, int)) &Mesh_wrapper::BuildCapsule),
				luabind::def("build_plane", (std::shared_ptr<Mesh>(*)(int, int, bool, bool)) &Mesh_wrapper::BuildPlane),
				luabind::def("build_plane", (std::shared_ptr<Mesh>(*)(int, int)) &Mesh_wrapper::BuildPlane),
				luabind::def("build_plane", (std::shared_ptr<Mesh>(*)()) &Mesh_wrapper::BuildPlane),

				luabind::def("draw_virtual", &Mesh_DrawVirtual),
				luabind::def("draw_line", &Mesh_DrawLine)
			]
			.def("add_vertex_attrib", (void(Mesh::*)(const std::string&, GLint, GLenum, bool, GLsizei)) &Mesh::AddVertexAttribute)
			.def("draw", (void(Mesh::*)(Camera&)) &Mesh::Draw)
			.def("draw", (void(Mesh::*)()) &Mesh::Draw),

			luabind::class_<Mesh, Mesh_wrapper>("Mesh_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// AnimatorBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Animator_wrapper : Animator, ScriptObject_wrapper
	{
		Animator_wrapper()
			: ScriptObject_wrapper("animator", ScriptObject::Dynamic)
			, Animator() {
		}

		virtual void Update(float dt) {
			Animator::Animate(dt);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope AnimatorBinding()
	{
		return (
			luabind::class_<Animator_wrapper, Transform, std::shared_ptr<Animator_wrapper>>("Animator")
			.def(luabind::constructor<>())

			.property("current_animation_index", &Animator::GetCurrentAnimationIndex, &Animator::SetCurrentAnimationIndex)
			.def("load_skeleton", (bool(Animator::*)(FileHandle*)) &Animator::LoadSkeleton)
			.def("load_mesh", (bool(Animator::*)(FileHandle*)) &Animator::LoadMesh)
			.def("add_animation", (bool(Animator::*)(int, FileHandle*)) &Animator::AddAnimation)
			.def("draw", (void(Animator::*)(Camera&)) &Animator::Draw)
			.def("draw", (void(Animator::*)()) &Animator::Draw),

			luabind::class_<Animator, Animator_wrapper>("Animator_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ParticleEmitterBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ParticleEmitter_wrapper : ParticleEmitter, ScriptObject_wrapper
	{
		ParticleEmitter_wrapper(unsigned int max_particles)
			: ScriptObject_wrapper("particle_emitter")
			, ParticleEmitter(max_particles) {
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope ParticleEmitterBinding()
	{
		return (
			luabind::class_<ParticleEmitter_wrapper, Transform, std::shared_ptr<ParticleEmitter_wrapper>>("ParticleEmitter")
			.def(luabind::constructor<unsigned int>())
			
			.def("simulate", &ParticleEmitter::Simulate)
			.def("draw", (void(ParticleEmitter::*)(Camera&)) &ParticleEmitter::Draw)
			.def("draw", (void(ParticleEmitter::*)()) &ParticleEmitter::Draw),

			luabind::class_<ParticleEmitter, ParticleEmitter_wrapper>("ParticleEmitter_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// PointCloudBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct PointCloud_wrapper : PointCloud<double>, ScriptObject_wrapper
	{
		PointCloud_wrapper(int max_leaf_size)
			: ScriptObject_wrapper("point_cloud")
			, PointCloud(max_leaf_size) {
		}

		luabind::object RadiusSearch(const dvec3& query, double radius)
		{
			luabind::object table = luabind::newtable(GetLuaState());
			auto result = PointCloud::RadiusSearch(query, radius);
			int index = 1;

			for (auto& pair : result) {
				table[index] = static_cast<int>(pair.first) + 1;
				index++;
			}

			return table;
		}

		int AddPoint_wrap(const dvec3& point) {
			int index = PointCloud::AddPoint(point);
			return index + 1;
		}

		void Build() {
			Logger::ScopedTimer timer(LOG, "root::PointCloud::Build()");
			PointCloud::Build();
		}

		void Clear() {
			PointCloud::Clear();
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope PointCloudBinding()
	{
		return (
			luabind::class_<PointCloud_wrapper, ScriptObject, std::shared_ptr<PointCloud_wrapper>>("PointCloud")
			.def(luabind::constructor<int>())
			.def("add_point", &PointCloud_wrapper::AddPoint_wrap)
			.def("build", &PointCloud_wrapper::Build)
			.def("radius_search", (luabind::object(PointCloud_wrapper::*)(const dvec3&, double)) &PointCloud_wrapper::RadiusSearch)
			.def("clear", &PointCloud_wrapper::Clear),

			luabind::class_<PointCloud<double>, PointCloud_wrapper>("PointCloud_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// ImprovedPerlinNoiseBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct ImprovedPerlinNoise_wrapper : ImprovedPerlinNoise, ScriptObject_wrapper
	{
		ImprovedPerlinNoise_wrapper(int size)
			: ScriptObject_wrapper("improved_perlin_noise")
			, ImprovedPerlinNoise(size) {
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope ImprovedPerlinNoiseBinding()
	{
		return (
			luabind::class_<ImprovedPerlinNoise_wrapper, ScriptObject, std::shared_ptr<ImprovedPerlinNoise_wrapper>>("ImprovedPerlinNoise")
			.def(luabind::constructor<int>())
			.enum_("Textures")
			[
				luabind::value("PERM_TABLE", ImprovedPerlinNoise::PERM_TABLE),
				luabind::value("GRADIENT", ImprovedPerlinNoise::GRADIENT)
			]
			.def("generate", &ImprovedPerlinNoise::GenerateData)
			.def("bind", &ImprovedPerlinNoise::Bind),

			luabind::class_<ImprovedPerlinNoise, ImprovedPerlinNoise_wrapper>("ImprovedPerlinNoise_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// WavesSpectrumBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct WavesSpectrum_wrapper : WavesSpectrum, ScriptObject_wrapper
	{
		WavesSpectrum_wrapper()
			: ScriptObject_wrapper("waves_spectrum")
			, WavesSpectrum() {
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope WavesSpectrumBinding()
	{
		return (
			luabind::class_<WavesSpectrum_wrapper, ScriptObject, std::shared_ptr<WavesSpectrum_wrapper>>("WavesSpectrum")
			.def(luabind::constructor<>())
			.enum_("Textures")
			[
				luabind::value("SPECTRUM_12", WavesSpectrum::SPECTRUM_12),
				luabind::value("SPECTRUM_34", WavesSpectrum::SPECTRUM_34),
				luabind::value("BUTTERFLY", WavesSpectrum::BUTTERFLY)
			]
			.property("slope_variance_delta", &WavesSpectrum::GetSlopeVarianceDelta)
			.property("size", &WavesSpectrum::GetSize)
			.def("bind", &WavesSpectrum::Bind),

			luabind::class_<WavesSpectrum, WavesSpectrum_wrapper>("WavesSpectrum_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// DeformationBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Deformation_wrapper : Deformation, ScriptObject_wrapper
	{
		Deformation_wrapper()
			: ScriptObject_wrapper("deformation")
			, Deformation() {
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope DeformationBinding()
	{
		return (
			luabind::class_<Deformation_wrapper, ScriptObject, std::shared_ptr<Deformation_wrapper>>("Deformation")
			.def(luabind::constructor<>())
			.def("local_to_deformed", &Deformation::LocalToDeformed)
			.def("deformed_to_local_bounds", &Deformation::DeformedToLocalBounds)
			.def("set_uniforms", (void(Deformation::*)(Camera&, const dmat3&, int, double, double, double)) &Deformation::SetUniforms),

			luabind::class_<Deformation, Deformation_wrapper>("Deformation_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// SphericalDeformationBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct SphericalDeformation_wrapper : SphericalDeformation, ScriptObject_wrapper
	{
		SphericalDeformation_wrapper(double radius)
			: ScriptObject_wrapper("spherical_deformation")
			, SphericalDeformation(radius) {
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope SphericalDeformationBinding()
	{
		return (
			luabind::class_<SphericalDeformation_wrapper, Deformation, std::shared_ptr<SphericalDeformation_wrapper>>("SphericalDeformation")
			.def(luabind::constructor<double>()),

			luabind::class_<SphericalDeformation, SphericalDeformation_wrapper>("SphericalDeformation_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TileProducerBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct TileProducer_wrapper : TileProducer, ScriptObject_wrapper
	{
		TileProducer_wrapper(const Terrain& owner, int tile_size)
			: ScriptObject_wrapper("tile_producer")
			, TileProducer(owner, tile_size) {
		}

		virtual bool Produce(TileData* data)
		{
			bool success = false;
			try {
				success = luabind::wrap_base::call<bool>("__produce", data);
			}
			catch (std::exception const& e) {
				LOG(Logger::Error) << e.what();
			}
			catch (...) {
				LOG(Logger::Error) << "Error!";
			}

			return success;
		}
		static void Produce_default(TileProducer* ptr, TileData* data) {
			ptr->TileProducer::Produce(data);
		}

		void WriteResidualTexture(const std::string& path, Texture* residual_tex)
		{
			float* height = new float[GetTileSize() * GetTileSize()];

			residual_tex->Bind();
			glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, height);

			const size_t len = sizeof(float) * GetTileSize() * GetTileSize();
			GetEngineInstance()->GetSystem<FileSystem>()->Write(path, height, len);
			delete[] height;
		}
	};

	/*----------------------------------------------------------------------------*/
	static void TileData_SetTexture(TileProducer::TileData& self, std::shared_ptr<Texture>& texture, int i) {
		self.textures[i] = texture;
	}
	/*----------------------------------------------------------------------------*/
	static Texture* TileData_GetTexture(TileProducer::TileData& self, int i) {
		return self.textures[i].get();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope TileProducerBinding()
	{
		return (
			luabind::class_<TileProducer_wrapper, ScriptObject, std::shared_ptr<TileProducer_wrapper>>("TileProducer")
			.def(luabind::constructor<const Terrain&, int>())

			.scope
			[
				luabind::class_<TileProducer::TileData>("TileData")
				.scope
				[
					luabind::def("get_hash", &TileProducer::TileData::GetHash)
				]

				.def_readonly("level", &TileProducer::TileData::level)
				.def_readonly("tx", &TileProducer::TileData::tx)
				.def_readonly("ty", &TileProducer::TileData::ty)
				.def("set_texture", &TileData_SetTexture)
				.def("get_texture", &TileData_GetTexture)
			]

			.property("owner", &TileProducer::GetOwner)
			.property("tile_size", &TileProducer::GetTileSize)
			.property("border", &TileProducer::GetBorder)
			.def("get_tile", &TileProducer::GetTile)
			.def("get_tile_coords", &TileProducer::GetTileCoords)
			.def("get_deformed_parameters", &TileProducer::GetDeformParameters)
			.def("set_uniforms", &TileProducer::SetUniforms)
			.def("write_residual_texture", &TileProducer_wrapper::WriteResidualTexture)
			.def("__produce", &TileProducer::Produce, &TileProducer_wrapper::Produce_default),

			luabind::class_<TileProducer, TileProducer_wrapper>("TileProducer_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// GraphBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Graph_wrapper : Graph, ScriptObject_wrapper
	{
		Graph_wrapper()
			: ScriptObject_wrapper("graph")
			, Graph() {
		}

		bool Read(FileHandle* file) {
			if (!FileHandle::Ready(file)) {
				return false;
			}

			std::string in = std::string((char*)file->GetData(), file->GetSize());
			std::vector<Node*> nodes;

			auto NextValue = [&in]() -> std::string {
				size_t token = in.find_first_of(" ");
				std::string v = in.substr(0, token);
				in = in.substr(token + 1, in.size());
				return v;
			};

			{
				// 1) READ GRAPH NODES:
				int ncount = std::atoi(NextValue().c_str());
				for (int i = 0; i < ncount; ++i)
				{
					dvec2 coord;
					coord.x = std::atof(NextValue().c_str());
					coord.y = std::atof(NextValue().c_str());
					nodes.push_back(NewNode(coord));
				}
			}
			{
				// 2) READ GRAPH CURVES:
				int ccount = std::atoi(NextValue().c_str());
				for (int i = 0; i < ccount; ++i)
				{
					int size = std::atoi(NextValue().c_str());
					float width = (float)std::atof(NextValue().c_str());

					Node* s = nodes[std::atoi(NextValue().c_str())];
					for (int j = 1; j < size - 1; j++) {
					}

					Node* e = nodes[std::atoi(NextValue().c_str())];
					Curve* c = NewCurve(nullptr, s, e);
					c->width = width;
				}
			}

			return true;
		}

		bool Write(const std::string& path) {
			if (m_nodes.empty() || m_curves.empty()) {
				return true;
			}

			std::map<Node*, int> nindices;
			std::stringstream out;

			{
				// 1) WRITE GRAPH NODES:
				out << m_nodes.size() << " ";
				int index = 0;

				Node::Iterator it = GetNodes();
				while (it.HasNext())
				{
					Node* n = it.Next();
					out << n->coord.x << " ";
					out << n->coord.y << " ";
					nindices[n] = index++;
				}
			}
			{
				// 2) WRITE GRAPH CURVES:
				out << m_curves.size() << " ";
				int index = 0;

				Curve::Iterator it = GetCurves();
				while (it.HasNext())
				{
					Curve* c = it.Next();
					out << c->GetSize() << " " << c->width << " ";
					out << nindices[c->start] << " ";

					for (int i = 1; i < c->GetSize() - 1; ++i) {
						out << c->GetXY(i).x << " ";
						out << c->GetXY(i).y << " ";
					}

					out << nindices[c->end] << " ";
				}
			}

			GetEngineInstance()->GetSystem<FileSystem>()->Write(path, out.str());
			return true;
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope GraphBinding()
	{
		return (
			luabind::class_<Graph_wrapper, ScriptObject, std::shared_ptr<Graph_wrapper>>("Graph")
			.def(luabind::constructor<>())

			.scope
			[
				luabind::class_<Graph::Node>("Node")
				.scope
				[
					luabind::class_<Graph::Node::Iterator>("Iterator")
					.def("has_next", &Graph::Node::Iterator::HasNext)
					.def("next", &Graph::Node::Iterator::Next)
				],

				luabind::class_<Graph::Curve>("Curve")
				.scope
				[
					luabind::class_<Graph::Curve::Iterator>("Iterator")
					.def("has_next", &Graph::Curve::Iterator::HasNext)
					.def("next", &Graph::Curve::Iterator::Next)
				]

				.def_readwrite("width", &Graph::Curve::width)
				.property("size", &Graph::Curve::GetSize)
				.property("bounds", &Graph::Curve::GetBounds)
			]
			
			.property("nodes", &Graph::GetNodes)
			.property("curves", &Graph::GetCurves)
			.def("add_curve", &Graph::AddCurve)
			.def("read", &Graph_wrapper::Read)
			.def("write", &Graph_wrapper::Write),

			luabind::class_<Graph, Graph_wrapper>("Graph_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// GraphLayerBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct GraphLayer_wrapper : GraphLayer, ScriptObject_wrapper
	{
		GraphLayer_wrapper(const TileProducer& producer)
			: ScriptObject_wrapper("graph_layer")
			, GraphLayer(producer) {
		}

		virtual void Blit(int level, int tx, int ty, int size)
		{
			try {
				luabind::wrap_base::call<void>("__blit", level, tx, ty, size);
			}
			catch (std::exception const& e) {
				LOG(Logger::Error) << e.what();
			}
			catch (...) {
				LOG(Logger::Error) << "Error!";
			}
		}
		static void Blit_default(GraphLayer* ptr, int level, int tx, int ty, int size) {
			ptr->GraphLayer::Blit(level, tx, ty, size);
		}
	};

	/*----------------------------------------------------------------------------*/
	inline luabind::scope GraphLayerBinding()
	{
		return (
			luabind::class_<GraphLayer_wrapper, ScriptObject, std::shared_ptr<GraphLayer_wrapper>>("GraphLayer")
			.def(luabind::constructor<const TileProducer&>())

			.property("producer", &GraphLayer::GetProducer)
			.def("draw_curve", &GraphLayer::DrawCurve)
			.def("draw_curve_altitude", &GraphLayer::DrawCurveAltitude)
			.def("__blit", &GraphLayer::Blit, &GraphLayer_wrapper::Blit_default),

			luabind::class_<GraphLayer, GraphLayer_wrapper>("GraphLayer_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TerrainBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	struct Terrain_wrapper : Terrain, ScriptObject_wrapper
	{
		Terrain_wrapper(double size, int max_level, int face)
			: ScriptObject_wrapper("terrain", ScriptObject::Dynamic)
			, Terrain(size, max_level, (Face)face)
		{
			RegisterSignalHandler("on_page_in", new SignalHandler<void, const TerrainTile&>());
			AddPageInCallback(std::bind(&Terrain_wrapper::OnPageIn, this, std::placeholders::_1));
		}

		void OnPageIn(const TerrainTile& tile) {
			CallSignal<void, const TerrainTile&>("on_page_in", tile);
		}

		virtual void Update(float dt) {
			Terrain::Process();
		}
	};
	/*----------------------------------------------------------------------------*/
	inline luabind::scope TerrainBinding()
	{
		return (
			luabind::class_<Terrain_wrapper, Transform, std::shared_ptr<Terrain_wrapper>>("Terrain")
			.def(luabind::constructor<double, int, int>())

			.enum_("Face")
			[
				luabind::value("PosX", Terrain::PosX),
				luabind::value("NegX", Terrain::NegX),
				luabind::value("PosY", Terrain::PosY),
				luabind::value("NegY", Terrain::NegY),
				luabind::value("PosZ", Terrain::PosZ),
				luabind::value("NegZ", Terrain::NegZ)
			]

			.scope
			[
				luabind::class_<TerrainTile>("Tile")
				.scope
				[
					luabind::class_<TerrainTile::Coord>("Coord")
					.def_readonly("tx", &TerrainTile::Coord::tx)
					.def_readonly("ty", &TerrainTile::Coord::ty)
					.def_readonly("ox", &TerrainTile::Coord::ox)
					.def_readonly("oy", &TerrainTile::Coord::oy)
					.def_readonly("length", &TerrainTile::Coord::length)
					.def_readonly("zmin", &TerrainTile::Coord::zmin)
					.def_readonly("zmax", &TerrainTile::Coord::zmax)
				]

				.property("owner", &TerrainTile::GetOwner)
				.property("coord", &TerrainTile::GetCoord)
				.property("level", &TerrainTile::GetLevel)
				.property("visible", &TerrainTile::IsVisible)
				.def("get_center", &TerrainTile::GetCenter)
				.def("get_child", &TerrainTile::GetChild)
				.def("is_leaf", &TerrainTile::IsLeaf)
			]

			.property("root", &Terrain::GetRoot)
			.property("use_tessellated_patch", &Terrain::GetUseTessellatedPatch, &Terrain::SetUseTessellatedPatch)
			.property("max_level", &Terrain::GetMaxLevel)
			.property("size", &Terrain::GetSize)
			.property("face", &Terrain::GetFace)
			.def("add_producer", &Terrain_wrapper::AddProducer)
			.def("get_deformation", &Terrain::GetDeformation)
			.def("set_deformation", &Terrain::SetDeformation)
			.def("get_face_matrix", (dmat3(Terrain::*)() const) &Terrain::GetFaceMatrix)
			.def("get_camera_dist", (double(Terrain::*)(const Box3D&, const dvec3&) const) &Terrain::GetCameraDist)
			.def("get_camera_dist", (double(Terrain::*)(const Box3D&) const) &Terrain::GetCameraDist)
			.def("cull", &Terrain::Cull)
			.def("draw", &Terrain::Draw),

			luabind::class_<Terrain, Terrain_wrapper>("Terrain_impl__")
		);
	}

	////////////////////////////////////////////////////////////////////////////////
	// RenderingBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline luabind::scope RenderingBinding()
	{
		return (
			luabind::namespace_("root")
			[
				luabind::class_<Frustum>("Frustum")
				.def(luabind::constructor<>())
				.def("transform", &Frustum::Transform),

				TransformBinding(),
				CameraBinding(),
				TextureBinding(),
				FrameBufferBinding(),
				MeshBinding(),
				AnimatorBinding(),
				ParticleEmitterBinding(),
				PointCloudBinding(),
				ImprovedPerlinNoiseBinding(),
				WavesSpectrumBinding(),

				// Terrain
				DeformationBinding(),
				SphericalDeformationBinding(),
				TileProducerBinding(),
				GraphBinding(),
				GraphLayerBinding(),
				TerrainBinding()
			]
		);
	}

} // farore namespace