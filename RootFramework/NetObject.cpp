#include "NetObject.h"
#include "Network.h"

#include "Logger.h"

void root::NetObject::WriteAllocationID(RakNet::Connection_RM3* connection, RakNet::BitStream* bs) const
{
	bs->Write(networkID);
}

void root::NetObject::SerializeConstruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection)
{
}

bool root::NetObject::DeserializeConstruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection)
{
	return true;
}

void root::NetObject::SerializeDestruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection)
{
}

bool root::NetObject::DeserializeDestruction(RakNet::BitStream* bs, RakNet::Connection_RM3* connection)
{
	return true;
}

RakNet::RM3SerializationResult root::NetObject::Serialize(RakNet::SerializeParameters* parameters)
{
	return RakNet::RM3SR_SERIALIZED_ALWAYS;
}

void root::NetObject::Deserialize(RakNet::DeserializeParameters* parameters)
{
}

RakNet::RM3ConstructionState root::NetObject::QueryConstruction(RakNet::Connection_RM3* connection, RakNet::ReplicaManager3* rm3)
{
	return QueryConstruction_ServerConstruction(connection, m_network->IsAutoritative());
}

bool root::NetObject::QueryRemoteConstruction(RakNet::Connection_RM3* connection)
{
	return QueryRemoteConstruction_ServerConstruction(connection, m_network->IsAutoritative());
}

RakNet::RM3QuerySerializationResult root::NetObject::QuerySerialization(RakNet::Connection_RM3* connection)
{
	return QuerySerialization_ClientSerializable(connection, m_network->IsAutoritative());
}

RakNet::RM3ActionOnPopConnection root::NetObject::QueryActionOnPopConnection(RakNet::Connection_RM3* connection) const
{
	return QueryActionOnPopConnection_Server(connection);
}

void root::NetObject::DeallocReplica(RakNet::Connection_RM3* connection)
{
}