#include "GwenRenderer.h"
#include "Texture2D.h"
#include "Shader.h"
#include "Logger.h"

void root::GwenRenderer::SetSize(unsigned int w, unsigned int h)
{
	if (m_w != w || m_h != h) {
		if (m_render_to_fbo)
		{
			m_fbo = std::unique_ptr<FrameBuffer>(new FrameBuffer());
			m_texture = Texture::FromEmpty(w, h);
		}

		m_w = w;
		m_h = h;
	}
}

void root::GwenRenderer::BlitFBOTextureToScreenQuad()
{
	if (m_texture) {
		Shader::Get()->UniformMat4("u_ViewProjMatrix", glm::mat4(1));
		Shader::Get()->Uniform("u_RenderingType", 2);

		Shader::Get()->Uniform("s_Tex0", 0);
		m_texture->Bind();

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);

		m_quad_mesh->Draw();
		glDisable(GL_BLEND);
	}
}

void root::GwenRenderer::Flush()
{
	m_batch.Flush();
	m_batch.Draw();
	m_batch.Clear();
}

void root::GwenRenderer::Begin()
{
	BindFBO();
	Shader::Get()->UniformMat4("u_ViewProjMatrix", glm::ortho(0.0f, float(m_w), float(m_h), 0.0f, -1.0f, 1.0f));
	glViewport(0, 0, m_w, m_h);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	m_batch.Clear();

	if (m_render_to_fbo)
	{
		m_fbo->Bind();
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
		glClear(GL_COLOR_BUFFER_BIT);
	}
}

void root::GwenRenderer::End()
{
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);
	Flush();
}

void root::GwenRenderer::SetDrawColor(Gwen::Color color)
{
	m_draw_color = { color.r, color.g, color.b, color.a };
}

void root::GwenRenderer::DrawFilledRect(Gwen::Rect rect)
{
	Shader::Get()->Uniform("u_RenderingType", 1);
	Translate(rect);

	m_batch.SetColor(m_draw_color);
	m_batch.Add(
		(float)rect.x,
		(float)rect.y,
		(float)rect.w,
		(float)rect.h
	);
}

void root::GwenRenderer::StartClip()
{
	Flush();
	Gwen::Rect rect = ClipRegion();

	GLint view[4];
	glGetIntegerv(GL_VIEWPORT, &view[0]);
	rect.y = view[3] - (rect.y + rect.h);

	rect.x = int(rect.x * Scale());
	rect.y = int(rect.y * Scale());
	rect.w = int(rect.w * Scale());
	rect.h = int(rect.h * Scale());

	glScissor(rect.x, rect.y, rect.w, rect.h);
	glEnable(GL_SCISSOR_TEST);
}

void root::GwenRenderer::EndClip()
{
	Flush();
	glDisable(GL_SCISSOR_TEST);
}

void root::GwenRenderer::LoadTexture(Gwen::Texture* texture)
{
	const std::string path = texture->name.Get();
	Image* image = dynamic_cast<Image*>(m_engine_instance->GetSystem<FileSystem>()->Search(path));
	if (!FileHandle::Ready(image)) {
		image = dynamic_cast<Image*>(m_engine_instance->GetSystem<FileSystem>()->Search(path, true));
	}

	if (FileHandle::Ready(image)) {
		Texture::Settings& settings = Texture::GetDefaultSettings();
		settings.filter_mode = GL_LINEAR;

		// alloc texture shared ptr user data
		texture->data = std::malloc(sizeof(std::shared_ptr<Texture>));
		std::memset(texture->data, 0, sizeof(std::shared_ptr<Texture>));

		// create a texture object from image
		std::shared_ptr<Texture> ptr = Texture::FromImage(image, settings);
		texture->width = ptr->GetW();
		texture->height = ptr->GetH();

		// assign created texture to user data
		(*static_cast<std::shared_ptr<Texture>*>(texture->data)) = ptr;
	}
	else {
		LOG(Logger::Error) << path << ": Missing image file";
		texture->failed = true;
	}
}

void root::GwenRenderer::FreeTexture(Gwen::Texture* texture)
{
	if (texture->data != nullptr)
	{
		// assign null to trigger destruction
		(*static_cast<std::shared_ptr<Texture>*>(texture->data)) = nullptr;
		std::free(texture->data);
	}
}

void root::GwenRenderer::DrawTexturedRect(Gwen::Texture* texture, Gwen::Rect rect, float u1, float v1, float u2, float v2)
{
	if (texture->data != nullptr && !texture->failed) {
		std::shared_ptr<Texture>& ptr = (*static_cast<std::shared_ptr<Texture>*>(texture->data));
		ptr->Bind();

		Shader::Get()->Uniform("u_RenderingType", 2);
		Shader::Get()->Uniform("s_Tex0", 0);
		Translate(rect);

		m_batch.SetColor(m_draw_color);
		m_batch.Add(
			(float)rect.x,
			(float)rect.y,
			(float)rect.w,
			(float)rect.h,
			u1, v1,
			u2, v2
		);
	}
	else {
		DrawFilledRect(rect);
	}
}

Gwen::Color root::GwenRenderer::PixelColour(Gwen::Texture* texture, unsigned int x, unsigned int y, const Gwen::Color& def)
{
	Image* image = dynamic_cast<Image*>(m_engine_instance->GetSystem<FileSystem>()->Search(texture->name.Get()));
	if (FileHandle::Ready(image)) {
		Color pixel_color = image->GetPixel(x, y);
		return Gwen::Color(
			pixel_color.r,
			pixel_color.g,
			pixel_color.b,
			pixel_color.a
		);
	}

	return def;
}

void root::GwenRenderer::LoadFont(Gwen::Font* font)
{
	const std::string path = Gwen::Utility::UnicodeToString(font->facename);
	FileHandle* file = m_engine_instance->GetSystem<FileSystem>()->Search(path, true);
	if (FileHandle::Ready(file))
	{
		int handle = m_text_renderer.BuildFont(file);
		if (handle != FONS_INVALID)
		{
			font->realsize = font->size * Scale();

			// alloc font handle user data
			font->data = std::malloc(sizeof(int));
			std::memset(font->data, 0, sizeof(int));

			// assign font handle to user data
			(*static_cast<int*>(font->data)) = handle;
		}
		else {
			LOG(Logger::Error) << path << ": Invalid font file";
			font->facename = L"missing";
		}
	}
	else {
		LOG(Logger::Error) << path << ": Missing font file";
		font->facename = L"missing";
	}
}

void root::GwenRenderer::FreeFont(Gwen::Font* font)
{
	if (font->data != nullptr)
	{
		// no real cleanup needed here
		std::free(font->data);
		font->data = nullptr;
	}
}

void root::GwenRenderer::RenderText(Gwen::Font* font, Gwen::Point pos, const Gwen::UnicodeString& text)
{
	Shader::Get()->Uniform("u_RenderingType", 3);
	Translate(pos.x, pos.y);

	int handle = (*static_cast<int*>(font->data));

	float x = (float)pos.x;
	float y = (float)pos.y;

	m_text_renderer.RenderText(handle, x, y, font->realsize, Gwen::Utility::UnicodeToString(text), m_draw_color);
}

Gwen::Point root::GwenRenderer::MeasureText(Gwen::Font* font, const Gwen::UnicodeString& text)
{
	if ((!font->data || fabs(font->realsize - font->size * Scale()) > 2) && font->facename != L"missing") {
		FreeFont(font);
		LoadFont(font);
	}

	Gwen::Point p;

	if (font->data) {
		int handle = (*static_cast<int*>(font->data));
		p.x = int(m_text_renderer.MeasureText(handle, font->realsize, Gwen::Utility::UnicodeToString(text)));
		p.y = int(font->realsize);
	}

	return p;
}