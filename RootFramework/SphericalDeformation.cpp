#include "SphericalDeformation.h"
#include "Terrain.h"

void root::SphericalDeformation::SetUniforms(Camera& camera, const dmat3& matrix, int level, double ox, double oy, double l)
{
	Deformation::SetUniforms(camera, matrix, level, ox, oy, l);

	dvec3 p0 = dvec3(ox, oy, m_radius);
	dvec3 p1 = dvec3(ox + l, oy, m_radius);
	dvec3 p2 = dvec3(ox, oy + l, m_radius);
	dvec3 p3 = dvec3(ox + l, oy + l, m_radius);
	dvec3 pc = (p0 + p3) * 0.5;

	dvec3 v0 = glm::normalize(p0);
	dvec3 v1 = glm::normalize(p1);
	dvec3 v2 = glm::normalize(p2);
	dvec3 v3 = glm::normalize(p3);

	Shader::Get()->UniformMat3("u_Deform_LocalToWorld", matrix);

	Shader::Get()->UniformMat4("u_Deform_ScreenQuadCorners", m_local_to_screen * glm::transpose(dmat4(
		v0.x * m_radius, v1.x * m_radius, v2.x * m_radius, v3.x * m_radius,
		v0.y * m_radius, v1.y * m_radius, v2.y * m_radius, v3.y * m_radius,
		v0.z * m_radius, v1.z * m_radius, v2.z * m_radius, v3.z * m_radius,
		1, 1, 1, 1
	)));

	Shader::Get()->UniformMat4("u_Deform_ScreenQuadVerticals", m_local_to_screen * glm::transpose(dmat4(
		v0.x, v1.x, v2.x, v3.x,
		v0.y, v1.y, v2.y, v3.y,
		v0.z, v1.z, v2.z, v3.z,
		0, 0, 0, 0
	)));

	Shader::Get()->Uniform("u_Deform_ScreenQuadCornerNorms",
		(float)glm::length(p0),
		(float)glm::length(p1),
		(float)glm::length(p2),
		(float)glm::length(p3)
	);

	Shader::Get()->Uniform("u_Deform_Radius", (float)m_radius);

	dvec3 uz = glm::normalize(pc);
	dvec3 ux = glm::normalize(glm::cross(dvec3(0, -1, 0), uz));
	dvec3 uy = glm::cross(uz, ux);
	dmat4 ltow = dmat4(matrix);

	dmat3 tangentFrameToWorld = dmat3(
		ltow[0].x, ltow[0].y, ltow[0].z,
		ltow[1].x, ltow[1].y, ltow[1].z,
		ltow[2].x, ltow[2].y, ltow[2].z
	);

	Shader::Get()->UniformMat4("u_Deform_TangentFrameToWorld", dmat4(tangentFrameToWorld * glm::transpose(dmat3(
		ux.x, uy.x, uz.x,
		ux.y, uy.y, uz.y,
		ux.z, uy.z, uz.z
	))));
}

void root::SphericalDeformation::SetUniforms(Camera& camera, const TerrainTile& tile)
{
	double ox = tile.GetCoord().ox;
	double oy = tile.GetCoord().oy;
	double l = tile.GetCoord().length;

	Deformation::SetUniforms(camera, tile);
	SphericalDeformation::SetUniforms(camera, tile.GetOwner().GetFaceMatrix(), tile.GetLevel(), ox, oy, l);
}