#include "TransformHistory.h"

void root::TransformHistory::Write(const Transform& transform, unsigned int t)
{
	if (m_history.Size() > 0) {
		const Cell& last = m_history.PeekTail();
		if (t - last.t >= m_write_interval)
		{
			m_history.Push({ transform.GetPosition(), transform.GetRotation(), t }, _FILE_AND_LINE_);
			if (m_history.Size() > m_max_history_length) {
				m_history.Pop();
			}
		}
	}
	else {
		m_history.Push({ transform.GetPosition(), transform.GetRotation(), t }, _FILE_AND_LINE_);
	}
}

void root::TransformHistory::Overwrite(const Transform& transform, unsigned int t)
{
	const int size = m_history.Size();
	if (m_history.Size() > 0) {
		for (int i = size - 1; i >= 0; i--) {
			Cell& cell = m_history[i];
			if (t >= cell.t)
			{
				if (i == size - 1 && t - cell.t >= m_write_interval) {
					m_history.Push({ transform.GetPosition(), transform.GetRotation(), t }, _FILE_AND_LINE_);
					if (m_history.Size() > m_max_history_length) {
						m_history.Pop();
					}

					return;
				}

				cell = { transform.GetPosition(), transform.GetRotation(), t };
				return;
			}
		}
	}
	else {
		m_history.Push({ transform.GetPosition(), transform.GetRotation(), t }, _FILE_AND_LINE_);
	}
}

root::TransformHistory::ReadResult root::TransformHistory::Read(Transform& transform, unsigned int when, unsigned int t)
{
	const int size = m_history.Size();
	if (size == 0) {
		return VALUES_UNCHANGED;
	}

	for (int i = size - 1; i >= 0; i--) {
		const Cell& cell = m_history[i];
		if (when >= cell.t)
		{
			if (i == size - 1) {
				if (t <= cell.t) {
					return VALUES_UNCHANGED;
				}

				double alpha = double((when - cell.t)) / double((t - cell.t));
				transform.SetPosition(glm::mix(cell.position, transform.GetPosition(), alpha));
				transform.SetRotation(glm::slerp(cell.rotation, transform.GetRotation(), alpha));
			}
			else
			{
				const Cell& next = m_history[i + 1];
				double alpha = float(when - cell.t) / double(next.t - cell.t);
				transform.SetPosition(glm::mix(cell.position, next.position, alpha));
				transform.SetRotation(glm::slerp(cell.rotation, next.rotation, alpha));
			}

			return INTERPOLATED;
		}
	}

	const Cell& last = m_history.Peek();
	transform.SetPosition(last.position);
	transform.SetRotation(last.rotation);
	return READ_OLDEST;
}