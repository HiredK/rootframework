/**
* @file FileFactory.h
* @brief Implements a system that automatically derives a class from a FileHandle
* by registering the expected extension(s).
*/

#pragma once

#include <algorithm>
#include <map>
#include <stdarg.h>
#include <ctype.h>

/**
* @def REGISTER_FILE
* @brief Define in class header file:
* REGISTER_FILE(class)
*/
#define REGISTER_FILE(c) \
	static const root::FileFactory::FileCreatorImpl<c> ctor;

/**
* @def REGISTER_FILE_IMPL
* @brief Define in class source file:
* REGISTER_FILE_IMPL(class, 2, "ext1", "ext2")
*/
#define REGISTER_FILE_IMPL(c, extc, ...) \
	const root::FileFactory::FileCreatorImpl<c> c::ctor(#c, extc, __VA_ARGS__);

namespace root
{
	class Engine;
	class FileHandle;

	class FileFactory
	{
		public:
			struct FileCreator
			{
				virtual FileHandle* Create(Engine* engine_instance, const std::string& path) = 0;
				virtual bool IsSupported(const std::string& path) const = 0;
			};

			template <class T>
			struct FileCreatorImpl : public virtual FileCreator
			{
				FileCreatorImpl<T>(const std::string& c, int argc, ...)
				{
					GetTable()[c] = this;

					va_list ap;
					va_start(ap, argc);

					while (argc--)
					{
						std::string ext = va_arg(ap, char*);
						std::transform(ext.begin(), ext.end(), ext.begin(), ::toupper);
						m_extv.push_back(ext);
					}

					va_end(ap);
				}

				virtual FileHandle* Create(Engine* engine_instance, const std::string& path) {
					return new T(engine_instance, path);
				}

				virtual bool IsSupported(const std::string& path) const {
					for (auto& e : m_extv) {
						if (e == ExtractExt(path)) {
							return true;
						}
					}

					return false;
				}

				std::vector<std::string> m_extv;
			};

			static FileHandle* Detect(Engine* engine_instance, const std::string& path) {
				for (auto& ctor : GetTable())
				{
					if (ctor.second->IsSupported(path)) {
						return ctor.second->Create(engine_instance, path);
					}
				}

				return nullptr;
			}

			static std::string ExtractExt(const std::string& path) {
				std::string ext = path.substr(path.find_last_of(".") + 1);
				std::transform(ext.begin(), ext.end(), ext.begin(), ::toupper);
				return ext;
			}

			static std::string RemoveExt(const std::string& path) {
				return path.substr(0, path.find_last_of("."));
			}

		protected:
			static std::map<std::string, FileCreator*>& GetTable() {
				static std::map<std::string, FileCreator*> table;
				return table;
			}
	};

} // root namespace