/**
* @file MathBinding.h
* @brief Todo
*/

#pragma once

#include "ScriptObjectBinding.h"
#include "Math.h"

namespace root
{
	////////////////////////////////////////////////////////////////////////////////
	// MathBinding:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline dvec4 dmat4_get(const dmat4& mat, unsigned int index) {
		return mat[index];
	}
	/*----------------------------------------------------------------------------*/
	inline void dmat4_set(dmat4& mat, unsigned int index, const dvec4& vec) {
		mat[index] = vec;
	}

	/*----------------------------------------------------------------------------*/
	inline dquat glm_angle_axis(double angle, const dvec3& v) {
		return glm::angleAxis(angle, v);
	}

	/*----------------------------------------------------------------------------*/
	static dvec4 glm_vec4_from_table(const luabind::object& table)
	{
		double* array_data = new double[4];
		unsigned int index = 0;

		for (luabind::iterator it(table), end; it != end; ++it) {
			array_data[index++] = luabind::object_cast<double>(*it);
		}

		return glm::make_vec4(array_data);
	}

	/*----------------------------------------------------------------------------*/
	static dmat3 glm_mat3_from_table(const luabind::object& table)
	{
		double* array_data = new double[9];
		unsigned int index = 0;

		for (luabind::iterator it(table), end; it != end; ++it) {
			array_data[index++] = luabind::object_cast<double>(*it);
		}

		return glm::make_mat3(array_data);
	}

	/*----------------------------------------------------------------------------*/
	static dmat4 glm_mat4_from_table(const luabind::object& table)
	{
		double* array_data = new double[16];
		unsigned int index = 0;

		for (luabind::iterator it(table), end; it != end; ++it) {
			array_data[index++] = luabind::object_cast<double>(*it);
		}

		return glm::make_mat4(array_data);
	}

	static dquat glm_quat_from_axis(int axis)
	{
		switch (axis) {
			case Axis::PosX:
				return dquat(dvec3(0, glm::radians( 90.0f), 0));
			case Axis::NegX:
				return dquat(dvec3(0, glm::radians(-90.0f), 0));
			case Axis::PosY:
				return dquat(dvec3(glm::radians( 90.0f), 0, 0));
			case Axis::NegY:
				return dquat(dvec3(glm::radians(-90.0f), 0, 0));
			case Axis::PosZ:
				return dquat();
			case Axis::NegZ:
				return dquat(dvec3(0, glm::radians(180.0f), 0));
			default:
				break;
		}

		return dquat();
	}

	static dquat glm_quat_from_axis_cubemap(int axis)
	{
		switch (axis) {
			case Axis::PosX:
				return dquat(dvec3(0, glm::radians( 90.0f), glm::radians(180.0f)));
			case Axis::NegX:
				return dquat(dvec3(0, glm::radians(-90.0f), glm::radians(180.0f)));
			case Axis::PosY:
				return dquat(dvec3(glm::radians( 90.0f), 0, 0));
			case Axis::NegY:
				return dquat(dvec3(glm::radians(-90.0f), 0, 0));
			case Axis::PosZ:
				return dquat(dvec3(glm::radians(180.0f), 0, 0));
			case Axis::NegZ:
				return dquat(dvec3(0, 0, glm::radians(180.0f)));
			default:
				break;
		}

		return dquat();
	}

	/*----------------------------------------------------------------------------*/
	inline luabind::scope MathBinding()
	{
		return (
			luabind::namespace_("math")
			[
				luabind::def("degrees", (double(*)(double)) &glm::degrees),
				luabind::def("degrees", (dvec3(*)(const dvec3&)) &glm::degrees),
				luabind::def("radians", (double(*)(double)) &glm::radians),
				luabind::def("radians", (dvec3(*)(const dvec3&)) &glm::radians),

				luabind::def("clamp", (double(*)(double, double, double)) &glm::clamp),
				luabind::def("smoothstep", (double(*)(double, double, double)) &glm::smoothstep),
				luabind::def("pow", (double(*)(double, double)) &glm::pow),
				luabind::def("log", (double(*)(double)) &glm::log),
				luabind::def("exp", (double(*)(double)) &glm::exp),
				luabind::def("tanh", (double(*)(double)) &glm::tanh),
				luabind::def("atan2", (double(*)(double, double)) &std::atan2), // std here ishh
				luabind::def("sin", (double(*)(double)) &glm::sin),
				luabind::def("acos", (double(*)(double)) &glm::acos),
				luabind::def("cos", (double(*)(double)) &glm::cos),

				luabind::def("ball_rand", (dvec3(*)(double)) &glm::ballRand),
				luabind::def("linear_rand", (double(*)(double, double)) &glm::linearRand),

				luabind::def("mix", (double(*)(double, double, double)) &glm::mix),
				luabind::def("mix", (dvec3(*)(dvec3 const &, dvec3 const &, double)) &glm::mix),
				luabind::def("lerp", (dquat(*)(dquat const &, dquat const &, double)) &glm::lerp),
				luabind::def("slerp", (dquat(*)(dquat const &, dquat const &, double)) &glm::slerp),

				luabind::def("abs", (double(*)(double)) &glm::abs),

				luabind::def("normalize", (dvec3(*)(const dvec3&)) &glm::normalize),
				luabind::def("normalize", (dquat(*)(const dquat&)) &glm::normalize),
				luabind::def("length", (double(*)(const dvec2&)) &glm::length),
				luabind::def("length", (double(*)(const dvec3&)) &glm::length),

				luabind::def("cross", (dvec3(*)(const dvec3&, const dvec3&)) &glm::cross),
				luabind::def("dot", (double(*)(const dvec3&, const dvec3&)) &glm::dot),

				luabind::def("ortho", (dmat4(*)(double, double, double, double, double, double)) &glm::ortho),
				luabind::def("unproject", (dvec3(*)(const dvec3&, const dmat4&, const dmat4&, const dvec4&)) &glm::unProject),
				luabind::def("lookat", (dmat4(*)(const dvec3&, const dvec3&, const dvec3&)) &glm::lookAt),

				luabind::def("min", (double(*)(double, double)) &glm::min),
				luabind::def("max", (double(*)(double, double)) &glm::max),

				luabind::def("ceil", (double(*)(double)) &glm::ceil),
				luabind::def("distance", (double(*)(const dvec3&, const dvec3&)) &glm::distance),
				luabind::def("round", (dvec4(*)(const dvec4&)) &glm::round),
				luabind::def("round", (double(*)(double)) &glm::round),
				luabind::def("round_even", (double(*)(double)) &glm::roundEven),

				luabind::def("perspective", (dmat4(*)(double, double, double, double)) &glm::perspective),
				luabind::def("translate", (dmat4(*)(const dmat4&, const dvec3&)) &glm::translate),
				luabind::def("scale", (dmat4(*)(const dmat4&, const dvec3&)) &glm::scale),

				luabind::def("column", (dvec4(*)(const dmat4&, int)) &glm::column),
				luabind::def("row", (dvec4(*)(const dmat4&, int)) &glm::row),

				luabind::def("rotate", (dmat4(*)(const dmat4&, double, const dvec3&)) &glm::rotate),
				luabind::def("rotate_x", (dvec3(*)(const dvec3&, const double&)) &glm::rotateX),
				luabind::def("rotate_y", (dvec3(*)(const dvec3&, const double&)) &glm::rotateY),
				luabind::def("rotate_z", (dvec3(*)(const dvec3&, const double&)) &glm::rotateZ),

				luabind::def("euler_angles", (dvec3(*)(const dquat&)) &glm::eulerAngles),

				luabind::def("conjugate", (dquat(*)(const dquat&)) &glm::conjugate),

				luabind::def("inverse", (dmat3(*)(const dmat3&)) &glm::inverse),
				luabind::def("transpose", (dmat3(*)(const dmat3&)) &glm::transpose),
				luabind::def("transpose", (dmat4(*)(const dmat4&)) &glm::transpose),
				luabind::def("inverse_transpose", (dmat3(*)(const dmat3&)) &glm::inverseTranspose),
				luabind::def("inverse_transpose", (dmat4(*)(const dmat4&)) &glm::inverseTranspose),
				luabind::def("inverse", (dmat4(*)(const dmat4&)) &glm::inverse),
				luabind::def("angle_axis", &glm_angle_axis),

				luabind::def("mat3_cast", (dmat3(*)(const dquat&)) &glm::mat3_cast),
				luabind::def("mat4_cast", (dmat4(*)(const dquat&)) &glm::mat4_cast)
			],

			luabind::class_<dvec2>("vec2")
			.def(luabind::constructor<dvec3>())
			.def(luabind::constructor<dvec4>())
			.def(luabind::constructor<double, double>())
			.def(luabind::constructor<double>())
			.def(luabind::constructor<>())
			.def_readwrite("x", &dvec2::x)
			.def_readwrite("y", &dvec2::y)
			.def(luabind::self + luabind::other<dvec2>())
			.def(luabind::self + luabind::other<double>())
			.def(luabind::self - luabind::other<dvec2>())
			.def(luabind::self - luabind::other<double>())
			.def(luabind::other<double>() / luabind::self)
			.def(luabind::self / luabind::other<double>())
			.def(luabind::self / luabind::other<dvec2>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dvec2>()),

			luabind::class_<dvec3>("vec3")
			.def(luabind::constructor<dvec4>())
			.def(luabind::constructor<dvec3>())
			.def(luabind::constructor<double, double, double>())
			.def(luabind::constructor<double>())
			.def(luabind::constructor<dvec2, double>())
			.def(luabind::constructor<>())
			.def_readwrite("x", &dvec3::x)
			.def_readwrite("y", &dvec3::y)
			.def_readwrite("z", &dvec3::z)
			.def(luabind::self + luabind::other<dvec3>())
			.def(luabind::self + luabind::other<double>())
			.def(luabind::self - luabind::other<dvec3>())
			.def(luabind::self - luabind::other<double>())
			.def(luabind::self / luabind::other<dvec3>())
			.def(luabind::self / luabind::other<double>())
			.def(luabind::self * luabind::other<dvec3>())
			.def(luabind::other<double>() * luabind::self)
			.def(luabind::self * luabind::other<double>())		
			.def(luabind::self * luabind::other<dmat3>()),

			luabind::class_<dvec4>("vec4")
			.scope
			[
				luabind::def("from_table", &glm_vec4_from_table)
			]
			.def(luabind::constructor<double, double, double, double>())
			.def(luabind::constructor<const dvec3&, double>())
			.def(luabind::constructor<double>())
			.def(luabind::constructor<>())
			.def_readwrite("x", &dvec4::x)
			.def_readwrite("y", &dvec4::y)
			.def_readwrite("z", &dvec4::z)
			.def_readwrite("w", &dvec4::w)
			.def(luabind::self - luabind::other<dvec4>())
			.def(luabind::self - luabind::other<double>())
			.def(luabind::self + luabind::other<dvec4>())
			.def(luabind::self / luabind::other<double>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dmat4>()),

			luabind::class_<dquat>("quat")
			.scope
			[
				luabind::def("from_axis", &glm_quat_from_axis),
				luabind::def("from_axis_cubemap", &glm_quat_from_axis_cubemap)
			]
			.def(luabind::constructor<>())
			.def(luabind::constructor<double, double, double, double>())
			.def(luabind::constructor<const dmat4&>())
			.def(luabind::constructor<const dquat&>())
			.def(luabind::constructor<const dvec3&>())
			.def(luabind::self * luabind::other<double>())
			.def(luabind::self * luabind::other<dquat>())
			.def(luabind::self * luabind::other<dvec3>())
			.def_readwrite("x", &dquat::x)
			.def_readwrite("y", &dquat::y)
			.def_readwrite("z", &dquat::z)
			.def_readwrite("w", &dquat::w),

			luabind::class_<dmat3>("mat3")
			.scope
			[
				luabind::def("from_table", &glm_mat3_from_table)
			]
			.def(luabind::constructor<>())
			.def(luabind::constructor<const dmat4&>())
			.def(luabind::self * luabind::other<dmat3>())
			.def(luabind::self * luabind::other<dvec3>()),

			luabind::class_<dmat4>("mat4")
			.scope
			[
				luabind::def("from_table", &glm_mat4_from_table)
			]
			.def(luabind::constructor<>())
			.def(luabind::constructor<const dmat4&>())
			.def(luabind::constructor<const dmat3&>())
			.def("get", &dmat4_get)
			.def("set", &dmat4_set)
			.def(luabind::self * luabind::other<dmat4>())
			.def(luabind::self * luabind::other<dvec4>()),

			luabind::class_<Box2D>("Box2D")
			.def(luabind::constructor<dvec2, dvec2>())
			.def("contains", (bool(Box2D::*)(const dvec2&) const) &Box2D::Contains)
			.def("contains", (bool(Box2D::*)(const Box2D&) const) &Box2D::Contains)
			.def("intersects", (bool(Box2D::*)(const Box2D&) const) &Box2D::Intersects),

			luabind::class_<Box3D>("Box3D")
			.def(luabind::constructor<dvec3, dvec3>())
			.def(luabind::constructor<>())
			.property("min", &Box3D::GetMin)
			.property("max", &Box3D::GetMax)
			.property("center", &Box3D::GetCenter)
			.def("enlarge", (void(Box3D::*)(const Box3D&)) &Box3D::Enlarge)
			.def("set", (void(Box3D::*)(const dvec3&, const dvec3&)) &Box3D::Set)
			.def("set", (void(Box3D::*)(const dvec3&)) &Box3D::Set)
			.def("set", (void(Box3D::*)(const Box3D&)) &Box3D::Set)
		);
	}

	/*----------------------------------------------------------------------------*/
	inline void MathBinding_Globals(lua_State* L)
	{
		luabind::globals(L)["math"]["PosX"] = Axis::PosX;
		luabind::globals(L)["math"]["NegX"] = Axis::NegX;
		luabind::globals(L)["math"]["PosY"] = Axis::PosY;
		luabind::globals(L)["math"]["NegY"] = Axis::NegY;
		luabind::globals(L)["math"]["PosZ"] = Axis::PosZ;
		luabind::globals(L)["math"]["NegZ"] = Axis::NegZ;
	}
}