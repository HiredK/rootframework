/**
* @file Deformation.h
*/

#pragma once

#include "Camera.h"

namespace root
{
	class TerrainTile;

	class Deformation
	{
		public:
			//! VIRTUALS:
			virtual dvec3 LocalToDeformed(const dvec3& local);
			virtual dmat4 LocalToDeformedDifferential(const dvec3& local, bool clamp = false);
			virtual dvec3 DeformedToLocal(const dvec3& deformed);
			virtual Box2D DeformedToLocalBounds(const dvec3& center, double radius);
			virtual dmat4 DeformedToTangentFrame(const dvec3& deformed);
			virtual double GetLocalDist(const dvec3& local, const Box3D& box);
			virtual Frustum::Visibility GetVisibility(const dvec3& position, const Frustum& frustum, const Box3D& box);
			virtual void SetUniforms(Camera& camera, const dmat3& matrix, int level, double ox, double oy, double l);
			virtual void SetUniforms(Camera& camera, const TerrainTile& tile);

		protected:
			//! MEMBERS:
			dmat4 m_local_to_camera;
			dmat4 m_local_to_screen;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Deformation inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline dvec3 Deformation::LocalToDeformed(const dvec3& local) {
		return local;
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4 Deformation::LocalToDeformedDifferential(const dvec3& local, bool clamp) {
		return glm::translate(dmat4(), dvec3(local.x, local.y, 0));
	}
	/*----------------------------------------------------------------------------*/
	inline dvec3 Deformation::DeformedToLocal(const dvec3& deformed) {
		return deformed;
	}
	/*----------------------------------------------------------------------------*/
	inline Box2D Deformation::DeformedToLocalBounds(const dvec3& center, double radius) {
		return Box2D(dvec2(center) - radius, dvec2(center) + radius);
	}
	/*----------------------------------------------------------------------------*/
	inline dmat4 Deformation::DeformedToTangentFrame(const dvec3& deformed) {
		return glm::translate(dmat4(), dvec3(-deformed.x, -deformed.y, 0));
	}
	/*----------------------------------------------------------------------------*/
	inline double Deformation::GetLocalDist(const dvec3& local, const Box3D& box) {
		return glm::max(glm::abs(local.z - box.GetMax().z),
			glm::max(glm::min(glm::abs(local.x - box.GetMin().x), glm::abs(local.x - box.GetMax().x)),
			glm::min(glm::abs(local.y - box.GetMin().y), glm::abs(local.y - box.GetMax().y)))
		);
	}
	/*----------------------------------------------------------------------------*/
	inline Frustum::Visibility Deformation::GetVisibility(const dvec3& position, const Frustum& frustum, const Box3D& box) {
		return frustum.IsInside(box);
	}
}