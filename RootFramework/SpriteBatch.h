/**
* @file SpriteBatch.h
* @brief
*/

#pragma once

#include "Mesh.h"

namespace root
{
	class SpriteBatch : public Mesh
	{
		public:
			//! CTOR/DTOR:
			SpriteBatch(GLsizei size, GLenum usage);
			SpriteBatch(GLsizei size);
			virtual ~SpriteBatch();

			//! SERVICES:
			unsigned int Add(float x, float y, float w, float h, float u1, float v1, float u2, float v2, int index);
			unsigned int Add(float x, float y, float w, float h, float u1, float v1, float u2, float v2);
			unsigned int Add(float x, float y, float w, float h);
			void Flush();
			void Clear();

		private:
			//! MEMBERS:
			GLsizei m_size, m_next;
	};

	////////////////////////////////////////////////////////////////////////////////
	// SpriteBatch inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline SpriteBatch::SpriteBatch(GLsizei size)
		: SpriteBatch(size, GL_STATIC_DRAW) {
	}
	/*----------------------------------------------------------------------------*/
	inline SpriteBatch::~SpriteBatch() {
	}

	/*----------------------------------------------------------------------------*/
	inline unsigned int SpriteBatch::Add(float x, float y, float w, float h, float u1, float v1, float u2, float v2) {
		return Add(x, y, w, h, u1, v1, u2, v2, -1);
	}
	/*----------------------------------------------------------------------------*/
	inline unsigned int SpriteBatch::Add(float x, float y, float w, float h) {
		return Add(x, y, w, h, 0, 0, 0, 0, -1);
	}
}