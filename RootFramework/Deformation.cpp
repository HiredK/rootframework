#include "Deformation.h"
#include "Terrain.h"
#include "Shader.h"

void root::Deformation::SetUniforms(Camera& camera, const dmat3& matrix, int level, double ox, double oy, double l)
{
	m_local_to_camera = camera.GetViewMatrix() * dmat4(matrix);
	m_local_to_screen = camera.GetProjMatrix() * m_local_to_camera;

	Shader::Get()->UniformMat4("u_Deform_ScreenQuadCorners", m_local_to_screen * glm::transpose(
		dmat4(ox, ox + l, ox, ox + l, oy, oy, oy + l, oy + l, 0, 0, 0, 0, 1, 1, 1, 1)
	));

	Shader::Get()->UniformMat4("u_Deform_ScreenQuadVerticals", m_local_to_screen * glm::transpose(
		dmat4(0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0)
	));

	Shader::Get()->Uniform("u_Deform_Offset", (float)ox, (float)oy, (float)l, (float)level);
}

void root::Deformation::SetUniforms(Camera& camera, const TerrainTile& tile)
{
	double ox = tile.GetCoord().ox;
	double oy = tile.GetCoord().oy;
	double l = tile.GetCoord().length;

	Deformation::SetUniforms(camera, tile.GetOwner().GetFaceMatrix(), tile.GetLevel(), ox, oy, l);

	float d1 = tile.GetOwner().GetSplitDist() + 1.0f;
	float d2 = 2.0f * tile.GetOwner().GetSplitDist();
	Shader::Get()->Uniform("u_Deform_Blending", d1, d2 - d1);

	const dvec3& local_camera_position = tile.GetOwner().GetLocalCameraPosition();
	Shader::Get()->Uniform("u_Deform_Camera",
		(float)((local_camera_position.x - ox) / l),
		(float)((local_camera_position.y - oy) / l),
		(float)((local_camera_position.z - 0.0) / (l * tile.GetOwner().GetDistFactor())),
		(float)local_camera_position.z
	);

	const dvec3& world_camera = camera.GetPosition();
	dmat4 A = LocalToDeformedDifferential(local_camera_position);
	dmat4 B = DeformedToTangentFrame(world_camera);
	dmat4 ltow = mat4(tile.GetOwner().GetFaceMatrix());
	dmat4 ltot = B * ltow * A;

	dmat3 localToTangent = dmat3(
		ltot[0][0], ltot[0][1], ltot[0][3],
		ltot[1][0], ltot[1][1], ltot[1][3],
		ltot[3][0], ltot[3][1], ltot[3][3]
	);

	dmat3 tileToTangent = localToTangent * glm::transpose(dmat3(
		tile.GetCoord().length, 0, tile.GetCoord().ox - local_camera_position.x,
		0, tile.GetCoord().length, tile.GetCoord().oy - local_camera_position.y,
		0, 0, 1
	));

	Shader::Get()->UniformMat4("u_Deform_TangentFrameToWorld", ltow);
	Shader::Get()->UniformMat3("u_Deform_TileToTangent", tileToTangent);
}