#include "Engine.h"
#include "Logger.h"

root::Logger root::LOG = root::Logger("default.log");

int main(int argc, char** argv)
{
	std::unique_ptr<root::Engine> engine_instance(new root::Engine());
	return engine_instance->Execute(argc, argv);
}