/**
* @file TileProducer.h
* @brief
*/

#pragma once

#include "TerrainTile.h"
#include "FrameBuffer.h"

namespace root
{
	class TileProducer
	{
		public:
			//! TYPEDEF/ENUMS:
			typedef std::vector<TileProducer*> List;

			struct TileData {
				TileData(int level, int tx, int ty);
				virtual ~TileData();

				static std::string GetHash(int level, int tx, int ty);
				std::vector<std::shared_ptr<Texture>> textures;
				int level, tx, ty;
			};

			struct QuadTree {
				QuadTree(QuadTree* parent = nullptr);
				bool IsLeaf() const;

				QuadTree* children[4];
				QuadTree* parent;
				TileData* data;
				bool need_tile;
			};

			//! CTOR/DTOR:
			TileProducer(const Terrain& owner, int tile_size);
			virtual ~TileProducer();

			//! VIRTUALS:
			virtual bool Produce(TileData* data) { return true; }
			virtual int GetBorder() const;

			//! SERVICES:
			TileData* GetTile(int level, int tx, int ty);
			dvec3 GetTileCoords(int level, int tx, int ty) const;
			void GetDeformParameters(const dvec3& coords, dvec2& nx, dvec2& ny, dvec2& lx, dvec2& ly);
			void SetUniforms(int level, int tx, int ty);
			void Refresh();

			//! ACCESSORS:
			const Terrain& GetOwner() const;
			int GetTileSize() const;

		protected:
			//! SERVICES:
			void GetTiles(const TerrainTile* tile, QuadTree* tree, QuadTree* parent_tree = nullptr);
			void PutTiles(const TerrainTile* tile, QuadTree* tree);

			//! MEMBERS:
			const Terrain& m_owner;
			std::map<std::string, std::unique_ptr<TileData>> m_data;
			QuadTree m_root;
			int m_tile_size;
	};

	////////////////////////////////////////////////////////////////////////////////
	// TileProducer::TileData inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TileProducer::TileData::TileData(int level, int tx, int ty)
		: level(level), tx(tx), ty(ty) {
		textures.resize(8); // 8 max slots
	}
	/*----------------------------------------------------------------------------*/
	inline TileProducer::TileData::~TileData() {
		textures.clear();
	}

	////////////////////////////////////////////////////////////////////////////////
	// TileProducer::QuadTree inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TileProducer::QuadTree::QuadTree(QuadTree* parent)
		: parent(parent), data(nullptr), need_tile(true)
	{
		for (unsigned int i = 0; i < 4; ++i) {
			children[i] = nullptr;
		}
	}
	/*----------------------------------------------------------------------------*/
	inline bool TileProducer::QuadTree::IsLeaf() const {
		return (children[0] == nullptr);
	}

	////////////////////////////////////////////////////////////////////////////////
	// TileProducer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline TileProducer::TileProducer(const Terrain& owner, int tile_size)
		: m_owner(owner), m_tile_size(tile_size) {
	}
	/*----------------------------------------------------------------------------*/
	inline TileProducer::~TileProducer() {
		m_data.clear();
	}

	/*----------------------------------------------------------------------------*/
	inline int TileProducer::GetBorder() const {
		return 2;
	}
	/*----------------------------------------------------------------------------*/
	inline const Terrain& TileProducer::GetOwner() const {
		return m_owner;
	}
	/*----------------------------------------------------------------------------*/
	inline int TileProducer::GetTileSize() const {
		return m_tile_size;
	}
}