#include "FileSystem.h"
#include "Engine.h"
#include "Logger.h"

bool root::FileSystem::Init(int argc, char** argv)
{
	LOG() << "Init root::FileSystem version " <<
		m_version.major << "." <<
		m_version.minor << "." <<
		m_version.patch;

	if (PHYSFS_init(argv[0])) {
		LOG() << "Initialized PhysFS version " <<
			PHYSFS_VER_MAJOR << "." <<
			PHYSFS_VER_MINOR << "." <<
			PHYSFS_VER_PATCH;

		if (!PHYSFS_addToSearchPath(PHYSFS_getBaseDir(), 1)) {
			LOG(Logger::Error) << PHYSFS_getLastError();
			return false;
		}

		if (!PHYSFS_setWriteDir(PHYSFS_getBaseDir())) {
			LOG(Logger::Error) << PHYSFS_getLastError();
			return false;
		}

		class BackgroundProcessTask : public Task
		{
			public:
				BackgroundProcessTask(GroupList& groups, std::recursive_mutex& mutex, bool& interrupt)
					: Task(BACKGROUND_REPEATING)
					, m_groups(groups)
					, m_mutex(mutex)
					, m_interrupt(interrupt) {
				}

				void Run()
				{
					/*auto start = std::chrono::high_resolution_clock::now();

					for (auto& i : m_groups)
					for (auto& j : i->files[Background])
					for (auto& k : j.second)
					{
						std::unique_lock<std::recursive_mutex> lock(m_mutex);
						if (m_interrupt) {
							LOG() << "Interrupt FileSystem thread!";
							m_interrupt = false;
							return;
						}

						k->Process();
					}

					auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start).count();
					std::this_thread::sleep_for(std::chrono::milliseconds(1000 - elapsed));*/
				}

			private:
				GroupList& m_groups;
				std::recursive_mutex& m_mutex;
				bool& m_interrupt;
		};

		m_engine_instance->GetTaskManager().Add(std::shared_ptr<Task>(
			new BackgroundProcessTask(m_groups, m_mutex, m_interrupt))
		);
	}

	return true;
}

void root::FileSystem::Update(float dt)
{
	if ((m_current_delay + dt) >= 1.0f) {
		for (auto& i : m_groups)
		for (auto& j : i->files[SingleThreaded])
		for (auto& k : j.second)
		{
			std::unique_lock<std::recursive_mutex> lock(m_mutex);
			k->Process();
		}

		m_current_delay = 0.0f;
	}
	else {
		m_current_delay += dt;
	}
}

root::FileSystem::Group* root::FileSystem::Mount(const std::string& path)
{
	std::unique_lock<std::recursive_mutex> lock(m_mutex);
	for (auto& group : m_groups) {
		if (group->path == path) {
			LOG() << path << ": Already mounted";
			return group.get();
		}
	}

	if (!PHYSFS_mount(path.c_str(), path.c_str(), 1)) {
		LOG(Logger::Error) << PHYSFS_getLastError();
		return nullptr;
	}

	LOG() << path << ": Added to search path";
	m_groups.push_back(std::shared_ptr<Group>(new Group(path)));
	return Discover(m_groups.back().get(), path + '/');
}

bool root::FileSystem::Unmount(const std::string& path)
{
	auto it = m_groups.begin();
	while (it != m_groups.end())
	{
		if ((*it)->path == path) {
			{
				std::unique_lock<std::recursive_mutex> lock(m_mutex);
				m_interrupt = true;
			}

			for (unsigned int process_type = 0; process_type < 2; ++process_type) {
				for (auto& j : (*it)->files[process_type])
				for (auto& k : j.second)
				{
					std::unique_lock<std::recursive_mutex> lock(m_mutex);
					k->SetAllocate(false);
					k->Process();
				}
			}

			if (PHYSFS_removeFromSearchPath(path.c_str()) == 0) {
				LOG(Logger::Error) << PHYSFS_getLastError();
				return false;
			}

			LOG() << path << ": Removed from search path";
			m_groups.erase(it);
			break;
		}

		++it;
	}

	return true;
}

root::FileHandle* root::FileSystem::Search(const std::string& path, bool allocate)
{
	std::vector<FileHandle*> results;
	for (auto& group : m_groups) {
		for (unsigned int process_type = 0; process_type < 2; ++process_type) {
			auto search = group->files[process_type].find(FileFactory::ExtractExt(path));
			if (search != group->files[process_type].end())
			{
				for (auto& file : search->second) {
					if (file->GetPath() == group->path + '/' + path ||
						file->GetPath() == path)
					{
						results.push_back(file.get());
						continue;
					}
				}
			}
		}
	}

	if (!results.empty())
	{
		if (allocate) {
			results.back()->SetAllocate(true);
			results.back()->Process();
		}

		return results.back();
	}

	return nullptr;
}

root::FileHandle* root::FileSystem::Write(const std::string& path, const void* data, size_t size, bool allocate)
{
	auto ExtractPath = [](const std::string& str) -> std::string {
		return str.substr(0, str.find_last_of("\\/"));
	};

	if (PHYSFS_mkdir(ExtractPath(path).c_str())) {
		PHYSFS_file* handle = PHYSFS_openWrite(path.c_str());
		if (handle)
		{
			PHYSFS_uint32 len = static_cast<PHYSFS_uint32>(size);
			if (PHYSFS_write(handle, data, len, 1))
			{
				LOG() << path << ": Written to disk";
				PHYSFS_close(handle);

				for (auto& i : m_groups) {
					Discover(i.get(), (*i).path + '/');
				}

				return Search(path, allocate);
			}
		}
	}

	LOG(Logger::Error) << PHYSFS_getLastError();
	return nullptr;
}

root::FileSystem::Group* root::FileSystem::Discover(Group* group, const std::string& path, bool recursive)
{
	char** rc = PHYSFS_enumerateFiles(path.c_str());
	for (char **i = rc; *i != 0; i++)
	{
		std::string current = path + std::string(*i);
		if (!PHYSFS_isDirectory(current.c_str()))
		{
			std::string ext = FileFactory::ExtractExt(current);

			bool found = false;
			for (unsigned int process_type = 0; process_type < 2; ++process_type) {
				for (auto& file : group->files[process_type][ext]) {
					if (file->GetPath() == current) {
						found = true;
						break;
					}
				}
			}

			if (!found) {
				FileHandle* file = FileFactory::Detect(m_engine_instance, current);
				if (!file) {
					file = new FileHandle(m_engine_instance, current);
				}

				unsigned int process_type = file->IsProcessBackground() ? Background : SingleThreaded;
				group->files[process_type][ext].push_back(std::shared_ptr<FileHandle>(file));
			}
		}
		else if (recursive) {
			Discover(group, path + *i + '/');
		}
	}

	PHYSFS_freeList(rc);
	return group;
}

void root::FileSystem::Cleanup()
{
	{
		std::unique_lock<std::recursive_mutex> lock(m_mutex);
		m_interrupt = true;
	}

	for (unsigned int process_type = 0; process_type < 2; ++process_type) {
		for (auto& i : m_groups)
		for (auto& j : i->files[process_type])
		for (auto& k : j.second)
		{
			std::unique_lock<std::recursive_mutex> lock(m_mutex);
			k->SetAllocate(false);
			k->Process();
		}
	}

	if (!PHYSFS_deinit()) {
		LOG(Logger::Error) << PHYSFS_getLastError();
	}

	m_groups.clear();
}