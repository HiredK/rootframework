  /**
* @file GwenRenderer.h
* @brief
*/

#include "SpriteBatch.h"
#include "TextRenderer.h"
#include "FrameBuffer.h"
#include "Engine.h"
#include "FileSystem.h"

#pragma warning(push)
#pragma warning(disable : 4267)
#pragma warning(disable : 4244)
#include "Gwen/Skins/Simple.h"
#include "Gwen/Skins/TexturedBase.h"
#include "Gwen/BaseRender.h"
#pragma warning(pop)

namespace root
{
	class Engine;

	class GwenRenderer : public Gwen::Renderer::Base
	{
		public:
			//! CTOR/DTOR:
			GwenRenderer(Engine* engine_instance, unsigned int w, unsigned int h);
			virtual ~GwenRenderer();

			//! SERVICES:
			void SetSize(unsigned int w, unsigned int h);
			void BlitFBOTextureToScreenQuad();
			void Flush();

			//! VIRTUALS:
			virtual void Begin();
			virtual void End();

			virtual void SetDrawColor(Gwen::Color color);
			virtual void DrawFilledRect(Gwen::Rect rect);

			virtual void StartClip();
			virtual void EndClip();

			virtual void LoadTexture(Gwen::Texture* texture);
			virtual void FreeTexture(Gwen::Texture* texture);
			virtual void DrawTexturedRect(Gwen::Texture* texture, Gwen::Rect rect, float u1 = 0.0f, float v1 = 0.0f, float u2 = 1.0f, float v2 = 1.0f);
			virtual Gwen::Color PixelColour(Gwen::Texture* texture, unsigned int x, unsigned int y, const Gwen::Color& def = Gwen::Color(255, 255, 255, 255));

			virtual void LoadFont(Gwen::Font* font);
			virtual void FreeFont(Gwen::Font* font);
			virtual void RenderText(Gwen::Font* font, Gwen::Point pos, const Gwen::UnicodeString& text);
			virtual Gwen::Point MeasureText(Gwen::Font* font, const Gwen::UnicodeString& text);

			//! ACCESSORS:
			void SetRenderToFBO(bool render_to_fbo);
			bool GetRenderToFBO() const;
			void BindFBO();

		protected:
			//! MEMBERS:
			Engine* m_engine_instance;
			unsigned int m_w;
			unsigned int m_h;

			// Displays all the controls
			// in a single draw call.
			SpriteBatch m_batch;

			// Atlas based text renderer.
			TextRenderer m_text_renderer;

			// Optional FBO used to optimize
			// the rendering by only redrawing
			// when a control is changed.
			std::unique_ptr<FrameBuffer> m_fbo;
			std::shared_ptr<Texture> m_texture;
			std::shared_ptr<Mesh> m_quad_mesh;
			bool m_render_to_fbo;

			// Current draw color object.
			Color m_draw_color;
	};

	////////////////////////////////////////////////////////////////////////////////
	// GwenRenderer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline GwenRenderer::GwenRenderer(Engine* engine_instance, unsigned int w, unsigned int h)
		: m_engine_instance(engine_instance)
		, m_w(0), m_h(0)
		, m_batch(2048, GL_STATIC_DRAW)
		, m_text_renderer(512, 512, FONS_ZERO_TOPLEFT)
		, m_quad_mesh(Mesh::BuildQuad())
		, m_render_to_fbo(false)
		, m_draw_color(Color::White)
	{
		SetSize(w, h);
	}
	/*----------------------------------------------------------------------------*/
	inline GwenRenderer::~GwenRenderer() {
	}

	/*----------------------------------------------------------------------------*/
	inline void GwenRenderer::SetRenderToFBO(bool render_to_fbo) {
		m_render_to_fbo = render_to_fbo;
		m_w = 0;
		m_h = 0;
	}
	/*----------------------------------------------------------------------------*/
	inline bool GwenRenderer::GetRenderToFBO() const {
		return m_render_to_fbo;
	}
	/*----------------------------------------------------------------------------*/
	inline void GwenRenderer::BindFBO() {
		m_fbo->Clear();
		m_fbo->Attach(m_texture.get(), GL_COLOR_ATTACHMENT0);
		m_fbo->BindOutput();
	}
}