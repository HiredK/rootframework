/**
* @file BulletDebugRenderer.h
* @brief
*/

#pragma once

#include "LinearMath/btIDebugDraw.h"
#include <GL/glew.h>

namespace root
{
	class BulletDebugRenderer : public btIDebugDraw
	{
		public:
			//! CTOR/DTOR:
			BulletDebugRenderer(int debug_mode = DBG_DrawWireframe);
			virtual ~BulletDebugRenderer();

			//! VIRTUALS:
			virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& from_color, const btVector3& to_color);
			virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color);
			virtual void drawSphere(const btVector3& p, btScalar radius, const btVector3& color);
			virtual void drawTriangle(const btVector3& a, const btVector3& b, const btVector3& c, const btVector3& color, btScalar alpha);
			virtual void drawContactPoint(const btVector3& PointOnB, const btVector3& normalOnB, btScalar distance, int lifeTime, const btVector3& color);
			virtual void reportErrorWarning(const char* str);
			virtual void draw3dText(const btVector3& location, const char* str);
			virtual void setDebugMode(int mode);
			virtual int	getDebugMode() const;

		private:
			//! MEMBERS:
			int m_debug_mode;
	};

	////////////////////////////////////////////////////////////////////////////////
	// BulletDebugRenderer inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline BulletDebugRenderer::BulletDebugRenderer(int debug_mode)
		: m_debug_mode(debug_mode) {
	}
	/*----------------------------------------------------------------------------*/
	inline BulletDebugRenderer::~BulletDebugRenderer() {
	}

	/*----------------------------------------------------------------------------*/
	inline void BulletDebugRenderer::setDebugMode(int debug_mode) {
		m_debug_mode = debug_mode;
	}
	/*----------------------------------------------------------------------------*/
	inline int BulletDebugRenderer::getDebugMode() const {
		return m_debug_mode;
	}
}