#include "Animator.h"
#include "Logger.h"

#include <ozz/base/io/archive.h>
#include <ozz/base/io/stream.h>
#include <ozz/base/containers/vector_archive.h>
#include <ozz/base/maths/math_archive.h>
#include <ozz/base/maths/simd_math_archive.h>
#include <ozz/geometry/runtime/skinning_job.h>
#include <ozz/animation/runtime/sampling_job.h>
#include <ozz/animation/runtime/local_to_model_job.h>
#include <ozz/animation/runtime/skeleton.h>
#include <ozz/animation/runtime/animation.h>

bool root::Animator::LoadSkeleton(FileHandle* file)
{
	Logger::ScopedTimer timer(LOG, "Animation::LoadSkeleton()");
	if (FileHandle::Ready(file)) {
		auto stream = std::unique_ptr<ozz::io::MemoryStream>(new ozz::io::MemoryStream());
		if (!stream->Write(file->GetData(), file->GetSize())) {
			LOG(Logger::Warning) << "Unable to read '" << file->GetPath() << "' skeleton";
			return false;
		}

		stream->Seek(0, ozz::io::Stream::kSet);
		ozz::io::IArchive archive(stream.get());

		if (!archive.TestTag<ozz::animation::Skeleton>()) {
			LOG(Logger::Warning) << "Archive '" << file->GetPath() << "' doesn't contain the expected object type";
			return false;
		}

		m_skeleton = std::shared_ptr<ozz::animation::Skeleton>(new ozz::animation::Skeleton());
		archive >> (*m_skeleton);

		ozz::memory::Allocator* allocator = ozz::memory::default_allocator();
		m_model_matrices = allocator->AllocateRange<ozz::math::Float4x4>(m_skeleton->num_joints());
		m_skinning_matrices = allocator->AllocateRange<ozz::math::Float4x4>(m_skeleton->num_joints());
		return true;
	}

	return false;
}

bool root::Animator::LoadMesh(FileHandle* file)
{
	Logger::ScopedTimer timer(LOG, "Animation::LoadMesh()");
	if (FileHandle::Ready(file)) {
		auto stream = std::unique_ptr<ozz::io::MemoryStream>(new ozz::io::MemoryStream());
		if (!stream->Write(file->GetData(), file->GetSize())) {
			LOG(Logger::Warning) << "Unable to read '" << file->GetPath() << "' mesh";
			return false;
		}

		stream->Seek(0, ozz::io::Stream::kSet);
		ozz::io::IArchive archive(stream.get());

		if (!archive.TestTag<SkinnedMesh>()) {
			LOG(Logger::Warning) << "Archive '" << file->GetPath() << "' doesn't contain the expected object type";
			return false;
		}

		m_mesh = std::shared_ptr<SkinnedMesh>(new SkinnedMesh());
		archive >> (*m_mesh);

		glGenBuffers(1, &m_dynamic_vbo);
		glGenBuffers(1, &m_dynamic_ibo);
		return true;
	}

	return false;
}

bool root::Animator::AddAnimation(int index, FileHandle* file)
{
	Logger::ScopedTimer timer(LOG, "Animation::AddAnimation()");
	if (FileHandle::Ready(file)) {
		auto stream = std::unique_ptr<ozz::io::MemoryStream>(new ozz::io::MemoryStream());
		if (!stream->Write(file->GetData(), file->GetSize())) {
			LOG(Logger::Warning) << "Unable to read '" << file->GetPath() << "' animation";
			return false;
		}

		stream->Seek(0, ozz::io::Stream::kSet);
		ozz::io::IArchive archive(stream.get());

		if (!archive.TestTag<ozz::animation::Animation>()) {
			LOG(Logger::Warning) << "Archive '" << file->GetPath() << "' doesn't contain the expected object type";
			return false;
		}

		auto sampler = std::shared_ptr<Sampler>(new Sampler(std::shared_ptr<ozz::animation::Animation>(new ozz::animation::Animation())));
		archive >> (*sampler->m_animation);

		ozz::memory::Allocator* allocator = ozz::memory::default_allocator();
		sampler->m_cache = allocator->New<ozz::animation::SamplingCache>(m_skeleton->num_joints());
		sampler->m_locals = allocator->AllocateRange<ozz::math::SoaTransform>(m_skeleton->num_soa_joints());

		m_current_animation_index = (m_current_animation_index == -1) ? index : -1;
		m_samplers[index] = sampler;
		return true;
	}

	return false;
}

void root::Animator::PlaybackController::Update(const ozz::animation::Animation* animation, float dt)
{
	if (m_play) {
		const float new_time = m_time + dt * m_playback_speed;
		const float loops = new_time / animation->duration();
		m_time = new_time - floorf(loops) * animation->duration();
	}
}

void root::Animator::Animate(float dt)
{
	if (m_current_animation_index != -1) {
		for (int i = 0; i < m_samplers.size(); ++i) {
			m_samplers[i]->m_controller.Update(m_samplers[i]->m_animation.get(), dt);

			ozz::animation::SamplingJob sampling_job;
			sampling_job.animation = m_samplers[i]->m_animation.get();
			sampling_job.cache = m_samplers[i]->m_cache;
			sampling_job.time = m_samplers[i]->m_controller.m_time;
			sampling_job.output = m_samplers[i]->m_locals;

			if (!sampling_job.Run()) {
				LOG(Logger::Warning) << "Failed Sampling Job!";
				return;
			}
		}

		ozz::animation::LocalToModelJob ltm_job;
		ltm_job.skeleton = m_skeleton.get();
		ltm_job.input = m_samplers[m_current_animation_index]->m_locals;
		ltm_job.output = m_model_matrices;

		if (!ltm_job.Run()) {
			LOG(Logger::Warning) << "Failed LocalToModel Job!";
		}
	}
}

void root::Animator::Draw()
{
	Shader::Get()->UniformMat4("u_ModelMatrix", Transform::ComputeMatrix());
	Shader::Get()->UniformMat3("u_NormalMatrix", mat3(glm::mat4_cast(m_rotation)));

	if (!m_skeleton || m_samplers.empty()) {
		DrawMesh();
		return;
	}

	for (size_t i = 0; i < m_model_matrices.Count(); ++i) {
		m_skinning_matrices[i] = m_model_matrices[i] * m_mesh->m_inverse_bind_poses[i];
	}

	const int vertex_count = m_mesh->GetVertexCount();

	const GLsizei positions_offset = 0;
	const GLsizei normals_offset = sizeof(float) * 3;
	const GLsizei tangents_offset = sizeof(float) * 6;
	const GLsizei positions_stride = sizeof(float) * 9;
	const GLsizei normals_stride = positions_stride;
	const GLsizei tangents_stride = positions_stride;
	const GLsizei skinned_data_size = vertex_count * positions_stride;

	const GLsizei colors_offset = skinned_data_size;
	const GLsizei colors_stride = sizeof(uint8_t) * 4;
	const GLsizei colors_size = vertex_count * colors_stride;
	const GLsizei uvs_offset = colors_offset + colors_size;
	const GLsizei uvs_stride = sizeof(float) * 2;
	const GLsizei uvs_size = vertex_count * uvs_stride;
	const GLsizei fixed_data_size = colors_size + uvs_size;

	const GLsizei vbo_size = skinned_data_size + fixed_data_size;
	void* vbo_map = m_scratch_buffer.Resize(vbo_size);
	size_t processed_vertex_count = 0;

	for (size_t i = 0; i < m_mesh->m_parts.size(); ++i) {
		const SkinnedMesh::Part& part = m_mesh->m_parts[i];
		const size_t part_vertex_count = part.m_positions.size() / 3;
		if (part_vertex_count == 0) {
			continue;
		}

		ozz::geometry::SkinningJob skinning_job;
		skinning_job.vertex_count = static_cast<int>(part_vertex_count);
		const int part_influences_count = part.GetInfluencesCount();
		skinning_job.influences_count = part_influences_count;
		skinning_job.joint_matrices = m_skinning_matrices;

		skinning_job.joint_indices = make_range(part.m_joint_indices);
		skinning_job.joint_indices_stride = sizeof(uint16_t)* part_influences_count;

		if (part_influences_count > 1) {
			skinning_job.joint_weights = make_range(part.m_joint_weights);
			skinning_job.joint_weights_stride = sizeof(float)* (part_influences_count - 1);
		}

		skinning_job.in_positions = make_range(part.m_positions);
		skinning_job.in_positions_stride = sizeof(float)* 3;

		skinning_job.out_positions.begin = reinterpret_cast<float*>(ozz::PointerStride(vbo_map, positions_offset + processed_vertex_count * positions_stride));
		skinning_job.out_positions.end = ozz::PointerStride(skinning_job.out_positions.begin, part_vertex_count * positions_stride);
		skinning_job.out_positions_stride = positions_stride;

		float* out_normal_begin = reinterpret_cast<float*>(ozz::PointerStride(vbo_map, normals_offset + processed_vertex_count * normals_stride));
		const float* out_normal_end = ozz::PointerStride(out_normal_begin, part_vertex_count * normals_stride);
		if (part.m_normals.size() / 3 == part_vertex_count) {
			skinning_job.in_normals = make_range(part.m_normals);
			skinning_job.in_normals_stride = sizeof(float)* 3;
			skinning_job.out_normals.begin = out_normal_begin;
			skinning_job.out_normals.end = out_normal_end;
			skinning_job.out_normals_stride = normals_stride;
		}

		float* out_tangent_begin = reinterpret_cast<float*>(ozz::PointerStride(vbo_map, tangents_offset + processed_vertex_count * tangents_stride));
		const float* out_tangent_end = ozz::PointerStride(out_tangent_begin, part_vertex_count * tangents_stride);
		if (part.m_tangents.size() / 3 == part_vertex_count) {
			skinning_job.in_tangents = make_range(part.m_tangents);
			skinning_job.in_tangents_stride = sizeof(float)* 3;
			skinning_job.out_tangents.begin = out_tangent_begin;
			skinning_job.out_tangents.end = out_tangent_end;
			skinning_job.out_tangents_stride = tangents_stride;
		}

		if (!skinning_job.Run()) {
			LOG(Logger::Warning) << "Failed Skinning Job!";
			return;
		}

		if (part_vertex_count == part.m_colors.size() / 4) {
			memcpy(ozz::PointerStride(vbo_map, colors_offset + processed_vertex_count * colors_stride), array_begin(part.m_colors), part_vertex_count * colors_stride);
		}

		if (part_vertex_count == part.m_texcoords.size() / 2) {
			memcpy(ozz::PointerStride(vbo_map, uvs_offset + processed_vertex_count * uvs_stride), array_begin(part.m_texcoords), part_vertex_count * uvs_stride);
		}

		processed_vertex_count += part_vertex_count;
	}

	glBindBuffer(GL_ARRAY_BUFFER, m_dynamic_vbo);
	glBufferData(GL_ARRAY_BUFFER, vbo_size, 0, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vbo_size, vbo_map);

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, positions_stride, GL_PTR_OFFSET(positions_offset));

	glEnableVertexAttribArray(1);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, uvs_stride, GL_PTR_OFFSET(uvs_offset));

	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 4, GL_UNSIGNED_BYTE, GL_FALSE, colors_stride, GL_PTR_OFFSET(colors_offset));

	glEnableVertexAttribArray(3);
	glVertexAttribPointer(3, 3, GL_FLOAT, GL_FALSE, normals_stride, GL_PTR_OFFSET(normals_offset));

	glEnableVertexAttribArray(4);
	glVertexAttribPointer(4, 3, GL_FLOAT, GL_FALSE, tangents_stride, GL_PTR_OFFSET(tangents_offset));

	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_dynamic_ibo);
	const ozz::Vector<uint16_t>::Std& indices = m_mesh->m_indices;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint16_t), array_begin(indices), GL_STREAM_DRAW);

	glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(indices.size()), GL_UNSIGNED_SHORT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(2);
	glDisableVertexAttribArray(3);
	glDisableVertexAttribArray(4);
}

void root::Animator::DrawMesh()
{
	const int vertex_count = m_mesh->GetVertexCount();

	const GLsizei positions_offset = 0;
	const GLsizei positions_stride = sizeof(float) * 3;
	const GLsizei positions_size = vertex_count * positions_stride;

	const GLsizei normals_offset = positions_offset + positions_size;
	const GLsizei normals_stride = sizeof(float) * 3;
	const GLsizei normals_size = vertex_count * normals_stride;

	const GLsizei colors_offset = normals_offset + normals_size;
	const GLsizei colors_stride = sizeof(uint8_t) * 4;
	const GLsizei colors_size = vertex_count * colors_stride;

	const GLsizei uvs_offset = colors_offset + colors_size;
	const GLsizei uvs_stride = sizeof(float) * 2;
	const GLsizei uvs_size = vertex_count * uvs_stride;

	const GLsizei vbo_size = positions_size + normals_size + colors_size + uvs_size;
	glBindBuffer(GL_ARRAY_BUFFER, m_dynamic_vbo);
	glBufferData(GL_ARRAY_BUFFER, vbo_size, 0, GL_STREAM_DRAW);
	size_t vertex_offset = 0;

	for (size_t i = 0; i < m_mesh->m_parts.size(); ++i)
	{
		const SkinnedMesh::Part& part = m_mesh->m_parts[i];
		const size_t part_vertex_count = part.GetVertexCount();

		glBufferSubData(GL_ARRAY_BUFFER, positions_offset + vertex_offset * positions_stride, part_vertex_count * positions_stride, array_begin(part.m_positions));

		const size_t part_normal_count = part.m_normals.size() / 3;
		if (part_vertex_count == part_normal_count) {
			glBufferSubData(GL_ARRAY_BUFFER, normals_offset + vertex_offset * normals_stride, part_normal_count * normals_stride, array_begin(part.m_normals));
		}

		const size_t part_color_count = part.m_colors.size() / 4;
		if (part_vertex_count == part_color_count) {
			glBufferSubData(GL_ARRAY_BUFFER, colors_offset + vertex_offset * colors_stride, part_color_count * colors_stride, array_begin(part.m_colors));
		}

		const size_t part_uvs_count = part.m_texcoords.size() / 2;
		if (part_vertex_count == part_uvs_count) {
			glBufferSubData(GL_ARRAY_BUFFER, uvs_offset + vertex_offset * uvs_stride, part_uvs_count * uvs_stride, array_begin(part.m_texcoords));
		}

		vertex_offset += part_vertex_count;
	}

	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, positions_stride, GL_PTR_OFFSET(positions_offset));
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_dynamic_ibo);
	const ozz::Vector<uint16_t>::Std& indices = m_mesh->m_indices;
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(uint16_t), array_begin(indices), GL_STREAM_DRAW);

	glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(indices.size()), GL_UNSIGNED_SHORT, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glDisableVertexAttribArray(0);
}

void root::Animator::Clear()
{
}

namespace ozz
{
	namespace io
	{
		OZZ_IO_TYPE_TAG("ozz-sample-Mesh-Part", root::Animator::SkinnedMesh::Part)
		OZZ_IO_TYPE_VERSION(1, root::Animator::SkinnedMesh::Part)

		OZZ_IO_TYPE_TAG("ozz-sample-Mesh", root::Animator::SkinnedMesh)
		OZZ_IO_TYPE_VERSION(1, root::Animator::SkinnedMesh)

		template <>
		void Load(IArchive& archive, root::Animator::SkinnedMesh::Part* parts, size_t count, uint32_t version) {
			for (size_t i = 0; i < count; ++i) {
				root::Animator::SkinnedMesh::Part& part = parts[i];
				archive >> part.m_positions;
				archive >> part.m_normals;
				archive >> part.m_tangents;
				archive >> part.m_texcoords;
				archive >> part.m_colors;
				archive >> part.m_joint_indices;
				archive >> part.m_joint_weights;
			}
		}

		template <>
		void Load(IArchive& archive, root::Animator::SkinnedMesh* meshes, size_t count, uint32_t version) {
			for (size_t i = 0; i < count; ++i) {
				root::Animator::SkinnedMesh& mesh = meshes[i];
				archive >> mesh.m_parts;
				archive >> mesh.m_indices;
				archive >> mesh.m_inverse_bind_poses;
			}
		}
	}
}