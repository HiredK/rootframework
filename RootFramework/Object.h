/**
* @file Object.h
* @brief
*/

#pragma once

#include <sstream>
#include <cstdint>
#include <iomanip>
#include "Color.h"

namespace root
{
	class Object
	{
		public:
			//! CTORS/DTORS:
			Object(const std::string& name = "object");
			virtual ~Object();

			//! OPERATORS:
			friend bool operator==(Object& a, Object& b);
			friend bool operator!=(Object& a, Object& b);

			//! ACCESSORS:
			void SetName(const std::string& name);
			const std::string& GetName() const;
			const Color& GetUniqueColor() const;
			std::uint64_t GetID() const;

		private:
			//! NON-COPYABLE:
			Object(const Object&);
			Object& operator=(const Object&);

			//! SERVICES:
			static std::uint64_t GenerateID();

			//! MEMBERS:
			std::string m_name;
			Color m_unique_color;
			std::uint64_t m_id;
	};

	////////////////////////////////////////////////////////////////////////////////
	// Object inline implementation:
	////////////////////////////////////////////////////////////////////////////////
	/*----------------------------------------------------------------------------*/
	inline Object::Object(const std::string& name)
		: m_id(GenerateID()), m_name(name)
	{
		std::ostringstream os;
		os << m_name << "_" << std::setfill('0') << std::setw(4) << m_id;
		m_name = os.str();

		m_unique_color.r = (static_cast<unsigned char>(m_id) & 0x000000FF) >> 0;
		m_unique_color.g = (static_cast<unsigned char>(m_id) & 0x0000FF00) >> 8;
		m_unique_color.b = (static_cast<unsigned char>(m_id) & 0x00FF0000) >> 16;
		m_unique_color.a = (static_cast<unsigned char>(m_id) & 0xFF000000) >> 24;
	}
	/*----------------------------------------------------------------------------*/
	inline Object::~Object() {
	}

	/*----------------------------------------------------------------------------*/
	inline bool operator==(Object& a, Object& b) {
		return (a.m_id == b.m_id);
	}
	/*----------------------------------------------------------------------------*/
	inline bool operator!=(Object& a, Object& b) {
		return !(a == b);
	}

	/*----------------------------------------------------------------------------*/
	inline std::uint64_t Object::GetID() const {
		return m_id;
	}
	/*----------------------------------------------------------------------------*/
	inline const Color& Object::GetUniqueColor() const {
		return m_unique_color;
	}
	/*----------------------------------------------------------------------------*/
	inline void Object::SetName(const std::string& name) {
		m_name = name;
	}
	/*----------------------------------------------------------------------------*/
	inline const std::string& Object::GetName() const {
		return m_name;
	}

	/*----------------------------------------------------------------------------*/
	inline std::uint64_t Object::GenerateID() {
		static std::uint64_t current_id = 0;
		return current_id++;
	}

} // root namespace